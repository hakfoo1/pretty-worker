/* exprfilter_parser.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EXPRFILTER_PARSER_HH
#define EXPRFILTER_PARSER_HH

#include "wdefines.h"
#include "exprfilter_scanner.hh"
#include <set>
#include <string>
#include <list>

class ExprFilterEval;

class ExprFilterParser
{
public:
    ExprFilterParser( const std::list< ExprToken > &tokens ) : m_tokens( tokens ),
                                                               m_ee( nullptr ),
                                                               m_valid( true )
    {}

    void parse( ExprFilterEval *ee );

    std::string getValidReconstructedFilter();
    std::list< std::string > getListOfExpectedStrings() const;
private:
    std::list< ExprToken > m_tokens;

    ExprToken getCurrentToken() const;
    void advanceToken();
    bool hasNextToken() const;

    void expr();
    void fexpr();
    void and_expr();
    void or_expr();

    void valid_filter();

    void pushExpectedStrings( const std::list< std::string > &l );

    class ExprFilterEval *m_ee;

    bool m_valid;

    std::string m_reconstructed_filter, m_valid_reconstructed_filter;

    std::set< std::string > m_expected_strings;
};

#endif
