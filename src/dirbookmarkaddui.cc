/* dirbookmarkaddui.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dirbookmarkaddui.hh"
#include "aguix/aguix.h"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/button.h"
#include "aguix/acontainerbb.h"
#include "worker_locale.h"
#include "bookmarkdbentry.hh"
#include "nwc_path.hh"
#include "nwc_fsentry.hh"
#include "bookmarkdbproxy.hh"

DirBookmarkAddUI::DirBookmarkAddUI( AGUIX &aguix, BookmarkDBProxy &data,
                                    const std::string &dirname,
                                    const std::string &basename ) : m_aguix( aguix ),
                                                                    m_data( data ),
                                                                    m_dirname( dirname ),
                                                                    m_basename( basename )
{
    char *tstr = NULL;

    m_okb_op1 = NULL;
    m_okb_op2 = NULL;
    m_okb_op3 = NULL;

    m_win = std::unique_ptr<AWindow>( new AWindow( &m_aguix,
                                                   0, 0,
                                                   400, 400,
                                                   catalog.getLocale( 1292 ),
                                                   AWindow::AWINDOW_DIALOG ) );
    m_win->create();

    AContainer *co1 = m_win->setContainer( new AContainer( m_win.get(), 1, 4 ), true );
    co1->setMaxSpace( 5 );

    AContainer *co1_1 = co1->add( new AContainerBB( m_win.get(), 1, 2 ), 0, 0 );

    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 756 ) ) +
                              strlen( dirname.c_str() ) + 1 );
    sprintf( tstr, catalog.getLocale( 756 ), dirname.c_str() );

    co1_1->add( new Text( &m_aguix, 0, 0, tstr ),
                0, 0, AContainer::CO_INCW );
    _freesafe( tstr );
    m_okb_op1 = (Button*)co1_1->add( new Button( &m_aguix,
                                                 0,
                                                 0,
                                                 catalog.getLocale( 11 ),
                                                 0 ), 0, 1, AContainer::CO_INCW );

    if ( basename.length() > 0 ) {
        AContainer *co1_2 = co1->add( new AContainerBB( m_win.get(), 1, 3 ), 0, 1 );
        
        tstr = (char*)_allocsafe( strlen( catalog.getLocale( 756 ) ) +
                                  strlen( dirname.c_str() ) + 1 );
        sprintf( tstr, catalog.getLocale( 756 ), dirname.c_str() );
        
        co1_2->add( new Text( &m_aguix, 0, 0, tstr ),
                    0, 0, AContainer::CO_INCW );
        _freesafe( tstr );
        
        tstr = (char*)_allocsafe( strlen( catalog.getLocale( 757 ) ) +
                                  strlen( basename.c_str() ) + 1 );
        sprintf( tstr, catalog.getLocale( 757 ),
                 basename.c_str() );
        
        co1_2->add( new Text( &m_aguix, 0, 0, tstr ),
                    0, 1, AContainer::CO_INCW );
        _freesafe( tstr );
        m_okb_op2 = (Button*)co1_2->add( new Button( &m_aguix,
                                                     0,
                                                     0,
                                                     catalog.getLocale( 11 ),
                                                     0 ), 0, 2, AContainer::CO_INCW );
    }

    if ( basename.length() > 0 ) {
        std::string s1 = NWC::Path::join( dirname, basename );
        NWC::FSEntry e( s1 );

        if ( e.isDir( true ) == true ) {
            AContainer *co1_3 = co1->add( new AContainerBB( m_win.get(), 1, 2 ), 0, 2 );
            
            tstr = (char*)_allocsafe( strlen( catalog.getLocale( 756 ) ) +
                                      strlen( s1.c_str() ) + 1 );
            sprintf( tstr, catalog.getLocale( 756 ), s1.c_str() );
            
            co1_3->add( new Text( &m_aguix, 0, 0, tstr ),
                        0, 0, AContainer::CO_INCW );
            _freesafe( tstr );
            m_okb_op3 = (Button*)co1_3->add( new Button( &m_aguix,
                                                         0,
                                                         0,
                                                         catalog.getLocale( 11 ),
                                                         0 ), 0, 1, AContainer::CO_INCW );
        }
    }
    
    m_cancelb = (Button*)co1->add( new Button( &m_aguix,
                                               0,
                                               0,
                                               catalog.getLocale( 8 ),
                                               0 ), 0, 3, AContainer::CO_INCW );

    m_win->contMaximize( true, true );
    m_win->setDoTabCycling( true );
}

DirBookmarkAddUI::~DirBookmarkAddUI()
{
}

int DirBookmarkAddUI::mainLoop()
{
    m_win->show();
    
    AGMessage *msg;
    int endmode = 0;
    for ( ; endmode == 0; ) {
        msg = m_aguix.WaitMessage( NULL );
        if ( msg != NULL ) {
            switch ( msg->type ) {
              case AG_CLOSEWINDOW:
                  endmode = -1;
                  break;
              case AG_BUTTONCLICKED:
                  if ( msg->button.button == m_okb_op1 ) {
                      endmode = 1;
                  } else if ( msg->button.button == m_okb_op2 ) {
                      endmode = 2;
                  } else if ( msg->button.button == m_okb_op3 ) {
                      endmode = 3;
                  } else if ( msg->button.button == m_cancelb ) {
                      endmode = -1;
                  }
                  break;
              case AG_KEYPRESSED:
                  if ( msg->key.key == XK_Escape ) {
                      endmode = -1;
                  }
                  break;
            }
            m_aguix.ReplyMessage( msg );
        }
    }

    switch ( endmode ) {
      case 1:
          m_data.addEntry( BookmarkDBEntry( "", m_dirname ) );
          break;
      case 2:
      case 3:
          if ( m_basename.length() > 0 ) {
              std::string s1 = NWC::Path::join( m_dirname, m_basename );
              NWC::FSEntry e( s1 );

              if ( endmode == 2 || e.isDir( true ) == false ) {
                  m_data.addEntry( BookmarkDBEntry( "", s1, "", true ) );
              } else {
                  m_data.addEntry( BookmarkDBEntry( "", s1, "", false ) );
              }
          }
          break;
    }

    m_win->hide();
    
    return endmode;
}
