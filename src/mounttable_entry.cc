/* mounttable_entry.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "mounttable_entry.hh"

MountTableEntry::MountTableEntry( const std::string &device ) : m_device( device ),
                                                                m_mode( 0 ),
                                                                m_rdev( 0 )
{
}

std::string MountTableEntry::getDevice() const
{
    return m_device;
}

std::string MountTableEntry::getMountPoint() const
{
    return m_mount_point;
}

void MountTableEntry::setDevice( const std::string &device )
{
    m_device = device;
}

void MountTableEntry::setMountPoint( const std::string &mount_point )
{
    m_mount_point = mount_point;
}

mode_t MountTableEntry::getMode() const
{
    return m_mode;
}

dev_t MountTableEntry::getRDev() const
{
    return m_rdev;
}

void MountTableEntry::setMode( mode_t m )
{
    m_mode = m;
}

void MountTableEntry::setRDev( dev_t d )
{
    m_rdev = d;
}
