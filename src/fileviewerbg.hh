/* fileviewerbg.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEVIEWERBG_HH
#define FILEVIEWERBG_HH

#include "wdefines.h"
#include <string>
#include <list>
#include "aguix/refcount.hh"
#include "aguix/backgroundmessagehandler.hh"

class FileViewerDestroyCallback;
class TextStorageFile;
class TextView;
class SearchTextStorage;
class Text;
class Worker;

class FileViewerBG : public BackgroundMessageHandler
{
public:
    ~FileViewerBG();
    
    void view( const std::list<std::string> &filelist,
               int initial_line_number = 0,
               bool highlight_initial_line = false,
               RefCount< FileViewerDestroyCallback > destroy_callback = NULL );
    void setLineWrap( bool nv );
    bool getLineWrap() const;
    void setShowLineNumbers( bool nv );
    bool getShowLineNumbers() const;
    void setUseAltFont( bool nv );
    bool getUseAltFont() const;
    
    void handleAGUIXMessage( AGMessage &msg );
protected:
    friend class FileViewer;

    FileViewerBG( Worker *worker );
    FileViewerBG( const FileViewerBG &other );
    FileViewerBG &operator=( const FileViewerBG &other );

    AWindow *getAWindow();
private:
    std::list< std::string > m_filelist;
    RefCount< FileViewerDestroyCallback > m_destroy_callback;
    AWindow *win;
    bool m_wrap_mode;
    bool m_showlinenumbers_mode;
    bool m_use_alt_font;
    std::shared_ptr< TextStorageFile > m_ts;
    TextView *tv;
    RefCount<SearchTextStorage> ts_search;
    StringGadget *search_sg;
    Button *okb;
    ChooseButton *wcb;
    ChooseButton *slncb;
    ChooseButton *snp_cb;
    ChooseButton *alt_font_cb;
    Button *jumplineb;
    Button *readmoreb;
    Button *reloadb;
    Button *m_next_match_b;
    Button *m_prev_match_b;
    Button *m_restart_search_b;
    Button *m_showintab_b;
    Text *m_find_pos_text;
    Worker *m_worker;

    int buildTextView( const std::string &filename,
                       class AContainer *ac1,
                       class Text *fnt,
                       const RefCount<class AWidth> &lencalc );
    void jumpToLine();
    void nextMatch( bool backwards = false );
    void restartSearchFromTop();
    void showInWorkerTab();
};

#endif
