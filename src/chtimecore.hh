/* chtimecore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHTIMECORE_HH
#define CHTIMECORE_HH

#include "wdefines.h"
#include <list>
#include <functional>
#include <string>
#include "fileentry.hh"

struct chtimeorder;
class FileEntry;
class NM_CopyOp_Dir;
class AGUIX;
class List;

class ChTimeCore
{
public:
    ChTimeCore( struct chtimeorder *chtimeorder,
                AGUIX *aguix );
    ~ChTimeCore();

    void setCancel( bool nv );
    bool getCancel() const;

    void registerFEToChange( const FileEntry &fe,
                             int row );

    int execute();

    typedef enum { CHANGE_OK,
                   CHANGE_SKIP,
                   CHANGE_CANCEL } change_t;

    void setPostChangeCallback( std::function< void( const FileEntry &fe, int row,
                                                     change_t res ) > cb );
private:
    struct chtimeorder *m_chtimeorder;

    bool m_cancel;

    class change_base_entry {
    public:
        change_base_entry( const FileEntry &tfe, int row );
        ~change_base_entry();
        const FileEntry &entry() const
        {
            return m_fe;
        }
        int m_row;  // corresponding row or -1
        NM_CopyOp_Dir *m_cod; // when fe is dir, this will be the corresponding structure
    private:
        FileEntry m_fe;  // element to copy
    };

    std::list< change_base_entry > m_change_list;

    std::function< void( const FileEntry &fe, int row, change_t res ) > m_post_cb;

    typedef struct changetime_info {
        changetime_info();
        bool forAll;
      
        time_t new_acc_time;
        time_t new_mod_time;

        bool apply_acc_time;
        bool apply_mod_time;
    } changetime_info_t;

    AGUIX *m_aguix;

    int worker_changetime( const change_base_entry &ss1,
                           changetime_info_t *ctinfo );

    int applyNewTime( const change_base_entry &ss1,
                      changetime_info_t newtime );

    int requestNewTime( const FileEntry &fe,
                        changetime_info_t *return_time );
};

#endif
