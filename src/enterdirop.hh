/* enterdirop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ENTERDIROP_H
#define ENTERDIROP_H

#include "wdefines.h"
#include "functionproto.h"

class Worker;

class EnterDirOp : public FunctionProto
{
public:
    EnterDirOp();
    ~EnterDirOp();
    EnterDirOp( const EnterDirOp &other );
    EnterDirOp &operator=( const EnterDirOp &other );
    
    EnterDirOp *duplicate() const override;
    int configure() override;
    bool isName( const char * ) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save( Datei * ) override;

    typedef enum { ENTERDIROP_ACTIVE = 0,
                   ENTERDIROP_ACTIVE2OTHER,
                   ENTERDIROP_SPECIAL,
                   ENTERDIROP_REQUEST } enterdir_t;
    void setMode( enterdir_t );
    void setDir( const char* );
protected:
    static const char *name;
    // Infos to save
    enterdir_t enterdirmode;
    char *dir;
};

#endif
