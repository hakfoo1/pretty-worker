/* filereq.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2004,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEREQ_H
#define FILEREQ_H

#include "wdefines.h"
#include <string>

class AGUIX;
class List;
class FieldListView;
class StringGadget;
class Verzeichnis;
class FileEntry;

class FileRequester
{
public:
  FileRequester(AGUIX *parent);
  ~FileRequester();
  FileRequester( const FileRequester &other );
  FileRequester &operator=( const FileRequester &other );

  int request( const char *title, const char *dir, const char *oktext, const char *canceltext, const char *infotext);
  int request_entry( const char *title,
                     const char *dir,
                     const char *oktext,
                     const char *canceltext,
                     const char *infotext,
                     bool allowEnterName );
  FileEntry *getFirstFE();
  FileEntry *getNextFE();
  std::string getLastEntryStr();
private:
  AGUIX *aguix;
  List *selfes;
  char *actdir;
  Verzeichnis *verz;
  std::string last_entry_str;
  
  FieldListView *lv;
  StringGadget *sg;
  void setDir(const char *);
  void setDirOneSelect(const char *);
  void reset();
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
