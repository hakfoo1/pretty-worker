/* ajson_scan.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef JSON_SCAN_H
#define JSON_SCAN_H

#include <string>
#include <iostream>
#include <list>

enum json_token {
    JSON_INVALID,
    JSON_BEGIN_ARRAY,
    JSON_END_ARRAY,
    JSON_BEGIN_OBJECT,
    JSON_END_OBJECT,
    JSON_NAME_SEPARATOR,
    JSON_VALUE_SEPARATOR,
    JSON_FALSE,
    JSON_TRUE,
    JSON_NULL,
    JSON_STRING,
    JSON_NUMBER
};

typedef std::pair< enum json_token, std::string > ajson_token_t;

int ajson_scanner( const std::string &input,
                   std::list< ajson_token_t > &tokens );

#endif /* JSON_SCAN_H */
