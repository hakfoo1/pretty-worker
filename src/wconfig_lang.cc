/* wconfig_lang.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2009-2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_lang.hh"
#include "wconfig.h"
#include "worker.h"
#include "aguix/utf8.hh"
#include "workerinitialsettings.hh"
#include "nwc_path.hh"
#include "worker_locale.h"
#include "aguix/fieldlistview.h"

LangPanel::LangPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

LangPanel::~LangPanel()
{
}

int LangPanel::create()
{
  worker_struct_dirent *namelist;
  DIR *dir2;
  char *tstr;
  std::string catdir;
  int trow;

  Panel::create();
  
  AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 9 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  lv = (FieldListView*)ac1->add( new FieldListView( _aguix,
                                                    0,
                                                    0,
                                                    200,
                                                    150,
                                                    0 ), 0, 1, AContainer::CO_MIN );
  lv->connect( this );
  lv->setHBarState(2);
  lv->setVBarState(2);
  lv->setDisplayFocus( true );
  lv->setAcceptFocus( true );

  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale(10) );
  lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
  lv->setData( trow, 0 );
  int x=1;
  if ( ( strcmp( _baseconfig.getLang(), "builtin" ) == 0 ) || ( strlen( _baseconfig.getLang() ) < 1 ) ) lv->setActiveRow( trow );

#ifdef USEOWNCONFIGFILES
  std::string ending = ".catalog2";
#else
  std::string ending = ".catalog";
#endif
  
  if ( UTF8::getUTF8Mode() != UTF8::UTF8_DISABLED && UTF8::getCurrentEncoding() == UTF8::ENCODING_UTF8 ) {
      ending += ".utf8";
  }
  
  if ( Worker::getDataDir().length() > 0 ) {
    catdir = Worker::getDataDir();
    catdir += "/catalogs";
    dir2 = worker_opendir( catdir.c_str() );
    if(dir2!=NULL) {
      while ( ( namelist = worker_readdir( dir2 ) ) != NULL ) {
        if ( strlen( namelist->d_name ) > ending.length() ) {
          if ( strcmp( ending.c_str(), ( namelist->d_name ) + strlen( namelist->d_name ) - ending.length() ) == 0 ) {
            // Dies ist ein Katalog
            tstr=namelist->d_name;
            tstr[ strlen( tstr ) - ending.length() ] = 0;
            trow = lv->addRow();
            lv->setText( trow, 0, tstr );
            lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
            lv->setData( trow, x++ );
            if ( strcmp( _baseconfig.getLang(), tstr ) == 0 ) lv->setActiveRow( trow );
          }
        }
      }
      worker_closedir( dir2 );
    }
  }
  catdir = WorkerInitialSettings::getInstance().getConfigBaseDir();
  catdir = NWC::Path::join( catdir, "catalogs" );
  dir2 = worker_opendir( catdir.c_str() );
  if(dir2!=NULL) {
    while ( ( namelist = worker_readdir( dir2 ) ) != NULL ) {
      if ( strlen( namelist->d_name ) > ending.length() ) {
        if ( strcmp( ending.c_str(), ( namelist->d_name ) + strlen( namelist->d_name ) - ending.length() ) == 0 ) {
          // Dies ist ein Katalog
          tstr=namelist->d_name;
          tstr[ strlen( tstr ) - ending.length() ] = 0;
          trow = 1;
          while ( lv->isValidRow( trow ) == true ) {
            if ( strcmp( lv->getText( trow, 0 ).c_str(), tstr ) == 0 ) break;
            trow++;
          }
          if ( lv->isValidRow( trow ) == false ) {
            // only add if it doesn't have already is inserted
            trow = lv->addRow();
            lv->setText( trow, 0, tstr );
            lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
            lv->setData( trow, x++ );
            if ( strcmp( _baseconfig.getLang(), tstr ) == 0 ) lv->setActiveRow( trow );
          }
        }
      }
    }
    worker_closedir( dir2 );
  }
  lv->redraw();
  
  contMaximize( true );
  return 0;
}

int LangPanel::saveValues()
{
  int trow = lv->getActiveRow();
  if ( lv->isValidRow( trow ) == true ) {
    if ( strcmp( lv->getText( trow, 0 ).c_str(), catalog.getLocale( 10 ) ) == 0 ) _baseconfig.setLang( "builtin" );
    else _baseconfig.setLang( lv->getText( trow, 0 ).c_str() );
  }
  return 0;
}
