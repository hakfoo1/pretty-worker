/* solidbutton.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SOLIDBUTTON_H
#define SOLIDBUTTON_H

#include "aguixdefs.h"
#include "guielement.h"

class SolidButton:public GUIElement {
public:
  SolidButton(class AGUIX *aguix,int x,int y,
         const char *text,int fg,int bg,bool state);
  SolidButton(class AGUIX *aguix,int x,int y,int width,
         const char *text,int fg,int bg,bool state);
  SolidButton(class AGUIX *aguix,int x,int y,int width,int height,
         const char *text,int fg,int bg,bool state);

  SolidButton(class AGUIX *aguix,int x,int y,
         const char *text,bool state);
  SolidButton(class AGUIX *aguix,int x,int y,int width,
         const char *text,bool state);
  SolidButton(class AGUIX *aguix,int x,int y,int width,int height,
         const char *text,bool state);
  SolidButton( AGUIX *aguix, int x, int y, int width,
               const char *text,
               const std::string &fg_name,
               const std::string &bg_name,
               bool state );
  SolidButton( AGUIX *aguix, int x, int y,
               const char *text,
               const std::string &fg_name,
               const std::string &bg_name,
               bool state );

  virtual ~SolidButton();
  SolidButton( const SolidButton &other );
  SolidButton &operator=( const SolidButton &other );
  const char *getText() const;
  void setText(const char *);
  void setFG(int);
  int getFG() const;
  void setBG(int);
  int getBG() const;
  bool getState() const;
  void setState(bool);
  virtual void redraw();
  virtual void flush();
  virtual bool handleMessage(XEvent *E,Message *msg);
  int setFont( const char * );
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
private:
  char *text;
  int fg;
  int bg;
  bool state;
  class AGUIXFont *font;
  static const char *type;

  void prepareBG( bool force = false );
  void applyFaces();

  void init( AGUIX *aguix, int x, int y,
             int width, int height,
             const char *text,
             int fg, int bg,
             bool state );
  void init( AGUIX *aguix, int x, int y,
             int width,
             const char *text,
             int fg, int bg,
             bool state );
  void init( AGUIX *aguix, int x, int y,
             const char *text,
             int fg, int bg,
             bool state );
};

#endif
