/* message.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include "aguixdefs.h"
#include <X11/Xlib.h>
#include <list>
#include <string>
#include <vector>
#include <tuple>

class Button;
class ChooseButton;
class StringGadget;
class GUIElement;
class CycleButton;
class AWindow;
class FieldListView;
class KarteiButton;
class Slider;
class Widget;
class PopUpMenu;
class TextView;

typedef struct message
{
  int type;
  enum { NONE, TIMEREVENT, XIMSTUCK } specialType;
  Widget *lockElement;
  bool ack;
  int loop;

  Window window;
  KeySym key;
  unsigned int keystate;
  unsigned int button;
  int mousex,mousey;
  int mouserx,mousery;
  int gadgettype; // brauchen wir eigentlich nicht mehr, da jedes GUIElement einen Typ hat
  void *gadget;  // kann auch GUIElement *gadget werden
  Time time;
  char *keybuf;
  int x, y, width, height;

  bool internal;
} Message;

enum {AG_NONE,AG_KEYPRESSED,AG_KEYRELEASED,AG_MOUSEPRESSED,AG_MOUSERELEASED,AG_MOUSECLICKED,AG_MOUSEMOVE,
      AG_EXPOSE,AG_CLOSEWINDOW,AG_ENTERWINDOW,AG_LEAVEWINDOW,AG_BUTTONPRESSED,AG_BUTTONRELEASED,
      AG_BUTTONCLICKED,AG_CHOOSEPRESSED,AG_CHOOSERELEASED,AG_CHOOSECLICKED,
      AG_STRINGGADGET_ACTIVATE,AG_STRINGGADGET_DEACTIVATE,AG_STRINGGADGET_OK,
      AG_STRINGGADGET_CANCEL,AG_STRINGGADGET_CONTENTCHANGE,AG_STRINGGADGET_CURSORCHANGE,
      AG_SIZECHANGED,
      AG_DND_END,
      AG_CYCLEBUTTONCLICKED,

      AG_FIELDLV_ONESELECT,
      AG_FIELDLV_MULTISELECT,
      AG_FIELDLV_PRESSED,
      AG_FIELDLV_HEADERCLICKED,
      AG_FIELDLV_DOUBLECLICK,
      AG_FIELDLV_ENTRY_PRESSED,
      AG_FIELDLV_VSCROLL,
      AG_FIELDLV_HSCROLL,

      AG_KARTEIBUTTONCLICKED,
      AG_SLIDER_PRESSED, AG_SLIDER_CHANGED,
      AG_POPUPMENU_CLICKED, AG_POPUPMENU_CLOSED, AG_POPUPMENU_ENTRYEDITED,
      AG_TEXTVIEW_END_REACHED,
      AG_CLIENT_MESSAGE,

      AG_PASTE_FILE_LIST
};

typedef struct agkey {
  int type;
  Window window;
  KeySym key;
  unsigned int keystate;
  char *keybuf;
  Time time;
} AGKey;

typedef AGKey AGKeyPressed;
typedef AGKey AGKeyReleased;

typedef struct agmouse {
  int type;
  Window window;
  unsigned int button;
  int x,y;
  Time time;
} AGMouse;

typedef AGMouse AGMousePressed;
typedef AGMouse AGMouseReleased;
typedef AGMouse AGMouseClicked;
typedef AGMouse AGMouseMove;

typedef struct agexpose {
  int type;
  Window window;
  int x,y;
  int w,h;
} AGExpose;

typedef struct agclosewindow {
  int type;
  Window window;
} AGCloseWindow;

typedef struct agcross {
  int type;
  Window window;
} AGCross;

typedef AGCross AGEnterWindow;
typedef AGCross AGLeaveWindow;

typedef struct agbutton {
    int type;
    Button *button;
    int state;       // 1 is left-click, 2 right-click, 3 and 4 mouse-wheel
    int keystate;    // the state of the keyboard modifier keys
} AGButton;

typedef AGButton AGButtonPressed;
typedef AGButton AGButtonReleased;
typedef AGButton AGButtonClicked;

typedef struct agchoose {
  int type;
  ChooseButton *button;
  bool state;
} AGChoose;

typedef AGChoose AGChoosePressed;
typedef AGChoose AGChooseReleased;
typedef AGChoose AGChooseClicked;

typedef struct agstringgadget {
  int type;
  StringGadget *sg;
  bool ok;  /* only set for _DEACTIVATE
	     * true when Return pressed, false otherwise (escape pressed, only deactivated...)
	     */
  unsigned int keystate; /* key state for ok */
} AGStringGadget;

typedef AGStringGadget AGStringGadgetActivate;
typedef AGStringGadget AGStringGadgetDeactivate;
typedef AGStringGadget AGStringGadgetOk;
typedef AGStringGadget AGStringGadgetCancel;
typedef AGStringGadget AGStringGadgetContentChange;
typedef AGStringGadget AGStringGadgetCursorChange;

typedef struct agsizechange {
  int type;
  Window window;
  int neww,newh;
    bool explicit_resize;
} AGSizeChange;

typedef struct agdnd {
  int type;
  Widget *source_element;
  Widget *target_element;
  AWindow *target_awindow;
  struct {
      int value;
      class FieldLVRowData *rowDataP;
      std::string *external_data;
  } specialinfo;
} AGDND;

typedef AGDND AGDNDEND;

typedef struct agcyclebutton {
  int type;
  CycleButton *cyclebutton;
  int option;
} AGCycleButton;

typedef AGCycleButton AGCycleButtonClicked;

typedef struct agfieldlv {
  int type;
  FieldListView *lv;
  int row;
  Time time;
  bool mouse;
  unsigned int button;
} AGFieldLV;

typedef AGFieldLV AGFieldLVOneSelect;
typedef AGFieldLV AGFieldLVMultiSelect;
typedef AGFieldLV AGFieldLVPressed;

typedef struct agkarteibutton {
  int type;
  KarteiButton *karteibutton;
  int option;
  unsigned int mousebutton;
} AGKarteiButton;

typedef AGKarteiButton AGKarteiButtonClicked;

typedef struct agslider {
  int type;
  Slider *slider;
  int offset;
  Time time;
  bool mouse;
} AGSlider;

typedef AGSlider AGSliderChanged;
typedef AGSlider AGSliderPressed;

typedef struct {
    int type;
    PopUpMenu *menu;
    int clicked_entry_id;
    std::list<int> *recursive_ids;
    std::string *edit_content;
} AGPopUpMenu;

typedef struct {
  int type;
  TextView *textview;
} AGTextView;

typedef enum AGClientMessageType {
    CM_GENERIC,
    CM_XIM_STUCK
} ag_client_message_t;

typedef struct {
    int type;
    ag_client_message_t cm_type;
} AGClientMessage;

typedef struct {
    int type;
    void *agmessage_data;
    std::tuple< int, std::string, std::vector< std::string > > *file_list;
} AGPaste;

typedef union agmessage {
  int type;
  AGKey key;
  AGMouse mouse;
  AGExpose expose;
  AGCloseWindow closewindow;
  AGCross cross;
  AGButton button;
  AGChoose choose;
  AGStringGadget stringgadget;
  AGSizeChange size;
  AGDND dnd;
  AGCycleButton cyclebutton;
  AGFieldLV fieldlv;
  AGKarteiButton karteibutton;
  AGSlider slider;
  AGPopUpMenu popupmenu;
  AGTextView textview;
  AGClientMessage clientmessage;
    AGPaste paste;
  long direct[20];
} AGMessage;

Message *AGUIX_allocMessage();
void AGUIX_freeMessage( Message *);
AGMessage *AGUIX_allocAGMessage();
AGMessage *AGUIX_allocAGMessage( Message * );
void AGUIX_freeAGMessage( AGMessage *);

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
