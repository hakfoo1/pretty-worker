/* icons.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef ICONS_HH
#define ICONS_HH

static int aguix_icon_pref[] __attribute__((unused)) = {
                                13,
                                13,
                                  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
                                  0,   0,  10,   0,   0,  10, 100,  10,   0,   0,  10,   0,   0,
                                  0,  10, 100,  33,   0,  33, 100,  33,   0,  33, 100,  10,   0,
                                  0,   0,  33, 100,  33, 100, 100, 100,  33, 100,  33,   0,   0,
                                  0,   0,   0,  33, 100, 100,  10, 100, 100,  33,   0,   0,   0,
                                  0,  10,  33, 100, 100,   0,   0,   0, 100, 100,  33,  10,   0,
                                  0, 100, 100, 100,  10,   0,   0,   0,  10, 100, 100, 100,   0,
                                  0,  10,  33, 100, 100,   0,   0,   0, 100, 100,  33,  10,   0,
                                  0,   0,   0,  33, 100, 100,  10, 100, 100,  33,   0,   0,   0,
                                  0,   0,  33, 100,  33, 100, 100, 100,  33, 100,  33,   0,   0,
                                  0,  10, 100,  33,   0,  33, 100,  33,   0,  33, 100,  10,   0,
                                  0,   0,  10,   0,   0,  10, 100,  10,   0,   0,  10,   0,   0,
                                  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
};

#define aguix_icon_pref_len ( sizeof( aguix_icon_pref ) / sizeof( aguix_icon_pref[0] ) )
#define aguix_icon_pref_width ( aguix_icon_pref[0] )

static int aguix_icon_plus[] __attribute__((unused)) = {
                                12,
                                12,
                                0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0, 100,   0,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0, 100,  50,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0, 100,  50,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0, 100,  50,   0,   0,   0,   0,   0,
                                0, 100, 100, 100, 100, 100, 100, 100, 100, 100,   0,   0,
                                0,   0,  50,  50,  50, 100,  50,  50,  50,  50,  50,   0,
                                0,   0,   0,   0,   0, 100,  50,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0, 100,  50,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0, 100,  50,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0,   0,  50,   0,   0,   0,   0,   0,
                                0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
};

#define aguix_icon_plus_len ( sizeof( aguix_icon_plus ) / sizeof( aguix_icon_plus[0] ) )
#define aguix_icon_plus_width ( aguix_icon_plus[0] )

static int aguix_icon_cross[] __attribute__((unused)) = {
                                 12,
                                 12,
                                 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
                                 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
                                 0,   0, 100, 100,   0,   0,   0,   0, 100, 100,   0,   0,
                                 0,   0,  10, 100, 100,   0,   0, 100, 100,  10,  10,   0,
                                 0,   0,   0,  10, 100, 100, 100, 100,  10,   0,   0,   0,
                                 0,   0,   0,   0,  10, 100, 100,  10,   0,   0,   0,   0,
                                 0,   0,   0,   0, 100, 100, 100, 100,   0,   0,   0,   0,
                                 0,   0,   0, 100, 100,  10,  10, 100, 100,   0,   0,   0,
                                 0,   0, 100, 100,  10,   0,   0,  10, 100, 100,   0,   0,
                                 0,   0,   0,  10,   0,   0,   0,   0,  10,  10,  10,   0,
                                 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
                                 0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
};

#define aguix_icon_cross_len ( sizeof( aguix_icon_cross ) / sizeof( aguix_icon_cross[0] ) )
#define aguix_icon_cross_width ( aguix_icon_cross[0] )

#endif /* ICONS_HH */
