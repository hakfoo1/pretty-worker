/* popupwindow.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "popupwindow.hh"

int PopUpWindow::group_id_counter = 0;

PopUpWindow::PopUpWindow( AGUIX *parent,
                          int x,
                          int y,
                          int width,
                          int height,
                          std::string title,
                          int group_id ) : AWindow( parent, x, y, width, height, title, AWindow::AWINDOW_DIALOG ),
                                           m_group_id( group_id ),
                                           m_prev_revert_to( 0 ),
                                           m_prev_focus( 0 ),
                                           m_old_focus_got( false )
{
    int tbg = _aguix->getFaces().getColor( "popup-bg" );

    if ( tbg >= 0 ) {
        _bg = tbg;
    }

    if ( m_group_id == -1 ) {
        m_group_id = group_id_counter++;
        if ( group_id_counter == -1 )
            group_id_counter++;
    }
    parent->registerPopUpWindow( this );
}

PopUpWindow::~PopUpWindow()
{
  destroy();
}

int PopUpWindow::getGroupID() const
{
    return m_group_id;
}

void PopUpWindow::setGroupID( int g )
{
    m_group_id = g;
}

void PopUpWindow::show()
{
    if ( onScreen == false ) {
        _aguix->popupOpened();
        _aguix->hideOtherPopUpWindows( m_group_id );
    }
    AWindow::show();
    toFront();
}

void PopUpWindow::hide()
{
    if ( onScreen == true ) {
        revertOldFocus();
        _aguix->popupClosed();
    }
    AWindow::hide();
}

int PopUpWindow::create()
{
    int erg = AWindow::create();
    
    XSetWindowAttributes set_attr;
    set_attr.override_redirect = True;
    XChangeWindowAttributes( _aguix->getDisplay(), win, CWOverrideRedirect, &set_attr );
    
    setTransientForAWindow( _aguix->getTransientWindow() );
    return erg;
}

void PopUpWindow::setXFocus()
{
    if ( onScreen == true ) {
        XGetInputFocus( _aguix->getDisplay(),
                        &m_prev_focus,
                        &m_prev_revert_to );

        m_old_focus_got = true;

        XSetInputFocus( _aguix->getDisplay(), win, 
                        RevertToParent, CurrentTime );
    }
}

void PopUpWindow::revertOldFocus()
{
    if ( m_old_focus_got ) {
        XSetInputFocus( _aguix->getDisplay(), m_prev_focus, 
                        m_prev_revert_to, CurrentTime );

        m_old_focus_got = false;
    }
}
