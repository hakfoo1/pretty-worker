#ifndef WIDGET_H
#define WIDGET_H

#include "focus.h"

class AGUIX;

class Widget : public Focus
{
  friend class AWindow;
public:
  Widget( AGUIX *aguix );
  virtual ~Widget();
  Widget( const Widget &other );
  Widget &operator=( const Widget &other );
  
  virtual int create();
  virtual void destroy();
  bool isCreated() const;

  virtual void redraw() = 0;
  virtual bool handleMessage( XEvent *,Message *msg ) = 0;
  virtual int handleMessageLock( XEvent *E, Message *msg );
  virtual int getWidth() const;
  virtual int getHeight() const;
  virtual int getX() const;
  virtual int getY() const;
  virtual void move( int nx, int ny ) = 0;
  virtual void resize( int nw, int nh ) = 0;
  virtual bool contains( Widget *elem ) const;
  virtual Window getWindow() const = 0;
  virtual bool isParent( Window ) const = 0;
  virtual bool isVisible() const = 0;
  
  bool getAllowTimerEventBreak() const;
  void setAllowTimerEventBreak( bool nv );
  long getNrOfLastEventBreak() const;
  void setNrOfLastEventBreak( long t );
protected:
  virtual void doCreateStuff();
  virtual void doDestroyStuff();
  virtual int setParent( class AWindow *parent );
  void setIsTopLevelWidget( bool nv );
  
  class AWindow *_parent;
  bool _created;
  AGUIX *_aguix;
  int _x, _y, _w, _h;
  bool _is_top_level_widget;
private:
  bool m_allow_timerevent_break;
  long m_nr_of_last_event_break;
};

#endif
