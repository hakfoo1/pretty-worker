/* karteibutton.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef KARTEIBUTTON_H
#define KARTEIBUTTON_H

#include "cyclebutton.h"
#include "refcount.hh"

class TextShrinker;

class KarteiButton:public CycleButton {
public:
  KarteiButton(AGUIX *aguix,int x,int y,int width,
               int data);
  KarteiButton(AGUIX *aguix,int x,int y,int width,int height,
               int data);

  ~KarteiButton();
  KarteiButton( const KarteiButton &other );
  KarteiButton &operator=( const KarteiButton &other );

  void redraw();
  void flush();
  bool handleMessage(XEvent *E,Message *msg);
  const char *getType() const;
  bool isType(const char *type) const;
  int getMaxSize() const;
  void setOptionChangeCallback( void (*optionChangeCallback_arg)( class Kartei *k1, unsigned int option ),
				class Kartei *k2_arg );
  using CycleButton::setOption;
  void setOption( int nv );
  int setFont( const char *fontname );

  void setTextShrinker( RefCount<TextShrinker> shrinker );
protected:
  static const char *type;
  void (*optionChangeCallback)( class Kartei *k1, unsigned int option );
  class Kartei *k2;
  int focusPos;

  int findClickedOption( int mx );

  RefCount<TextShrinker> m_shrinker;
  std::vector<int> m_width_per_option;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
