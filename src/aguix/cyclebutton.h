/* cyclebutton.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CYCLEBUTTON_H
#define CYCLEBUTTON_H

#include "aguixdefs.h"
#include "guielement.h"
#include <vector>
#include <string>

class AWindow;
class AGUIXFont;

class CycleButton:public GUIElement {
public:
  CycleButton(AGUIX *aguix,int x,int y,int width,
              int data);
  CycleButton(AGUIX *aguix,int x,int y,int width,int height,
              int data);

  virtual ~CycleButton();
  CycleButton( const CycleButton &other );
  CycleButton &operator=( const CycleButton &other );

  const char *getOption(int);
  int addOption(const char *);
  int addOption( const std::string &text );
  int setOption( int, const char * );
  int getSelectedOption() const;
  void setFG(int);
  int getFG() const;
  void setBG(int);
  int getBG() const;
  int getData() const;
  void setData(int);
  void setState(int);
  int getState() const;
  virtual void redraw();
  virtual void flush();
  bool isInside(int x,int y) const;
  virtual bool handleMessage(XEvent *E,Message *msg);
  int setFont( const char * );
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
  void setOption(int index);
  int getMaxSize() const;

  int getNrOfOptions() const;
  void removeOption( int o );
protected:
  std::vector<std::string> options;
  int act_opt;
  int fg;
  int bg;
  int data;
  int state;
  int instate;
  AGUIXFont *font;
  static const char *type;
  
  void prepareBG( bool force = false );
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
