/* thread.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2016 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "thread.hh"

Thread::Thread() : _id( 0 ), m_running( false ), m_cancel( false )
{
}

Thread::~Thread()
{
  join();
}

int Thread::start()
{
  int erg;

  // lock var so actual run() only starts after _id is stored
  _lock.lock();
  m_running = true;
  erg = pthread_create( &_id, NULL, thread_entry, this );
  _lock.unlock();
  return erg;
}

void *Thread::thread_entry( void *me )
{
    ((Thread*)me)->thread_run();
    return NULL;
}

int Thread::join()
{
  int erg = 0;

  if ( _id != 0 ) {
    erg = pthread_join( _id, NULL );
    _id = 0;
  }
  return erg;
}

bool Thread::amIThisThread() const
{
  if ( pthread_equal( _id, pthread_self() ) )
    return true;
  return false;
}

void Thread::thread_run()
{
    _lock.lock();
    _lock.unlock();
    // after having got the lock _id is correct so amIThisThread works
    run();
    _lock.lock();
    m_running = false;
    _lock.unlock();
}

bool Thread::running()
{
    return m_running;
}

void Thread::signalCancel()
{
    m_cancel = true;
}

bool Thread::isCanceled()
{
    return m_cancel;
}
