/* awidth.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2005 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: awidth.h,v 1.1 2005/08/16 20:44:34 ralf Exp $ */

#ifndef AWIDTH_H
#define AWIDTH_H

#include "aguixdefs.h"

/*
 * AWidth
 * abstract class for width calculation
 */
class AWidth
{
 public:
  AWidth() {}
  virtual ~AWidth() {}
  AWidth( const AWidth &other );
  AWidth &operator=( const AWidth &other );

  /* method should return width of str with len characters (<0 means up to the null byte) */
  virtual int getWidth( const char *str, int len = -1 ) = 0;

  /* method should return number of characters fitting into given width
   * returning not more than strlen(str) and return_width should be the actual width (can be less than
   * width)
   */
  virtual int getStrlen4Width( const char *str, int width, int *return_width ) = 0;
  virtual int getStrlen4WidthMaxlen( const char *str, int maxlen, int width, int *return_width ) = 0;
};

class ACharWidth : public AWidth
{
 public:
  ACharWidth();
  ~ACharWidth();
  ACharWidth( const ACharWidth &other );
  ACharWidth &operator=( const ACharWidth &other );
  int getWidth( const char *str, int len = -1 );
  int getStrlen4Width( const char *str, int width, int *return_width );
  int getStrlen4WidthMaxlen( const char *str, int maxlen, int width, int *return_width );
};  

class AFontWidth  : public AWidth
{
 public:
  AFontWidth( class AGUIX *aguix, class AGUIXFont *font );
  ~AFontWidth();
  AFontWidth( const AFontWidth &other );
  AFontWidth &operator=( const AFontWidth &other );
  int getWidth( const char *str, int len = -1 );
  int getStrlen4Width( const char *str, int width, int *return_width );
  int getStrlen4WidthMaxlen( const char *str, int maxlen, int width, int *return_width );
 private:
  class AGUIX *_aguix;
  class AGUIXFont *_font;
};

#endif
