#ifndef FOCUS_H
#define FOCUS_H

#include "aguixdefs.h"
#include <map>

//TODO make it abstract
//  which method? lostFocus?
class Focus
{
protected:
  Focus();
public:
  virtual ~Focus();
  bool getAcceptFocus() const;
  void setAcceptFocus( bool nv );
  bool getHasFocus() const;
protected:
  void setCanHandleFocus();
  
  /* check whether element is still in map for window w
   * and set _has_focus
   * use in setFocusElement
   */
  void checkFocusState( const AWindow *w );
  
  static void setFocusElement( const AWindow *w, Focus *f );
  static Focus *getFocusElement( const AWindow *w );
private:
  static std::map<const class AWindow*, Focus*> _focus_map;
  bool _accept_focus;
  bool _can_handle_focus;
  bool _has_focus;

  virtual void lostFocus();
  virtual void gotFocus();
};

#endif
