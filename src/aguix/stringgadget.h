/* stringgadget.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRINGGADGET_H
#define STRINGGADGET_H

#include "aguixdefs.h"
#include "guielement.h"
#include <string>
#include <vector>

class StringGadget:public GUIElement {
public:
  StringGadget(class AGUIX *aguix,int x,int y,int width,int height,const char *text,int data);
  StringGadget(class AGUIX *aguix,int x,int y,int width,const char *text,int data);
  virtual ~StringGadget();
  StringGadget( const StringGadget &other );
  StringGadget &operator=( const StringGadget &other );
  void resize(int w,int h);
  int getData() const;
  void setData(int);
  virtual void redraw();
  virtual void flush();
  bool isInside(int x,int y) const;
  virtual bool handleMessage(XEvent *E,Message *msg);
  const char *getText() const;
  void setText(const char *new_text);
  void setXOffset(int new_offset);
  int getXOffset() const;
  void activate();
  void deactivate();
  void setCursor(int new_pos);
  int getCursor() const;
  bool isActive() const;
  int setFont( const char * );
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
  virtual bool isParent(Window) const;
  bool isPosChangeForbidden() const;
  void setForbidPosChange(bool nv);
  void setStrongKeyCapture(bool nv);
  void paste( const unsigned char*) override;
  void cancelpaste() override;
  virtual void cancelcut();
  void insertAtCursor( const char *str );
  void selectAll( bool copy_selection = true );
  void setPasswordMode( bool nv );
  void changeHeightForCurFont();
  void setSelection( int start, int end, bool copy_selection = true );
  virtual void setReadOnly( bool nv );
protected:
  virtual void doCreateStuff();
  virtual void doDestroyStuff();
private:
  class SGContent
  {
  public:
    SGContent();

    void setXOffset( int np );
    void setSelStart( int np );
    void setSelEnd( int np );
    void setCursorPos( int np );
    void setPasswordMode( bool mode );
    const char *getText() const;
    int getXOffset() const;
    int getSelStart() const;
    int getSelEnd() const;
    int getCursorPos() const;

    void insertAtCursor( const char *text );
    void setText( const char *text );

    const char *getRealText() const;

    void removeRange( int start, int end );
  private:
    std::string real_text, pw_text;
    int xoffset, selstart, selend, cursorpos;
    bool password_mode;
    std::vector<int> real_char2byte_lookup;
    std::vector<int> pw_char2byte_lookup;

    int pw2realPos( int pos );
    int real2pwPos( int pos );
    void fixPositions();
    void fixPosition( int &pos );
  };

  SGContent content;

  int data;
  bool active;
  static const char *type;
  class AGUIXFont *font;
  bool forbidPosChange;
  bool ignoreRelease;
  bool strongkeycapture;
  bool m_read_only;
  
  void updateWin();
  void textRedraw();
  void applySelection( bool both_buffers = false );
  void removeSelection();
  void insertSelection( bool from_clipboard = false );

  bool wantpaste;
  time_t pasterequest;
  bool paste_from_clipboard;
  bool passwordMode;
  void lostFocus();
  void gotFocus();

  int getInnerWidth() const;

  void prepareBG( bool force = false );
  int getBorderWidth() const;
  int clearTextBG();

  void jumpToNextWord();
  void jumpToPrevWord();
};

#endif

