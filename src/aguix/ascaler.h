/* ascaler.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2004 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: ascaler.h,v 1.3 2004/10/27 20:35:01 ralf Exp $ */

#ifndef ASCALER_H
#define ASCALER_H

#include "aguixdefs.h"

namespace AScaler {

  int scaleElements( int wantedWidth,
		     int elems,
		     const int *minWidths,
		     const int *maxWidths,
		     const int *weights,
		     int *returnWidths );
  
  int scaleElementsSimple( int wantedWidth,
			   int elems,
			   const int *minWidths,
			   const int *maxWidths,
			   const int *weights,
			   int *returnWidths );
  
}

#endif
