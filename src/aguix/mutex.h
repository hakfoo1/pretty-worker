/* mutex.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2002-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MUTEX_H
#define MUTEX_H

#include "aguixdefs.h"
#include <pthread.h>
#include <mutex>

class MutEx
{
 public:
    MutEx();
    virtual ~MutEx();
    MutEx( const MutEx &other );
    MutEx &operator=( const MutEx &other );
    virtual void lock();
    virtual void unlock();
    virtual bool trylock();
 protected:
    // the actual pthread mutex for locking and condvar
    pthread_mutex_t _mutex;

    // a mutex for the owner data
    std::mutex m_owner_data_mutex;
    pthread_t _owner;
    int _owncount;
};

#endif
