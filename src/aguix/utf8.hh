/* utf8.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef UTF8_HH
#define UTF8_HH

#include "aguixdefs.h"    
#include <vector>
#include <string>

class UTF8
{
public:
  UTF8();
  ~UTF8();

  typedef enum { UTF8_NORMAL, UTF8_DISABLED } utf8_mode_t;
  static utf8_mode_t getUTF8Mode();
  static void disableUTF8();

  /**
   * returns true if first character in str is a valid unicode characer
   * depends on locale settings
   */
  static bool isValidCharacter( const char *str );

  /**
   * returns the number of bytes for the first character in str
   * depends on locale settings
   */
  static size_t getLenOfCharacter( const char *str );

  /**
   * builds a lookup list which maps from characterlen to strlen
   * (i.e., first x characters use y bytes => list[x] = y)
   * depends on locale settings
   */
  static int buildCharacterLookupList( const char *str, std::vector<int> &list );
  static int buildCharacterLookupListMaxlen( const char *str, int maxlen, std::vector<int> &list );

  /**
   * updates pos to next valid character
   * checks utf8 mode
   * returns position change
   */
  static int movePosToNextChar( const char *str, int &pos );

  /**
   * updates pos to prev valid character
   * pos doesn't have to point to a valid character
   * checks utf8 mode
   * returns position change
   */
  static int movePosToPrevChar( const char *str, int &pos );

  static int findCharacterPosition( int byte_pos, std::vector<int> &char2byte_lookup );

  static int getNumberOfCharacters( const char *str );

  static bool isValidCharacterString( const char *str );

  static std::string convertToValidCharacterString( const char *str );

    /**
     * return pos so that it points t o null byte or a valid (utf8)
     * character. If it is already a valid position, it will not
     * change.
     */
    static int movePosBackToValidPos( const char *str, int pos );

  typedef enum { ENCODING_UTF8, ENCODING_8BIT, ENCODING_UNSUPPORTED } encodings_enum_t;

  static encodings_enum_t checkSupportedEncodings();
  static encodings_enum_t getCurrentEncoding();
private:
  static utf8_mode_t utf8_mode;
  static encodings_enum_t cur_enc;
  static bool cur_enc_inited;

};

#endif
