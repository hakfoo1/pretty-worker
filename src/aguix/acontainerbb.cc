/* acontainerbb.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2004-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "acontainerbb.h"
#include "bevelbox.h"
#include "awindow.h"

AContainerBB::AContainerBB( AWindow *twin, int txelems, int tyelems ) : AContainer( twin, txelems, tyelems )
{
  setBorderWidth( 11 );
  bb = NULL;
  if ( twin != NULL ) {
    bb = (BevelBox*)twin->add( new BevelBox( twin->getAGUIX() ) );
    bb->setState( 1 );
  }
}

AContainerBB::AContainerBB( AWindow *twin,
                            int txelems,
                            int tyelems,
                            int border_width,
                            int min_space,
                            int max_space ) : AContainer( twin, txelems, tyelems, border_width, min_space, max_space )
{
    bb = NULL;
    if ( twin != NULL ) {
        bb = (BevelBox*)twin->add( new BevelBox( twin->getAGUIX() ) );
        bb->setState( 1 );
    }
}

AContainerBB::~AContainerBB()
{
  if ( bb != NULL ) {
    delete bb;
  }
}

int AContainerBB::rearrange( int vhmode )
{
  int tx, ty, tw, th, i;
  
  AContainer::rearrange( vhmode );
  if ( bb != NULL ) {
    tw = getWidth();
    th = getHeight();

    i = getBorderWidth() / 2;
    tx = ty = i;
    tw -= i * 2;
    th -= i * 2;
    if ( tw < 1 ) tw = 1;
    if ( th < 1 ) th = 1;

    bb->move( getX() + tx, getY() + ty );
    bb->resize( tw, th );
    bb->toBack();
  }
  return 0;
}

int AContainerBB::remove( const Widget *v )
{
  if ( v == bb ) bb = NULL;
  return AContainer::remove( v );
}

bool AContainerBB::getBBState() const
{
  if ( bb == NULL ) return false;
  return ( bb->getState() == 1 ) ? true : false;
}

void AContainerBB::setBBState( bool nv )
{
  if ( bb == NULL ) return;
  bb->setState( ( nv == true ) ? 1 : 0 );
}

void AContainerBB::hide()
{
    AContainer::hide();

    if ( bb ) bb->hide();
}

void AContainerBB::show()
{
    AContainer::show();

    if ( bb ) bb->show();
}
