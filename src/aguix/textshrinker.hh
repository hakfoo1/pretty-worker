/* textshrinker.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2007 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TEXTSHRINKER_HH
#define TEXTSHRINKER_HH

#include "aguixdefs.h"
#include <string>
#include "awidth.h"

/*
 * TextShrinker
 * abstract class for shrinking texts
 */
class TextShrinker
{
public:
    TextShrinker() {}
    virtual ~TextShrinker() {}
    
    virtual std::string shrink( const std::string &str, int len_limit, AWidth &len_calc ) = 0;
};

class HalfShrinker : public TextShrinker
{
public:
    HalfShrinker();
    ~HalfShrinker();
    
    std::string shrink( const std::string &str, int len_limit, AWidth &len_calc );
};

#endif
