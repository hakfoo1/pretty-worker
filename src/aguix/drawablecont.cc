/* drawablecont.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "drawablecont.hh"

DrawableCont::DrawableCont( AGUIX *aguix, Drawable buffer ) :_aguix( aguix ),
                                                             _buffer( buffer )
{
#ifdef HAVE_XFT
  _xftdraw = NULL;
#endif
}

DrawableCont::~DrawableCont()
{
#ifdef HAVE_XFT
  if ( _xftdraw != NULL ) {
    XftDrawDestroy( _xftdraw );
  }
#endif
}

Drawable DrawableCont::getDrawable()
{
  return _buffer;
}

#ifdef HAVE_XFT
XftDraw *DrawableCont::getXftDraw()
{
  if ( _xftdraw == NULL ) {
    _xftdraw = XftDrawCreate( _aguix->getDisplay(),
                              _buffer,
                              DefaultVisual( _aguix->getDisplay(), _aguix->getScreen() ),
                              _aguix->getColormap() );
  }
  return _xftdraw;
}
#endif

void DrawableCont::clip( int x, int y, int width, int height, GC *gc )
{
#ifdef HAVE_XFT
  XPoint p[4];
  Region region;

  p[0].x = x;
  p[0].y = y;
  p[1].x = x + width/* - 1*/;
  p[1].y = y;
  p[2].x = x + width/* - 1*/;
  p[2].y = y + height/* - 1*/;
  p[3].x = x;
  p[3].y = y + height/* - 1*/;

  region = XPolygonRegion( p, 4, EvenOddRule );
  XftDrawSetClip( getXftDraw(), region );
  XDestroyRegion( region );
#endif
  if ( gc != NULL ) {
    XRectangle clip_rect;
    
    clip_rect.x = x;
    clip_rect.y = y;
    clip_rect.width = width;
    clip_rect.height = height;
    
    XSetClipRectangles( _aguix->getDisplay(), *gc, 0, 0, &clip_rect, 1, Unsorted );
  }
}

void DrawableCont::unclip( GC *gc )
{
#ifdef HAVE_XFT
  Region region;
  region = XCreateRegion();
  XftDrawSetClip( getXftDraw(), region );
  XDestroyRegion( region );
#endif
  if ( gc != NULL ) {
    XSetClipMask( _aguix->getDisplay(), *gc, None );
  }
}
