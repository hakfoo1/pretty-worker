/* searchtextstorage.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SEARCHTEXTSTORAGE_HH
#define SEARCHTEXTSTORAGE_HH

#include "aguixdefs.h"
#include <string>
#include <utility>
#include "compregex.hh"

class TextStorage;
class TextView;

class SearchTextStorage
{
public:
    SearchTextStorage( TextStorage &ts );
    virtual ~SearchTextStorage();

    void search( const std::string &search_string );
    void searchBackwards( const std::string &search_string );
    void highlightCurMatch( TextView &tv );

    void setSearchStartLine( int line );

    int getCurMatchLine() const;
private:
    TextStorage &m_ts;
    std::string m_last_search_string;
    std::pair<int, int> m_start_search;
    struct {
        int line;
        int start_offset;
        int end_offset;
    } m_cur_match;

    CompRegEx m_cre;

    int prevPos( std::pair< int, int > &pos );
};

#endif
