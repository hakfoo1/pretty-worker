/* fieldlistviewdnd.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2002-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FIELDLISTVIEWDND_H
#define FIELDLISTVIEWDND_H

#include "aguixdefs.h"
#include "fieldlistview.h"
#include "dndtext.h"
#include "dndwidget.hh"
#include <vector>
#include <string>

class FieldListViewDND : public FieldListView, public DNDWidget
{
public:
  FieldListViewDND( AGUIX *aguix, int x, int y, int width, int height, int data );
  virtual ~FieldListViewDND();
  FieldListViewDND( const FieldListViewDND &other );
  FieldListViewDND &operator=( const FieldListViewDND &other );

  virtual const char *getType() const;
  virtual bool isType(const char *type) const;
  void removealldndtexts();
  void replacedndtext( int, const char* );
  void adddndtext( const char* );
  
  void setSizeDNDText(int);

  void cleanupDNDStart();
  void processDNDDrop( AGMessage *agmsg );
  void cancelDND();
  void addDragData( AGMessage *agmsg );
  int fillDropData( std::string *res );

  void setDNDDataCallback( std::function< std::string( FieldListViewDND *lv, const FieldLVRowData *rowdata ) > cb );
private:
  static const char *type;
  virtual void handleSelect(Message *msg);
  std::vector<std::string> dndtexts;

  typedef enum { DND_IDLE, DND_DEACTIVE, DND_ACTIVE } dndMode_t;
  dndMode_t dndMode;
  DNDText *dndtext;
  void handleDND( Message *msg );

  struct dnd_data {
      dnd_data() : row( -1 ),
          element( NULL )
      {
      }
      ~dnd_data()
      {
          delete element;
      }

      int row;
      FieldLVRowData *element;
  };

  dnd_data m_dnd_start_data;

  std::function< std::string( FieldListViewDND *lv, const FieldLVRowData *rowdata ) > m_dnd_data_callback;
};

#endif
