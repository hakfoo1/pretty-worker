/* acontainerbb.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2004-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: acontainerbb.h,v 1.4 2005/05/15 13:44:42 ralf Exp $ */

#ifndef ACONTAINERBB_H
#define ACONTAINERBB_H

#include "aguixdefs.h"
#include "acontainer.h"

class AContainerBB : public AContainer
{
public:
  AContainerBB( class AWindow *twin, int txelems = 1, int tyelems = 1 );
  AContainerBB( class AWindow *twin, int txelems, int tyelems, int border_width, int min_space, int max_space );
  ~AContainerBB();
  AContainerBB( const AContainerBB &other );
  AContainerBB &operator=( const AContainerBB &other );
  
  int rearrange( int vhmode = 0 );
  int remove( const Widget *v );
  bool getBBState() const;
  void setBBState( bool nv );

    void hide();
    void show();
private:
  class BevelBox *bb;
};

#endif
