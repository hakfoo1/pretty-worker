/* awidth.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2005 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: awidth.cc,v 1.1 2005/08/16 20:44:46 ralf Exp $ */

#include "awidth.h"
#include "aguix.h"

ACharWidth::ACharWidth() : AWidth()
{
}

ACharWidth::~ACharWidth()
{
}

int ACharWidth::getWidth( const char *str, int len )
{
  int slen;
  
  if ( str == NULL ) return 0;
  
  if ( len < 0 ) {
      slen = strlen( str );
  } else {
      if ( AGUIXUtils::stringIsShorter( str, len ) == true ) {
          slen = strlen( str );
      } else {
          slen = len;
      }
  }

  return slen;
}

int ACharWidth::getStrlen4Width( const char *str, int width, int *return_width )
{
    return getStrlen4WidthMaxlen( str, -1, width, return_width );
}

int ACharWidth::getStrlen4WidthMaxlen( const char *str, int maxlen, int width, int *return_width )
{
  int slen;
  
  if ( str == NULL ) {
    if ( return_width != NULL ) *return_width = 0;
    return 0;
  }

  if ( maxlen < 0 ) {
      slen = strlen( str );
  } else {
      if ( AGUIXUtils::stringIsShorter( str, maxlen ) == true ) {
          slen = strlen( str );
      } else {
          slen = maxlen;
      }
  }

  if ( ( slen > width ) && ( width >= 0 ) ) slen = width;

  if ( return_width != NULL )
    *return_width = slen;
  return slen;
}

AFontWidth::AFontWidth( class AGUIX *aguix, class AGUIXFont *font ) : AWidth(), _aguix( aguix ), _font( font )
{
}

AFontWidth::~AFontWidth()
{
}

int AFontWidth::getWidth( const char *str, int len )
{
  if ( ( str == NULL ) || ( _aguix == NULL ) ) return 0;
  
  return _aguix->getTextWidth( str, _font, len );
}

int AFontWidth::getStrlen4Width( const char *str, int width, int *return_width )
{
  return _aguix->getStrlen4Width( str, width, return_width, _font );
}

int AFontWidth::getStrlen4WidthMaxlen( const char *str, int maxlen, int width, int *return_width )
{
    return _aguix->getStrlen4WidthMaxlen( str, maxlen, width, return_width, _font );
}
