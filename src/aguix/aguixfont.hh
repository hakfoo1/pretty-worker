/* aguixfont.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2001-2006 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AGUIXFONT_HH
#define AGUIXFONT_HH

#include "aguixdefs.h"
#include <X11/Xlib.h>

#ifdef HAVE_XFT
#include <X11/Xft/Xft.h>
#endif

#include <string>
#include "aguixcolor.hh"

class AGUIX;

class AGUIXFont
{
public:
  AGUIXFont(AGUIX*);
  ~AGUIXFont();
  AGUIXFont( const AGUIXFont &other );
  AGUIXFont &operator=( const AGUIXFont &other );

  int setFont( const char* );
  void removeFont();
  GC getGC() const;
  const char *getName() const;
  int getCharHeight() const;
  int getBaseline() const;
  
  int textWidth( const char *str, int count ) const;
  void drawText( class DrawableCont &dc, const char *text, int x, int y, const AGUIXColor &color );

  int getNumberOfMissingCharsets() const;
  void clip( int x, int y, int width, int height );
  void unclip();
private:
  std::string name;
  GC gc;
  AGUIX *aguix;
  XFontStruct *font;
  int charHeight;
  XFontSet _fontset;
  int _nmissing;
  XFontSetExtents *extents;

#ifdef HAVE_XFT
  XftFont *_xftfont;
#endif
};

#endif
