/* button.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BUTTON_H
#define BUTTON_H

#include "aguixdefs.h"
#include "guielement.h"

class AWindow;
class AGUIXFont;
class AIcon;

class Button:public GUIElement {
public:
  Button( AGUIX *aguix, int x, int y, int width,
          const char *text, int data );

  Button(AGUIX *aguix,int x,int y,
         const char *text, int fg, int bg, int data);
  Button( AGUIX *aguix, int x, int y,
          const char *text, int data );
  Button( AGUIX *aguix, int x, int y,
          const char *text,
          const std::string &fg_name,
          const std::string &bg_name,
          int data );

  Button(AGUIX *aguix,int x,int y,int width,int height,
         const char *text, int fg, int bg, int data);
  Button( AGUIX *aguix, int x, int y, int width, int height,
          const char *text, int data );

  Button(AGUIX *aguix,int x,int y,int width,
         const char *text_normal,const char *text_high,
         int fg_normal, int fg_high,
         int bg_normal, int bg_high, int data);
  Button( AGUIX *aguix, int x, int y, int width,
          const char *text_normal, const char *text_high,
          int data );

  Button( AGUIX *aguix, int x, int y,
          const char *text_normal, const char *text_high,
          int fg_normal, int fg_high,
          int bg_normal, int bg_high, int data );
  Button( AGUIX *aguix, int x, int y,
          const char *text_normal, const char *text_high,
          int data );

  virtual ~Button();
  Button( const Button &other );
  Button &operator=( const Button &other );

  const char *getText(int) const;
  void setText(int,const char *);
  bool getDualState() const;
  void setDualState(bool);
  void setFG(int,int);
  void setFG( int type, const std::string &fg_name );
  int getFG(int) const;
  void setBG(int,int);
  void setBG( int type, const std::string &fg_name );
  int getBG(int) const;
  int getData() const;
  void setData(int);
  void setState(int);
  int getState() const;
  virtual void redraw();
  virtual void flush();
  bool isInside(int x,int y) const;
  virtual bool handleMessage(XEvent *E,Message *msg);
  bool getShowDual() const;
  void setShowDual(bool);
  int setFont( const char* );
  virtual const char *getType() const;
  virtual bool isType(const char *type) const;

  void deactivate();
  void activate();
  void deactivate(int mode);
  void activate(int mode);

  bool getAllowWheel() const;
  void setAllowWheel( bool nv );

  int getMaximumWidth();

    typedef enum {
                  ICON_LEFT,
                  ICON_RIGHT
    } icon_position;

    void setIcon( const int *pixels,
                  size_t pixels_length,
                  icon_position pos );
    int getPreferredHeight();

    void setDisabled( bool nv );
    bool getDisabled() const;

    void setStrikeOut( bool nv );
    bool getStrikeOut() const;

    void maximize( bool grow_only = false );
private:
  char *text[2];
  int fg[2];
  int bg[2];
  bool dual;
  int data;
  int state;
  int instate;
  bool showdual;
  AGUIXFont *font;
  bool active[2];
  bool allowWheel;
    std::shared_ptr< AIcon > m_icon;
    icon_position m_icon_pos = ICON_LEFT;

    bool m_disabled = false;
    bool m_strike_out = false;

  static const char *type;
  void init_class();
  void prepareBG( bool force = false );
  void applyFaces();
};

#endif
