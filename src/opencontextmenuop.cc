/* opencontextmenuop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "opencontextmenuop.hh"
#include "listermode.h"
#include "virtualdirmode.hh"
#include "worker_locale.h"
#include "worker.h"
#include "datei.h"
#include "aguix/choosebutton.h"
#include "aguix/button.h"
#include "aguix/awindow.h"

const char *OpenContextMenuOp::name = "OpenContextMenuOp";

OpenContextMenuOp::OpenContextMenuOp() : FunctionProto()
{
    m_highlight_user_action = false;
    hasConfigure = true;
}

OpenContextMenuOp::~OpenContextMenuOp()
{
}

OpenContextMenuOp *OpenContextMenuOp::duplicate() const
{
    OpenContextMenuOp *ta = new OpenContextMenuOp();
    ta->m_highlight_user_action = m_highlight_user_action;
    return ta;
}

bool OpenContextMenuOp::isName(const char *str)
{
    if ( strcmp( str, name ) == 0 )
        return true;
    else
        return false;
}

const char *OpenContextMenuOp::getName()
{
    return name;
}

int OpenContextMenuOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    Lister *l1;
    ListerMode *lm1;
    
    l1 = msg->getWorker()->getActiveLister();
    if ( l1 == NULL )
        return 1;

    lm1 = l1->getActiveMode();
    if ( auto vdm = dynamic_cast< VirtualDirMode *>( lm1 ) ) {
        VirtualDirMode::context_menu_settings sets;

        sets.setHighlightUserAction( m_highlight_user_action );
        
        vdm->openContextMenu( sets );
    }
        
    return 0;
}

const char *OpenContextMenuOp::getDescription()
{
    return catalog.getLocale( 1293);
}

bool
OpenContextMenuOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;

    if ( m_highlight_user_action ) {
        fh->configPutPairBool( "highlightuseraction", m_highlight_user_action );
    }
    return true;
}

int OpenContextMenuOp::configure()
{
    AGUIX *aguix = Worker::getAGUIX();
    Button *okb, *cancelb;
    AWindow *win;
    AGMessage *msg;
    int endmode = -1;
    char *tstr;
    
    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe( tstr );
    
    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );
    
    ChooseButton *highlight_cb = ac1->addWidget( new ChooseButton( aguix, 0, 0, ( m_highlight_user_action == true ) ? 1 : 0,
                                                                   catalog.getLocale( 1078 ), LABEL_RIGHT, 0 ),
                                                 0, 0, AContainer::CO_INCWNR );
    
    AContainer *ac1_5 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
    ac1_5->setMinSpace( 5 );
    ac1_5->setMaxSpace( -1 );
    ac1_5->setBorderWidth( 0 );
    okb = (Button*)ac1_5->add( new Button( aguix,
                                           0,
                                           0,
                                           catalog.getLocale( 11 ),
                                           0 ), 0, 0, AContainer::CO_FIX );
    cancelb = (Button*)ac1_5->add( new Button( aguix,
                                               0,
                                               0,
                                               catalog.getLocale( 8 ),
                                               0 ), 1, 0, AContainer::CO_FIX );
    win->contMaximize( true );
    
    win->setDoTabCycling( true );
    win->show();
    for( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cancelb ) endmode = 1;
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
    
    if ( endmode == 0 ) {
        // ok
        m_highlight_user_action = highlight_cb->getState();
    }
    delete win;

    return endmode;
}

void OpenContextMenuOp::setHighlightUserAction( bool nv )
{
    m_highlight_user_action = nv;
}
