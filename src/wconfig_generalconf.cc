/* wconfig_generalconf.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_generalconf.hh"
#include "wconfig.h"
#include "worker.h"
#include "aguix/acontainerbb.h"
#include "worker_locale.h"
#include "aguix/choosebutton.h"
#include "aguix/cyclebutton.h"
#include "aguix/stringgadget.h"

GeneralConfPanel::GeneralConfPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

GeneralConfPanel::~GeneralConfPanel()
{
}

int GeneralConfPanel::create()
{
    Panel::create();
    
    AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );
    
    ac1->add( new Text( _aguix, 0, 0, catalog.getLocale( 784 ) ), 0, 0, AContainer::CO_INCWNR );

#if 0
    AContainerBB *ac1_1 = (AContainerBB*)ac1->add( new AContainerBB( this, 1, 5 ), 0, 1 );
#else
    AContainerBB *ac1_1 = (AContainerBB*)ac1->add( new AContainerBB( this, 1, 4 ), 0, 1 );
#endif
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( 5 );
    ac1_1->setBorderWidth( 7 );

    m_cb = (ChooseButton*)ac1_1->add( new ChooseButton( _aguix,
                                                        0,
                                                        0,
                                                        _baseconfig.getSaveWorkerStateOnExit(),
                                                        catalog.getLocale( 785 ),
                                                        LABEL_LEFT,
                                                        0 ), 0, 0, AContainer::CO_INCWNR );
    
    AContainer *ac1_scm = ac1_1->add( new AContainer( this, 2, 1 ), 0, 1 );
    ac1_scm->setMinSpace( 5 );
    ac1_scm->setMaxSpace( 5 );
    ac1_scm->setBorderWidth( 0 );

    ac1_scm->add( new Text( _aguix, 0, 0, catalog.getLocale( 1150 ) ), 0, 0, AContainer::CO_FIX );

    m_string_compare_mode_cycb = ac1_scm->addWidget( new CycleButton( _aguix, 0, 0, 10, 0 ), 1, 0, AContainer::CO_INCWNR );

    m_string_compare_mode_cycb->addOption( catalog.getLocale( 1151 ) );
    m_string_compare_mode_cycb->addOption( catalog.getLocale( 1152 ) );
    m_string_compare_mode_cycb->addOption( catalog.getLocale( 1153 ) );
    m_string_compare_mode_cycb->resize( m_string_compare_mode_cycb->getMaxSize(),
                                        m_string_compare_mode_cycb->getHeight() );
    ac1_scm->readLimits();

    if ( _baseconfig.getUseStringCompareMode() == StringComparator::STRING_COMPARE_VERSION ) {
        m_string_compare_mode_cycb->setOption( 1 );
    } else if ( _baseconfig.getUseStringCompareMode() == StringComparator::STRING_COMPARE_NOCASE ) {
        m_string_compare_mode_cycb->setOption( 2 );
    } else {
        m_string_compare_mode_cycb->setOption( 0 );
    }

    m_exregex_cb = (ChooseButton*)ac1_1->add( new ChooseButton( _aguix,
                                                                0,
                                                                0,
                                                                _baseconfig.getUseExtendedRegEx(),
                                                                catalog.getLocale( 1077 ),
                                                                LABEL_LEFT,
                                                                0 ), 0, 2, AContainer::CO_INCWNR );

    AContainer *ac1_1_2 = ac1_1->add( new AContainer( this, 2, 1 ), 0, 3 );
    ac1_1_2->setBorderWidth( 0 );
    ac1_1_2->setMinSpace( 5 );
    ac1_1_2->setMaxSpace( 5 );

    ac1_1_2->add( new Text( _aguix, 0, 0, catalog.getLocale(1385) ), 0, 0, AContainer::CO_FIX );
    m_bg_check_sg = ac1_1_2->addWidget( new StringGadget( _aguix, 0, 0, 200, _baseconfig.getDisableBGCheckPrefix().c_str(), 0 ), 1, 0, AContainer::CO_INCW );

#if 0
    m_dialog_cb = (ChooseButton*)ac1_1->add( new ChooseButton( _aguix,
                                                               0,
                                                               0,
                                                               _baseconfig.getApplyWindowDialogType(),
                                                               catalog.getLocale( 999 ),
                                                               LABEL_LEFT,
                                                               0 ), 0, 3, AContainer::CO_INCWNR );
#endif
    
    contMaximize( true );
    return 0;
}

int GeneralConfPanel::saveValues()
{
    _baseconfig.setSaveWorkerStateOnExit( m_cb->getState() );
    _baseconfig.setUseExtendedRegEx( m_exregex_cb->getState() );

    if ( m_string_compare_mode_cycb->getSelectedOption() == 1 ) {
        _baseconfig.setUseStringCompareMode( StringComparator::STRING_COMPARE_VERSION );
    } else if ( m_string_compare_mode_cycb->getSelectedOption() == 2 ) {
        _baseconfig.setUseStringCompareMode( StringComparator::STRING_COMPARE_NOCASE );
    } else {
        _baseconfig.setUseStringCompareMode( StringComparator::STRING_COMPARE_REGULAR );
    }

    _baseconfig.setDisableBGCheckPrefix( m_bg_check_sg->getText() );

#if 0
    _baseconfig.setApplyWindowDialogType( m_dialog_cb->getState() );
#endif
    return 0;
}
