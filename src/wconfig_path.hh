/* wconfig_path.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_PATH_HH
#define WCONFIG_PATH_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;
class SolidButton;
class CycleButton;

class PathPanel : public WConfigPanel
{
public:
  PathPanel( AWindow &basewin, WConfig &baseconfig );
  ~PathPanel();
  int create();
  int saveValues();

  /* gui elements callback */
  void run( Widget *, const AGMessage &msg ) override;

  /* we are interested in updated colors */
  panel_action_t setColors( List *colors );
  panel_action_t setRows( int r );
  int addButtons( List *buttons, WConfigPanelCallBack::add_action_t action );
  int addHotkeys( List *hotkeys, WConfigPanelCallBack::add_action_t action );

    bool escapeInUse() override;
protected:
  void showPathBank();
  void updateBankStr();
    void setMode( int m );

  int banknr, maxbank;
  Button **pbs;
  unsigned int rows;
  Button *nextbb, *prevbb, *newbb, *delbb, *copyb, *swapb, *delb;
  CycleButton *cyb;
  SolidButton *sbsb;
  Text *bnrt, *maxbankt;
  bool extmode;
  int m_mode;
  int selindex;

    bool m_escape_active = false;
};
 
#endif
