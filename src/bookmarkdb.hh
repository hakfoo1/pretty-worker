/* bookmarkdb.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2007-2008,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BOOKMARKDB_HH
#define BOOKMARKDB_HH

#include "wdefines.h"
#include <string>
#include <list>
#include <memory>

class BookmarkDB
{
public:
    BookmarkDB( const std::string &filename );
    ~BookmarkDB();

    void addEntry( const class BookmarkDBEntry &entry );
    void delEntry( const class BookmarkDBEntry &entry );
    void updateEntry( const class BookmarkDBEntry &entry,
                      const class BookmarkDBEntry &newentry );
    std::unique_ptr<class BookmarkDBEntry> getEntry( const std::string &name );

    std::list<std::string> getCats();
    std::list< class BookmarkDBEntry > getEntries( const std::string &cat );
    std::list< std::string > getNamesOfEntries( const std::string &cat );

    int read();
    int write();
private:
    std::list< class BookmarkDBEntry > _entries;
    typedef std::list< class BookmarkDBEntry >::iterator entries_it;

    std::string m_filename;
    
    int match( int token );
};

#endif
