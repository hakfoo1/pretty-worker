/* dirbookmarkui.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DIRBOOKMARKUI_HH
#define DIRBOOKMARKUI_HH

#include "wdefines.h"
#include <memory>
#include <string>
#include "aguix/fieldlistview.h"
#include <future>
#include "existence_test.hh"

class BookmarkDBEntry;
class BookmarkDBProxy;
class Button;
class Text;
class SolidButton;
class AContainer;
class PrefixDB;
class Worker;

class DirBookmarkUI
{
public:
    DirBookmarkUI( Worker &worker, AGUIX &aguix, BookmarkDBProxy &data );
    ~DirBookmarkUI();

    int mainLoop();
    BookmarkDBEntry getSelectedEntry();
    std::string getFilterString();

    void setCurrentDirname( const std::string &dirname );
    void setCurrentBasename( const std::string &basename );
private:
    Worker &m_worker;
    AGUIX &m_aguix;
    BookmarkDBProxy &m_data;
    std::unique_ptr<class BookmarkDBFilter> m_filtered_data;

    std::string m_dirname, m_basename;
    
    std::unique_ptr<class AWindow> m_win;
    FieldListView *m_lv;
    Text *m_infixtext;
    Button *m_okb;
    Button *m_cancelb;
    Button *m_addb;
    Button *m_editb;
    Button *m_delb;
    Button *m_prev_cat_b, *m_next_cat_b;
    SolidButton *m_cat_sb;
    AContainer *m_co1;

    std::unique_ptr<BookmarkDBEntry> m_selected_entry;

    std::list< std::tuple< std::future< ExistenceTest::existence_state_t >, int, bool > > m_existence_tests;
    std::map< std::string, ExistenceTest::existence_state_t > m_existence_results;

    void showData( const std::string &cat );
    BookmarkDBEntry getActiveEntry( const std::string &cat );
    std::string switchCat( const std::string &current_cat, int dir );
    void updateCategoryText( const std::string &current_cat );
    void maximizeWin();

    void highlightBestHit( PrefixDB &pdb );

    void updateLVData( int row, int field,
                       struct FieldListView::on_demand_data &data );

    void checkExistResults();
    void moveTests();
};

#endif
