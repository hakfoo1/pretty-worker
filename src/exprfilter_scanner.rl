/* exprfilter_scanner.rl
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2016 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * to generate output, run
 * ragel exprfilter_scanner.rl -o exprfilter_scanner.cc
 */

#include "exprfilter_scanner.hh"

%%{ machine expr_parser;

date = [0-9]{4} '-' [0-9]{2} "-" [0-9]{2};
word = (any - space - [<=>!~&|()"] - digit) (any - space - [<=>!~&|()])*;
quoted_word = '"' (any - ["])* '"';
number = [0-9]+;

main := |*
     '<' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::LT, s1 ) ); };
     '<=' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::LE, s1 ) ); };
     '=' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::EQ, s1 ) ); };
     '==' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::EQ, s1 ) ); };
     '!=' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::NE, s1 ) ); };
     '>=' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::GE, s1 ) ); };
     '>' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::GT, s1 ) ); };
     '~' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::APPROX, s1 ) ); };
     '&' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::AND, s1 ) ); };
     '|' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::OR, s1 ) ); };
     '&&' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::AND, s1 ) ); };
     '||' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::OR, s1 ) ); };
     '(' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::OPEN_PAR, s1 ) ); };
     ')' => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::CLOSE_PAR, s1 ) ); };
     date => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::DATE, s1 ) ); };
     word => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::WORD, s1 ) ); };
     quoted_word => { if ( te - ts >= 2 ) { std::string s1( ts + 1, te - ts - 2 ); output_tokens.push_back( ExprToken( ExprToken::WORD, s1 ) ); } };
     number => { std::string s1( ts, te - ts ); output_tokens.push_back( ExprToken( ExprToken::NUMBER, s1 ) ); };
 space;
*|;
}%%

%% write data nofinal;

int exprfilter_scan_expr( const std::string &input, std::list< ExprToken > &output_tokens )
{
	const char *p = input.c_str();
	const char *pe = p + input.length();
	const char *eof = pe;
	const char *ts = NULL, *te = NULL;
	int cs = expr_parser_start;
 
	%% write exec;
 
	if (cs == expr_parser_error)
		return -1;

	return 0;
}
