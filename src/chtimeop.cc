/* chtimeop.c
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2015-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "chtimeop.hh"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "datei.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "worker_locale.h"
#include "chtimeorder.hh"
#include "virtualdirmode.hh"

const char *ChTimeOp::name = "ChTimeOp";

ChTimeOp::ChTimeOp() : FunctionProto(),
                       m_onfiles( true ),
                       m_ondirs( false ),
                       m_recursive( false ),
                       m_requestflags( false )
{
    hasConfigure = true;
    m_category = FunctionProto::CAT_FILEOPS;
}

ChTimeOp::~ChTimeOp()
{
}

ChTimeOp* ChTimeOp::duplicate() const
{
    ChTimeOp *ta = new ChTimeOp();

    ta->m_onfiles = m_onfiles;
    ta->m_ondirs = m_ondirs;
    ta->m_recursive = m_recursive;
    ta->m_requestflags = m_requestflags;

    return ta;
}

bool
ChTimeOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char * ChTimeOp::getName()
{
    return name;
}

int ChTimeOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    ListerMode *lm1;

    if ( msg->mode != msg->AM_MODE_DNDACTION ) {
        Lister *l1 = msg->getWorker()->getActiveLister();
        if ( l1 ) {
            startlister = l1;
            lm1 = startlister->getActiveMode();
            if ( lm1 ) {
                if ( dynamic_cast< VirtualDirMode * > ( lm1 ) ) {
                    vdm_chtime( msg );
                } else {
                    lm1->not_supported();
                }
            }
        }
    }

    return 0;
}

bool ChTimeOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;
    fh->configPutPairBool( "onfiles", m_onfiles );
    fh->configPutPairBool( "ondirs", m_ondirs );
    fh->configPutPairBool( "recursive", m_recursive );
    fh->configPutPairBool( "requestflags", m_requestflags );
    return true;
}

const char * ChTimeOp::getDescription()
{
    return catalog.getLocale( 1304 );
}

int ChTimeOp::vdm_chtime( ActionMessage *am )
{
    ListerMode *lm1 = NULL;
    NM_specialsourceExt *specialsource = NULL;
    bool cont = true;
    struct chtimeorder cmorder;
  
    if ( ! startlister ) return 1;

    lm1 = startlister->getActiveMode();
    if ( ! lm1 ) return 1;

    if ( m_requestflags == true ) {
        if ( doconfigure( 1 ) != 0 ) cont = false;
    } else {
        // set values in t* variables
        m_tonfiles = m_onfiles;
        m_tondirs = m_ondirs;
        m_trecursive = m_recursive;
    }
  
    if ( cont ) {
        memset( &cmorder, 0, sizeof( cmorder ) );
        // detect what to copy
        if ( am->mode == am->AM_MODE_ONLYACTIVE ) {
            cmorder.source = cmorder.NM_ONLYACTIVE;
        } else if ( am->mode == am->AM_MODE_DNDACTION ) {
            // insert DND-element into list
            cmorder.source = cmorder.NM_SPECIAL;
            cmorder.sources = new std::list<NM_specialsourceExt*>;
            specialsource = new NM_specialsourceExt( NULL );
            //TODO
            cmorder.sources->push_back( specialsource );
        } else if ( am->mode == am->AM_MODE_SPECIAL ) {
            cmorder.source = cmorder.NM_SPECIAL;
            cmorder.sources = new std::list<NM_specialsourceExt*>;
            specialsource = new NM_specialsourceExt( am->getFE() );
            cmorder.sources->push_back( specialsource );
        } else {
            cmorder.source = cmorder.NM_ALLENTRIES;
        }

        cmorder.onfiles = m_tonfiles;
        cmorder.ondirs = m_tondirs;
        cmorder.recursive = m_trecursive;

        if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
            vdm->chtimef( &cmorder );
        }

        if ( cmorder.source == cmorder.NM_SPECIAL ) {
            delete specialsource;
            delete cmorder.sources;
        }
    }

    return 0;
}

int ChTimeOp::configure()
{
    return doconfigure( 0 );
}

int ChTimeOp::doconfigure( int mode )
{
    AGUIX *aguix = Worker::getAGUIX();
    AWindow *win;
    ChooseButton *ofcb, *odcb, *rfcb = NULL, *rcb;
    AGMessage *msg;
    int endmode = -1;
    char *tstr;

    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) +
                              strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ),
             getDescription() );
    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe(tstr);
  
    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 6 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    ac1->add( new Text( aguix, 0, 0, getDescription() ), 0, 0, AContainer::CO_INCWNR );

    ofcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( m_onfiles == true ) ? 1 : 0,
                                                      catalog.getLocale( 295 ), LABEL_RIGHT, 0 ), 0, 1, AContainer::CO_INCWNR );

    odcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( m_ondirs == true ) ? 1 : 0,
                                                      catalog.getLocale( 296 ), LABEL_RIGHT, 0 ), 0, 2, AContainer::CO_INCWNR );

    rcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( m_recursive == true ) ? 1 : 0,
                                                     catalog.getLocale( 297 ), LABEL_RIGHT, 0 ), 0, 3, AContainer::CO_INCWNR );

    if ( mode == 0 ) {
        rfcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, 20, 20, ( m_requestflags == true ) ? 1 : 0,
                                                          catalog.getLocale( 294 ), LABEL_RIGHT, 0 ), 0, 4, AContainer::CO_INCWNR );
    }

    AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 5 );
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( -1 );
    ac1_1->setBorderWidth( 0 );

    Button *okb = (Button*)ac1_1->add( new Button( aguix,
                                                   0,
                                                   0,
                                                   catalog.getLocale( 11 ),
                                                   0 ), 0, 0, AContainer::CO_FIX );
    Button *cb = (Button*)ac1_1->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 8 ),
                                                  0 ), 1, 0, AContainer::CO_FIX );
  
    okb->takeFocus();
    win->setDoTabCycling( true );
    win->contMaximize( true );
    win->show();

    for ( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cb ) endmode = 1;
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_1:
                                ofcb->setState( ( ofcb->getState() == true ) ? false : true );
                                break;
                            case XK_2:
                                odcb->setState( ( odcb->getState() == true ) ? false : true );
                                break;
                            case XK_3:
                                rcb->setState( ( rcb->getState() == true ) ? false : true );
                                break;
                            case XK_Return:
                                if ( cb->getHasFocus() == false )
                                    endmode = 0;
                                break;
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
  
    if ( endmode == 0 ) {
        // ok
        if ( mode == 1 ) {
            m_tonfiles = ofcb->getState();
            m_tondirs = odcb->getState();
            m_trecursive = rcb->getState();
        } else {
            m_onfiles = ofcb->getState();
            m_ondirs = odcb->getState();
            m_recursive = rcb->getState();
            m_requestflags = rfcb->getState();
        }
    }
  
    delete win;

    return endmode;
}

void ChTimeOp::setOnFiles( bool nv )
{
    m_onfiles = nv;
}

void ChTimeOp::setOnDirs( bool nv )
{
    m_ondirs = nv;
}

void ChTimeOp::setRecursive( bool nv )
{
    m_recursive = nv;
}

void ChTimeOp::setRequestFlags( bool nv )
{
    m_requestflags = nv;
}

bool ChTimeOp::isInteractiveRun() const
{
    return true;
}

void ChTimeOp::setInteractiveRun()
{
    m_requestflags = true;
}
