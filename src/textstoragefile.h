/* textstoragefile.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2005-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TEXTSTORAGEFILE_H
#define TEXTSTORAGEFILE_H

#include "aguix/textstorage.h"

class TextStorageFile : public TextStorage
{
 public:
  TextStorageFile( const std::string &filename,
                   const RefCount<AWidth> &lencalc,
                   size_t initial_size,
                   enum TextStorageString::text_storage_converion convert );
  int getNrOfLines() const;
  int getLine( int line_nr, unsigned int offset, int len, std::string &return_line ) const;
  int getLine( int line_nr, unsigned int offset, std::string &return_line ) const;
  int getMaxLineWidth() const;
  int getLineWidth( int line_nr ) const;
  void setLineLimit( int new_limit );
  int getLineLimit();
  std::pair<int,int> getRealLinePair( int line_nr ) const;
  int findLineNr( std::pair<int,int> real_line ) const;
  AWidth &getAWidth();
  void setAWidth( const RefCount<AWidth> &lencalc ) override;

  int getNrOfUnwrappedLines() const;
  int getUnwrappedLine( int line_nr,
                        unsigned int offset,
                        int len,
                        std::string &return_line ) const;
  int getUnwrappedLine( int line_nr,
                        unsigned int offset,
                        std::string &return_line ) const;
  int getUnwrappedLineRaw( int line_nr,
                           unsigned int offset,
                           int len,
                           std::string &return_line ) const;
  int getUnwrappedLineRaw( int line_nr,
                           unsigned int offset,
                           std::string &return_line ) const;
  std::pair< std::pair< int, int >, int > getLineForOffset( int unwrapped_line_nr, int line_offset ) const;

  void readMore( size_t size = 0 );
  void reloadFile();
  bool incompleteFile();

    void setFileName( const std::string &filename, size_t initial_size = 0 );
    size_t getCurrentSize() const;

    void setConvertMode( enum TextStorageString::text_storage_converion convert );
private:
  TextStorageString m_tss;

  std::string m_filename;
  size_t m_current_size;
  RefCount<AWidth> m_lencalc;

  bool m_incomplete_file;
  ssize_t m_file_size;
  enum TextStorageString::text_storage_converion m_convert;

  void readFile();
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
