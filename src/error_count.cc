/* error_count.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "error_count.hh"
#include <map>
#include <atomic>

namespace ErrorCount {

    // at the moment I only account those specific errors that I
    // actually evaluate
    static std::atomic< size_t > enomem_count{0};

    void account_errno( int err )
    {
        if ( err == ENOMEM ) {
            enomem_count++;
        }
    }

    size_t errno_count( int err )
    {
        if ( err == ENOMEM ) return enomem_count;

        return 0;
    }

}
