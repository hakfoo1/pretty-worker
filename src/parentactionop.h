/* parentactionop.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2004-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: parentactionop.h,v 1.3 2005/10/19 18:03:11 ralf Exp $ */

#ifndef PARENTACTIONOP_H
#define PARENTACTIONOP_H

#include "wdefines.h"
#include "functionproto.h"

class Worker;

class ParentActionOp:public FunctionProto
{
public:
  ParentActionOp();
  virtual ~ParentActionOp();
  ParentActionOp( const ParentActionOp &other );
  ParentActionOp &operator=( const ParentActionOp &other );

  ParentActionOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  const char *getDescription() override;

  static const char *name;
protected:
  // Infos to save

  // temp variables
};

#endif
