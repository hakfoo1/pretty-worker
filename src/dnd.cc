/* dnd.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dnd.h"
#include "lister.h"
#include "listermode.h"
#include "virtualdirmode.hh"
#include "nmrowdata.h"
#include "worker.h"
#include "fileentry.hh"
#include "aguix/fieldlvrowdata.h"

DNDMsg::DNDMsg(Worker *tworker)
{
  this->worker=tworker;

  sl=el=NULL;
  slm=elm=NULL;

  rowDataP = NULL;
  external_data = NULL;
  value = 0;

  source_widget = NULL;
  dest_widget = NULL;
}

const FileEntry *DNDMsg::getFE() const
{
    NMRowData *rdp = dynamic_cast< NMRowData *>( rowDataP );
    if ( rdp ) {
        return rdp->getFE();
    }
    return NULL;
}

std::string DNDMsg::getDestDir() const
{
  if(elm!=NULL) {
    if ( auto vdm = dynamic_cast< VirtualDirMode *>( elm ) ) {
        return vdm->getCurrentDirectory();
    }
  }
  return NULL;
}

Lister *DNDMsg::getDestLister() const
{
  return el;
}

ListerMode *DNDMsg::getDestMode() const
{
  return elm;
}

Lister *DNDMsg::getSourceLister() const
{
  return sl;
}

ListerMode *DNDMsg::getSourceMode() const
{
  return slm;
}

void DNDMsg::setEnd(AGDNDEND*e)
{
    Lister *l1, *l2;
    ListerMode *lm1 = NULL, *lm2 = NULL;
    Widget *wel;

    delete rowDataP;
    rowDataP = NULL;
    if ( e->specialinfo.rowDataP != NULL ) {
        rowDataP = e->specialinfo.rowDataP->duplicate();
    }

    value = e->specialinfo.value;

    delete external_data;
    external_data = NULL;
    if ( e->specialinfo.external_data ) {
        external_data = new std::string( *e->specialinfo.external_data );
    }

    sl = NULL;
    slm = NULL;
    el = NULL;
    elm = NULL;
  
    l1 = worker->getLister( 0 );
    lm1 = l1->getActiveMode();
    l2 = worker->getLister( 1 );
    lm2 = l2->getActiveMode();

    wel = e->source_element;
    source_widget = wel;
    if ( wel ) {
        if ( lm1 != NULL ) {
            if ( lm1->isyours( wel ) == true ) {
                slm = lm1;
                sl = l1;
            }
        }
        if ( sl == NULL && lm2 != NULL ) {
            if ( lm2->isyours( wel ) == true ) {
                slm = lm2;
                sl = l2;
            }
        }
    }

    wel = e->target_element;
    dest_widget = wel;
    if ( wel != NULL ) {
        if ( lm1 != NULL ) {
            if ( lm1->isyours( wel ) == true ) {
                elm = lm1;
                el = l1;
            }
        }
        if ( el == NULL && lm2 != NULL ) {
            if ( lm2->isyours( wel ) == true ) {
                elm = lm2;
                el = l2;
            }
        }
    } else if ( e->target_awindow != NULL ) {
        if ( l1 != NULL ) {
            if ( l1->getAWindow() == e->target_awindow ) {
                el = l1;
            }
        }
        if ( el == NULL ) {
            if ( l2 != NULL ) {
                if ( l2->getAWindow() == e->target_awindow ) {
                    el = l2;
                }
            }
        }
    }
}

DNDMsg::~DNDMsg()
{
    delete rowDataP;
    delete external_data;
}

int DNDMsg::getValue() const
{
    return value;
}

FieldLVRowData *DNDMsg::getRowDataP() const
{
    return rowDataP;
}

std::string *DNDMsg::getExternalData() const
{
    return external_data;
}

Widget *DNDMsg::getSourceWidget() const
{
    return source_widget;
}

Widget *DNDMsg::getDestWidget() const
{
    return dest_widget;
}
