/* clipboardop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "clipboardop.hh"
#include "listermode.h"
#include "worker.h"
#include "ownop.h"
#include "wpucontext.h"
#include "execlass.h"
#include "fileviewer.hh"
#include "nmspecialsourceext.hh"
#include "fileentry.hh"
#include "worker_locale.h"
#include "datei.h"
#include "aguix/cyclebutton.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "aguix/stringgadget.h"
#include "aguix/awindow.h"
#include "dnd.h"

const char *ClipboardOp::name = "ClipboardOp";

ClipboardOp::ClipboardOp() : FunctionProto()
{
    m_string = "";
    m_category = FunctionProto::CAT_OTHER;

    hasConfigure = true;
}

ClipboardOp::~ClipboardOp()
{
}

ClipboardOp*
ClipboardOp::duplicate() const
{
    ClipboardOp *ta = new ClipboardOp();
    ta->setClipboardString( m_string );
    return ta;
}

bool
ClipboardOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *
ClipboardOp::getName()
{
    return name;
}

int
ClipboardOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    std::string res_str;
    if ( wpu->parse( m_string.c_str(),
                     res_str,
                     a_max( EXE_STRING_LEN - 1024, 256 ),
                     false, WPUContext::PERSIST_FLAGS ) == WPUContext::PARSE_SUCCESS ) {
        Worker::getAGUIX()->copyToClipboard( res_str, AGUIX::BOTH_BUFFERS );
    }

    return 0;
}

bool
ClipboardOp::save(Datei *fh)
{
    if ( fh == NULL ) return false;
    fh->configPutPairString( "clipboardstring", m_string.c_str() );
    return true;
}

const char *
ClipboardOp::getDescription()
{
    return catalog.getLocale( 1302 );
}

int
ClipboardOp::configure()
{
    return doconfigure(0);
}

int
ClipboardOp::doconfigure(int mode)
{
    AGUIX *aguix = Worker::getAGUIX();
    Button *okb, *cancelb, *flagb;
    AWindow *win;
    StringGadget *sgcf;
    AGMessage *msg;

    int endmode = -1;
    char *tstr;
    const int cincw = AContainer::ACONT_MINH +
        AContainer::ACONT_MINW +
        AContainer::ACONT_MAXH;
    const int cfix = AContainer::ACONT_MINH +
        AContainer::ACONT_MINW +
        AContainer::ACONT_MAXH +
        AContainer::ACONT_MAXW;
  
    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe( tstr );

    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    AContainer *ac1_3 = ac1->add( new AContainer( win, 3, 1 ), 0, 0 );
    ac1_3->setMinSpace( 5 );
    ac1_3->setMaxSpace( 5 );
    ac1_3->setBorderWidth( 0 );
    ac1_3->add( new Text( aguix, 0, 0, catalog.getLocale( 979 ) ), 0, 0, cfix );
    sgcf = (StringGadget*)ac1_3->add( new StringGadget( aguix, 0, 0, 100, m_string.c_str(), 0 ),
                                      1, 0, cincw );
    flagb = (Button*)ac1_3->add( new Button( aguix,
                                             0,
                                             0,
                                             "F",
                                             0 ), 2, 0, cfix );
  
    AContainer *ac1_5 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
    ac1_5->setMinSpace( 5 );
    ac1_5->setMaxSpace( -1 );
    ac1_5->setBorderWidth( 0 );
    okb = (Button*)ac1_5->add( new Button( aguix,
                                           0,
                                           0,
                                           catalog.getLocale( 11 ),
                                           0 ), 0, 0, cfix );
    cancelb = (Button*)ac1_5->add( new Button( aguix,
                                               0,
                                               0,
                                               catalog.getLocale( 8 ),
                                               0 ), 1, 0, cfix );
    win->contMaximize( true );

    win->setDoTabCycling( true );
    win->show();
    for( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cancelb ) endmode = 1;
                    else if ( msg->button.button == flagb ) {
                        tstr = OwnOp::getFlag();
                        if ( tstr != NULL ) {
                            sgcf->insertAtCursor( tstr );
                            _freesafe( tstr );
                        }
                    }
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Return:
                                if ( cancelb->getHasFocus() == true ) {
                                    endmode = 1;
                                } else {
                                    endmode=0;
                                }
                                break;
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
  
    if ( endmode == 0 ) {
        // ok
        setClipboardString( sgcf->getText() );
    }
    delete win;

    return endmode;
}

void ClipboardOp::setClipboardString( const std::string &str )
{
    m_string = str;
}
