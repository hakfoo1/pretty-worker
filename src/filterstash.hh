/* filterstash.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILTERSTASH_H
#define FILTERSTASH_H

#include "wdefines.h"
#include "nmfilter.hh"

#include <list>
#include <string>

class ListerMode;

class FilterStash {
public:
    FilterStash( const std::string &file_name );

    int stash( ListerMode *lm );
    int applyTop( ListerMode *lm );
    int applyPop( ListerMode *lm );
    int applyEntry( ListerMode *lm, size_t pos );
    int deleteEntry( size_t pos );

    std::list< std::list< NM_Filter > > getStash();
private:
    int write();
    int read();
    
    std::list< std::list< NM_Filter > > m_stash;
    std::string m_file_name;
    std::string m_lock_file;
    time_t m_lastmod = 0;
    loff_t m_lastsize = 0;
    bool m_dirty = false;
};

#endif /* FILTERSTASH_H */
