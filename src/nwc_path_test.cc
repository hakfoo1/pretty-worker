#include <check.h>
#include <stdlib.h>
#include "nwc_path.hh"

START_TEST(prefix_test)
{
    ck_assert( NWC::Path::is_prefix_dir( "/a/b/c/", "/a/b" ) == false );
    ck_assert( NWC::Path::is_prefix_dir( "/a/b/", "/a/b/c" ) == true );
    ck_assert( NWC::Path::is_prefix_dir( "/a/b", "/a/b/c" ) == true );
    ck_assert( NWC::Path::is_prefix_dir( "/a/bc", "/a/b/c" ) == false );
    ck_assert( NWC::Path::is_prefix_dir( "/a/b/c", "/a/bc" ) == false );
    ck_assert( NWC::Path::is_prefix_dir( "/a/b", "/a/bc/c" ) == false );
    ck_assert( NWC::Path::is_prefix_dir( "/a/bc/c", "/a/b" ) == false );
}
END_TEST

START_TEST(dirname)
{
    ck_assert( NWC::Path::dirname( "" ) == "" );
    ck_assert( NWC::Path::dirname( "/" ) == "/" );
    ck_assert( NWC::Path::dirname( "/x" ) == "/" );
    ck_assert( NWC::Path::dirname( "/x/" ) == "/x" );
    ck_assert( NWC::Path::dirname( "/x/y" ) == "/x" );
    ck_assert( NWC::Path::dirname( "x/y/z" ) == "x/y" );
    ck_assert( NWC::Path::dirname( "x" ) == "" );
}
END_TEST

START_TEST(join)
{
    ck_assert( NWC::Path::join( "", "x" ) == "x" );
    ck_assert( NWC::Path::join( "/", "x" ) == "/x" );
    ck_assert( NWC::Path::join( "x", "y" ) == "x/y" );
    ck_assert( NWC::Path::join( "/x", "y" ) == "/x/y" );
    ck_assert( NWC::Path::join( "/x", "/y" ) == "/y" );
    ck_assert( NWC::Path::join( "/x", "" ) == "/x/" );
}
END_TEST

Suite * prefix_suite( void )
{
    Suite *s;
    TCase *tc_prefix1;

    s = suite_create("NWC::Path");

    /* Core test case */
    tc_prefix1 = tcase_create( "prefix1" );

    tcase_add_test( tc_prefix1, prefix_test );
    tcase_add_test( tc_prefix1, dirname );
    tcase_add_test( tc_prefix1, join );
    suite_add_tcase( s, tc_prefix1 );

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = prefix_suite();
    sr = srunner_create( s );

    srunner_run_all( sr, CK_NORMAL );
    number_failed = srunner_ntests_failed( sr );
    srunner_free( sr );
    return ( number_failed == 0 ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
