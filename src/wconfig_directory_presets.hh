/* wconfig_directory_presets.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_DIRECTORY_PRESETS_HH
#define WCONFIG_DIRECTORY_PRESETS_HH

#include "wdefines.h"
#include "wconfig_panel.hh"
#include "wconfig_types.hh"

#include <map>

class WConfig;
class AContainerBB;

class DirectoryPresetsPanel : public WConfigPanel
{
public:
    DirectoryPresetsPanel( AWindow &basewin, WConfig &baseconfig );
    ~DirectoryPresetsPanel();
    int create();
    int saveValues();

    void run( Widget *, const AGMessage &msg ) override;
protected:
    FieldListView *m_lv = NULL;
    Button *m_addb = NULL, *m_delb = NULL;
    ChooseButton *m_reverse_sort_chb = NULL;
    CycleButton *m_sort_cyb = NULL;
    CycleButton *m_dirmode_cyb = NULL;
    AContainerBB *m_sort_cont = NULL;
    AContainerBB *m_filter_cont = NULL;
    ChooseButton *m_show_hidden_cb = NULL;
    FieldListView *m_filter_lv = NULL;
    Button *m_newfilter_b = NULL;
    Button *m_delfilter_b = NULL;
    Button *m_editfilter_b = NULL;

    std::map< std::string, struct directory_presets > m_presets;

    void updateView( const std::string &path );
    void setLVC4Filter( FieldListView *filv, int row, NM_Filter &fi );
    int configureFilter( NM_Filter &fi );
};
 
#endif
