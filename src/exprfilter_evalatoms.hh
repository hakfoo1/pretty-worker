/* exprfilter_evalatoms.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2016 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef EXPRFILTER_EVALATOMS_HH
#define EXPRFILTER_EVALATOMS_HH

#include "wdefines.h"
#include <string>
#include "stringmatcher_fnmatch.hh"

class ExprFilterEval;
class NWCEntrySelectionState;

class ExprFilterEvalAtom
{
public:
    virtual ~ExprFilterEvalAtom() {}
    virtual void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm ) = 0;
};

class ExprAtomCmp : public ExprFilterEvalAtom
{
public:
    typedef enum {
        EAC_LT,
        EAC_LE,
        EAC_EQ,
        EAC_NE,
        EAC_GE,
        EAC_GT,
        EAC_APPROX,
    } eval_atom_cmp_t;
    
    ExprAtomCmp( eval_atom_cmp_t cmp_type ) : m_cmp_type( cmp_type ) {}
    ExprAtomCmp() = delete;
protected:
    eval_atom_cmp_t m_cmp_type;
};

class ExprAtomStringCmp : public ExprAtomCmp
{
public:
    ExprAtomStringCmp( const std::string &val,
                       eval_atom_cmp_t cmp_type );
protected:
    bool match( const std::string &val );
private:
    std::string m_val;
    StringMatcherFNMatch m_matcher;
};

class ExprAtomNameCmp : public ExprAtomStringCmp
{
public:
    ExprAtomNameCmp( const std::string &val,
                     eval_atom_cmp_t cmp_type ) : ExprAtomStringCmp( val, cmp_type )
    {}

    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
};

class ExprAtomTypeCmp : public ExprAtomStringCmp
{
public:
    ExprAtomTypeCmp( const std::string &val,
                     eval_atom_cmp_t cmp_type ) : ExprAtomStringCmp( val, cmp_type )
    {}

    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
};

class ExprAtomSizeCmp : public ExprAtomCmp
{
public:
    ExprAtomSizeCmp( loff_t size,
                     eval_atom_cmp_t cmp_type ) : ExprAtomCmp( cmp_type ),
                                                  m_size( size ) {}

    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
private:
    loff_t m_size;
};

class ExprAtomAnd : public ExprFilterEvalAtom
{
public:
    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
};

class ExprAtomOr : public ExprFilterEvalAtom
{
public:
    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
};

class ExprAtomStateCmp : public ExprAtomCmp
{
public:
    ExprAtomStateCmp( const std::string &val,
                      eval_atom_cmp_t cmp_type ) : ExprAtomCmp( cmp_type ),
                                                   m_val( val )
    {}

    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
private:
    std::string m_val;
};

class ExprAtomTimeCmp : public ExprAtomCmp
{
public:
    typedef enum {
        COMPARE_MOD,
        COMPARE_ACCESS
    } time_cmp_t;

    ExprAtomTimeCmp( time_t val,
                     bool relative_to_now,
                     time_cmp_t time_cmp_type,
                     eval_atom_cmp_t cmp_type ) : ExprAtomCmp( cmp_type ),
                                                  m_time_stamp( val ),
                                                  m_relative_to_now( relative_to_now ),
                                                  m_time_cmp_type( time_cmp_type )
    {}

    void eval( const NWCEntrySelectionState &element, ExprFilterEval &vm );
private:
    time_t m_time_stamp;
    bool m_relative_to_now;
    time_cmp_t m_time_cmp_type;
};

#endif
