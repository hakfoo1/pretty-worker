/* chownop.c
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "chownop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "datei.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "worker_locale.h"
#include "chownorder.hh"
#include "virtualdirmode.hh"

const char *ChOwnOp::name = "ChOwnOp";

ChOwnOp::ChOwnOp() : FunctionProto()
{
  onfiles = true;
  ondirs = false;
  recursive = false;
  hasConfigure = true;
    m_category = FunctionProto::CAT_FILEOPS;

    setRequestParameters( true );
}

ChOwnOp::~ChOwnOp()
{
}

ChOwnOp*
ChOwnOp::duplicate() const
{
  ChOwnOp *ta = new ChOwnOp();
  ta->onfiles = onfiles;
  ta->ondirs = ondirs;
  ta->recursive = recursive;
  ta->setRequestParameters( requestParameters() );
  return ta;
}

bool
ChOwnOp::isName( const char *str )
{
  if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *
ChOwnOp::getName()
{
  return name;
}

int
ChOwnOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;

  if ( msg->mode != msg->AM_MODE_DNDACTION ) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if ( l1 != NULL ) {
      startlister = l1;
      lm1 = startlister->getActiveMode();
      if ( lm1 != NULL ) {
          if ( dynamic_cast< VirtualDirMode * > ( lm1 ) ) {
              normalmodechown( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

bool
ChOwnOp::save( Datei *fh )
{
  if ( fh == NULL ) return false;
  fh->configPutPairBool( "onfiles", onfiles );
  fh->configPutPairBool( "ondirs", ondirs );
  fh->configPutPairBool( "recursive", recursive );
  fh->configPutPairBool( "requestflags", requestParameters() );
  return true;
}

const char *
ChOwnOp::getDescription()
{
  return catalog.getLocale( 1284 );
}

int
ChOwnOp::normalmodechown( ActionMessage *am )
{
  ListerMode *lm1 = NULL;
  NM_specialsourceExt *specialsource = NULL;
  bool cont = true;
  struct NM_chownorder coorder;
  
  if ( startlister == NULL ) return 1;
  lm1 = startlister->getActiveMode();
  if ( lm1 == NULL ) return 1;

  if ( requestParameters() == true ) {
    if ( doconfigure( 1 ) != 0 ) cont = false;
  } else {
    // set values in t* variables
    tonfiles = onfiles;
    tondirs = ondirs;
    trecursive = recursive;
  }
  
  if ( cont == true ) {
    memset( &coorder, 0, sizeof( coorder ) );
    if ( am->mode == am->AM_MODE_ONLYACTIVE )
      coorder.source = coorder.NM_ONLYACTIVE;
    else if ( am->mode == am->AM_MODE_DNDACTION ) {
      // insert DND-element into list
      coorder.source = coorder.NM_SPECIAL;
      coorder.sources = new std::list<NM_specialsourceExt*>;
      specialsource = new NM_specialsourceExt( NULL );
      //TODO: specialsource nach am besetzen (je nachdem wir ich das realisiere)
      coorder.sources->push_back( specialsource );
    } else if ( am->mode == am->AM_MODE_SPECIAL ) {
      coorder.source = coorder.NM_SPECIAL;
      coorder.sources = new std::list<NM_specialsourceExt*>;
      specialsource = new NM_specialsourceExt( am->getFE() );
      coorder.sources->push_back( specialsource );
    } else
      coorder.source = coorder.NM_ALLENTRIES;

    coorder.onfiles = tonfiles;
    coorder.ondirs = tondirs;
    coorder.recursive = trecursive;

    if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
        vdm->chownf( &coorder );
    }

    if ( coorder.source == coorder.NM_SPECIAL ) {
      if ( specialsource != NULL ) delete specialsource;
      delete coorder.sources;
    }
  }
  return 0;
}

int
ChOwnOp::configure()
{
  return doconfigure( 0 );
}

int
ChOwnOp::doconfigure( int mode )
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  ChooseButton *ofcb, *odcb, *rfcb = NULL, *rcb;
  AGMessage *msg;
  int endmode = -1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getDescription() ) + 1 );
  sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe( tstr );

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 6 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  ac1->add( new Text( aguix, 0, 0, getDescription() ), 0, 0, cincwnr );

  ofcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, ( onfiles == true ) ? 1 : 0,
                                                    catalog.getLocale( 295 ), LABEL_RIGHT, 0 ), 0, 1, cincwnr );

  odcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, ( ondirs == true ) ? 1 : 0,
                                                    catalog.getLocale( 296 ), LABEL_RIGHT, 0 ), 0, 2, cincwnr );

  rcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, ( recursive == true ) ? 1 : 0,
                                                   catalog.getLocale( 297 ), LABEL_RIGHT, 0 ), 0, 3, cincwnr );

  if ( mode == 0 ) {
    rfcb = (ChooseButton*)ac1->add( new ChooseButton( aguix, 0, 0, ( requestParameters() == true ) ? 1 : 0,
                                                      catalog.getLocale( 294 ), LABEL_RIGHT, 0 ), 0, 4, cincwnr );
  }

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 5 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( -1 );
  ac1_1->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_1->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_1->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );

  okb->takeFocus();
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for( ; endmode == -1; ) {
    msg = aguix->WaitMessage( win );
    if ( msg != NULL ) {
      switch ( msg->type ) {
        case AG_CLOSEWINDOW:
          if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
          break;
        case AG_BUTTONCLICKED:
          if ( msg->button.button == okb ) endmode = 0;
          else if ( msg->button.button == cb ) endmode = 1;
          break;
        case AG_KEYPRESSED:
          if ( win->isParent( msg->key.window, false ) == true ) {
            switch ( msg->key.key ) {
              case XK_1:
                ofcb->setState( ( ofcb->getState() == true ) ? false : true );
                break;
              case XK_2:
                odcb->setState( ( odcb->getState() == true ) ? false : true );
                break;
              case XK_3:
                rcb->setState( ( rcb->getState() == true ) ? false : true );
                break;
              case XK_Return:
                if ( cb->getHasFocus() == false ) {
                  endmode = 0;
                }
                break;
              case XK_Escape:
                endmode = 1;
                break;
            }
          }
          break;
      }
      aguix->ReplyMessage( msg );
    }
  }
  
  if ( endmode == 0 ) {
    // ok
    if ( mode == 1 ) {
      tonfiles = ofcb->getState();
      tondirs = odcb->getState();
      trecursive = rcb->getState();
    } else {
      onfiles = ofcb->getState();
      ondirs = odcb->getState();
      recursive = rcb->getState();
      setRequestParameters( rfcb->getState() );
    }
  }
  
  delete win;

  return endmode;
}

void ChOwnOp::setOnFiles( bool nv )
{
  onfiles = nv;
}

void ChOwnOp::setOnDirs( bool nv )
{
  ondirs = nv;
}

void ChOwnOp::setRecursive( bool nv )
{
  recursive = nv;
}

bool ChOwnOp::isInteractiveRun() const
{
    return true;
}

void ChOwnOp::setInteractiveRun()
{
    setRequestParameters( true );
}
