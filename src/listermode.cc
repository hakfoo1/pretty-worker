/* listermode.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "listermode.h"
#include "datei.h"
#include "execlass.h"
#include "aguix/lowlevelfunc.h"
#include "aguix/util.h"
#include "aguix/request.h"
#include "worker_locale.h"
#include "worker.h"
#include "argclass.hh"

const char *ListerMode::type="ListerMode";

ListerMode::ListerMode(Lister *parent)
{
  this->parentlister=parent;
  this->parentawindow=parent->getAWindow();
  aguix=parent->getAGUIX();
}

ListerMode::~ListerMode()
{
}

void ListerMode::messageHandler(AGMessage *)
{
}

void ListerMode::messageHandlerInactive(AGMessage *)
{
}

void ListerMode::on()
{
  parentlister->setActiveMode(this);
}

void ListerMode::off()
{
  parentlister->setActiveMode(NULL);
}

bool ListerMode::isType(const char *str)
{
  if(strcmp(str,type)==0) return true; else return false;
}

const char *ListerMode::getType()
{
  return type;
}

void ListerMode::activate()
{
}

void ListerMode::deactivate()
{
}

int ListerMode::configure()
{
  //TODO
  return 0;
}

void ListerMode::cyclicfunc(cyclicfunc_mode_t mode)
{
}

void ListerMode::cyclicfuncInactive( cyclicfunc_mode_t mode )
{
}

const char *ListerMode::getLocaleName()
{
  return "";
}

int ListerMode::load()
{
  return 0;
}

bool ListerMode::save(Datei *fh)
{
  return false;
}

void ListerMode::relayout()
{
}

bool ListerMode::startdnd(DNDMsg *dm)
{
  return false;
}

bool ListerMode::isyours( Widget *elem )
{
  return false;
}

void ListerMode::lvbDoubleClicked()
{
}

void ListerMode::runCommand( const std::string &command )
{
    if ( m_commands.count( command ) > 0 &&
         m_commands[command] != NULL ) {
        m_commands[command]();
    } else {
        std::string text = AGUIXUtils::formatStringToString( catalog.getLocale( 993 ),
                                                             command.c_str() );

        parentlister->getWorker()->getRequester()->request( catalog.getLocale( 123 ),
                                                            text.c_str(),
                                                            catalog.getLocale( 11 ) );
    }
}

void ListerMode::registerCommand( const std::string &command,
                                  std::function< void (void) > callback,
                                  const std::string &description,
                                  FunctionProto::command_categories_t category )
{
    m_commands[ command ] = callback;

    if ( ! description.empty() &&
         m_command_info.count( std::make_pair( command, false ) ) == 0 ) {
        m_command_info[ std::make_pair( command, false ) ] = std::make_tuple( description, category, nullptr );
    }
}

void ListerMode::runCommand( const std::string &command, const std::list< RefCount< ArgClass > > &args )
{
    if ( m_commands_with_args.count( command ) > 0 &&
         m_commands_with_args[command] != NULL ) {
        m_commands_with_args[command]( args );
    } else {
        std::string text = AGUIXUtils::formatStringToString( catalog.getLocale( 993 ),
                                                             command.c_str() );

        parentlister->getWorker()->getRequester()->request( catalog.getLocale( 123 ),
                                                            text.c_str(),
                                                            catalog.getLocale( 11 ) );
    }
}

void ListerMode::registerCommand( const std::string &command,
                                  const std::function< void ( const std::list< RefCount< ArgClass > > ) > callback,
                                  const std::string &description,
                                  FunctionProto::command_categories_t category,
                                  const std::function< std::string ( const std::string &current_input ) > help_callback )
{
    m_commands_with_args[command] = callback;

    if ( ! description.empty() &&
         m_command_info.count( std::make_pair( command, true ) ) == 0 ) {
        m_command_info[ std::make_pair( command, true ) ] = std::make_tuple( description, category, help_callback );
    }
}

void ListerMode::runCommandWithStrArgs( const std::string &command_str )
{
    char **args = NULL;
    int n;
    std::list< RefCount< ArgClass > > string_args;
    std::string command;

    n = Worker_buildArgvList( command_str.c_str(), &args );
    
    if ( n > 0 ) {
        for ( int i = 0; i < n; i++ ) {
            char *tstr1 = AGUIX_unquoteString( args[i] );

            if ( tstr1 ) {
                if ( i == 0 ) {
                    command = tstr1;
                } else {
                    string_args.push_back( RefCount< ArgClass >( new StringArg( tstr1 ) ) );
                }

                _freesafe( tstr1 );
            }
        }

        if ( ! command.empty() ) {
            if ( m_commands.count( command ) == 0 &&
                 m_commands_with_args.count( command ) > 0 &&
                 string_args.empty() ) {
                std::string text = AGUIXUtils::formatStringToString( catalog.getLocale( 1390 ),
                                                                     command.c_str() );

                parentlister->getWorker()->getRequester()->request( catalog.getLocale( 123 ),
                                                                    text.c_str(),
                                                                    catalog.getLocale( 11 ) );
            } else if ( string_args.size() > 0 &&
                        m_commands_with_args.count( command ) > 0 ) {
                runCommand( command, string_args );
            } else {
                runCommand( command );
            }
        }
    }

    Worker_freeArgvList( args );
}

bool ListerMode::pathsChanged( const std::set< std::string > changed_paths )
{
    return false;
}

int ListerMode::getSelFiles( std::list< NM_specialsourceExt > &return_list,
                             lm_getfiles_t get_mode,
                             bool unselect )
{
    return 0;
}

std::string ListerMode::getCurrentDirectory()
{
    return "";
}

bool ListerMode::setEntrySelectionState( const std::string &filename, bool state )
{
    return false;
}

void ListerMode::updateOnBookmarkChange()
{
}

void ListerMode::not_supported()
{
    parentlister->getWorker()->getRequester()->request( catalog.getLocale( 123 ),
                                                        catalog.getLocale( 1000 ),
                                                        catalog.getLocale( 11 ) );
}

std::list< WorkerTypes::reg_command_info_t > ListerMode::getListOfCommandsWithoutArgs() const
{
    std::list< WorkerTypes::reg_command_info_t > res;

    for ( auto &e : m_commands ) {
        WorkerTypes::reg_command_info_t t = { .name = e.first,
                                              .description = "",
                                              .category = FunctionProto::CAT_OTHER,
                                              .has_args = false };
        if ( m_command_info.count( std::make_pair( e.first, false ) ) > 0 ) {
            t.description = std::get<0>( m_command_info.at( std::make_pair( e.first, false ) ) );
            t.category = std::get<1>( m_command_info.at( std::make_pair( e.first, false ) ) );
        }

        res.push_back( t );
    }

    return res;
}

std::list< WorkerTypes::reg_command_info_t > ListerMode::getListOfCommandsWithArgs() const
{
    std::list< WorkerTypes::reg_command_info_t > res;

    for ( auto &e : m_commands_with_args ) {
        WorkerTypes::reg_command_info_t t = { .name = e.first,
                                              .description = "",
                                              .category = FunctionProto::CAT_OTHER,
                                              .has_args = true };
        if ( m_command_info.count( std::make_pair( e.first, true ) ) > 0 ) {
            t.description = std::get<0>( m_command_info.at( std::make_pair( e.first, true ) ) );
            t.category = std::get<1>( m_command_info.at( std::make_pair( e.first, true ) ) );
        }

        res.push_back( t );
    }

    return res;
}

WorkerTypes::reg_command_info_t ListerMode::getCommandInfo( const std::string &command, bool args ) const
{
    if ( m_command_info.count( std::make_pair( command, args ) ) == 0 ) return {};

    WorkerTypes::reg_command_info_t res;

    res.name = command;
    res.description = std::get<0>( m_command_info.at( std::make_pair( command, args ) ) );
    res.has_args = args;
    res.category = std::get<1>( m_command_info.at( std::make_pair( command, args ) ) );

    return res;
}

std::string ListerMode::getCommandInputHelp( const std::string &command,
                                             const std::string &current_input ) const
{
    auto f = std::get<2>( m_command_info.at( std::make_pair( command, true ) ) );
    if ( f ) {
        return f( current_input );
    }
    return "";
}

bool ListerMode::commandHasInputHelp( const std::string &command ) const
{
    if ( std::get<2>( m_command_info.at( std::make_pair( command, true ) ) ) ) {
        return true;
    }
    return false;
}

void ListerMode::finalizeBGOps()
{
}

void ListerMode::interpretFinalize()
{
}
