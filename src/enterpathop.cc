/* enterpathop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "enterpathop.h"
#include "listermode.h"
#include "worker.h"
#include "worker_locale.h"
#include "datei.h"
#include "aguix/button.h"
#include "aguix/cyclebutton.h"
#include "aguix/acontainer.h"
#include "aguix/lowlevelfunc.h"
#include "aguix/awindow.h"

const char *EnterPathOp::name="EnterPathOp";

EnterPathOp::EnterPathOp() : FunctionProto()
{
  enterpathside=ENTERPATHOP_THISSIDE;
  hasConfigure = true;
}

EnterPathOp::~EnterPathOp()
{
}

EnterPathOp*
EnterPathOp::duplicate() const
{
  EnterPathOp *ta=new EnterPathOp();
  ta->enterpathside=enterpathside;
  return ta;
}

bool
EnterPathOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
EnterPathOp::getName()
{
  return name;
}

int
EnterPathOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    if ( msg->mode != msg->AM_MODE_DNDACTION ) {
        startlister = msg->getWorker()->getActiveLister();

        normalmodeenterpath( msg );
    }
    return 0;
}

bool
EnterPathOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  switch(enterpathside) {
    case ENTERPATHOP_OTHERSIDE:
      fh->configPutPair( "mode", "other" );
      break;
    case ENTERPATHOP_LEFTSIDE:
      fh->configPutPair( "mode", "left" );
      break;
    case ENTERPATHOP_RIGHTSIDE:
      fh->configPutPair( "mode", "right" );
      break;
    default:
      fh->configPutPair( "mode", "current" );
      break;
  }
  return true;
}

const char *
EnterPathOp::getDescription()
{
  return catalog.getLocale(1275);
}

void
EnterPathOp::normalmodeenterpath( ActionMessage *am )
{
  ListerMode *lm1=NULL;
  Lister *ll,*lr,*tl;
  
  ll = am->getWorker()->getLister(0);
  lr = am->getWorker()->getLister(1);
  
  tl=NULL;
  switch(enterpathside) {
    case ENTERPATHOP_OTHERSIDE:
      if(startlister==ll) tl=lr;
      else tl=ll;
      break;
    case ENTERPATHOP_LEFTSIDE:
      tl=ll;
      break;
    case ENTERPATHOP_RIGHTSIDE:
      tl=lr;
      break;
    default:
      tl=startlister;
      break;
  }
  if(tl!=NULL) {
    lm1=tl->getActiveMode();
    if(lm1!=NULL) {
        lm1->runCommand( "activate_path_input" );
    }
  }
}

int
EnterPathOp::configure()
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  CycleButton *rcyb;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getDescription())+1);
  sprintf(tstr,catalog.getLocale(293),getDescription());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  ac1_1->setBorderWidth( 0 );

  ac1_1->add( new Text( aguix, 0, 0, catalog.getLocale( 323 ) ), 0, 0, cfix );
  rcyb = (CycleButton*)ac1_1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, cincw );
  rcyb->addOption(catalog.getLocale(324));
  rcyb->addOption(catalog.getLocale(325));
  rcyb->addOption(catalog.getLocale(326));
  rcyb->addOption(catalog.getLocale(327));
  rcyb->resize(rcyb->getMaxSize(),rcyb->getHeight());
  switch(enterpathside) {
    case ENTERPATHOP_OTHERSIDE:
      rcyb->setOption(1);
      break;
    case ENTERPATHOP_LEFTSIDE:
      rcyb->setOption(2);
      break;
    case ENTERPATHOP_RIGHTSIDE:
      rcyb->setOption(3);
      break;
    default:
      rcyb->setOption(0);
      break;
  }
  ac1_1->readLimits();
  
  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( -1 );
  ac1_2->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_2->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );

  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
            if ( win->isParent( msg->key.window, false ) == true ) {
                switch ( msg->key.key ) {
                    case XK_Escape:
                        endmode = 1;
                        break;
                }
            }
            break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
    // ok
    switch(rcyb->getSelectedOption()) {
      case 1:
        enterpathside=ENTERPATHOP_OTHERSIDE;
        break;
      case 2:
        enterpathside=ENTERPATHOP_LEFTSIDE;
        break;
      case 3:
        enterpathside=ENTERPATHOP_RIGHTSIDE;
        break;
      default:
        enterpathside=ENTERPATHOP_THISSIDE;
        break;
    }
  }
  
  delete win;

  return endmode;
}

void EnterPathOp::setSide(enterpath_t nv)
{
  enterpathside=nv;
}

