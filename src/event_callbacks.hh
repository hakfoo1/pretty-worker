/* event_callbacks.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EVENT_CALLBACKS_HH
#define EVENT_CALLBACKS_HH

#include "wdefines.h"
#include "generic_callback.hh"
#include <map>
#include <list>
#include "aguix/refcount.hh"

class EventCallbacks
{
public:
    typedef enum {
        TIMEOUT
    } event_callback_type_t;

    int fireCallbacks( const event_callback_type_t type );

    int registerCallback( RefCount< GenericCallback<void> > cb,
                          const event_callback_type_t type );

    int unregisterCallback( RefCount< GenericCallback<void> > cb );

private:
    std::map< event_callback_type_t, std::list< RefCount< GenericCallback<void> > > > m_callbacks;
};

#endif /* EVENT_CALLBACKS_HH */
