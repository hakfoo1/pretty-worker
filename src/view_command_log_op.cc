/* viewcommandlogop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2021-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "view_command_log_op.hh"
#include "worker_locale.h"
#include "worker.h"
#include "aguix/aguix.h"
#include "view_command_log_bg.hh"

const char *ViewCommandLogOp::name = "ViewCommandLogOp";

ViewCommandLogOp::ViewCommandLogOp() : FunctionProto()
{
}

ViewCommandLogOp::~ViewCommandLogOp()
{
}

ViewCommandLogOp *ViewCommandLogOp::duplicate() const
{
    ViewCommandLogOp *ta = new ViewCommandLogOp();
    return ta;
}

bool ViewCommandLogOp::isName(const char *str)
{
    if ( strcmp( str, name ) == 0 ) {
        return true;
    } else {
        return false;
    }
}

const char *ViewCommandLogOp::getName()
{
    return name;
}

int ViewCommandLogOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    auto log = std::make_unique< std::list< FileCommandLog::Entry > >( msg->getWorker()->getCommandLog() );

    RefCount< ViewCommandLogBG > vcl( new ViewCommandLogBG( msg->getWorker(),
                                                            std::move( log ) ) );
    vcl->createWindow();

    if ( vcl->getAWindow() != NULL ) {
        Worker::getAGUIX()->registerBGHandler( vcl->getAWindow(), vcl );
    }

    return 0;
}

const char *ViewCommandLogOp::getDescription()
{
    return catalog.getLocale( 1409 );
}
