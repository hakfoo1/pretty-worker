/* tablookup.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "tablookup.hh"

int TabLookup::insertEntry( const std::string &path )
{
    m_tab_dirs.push_back( path );
    return m_tab_dirs.size() - 1;
}

int TabLookup::insertEntry( int pos,
                            const std::string &path )
{
    if ( pos < 0 || pos >= (int)m_tab_dirs.size() )
        return insertEntry( path );

    std::list< std::string >::iterator it1;
    int tpos = pos;

    for ( it1 = m_tab_dirs.begin();
          it1 != m_tab_dirs.end();
          it1++ ) {
        if ( tpos < 1 ) break;
        tpos--;
    }
    
    m_tab_dirs.insert( it1, path );
    return pos;
}

void TabLookup::updateEntry( int pos,
                             const std::string &path )
{
    if ( pos < 0 || pos >= (int)m_tab_dirs.size() )
        return;

    std::list< std::string >::iterator it1;

    for ( it1 = m_tab_dirs.begin();
          it1 != m_tab_dirs.end();
          it1++ ) {
        if ( pos < 1 ) break;
        pos--;
    }
    
    *it1 = path;
}

void TabLookup::removeEntry( int pos )
{
    if ( pos < 0 || pos >= (int)m_tab_dirs.size() )
        return;

    std::list< std::string >::iterator it1;

    for ( it1 = m_tab_dirs.begin();
          it1 != m_tab_dirs.end();
          it1++ ) {
        if ( pos < 1 ) break;
        pos--;
    }

    m_tab_dirs.erase( it1 );
}

std::string TabLookup::getEntry( int pos ) const
{
    if ( pos < 0 || pos >= (int)m_tab_dirs.size() ) return "";

    std::list< std::string >::const_iterator it1;

    for ( it1 = m_tab_dirs.begin();
          it1 != m_tab_dirs.end();
          it1++ ) {
        if ( pos < 1 ) break;
        pos--;
    }

    return *it1;
}

void TabLookup::clearEntries()
{
    m_tab_dirs.clear();
}

int TabLookup::getNrOfEntries() const
{
    return m_tab_dirs.size();
}
