/* wcfiletype.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCFILETYPE_HH
#define WCFILETYPE_HH

#include "wdefines.h"
#include <list>
#include <string>
#include <map>
#include "aguix/refcount.hh"
#include "functionproto.h"

class List;
class Datei;
class StringMatcherFNMatch;

class WCFiletype
{
public:
  WCFiletype();
  ~WCFiletype();
  WCFiletype( const WCFiletype &other );
  WCFiletype &operator=( const WCFiletype &other );

  WCFiletype *duplicate() const;
  void setName(const char*);
  void setPattern(const char*);
  void setUsePattern(bool);
  void setFiledesc( const short* );
  void setUseFiledesc(bool);
  std::string getName() const;
  char *getPattern();
  bool getUsePattern() const;
  short *getFiledesc();
  bool getUseFiledesc() const;
  command_list_t getDNDActions();
  command_list_t getDoubleClickActions();
  command_list_t getShowActions();
  command_list_t getRawShowActions();
  command_list_t getUserActions(int);
  std::pair< command_list_t, bool > getCustomActions( const std::string &customname );
  void setDNDActions( const command_list_t & );
  void setDoubleClickActions( const command_list_t & );
  void setShowActions( const command_list_t & );
  void setRawShowActions( const command_list_t & );
  void setUserActions(int, const command_list_t &);
  void setCustomActions( const std::string &customname, const command_list_t & );
  bool save(Datei*) const;

  //TODO besser als enum type aber das macht mit config Probleme
#define NORMALTYPE 0
#define NOTYETTYPE 1
#define UNKNOWNTYPE 2
#define VOIDTYPE 3
#define DIRTYPE 4

  int getinternID() const;
  void setinternID(int);

  void setPatternIgnoreCase( bool nv );
  bool getPatternIgnoreCase() const;
  void setPatternUseRegExp( bool nv );
  bool getPatternUseRegExp() const;
  bool patternMatchString( const char * );
  void setPatternUseFullname( bool nv );
  bool getPatternUseFullname() const;
  
  static int sortfunction( void *, void *, int );
  
  void clearFiledesc();
  void setFiledesc( int pos, short v );

  void setExtCond( const char* );
  void setUseExtCond( bool );
  const char *getExtCond() const;
  bool getUseExtCond() const;
  void addSubType( WCFiletype *newtype );
  void removeSubType( WCFiletype *remtype );
  const std::list<WCFiletype*> *getSubTypeList();
  void setParentType( WCFiletype *pt );
  WCFiletype *getParentType() const;
  std::vector<unsigned int> *getTypePos();
  void sortSubTypeList();
  static bool sortfunctionType( WCFiletype *ft1, WCFiletype *ft2 );

  enum colordef_t {
    DEFAULTCOL, CUSTOMCOL, EXTERNALCOL, PARENTCOL
  };
  colordef_t getColorMode() const;
  void setColorMode( colordef_t newmode );
  const int *getCustomColor( int m ) const;
  void setCustomColor( int m, const int *c );
  void setCustomColor( int m, int pos, int c );
  const char *getColorExt() const;
  void setColorExt( const char *s );

    void fillNamesOfCustomActions( bool recursive, std::list<std::string> &names );
    command_list_t getActionList( bool recursive, const class ActionDescr &descr );

    void removeCustomAction( const std::string &customname );
    void clearCustomActions();

    bool getUseMagic() const;
    void setUseMagic( bool nv );
    bool getMagicCompressed() const;
    void setMagicCompressed( bool nv );
    std::string getMagicPattern() const;
    void setMagicPattern( const std::string &pat );
    bool magicMatch( const std::string &match_string );

    bool getMagicMime() const;
    void setMagicMime( bool nv );

    bool getMagicIgnoreCase() const;
    void setMagicIgnoreCase( bool nv );

    static void setUseExtendedRegEx( bool nv );

    void setPatternIsCommaSeparated( bool nv );
    bool getPatternIsCommaSeparated() const;

    bool nameContainsFlags() const;

    int priority() const;
    void setPriority( int prio );
private:
    void splitPattern();
    
  char *name;
  char *pattern;
  bool usepattern;
  short filedesc[64];  // 0-255 ASCII-Code, -1 dont care, -2 end
  bool usefiledesc;
  command_list_t dndaction;
  command_list_t doubleclickaction;
  command_list_t showaction;
  command_list_t rawshowaction;
  command_list_t useraction[10];
  std::map<std::string, command_list_t> customaction;
  int internID;
  
  bool patternIgnoreCase;
  bool patternUseRegExp;
  bool patternUseFullname;
  
  char *extCond;
  bool useExtCond;
#ifdef HAVE_REGEX
  regex_t patternReg;
  bool patternRegCompiled;
  void freeCompPatternReg();
  void compilePatternReg();
#endif

  std::list<WCFiletype*> *subtypes;
  WCFiletype *parentType;
  void getTypePos( const WCFiletype *child, std::vector<unsigned int> *v );

  colordef_t colmode;
  int colfg[4], colbg[4];
  char *colext;

    bool m_use_magic;
    bool m_magic_compressed;
    std::string m_magic_pattern;
    RefCount<StringMatcherFNMatch> m_magic_matcher;

    bool m_magic_mime;
    bool m_magic_ignore_case;

    bool m_pattern_is_comma_separated;

    std::vector< std::string > m_pattern_split;

    static bool s_use_extended_regex;

    bool m_name_contains_flags = false;

    int m_priority = 0;
};

#endif
