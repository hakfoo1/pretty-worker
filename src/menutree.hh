/* menutree.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013-2015,2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MENUTREE_HH
#define MENUTREE_HH

#include "wdefines.h"
#include "menutreenode.hh"
#include "stringmatcher_flexiblematch.hh"
#include <memory>
#include "pers_string_list.hh"

class MenuTree
{
public:
    MenuTree();
    MenuTree( const MenuTree &other ) = delete;
    MenuTree &operator=( const MenuTree &rhs ) = delete;

    MenuTreeNode *getRootNode();

    int setFilter( const std::string &filter );
    int setVisibleList( std::shared_ptr< PersistentStringList > list );

    std::string createPath( const std::string &separator );
    std::string createPath( MenuTreeNode *node,
                            const std::string &separator );
    std::string createPath( MenuTreeNode *me, MenuTreeNode *node,
                            const std::string &separator );

    void setCurrentNode( MenuTreeNode *node );
    MenuTreeNode *getCurrentNode() const;

    void fillFullname( std::list< std::pair< std::string, MenuTreeNode * > > &entries );
private:
    void fillFullname( MenuTreeNode *node,
                       const std::string &prefix,
                       std::list< std::pair< std::string, MenuTreeNode * > > &entries );

    int checkVisibility( const std::string &prefix,
                         MenuTreeNode *node,
                         bool root = true );

    static std::string createFullname( MenuTreeNode *node,
                                       const std::string &separator );

    MenuTreeNode m_root;
    MenuTreeNode *m_current_node;
    std::string m_filter;
    StringMatcherFlexibleMatch m_matcher;
    std::shared_ptr< PersistentStringList > m_visible_list;
};

#endif
