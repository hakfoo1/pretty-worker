/* chmodowncore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHMODOWNCORE_HH
#define CHMODOWNCORE_HH

#include "wdefines.h"
#include <list>
#include <functional>
#include <string>
#include "fileentry.hh"

struct NM_chmodorder;
struct NM_chownorder;
class FileEntry;
class NM_CopyOp_Dir;
class AGUIX;
class List;

class ChModOwnCore
{
public:
    ChModOwnCore( struct NM_chmodorder *chmodorder,
                  struct NM_chownorder *chownorder,
                  AGUIX *aguix );
    ~ChModOwnCore();

    void setCancel( bool nv );
    bool getCancel() const;

    void registerFEToChange( const FileEntry &fe,
                             int row );

    int executeChMod();
    int executeChOwn();

    typedef enum { NM_CHANGE_OK,
                   NM_CHANGE_SKIP,
                   NM_CHANGE_CANCEL } nm_change_t;

    void setPostChangeCallback( std::function< void( const FileEntry &fe, int row,
                                                     nm_change_t res ) > cb );
private:
    struct NM_chmodorder *m_chmodorder;
    struct NM_chownorder *m_chownorder;

    bool m_cancel;

    class change_base_entry {
    public:
        change_base_entry( const FileEntry &tfe, int row );
        ~change_base_entry();
        const FileEntry &entry() const
        {
            return m_fe;
        }
        int m_row;  // corresponding row or -1
        NM_CopyOp_Dir *m_cod; // when fe is dir, this will be the corresponding structure
    private:
        FileEntry m_fe;  // element to copy
    };

    std::list< change_base_entry > m_change_list;

    std::function< void( const FileEntry &fe, int row, nm_change_t res ) > m_post_cb;

    typedef struct changemod_info{
        changemod_info();
        bool forAll;
      
        mode_t mode;
      
        typedef enum {
            CHMOD_SET,
            CHMOD_ADD,
            CHMOD_REM
        } chmod_op_t;
      
        chmod_op_t op;
    } changemod_info_t;

    typedef struct changeown_info{
        changeown_info();
        bool forAll;
        uid_t newuid;
        gid_t newgid;
    } changeown_info_t;

    AGUIX *m_aguix;

    typedef struct {
        union {
            uid_t uid;
            gid_t gid;
        } id;
        char *name;
    } chownrequest_id_name_t;
  
    List *m_chown_user_list;
    List *m_chown_group_list;

    int worker_changemod( const change_base_entry &ss1,
                          changemod_info_t *cminfo );
    int worker_changeown( const change_base_entry &ss1,
                          changeown_info_t *coinfo );

    int applyNewMode( const change_base_entry &ss1, changemod_info_t newmode );
    int applyNewOwner( const change_base_entry &ss1, uid_t newuid, gid_t newgid );

    int requestNewMode( const FileEntry &fe, changemod_info_t *return_mode );
    int requestNewOwner( const FileEntry &fe, uid_t *return_owner, gid_t *return_group );

    void freeOwnerRequestInfos();
    void buildOwnerRequestInfos();
};

#endif
