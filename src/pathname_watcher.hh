/* pathname_watcher.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PATHNAMEWATCHER_HH
#define PATHNAMEWATCHER_HH

#include "wdefines.h"
#include <string>
#include <map>
#include <list>

class PathnameWatcher
{
public:
    PathnameWatcher();
    ~PathnameWatcher();

    int getWatcherFD();

    int watchPath( const std::string &pathname );
    int unwatchPath( const std::string &pathname, bool force = false );
    int unwatchAll();

    std::list< std::string > getChangedPaths();
private:
    int m_inotify_fd;
    std::map< std::string, std::pair< int, int > > m_watched_paths;
};

#endif
