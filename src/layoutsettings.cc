/* layoutsettings.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2010 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "layoutsettings.hh"
#include <map>
#include <algorithm>

LayoutSettings::LayoutSettings() : m_button_vert( false ),
                                   m_listview_vert( false ),
                                   m_listview_weight( 5 ),
                                   m_weight_rel_to_active( false )
{
}

void LayoutSettings::setOrders( const std::list< layoutID_t > &orders )
{
    m_orders = orders;
}

const std::list< LayoutSettings::layoutID_t > &LayoutSettings::getOrders() const
{
    return m_orders;
}

void LayoutSettings::clearOrders()
{
    m_orders.clear();
}

void LayoutSettings::pushBackOrder( layoutID_t nv )
{
    m_orders.push_back( nv );
}

void LayoutSettings::setButtonVert( bool nv )
{
    m_button_vert = nv;
}

bool LayoutSettings::getButtonVert() const
{
    return m_button_vert;
}

void LayoutSettings::setListViewVert( bool nv )
{
    m_listview_vert = nv;
}

bool LayoutSettings::getListViewVert() const
{
    return m_listview_vert;
}

LayoutSettings::available_elements_t LayoutSettings::getAvailableElements( bool bv, bool lvv )
{
    std::list< layoutID_t > tl;
    available_elements_t l;

    if ( bv == false && lvv == false ) {
        tl.clear();
        tl.push_back( LO_STATEBAR );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_LISTVIEWS );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_BUTTONS );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_CLOCKBAR );
        l.push_back( tl );
    } else if ( bv == false && lvv == true ) {
        tl.clear();
        tl.push_back( LO_STATEBAR );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_LISTVIEWS );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_LISTVIEWS );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_BUTTONS );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_CLOCKBAR );
        l.push_back( tl );
    } else if ( bv == true && lvv == false ) {
        tl.clear();
        tl.push_back( LO_STATEBAR );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_BLL );
        tl.push_back( LO_LBL );
        tl.push_back( LO_LLB );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_CLOCKBAR );
        l.push_back( tl );
    } else if ( bv == true && lvv == true ) {
        tl.clear();
        tl.push_back( LO_STATEBAR );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_BL );
        tl.push_back( LO_LB );
        l.push_back( tl );
        
        tl.clear();
        tl.push_back( LO_CLOCKBAR );
        l.push_back( tl );
    }
    return l;
}

int LayoutSettings::countID( const available_elements_t &l, layoutID_t ID )
{
    int count = 0;
    for ( available_elements_t::const_iterator it1 =  l.begin();
          it1 != l.end();
          it1++ ) {

        if ( std::find( it1->begin(),
                        it1->end(),
                        ID ) != it1->end() ) {
            count++;
        }
    }
    return count;
}

std::list< LayoutSettings::layoutID_t > LayoutSettings::fixUsedList( const available_elements_t &avail_l,
                                                                     const std::list< layoutID_t > &used_l )
{
    std::list< layoutID_t > new_l;
    std::map< layoutID_t, int > count_map;

    for ( std::list< layoutID_t >::const_iterator it1 = used_l.begin();
          it1 != used_l.end();
          it1++ ) {
        
        if ( count_map[*it1] < countID( avail_l, *it1 ) ) {
            new_l.push_back( *it1 );
            count_map[*it1]++;
        }
    }
    return new_l;
}

bool LayoutSettings::operator==( const LayoutSettings &rhs )
{
    if ( m_orders != rhs.getOrders() ) return false;
    if ( m_button_vert != rhs.getButtonVert() ) return false;
    if ( m_listview_vert != rhs.getListViewVert() ) return false;
    if ( m_listview_weight != rhs.getListViewWeight() ) return false;
    return true;
}

bool LayoutSettings::operator!=( const LayoutSettings &rhs )
{
    return !( operator==( rhs ) );
}

void LayoutSettings::setListViewWeight( int nv )
{
    if ( nv < 1 || nv > 9 ) return;
    m_listview_weight = nv;
}

int LayoutSettings::getListViewWeight() const
{
    return m_listview_weight;
}

void LayoutSettings::setWeightRelToActive( bool nv )
{
    m_weight_rel_to_active = nv;
}

bool LayoutSettings::getWeightRelToActive() const
{
    return m_weight_rel_to_active;
}
