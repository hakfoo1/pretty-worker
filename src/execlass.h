/* execlass.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2002-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EXECLASS_H
#define EXECLASS_H

#include "wdefines.h"
#include <string>

class ExeClass
{
public:
  ExeClass( class Worker *w = NULL );
  ~ExeClass();
  ExeClass( const ExeClass &other );
  ExeClass &operator=( const ExeClass &other );

  void addCommand( const char *format, ... );
  void execute();
  char *getOutput();
  char *getOutput( int *return_error );
  int getOutputAndRV( char **return_output, int *return_value, int *return_error );
  int getReturnCode();
  int getReturnCode( int *return_error );
  void cdDir( const char *dir );
  int getLastError();

  int readErrorOutput( std::string &return_str, int maxsize );
  int readOutput( std::string &return_str );
protected:
  char *tmpfile, *tmpoutput, *tmperror;
  FILE *fp;
  int lasterror;
  class Worker *worker;
};

int Worker_buildArgvList( const char *commandstr, char ***argvlist );
void Worker_freeArgvList( char **argvlist );
std::string Worker_secureCommandForShell( const char *str );
char *Worker_replaceEnv( const char *str );

#endif
