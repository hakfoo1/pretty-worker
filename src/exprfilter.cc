/* exprfilter.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "exprfilter.hh"
#include "exprfilter_parser.hh"
#include "exprfilter_evalatoms.hh"

ExprFilter::ExprFilter( const std::string &filter ) :
    m_filter_string( filter )
{
    std::list< ExprToken > tokens;

    exprfilter_scan_expr( filter.c_str(), tokens );

    ExprFilterParser efp( tokens );

    efp.parse( &m_efe );

    m_valid_reconstructed_filter = efp.getValidReconstructedFilter();
    m_expected_strings = efp.getListOfExpectedStrings();

    // ignore errors on purpose here, whatever is reasonable ok will be used
}

ExprFilter::~ExprFilter()
{
}

bool ExprFilter::check( NWCEntrySelectionState &e )
{
    bool res = false;

    try {
        res = m_efe.eval( e );
    } catch ( int ) {
        // default value in case of errors is true, match any entry
        res = true;
    }

    return res;
}

std::string ExprFilter::getValidReconstructedFilter() const
{
    return m_valid_reconstructed_filter;
}

std::list< std::string > ExprFilter::getListOfExpectedStrings() const
{
    return m_expected_strings;
}

bool ExprFilter::containsTypeExpression() const
{
    return m_efe.contains<ExprAtomTypeCmp>();
}
