/* exprfilter.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EXPRFILTER_HH
#define EXPRFILTER_HH

#include "genericdirectoryfilter.hh"
#include "exprfilter_eval.hh"
#include <string>
#include <list>

class NWCEntrySelectionState;

class ExprFilter : public GenericDirectoryFilter
{
public:
    ExprFilter( const std::string &filter );
    ~ExprFilter();

    bool check( NWCEntrySelectionState &e );

    std::string getValidReconstructedFilter() const;
    std::list< std::string > getListOfExpectedStrings() const;
    bool containsTypeExpression() const;
private:
    std::string m_filter_string;
    std::string m_valid_reconstructed_filter;
    std::list< std::string > m_expected_strings;

    ExprFilterEval m_efe;
};

#endif
