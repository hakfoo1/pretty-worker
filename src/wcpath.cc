/* wcpath.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wcpath.hh"
#include "datei.h"
#include "wcdoubleshortkey.hh"
#include "simplelist.hh"
 
WCPath::WCPath()
{
  name=dupstring("");
  path=dupstring("");
  fg=1;
  bg=0;
  check=false;
}

WCPath::~WCPath()
{
  if(name!=NULL) _freesafe(name);
  if(path!=NULL) _freesafe(path);
}

WCPath *WCPath::duplicate()
{
  WCPath *tpath=new WCPath();
  tpath->setName(name);
  tpath->setPath(path);
  tpath->setFG(fg);
  tpath->setBG(bg);
  tpath->setCheck(check);
  tpath->setDoubleKeys( dkeys );
  return tpath;
}

void WCPath::setName(const char *nname)
{
  if(name!=NULL) _freesafe(name);
  if(nname==NULL) {
    name=dupstring("");
  } else {
    name=dupstring(nname);
  }
}

void WCPath::setPath(const char *npath)
{
  if(path!=NULL) _freesafe(path);
  if(npath==NULL) {
    path=dupstring("");
  } else {
    path=dupstring(npath);
  }
}

void WCPath::setFG(int nfg)
{
  fg=nfg;
}

void WCPath::setBG(int nbg)
{
  bg=nbg;
}

char *WCPath::getName()
{
  return name;
}

char *WCPath::getPath()
{
  return path;
}

int WCPath::getFG() const
{
  return fg;
}

int WCPath::getBG() const
{
  return bg;
}

void WCPath::setCheck(bool ncheck)
{
  check=ncheck;
}

bool WCPath::getCheck() const
{
  return check;
}

bool WCPath::save(Datei *fh)
{
  int id;
  WCDoubleShortkey *dk;

  if(fh==NULL) return false;

  fh->configPutPairString( "title", name );
  fh->configPutPairNum( "color", fg, bg );
  fh->configPutPairString( "path", path );

  fh->configOpenSection( "shortkeys" );
  id = dkeys->initEnum();
  dk = (WCDoubleShortkey*)dkeys->getFirstElement( id );
  while ( dk != NULL ) {
    dk->save( fh );
    dk = (WCDoubleShortkey*)dkeys->getNextElement( id );
  }
  dkeys->closeEnum( id );
  fh->configCloseSection(); //shortkeys

  return true;
}
