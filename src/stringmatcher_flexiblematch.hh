/* stringmatcher_flexiblematch.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012-2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRINGMATCHER_FLEXIBLEMATCH_HH
#define STRINGMATCHER_FLEXIBLEMATCH_HH

#include "wdefines.h"
#include "stringmatcher.hh"

class StringMatcherFlexibleMatch : public StringMatcher
{
public:
    StringMatcherFlexibleMatch();
    ~StringMatcherFlexibleMatch();
    StringMatcherFlexibleMatch( const StringMatcherFlexibleMatch &other );
    StringMatcherFlexibleMatch &operator=( const StringMatcherFlexibleMatch &other );

    bool match( const std::string &str );
    template <bool with_segments> int countNonMatchingBlocks( const std::string &str,
                                                              std::vector< size_t > *return_segments );

    void setMatchString( const std::string &str );
    std::string getMatchString() const;
    void setMatchCaseSensitive( bool nv );
    bool getMatchCaseSensitive() const;
private:
    std::string m_match_string;
    bool m_match_case_sensitive;

    std::string m_match_string_lowercase;

    struct state_description {
        const char *sub_string;
        int string_len;

        state_description( const char *_sub_string,
                           int _string_len ) : sub_string( _sub_string ),
                                               string_len( _string_len )
        {
        }
    };

    std::vector< state_description > m_state_list;

    void createLoweredCase();
    void buildStateDescription();
};

#endif
