/* flattypelist.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006-2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FLATTYPELIST_HH
#define FLATTYPELIST_HH

#include "wdefines.h"
#include <vector>
#include <list>
#include <string>
#include <map>

class WCFiletype;
class FieldListView;
class List;

class FlatTypeList
{
public:
    FlatTypeList( List *types, const std::string &filter );
    ~FlatTypeList();

    typedef struct _flattypelist_t {
        _flattypelist_t() : filetype( NULL ),
                            depth( 0 ),
                            visible( true ) {}
        WCFiletype *filetype;
        int depth;
        bool visible;
    } flattypelist_t;

    void buildLV( FieldListView *lv,
                  WCFiletype *highlightft );
    flattypelist_t getEntry( int visible_row );
    int getNrOfEntries();
    WCFiletype *findParent( int row );
private:
    void build( List *types );
    void addSubTypes( const std::list<WCFiletype*> *types, int depth );
    std::string getStringForName( flattypelist_t &type );
    bool checkFilter( WCFiletype *type );

    std::string m_filter;
    std::vector<flattypelist_t> m_flat_list;
    std::map<int, int> m_visible_entry;
};

#endif
