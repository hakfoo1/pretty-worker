/* normalmode_requests.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NORMALMODE_REQUESTS_HH
#define NORMALMODE_REQUESTS_HH

#include "wdefines.h"
#include <map>
#include <string>
#include <list>

class AWindow;

class Requests
{
public:
    Requests();
    ~Requests();
    Requests( const Requests &other );
    Requests &operator=( const Requests &other );
    typedef enum { SKIP_CANCEL,
		   NO_SPACE_LEFT,
		   WRITE_ERROR } request_t;
    typedef enum { OKAY, SKIP, CANCEL, REQUEST_ERROR } request_choose_t;
    request_choose_t request( request_t type, AWindow *win, const std::string &text );
    request_choose_t request( request_t type, AWindow *win, const char *format, const char *str2, int error_number );
    request_choose_t request_va( request_t type, AWindow *win, const char *format, ... );
private:
    std::map<request_t, std::list<request_choose_t> > request_chooses;
    std::map<request_choose_t, std::string> choose_strings;
};

#endif
