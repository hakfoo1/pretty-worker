/* wconfig_buttongrpconf.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_buttongrpconf.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/stringgadget.h"

ButtonGrpConfPanel::ButtonGrpConfPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  _last_rows = (int)_baseconfig.getRows();
  _last_columns = (int)_baseconfig.getColumns();
}

ButtonGrpConfPanel::~ButtonGrpConfPanel()
{
}

int ButtonGrpConfPanel::create()
{
  Panel::create();

  char buf[ A_BYTESFORNUMBER( int ) ];

  AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 682 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 2 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 0 );
  ac1_1->setMaxSpace( 0 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 17 ) ),
              0, 0, AContainer::CO_FIX );
  
  sprintf( buf, "%d", _last_rows );

  rowsg = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 30, buf, 0 ),
                                     1, 0, AContainer::CO_INCW );
  rowsg->connect( this );

  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 18 ) ),
              0, 1, AContainer::CO_FIX );
  
  sprintf( buf, "%d", _last_columns );

  colsg = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 30, buf, 0 ),
                                     1, 1, AContainer::CO_INCW );
  colsg->connect( this );

  contMaximize( true );
  return 0;
}

int ButtonGrpConfPanel::saveValues()
{
  int val = 0;

  if ( sscanf( rowsg->getText(), "%d", &val ) == 1 ) {
    if ( val >= 1 && val <= 10 ) {
      _baseconfig.setRows( val );
    }
  }

  if ( sscanf( colsg->getText(), "%d", &val ) == 1 ) {
    if ( val >= 1 && val <= 20 ) {
      _baseconfig.setColumns( val );
    }
  }
  return 0;
}

void ButtonGrpConfPanel::run( Widget *elem, const AGMessage &msg )
{
  int val = 0;
  char buf[ A_BYTESFORNUMBER( int ) ];

  if ( msg.type == AG_STRINGGADGET_DEACTIVATE ) {
    if ( msg.stringgadget.sg == rowsg ) {
      if ( sscanf( rowsg->getText(), "%d", &val ) == 1 ) {
        if ( val < 1 || val > 10 ) {
          sprintf( buf, "%d", _last_rows );
          rowsg->setText( buf );
        }
      }
    } else if ( msg.stringgadget.sg == colsg ) {
      if ( sscanf( colsg->getText(), "%d", &val ) == 1 ) {
        if ( val < 1 || val > 20 ) {
          sprintf( buf, "%d", _last_columns );
          colsg->setText( buf );
        }
      }
    }
  }
}

void ButtonGrpConfPanel::hide()
{
  int val = 0;

  if ( _conf_cb != NULL ) {
    if ( sscanf( rowsg->getText(), "%d", &val ) == 1 ) {
      if ( val >= 1 && val <= 10 && val != _last_rows ) {
        _conf_cb->setRows( val );
        _last_rows = val;
      }
    }
    if ( sscanf( colsg->getText(), "%d", &val ) == 1 ) {
      if ( val >= 1 && val <= 20 && val != _last_columns ) {
        _conf_cb->setColumns( val );
        _last_columns = val;
      }
    }
  }
  WConfigPanel::hide();
}
