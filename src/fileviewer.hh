/* fileviewer.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEVIEWER_HH
#define FILEVIEWER_HH

#include "wdefines.h"
#include <string>
#include <list>
#include "aguix/refcount.hh"

class Worker;

class FileViewerDestroyCallback
{
public:
    virtual ~FileViewerDestroyCallback() = 0;
};

class FileViewer
{
public:
  FileViewer( Worker *worker );
  ~FileViewer();
  FileViewer( const FileViewer &other );
  FileViewer &operator=( const FileViewer &other );

  void view( const std::list<std::string> &filelist,
	     RefCount< FileViewerDestroyCallback > destroy_callback,
	     int initial_line_number = 0,
	     bool highlight_initial_line = false );
  static void setGlobalLineWrap( bool nv );
  static bool getGlobalLineWrap();
  static void setGlobalShowLineNumbers( bool nv );
  static bool getGlobalShowLineNumbers();
  static void setGlobalUseAltFont( bool nv );
    void setLineWrap( bool nv );
    bool getLineWrap();
    void setShowLineNumbers( bool nv );
    bool getShowLineNumbers();
    void setUseAltFont( bool nv );
    bool getUseAltFont() const;
protected:
    static bool global_wrap_mode;
    static bool global_show_line_numbers_mode;
    static bool global_use_alt_font_mode;
    bool m_use_wrap_mode;
    bool m_use_show_line_numbers_mode;
    bool m_use_alt_font_mode;
    Worker *m_worker;
};

#endif
