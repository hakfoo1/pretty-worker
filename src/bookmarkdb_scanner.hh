/* bookmarkdb_scanner.hh
 * This file belongs to Worker, a file manager for UNIX/X11.
 * Copyright (C) 2007-2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DIRBOOKMARK_SCANNER_HH
#define DIRBOOKMARK_SCANNER_HH

#include "wdefines.h"
#include <string>

#ifndef BMDB_HEADER_NO_PROTOS
void bmdbrestart( FILE *input_file );
int bmdblex();

void bmdb_readtoken();
#endif

extern int bmdb_token;
extern int bmdb_linenr;
extern std::string bmdb_token_str;

typedef enum {
    BMDB_ENTRY = 256,
    BMDB_NAME,
    BMDB_USEPARENT,
    BMDB_STRING,
    BMDB_TRUE,
    BMDB_FALSE,
    BMDB_BOOKMARKS,
    BMDB_ALIAS,
    BMDB_CATEGORY
} bmdb_token_t;

#endif
