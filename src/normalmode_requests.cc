/* normalmode_requests.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "normalmode_requests.hh"
#include "aguix/awindow.h"
#include "worker_locale.h"
#include <stdarg.h>

Requests::Requests()
{
    std::map<request_t,std::list<request_choose_t> > m;
    std::list<request_choose_t> l;

    choose_strings[OKAY] = "Okay";
    choose_strings[SKIP] = "Skip";
    choose_strings[CANCEL] = "Cancel";

    l.push_back( SKIP );
    l.push_back( CANCEL );
    request_chooses[NO_SPACE_LEFT] = l;

    request_chooses[WRITE_ERROR] = l;
  
    request_chooses[SKIP_CANCEL] = l;
}

Requests::~Requests()
{
}

Requests::request_choose_t Requests::request( request_t reqtype,
                                              AWindow *win, 
                                              const std::string &text )
{
    std::list<request_choose_t> &l = request_chooses[reqtype];
    std::list<request_choose_t>::iterator it1;
    std::string buttons, s1;
    request_choose_t erg;
    int c, i;
  
    if ( win == NULL ) return REQUEST_ERROR;

    if ( l.size() > 0 ) {
        for ( it1 = l.begin(); it1 != l.end(); it1++ ) {
            s1 = choose_strings[*it1];
            if ( s1.length() < 1 )
                break;
            if ( it1 != l.begin() )
                buttons += "|";
            buttons += s1;
        }
        if ( it1 == l.end() ) {
            c = win->request( catalog.getLocale( 347 ), text.c_str(), buttons.c_str() );
            erg = REQUEST_ERROR;
      
            i = 0;
            for ( it1 = l.begin(); it1 != l.end(); it1++, i++ ) {
                if ( i == c ) erg = *it1;
            }
            return erg;
        }
    }
    return REQUEST_ERROR;
}

Requests::request_choose_t Requests::request_va( request_t reqtype,
                                                 AWindow *win,
                                                 const char *format, ... )
{
    va_list va;
    int n, size = 1024;
    char *tstr;
    std::string string1;
  
    if ( format != NULL ) {
        // first create str
        while ( 1 ) {
            tstr = (char*)_allocsafe( size );
            va_start( va, format );
            n = vsnprintf( tstr, size, format, va );
            va_end( va );
            if ( ( n > -1 ) && ( n < size ) ) break;
            _freesafe( tstr );
            size *= 2;
        }
        string1 = tstr;
        _freesafe( tstr );
        return request( reqtype, win, string1 );
    }
    return REQUEST_ERROR;
}

Requests::request_choose_t Requests::request( request_t reqtype,
                                              AWindow *win, 
                                              const char *format,
                                              const char *str2,
                                              int error_number )
{
    const char *myerrstr;
    char *textstr;
    std::string s1;
  
    myerrstr = strerror( error_number );
    textstr = (char*)_allocsafe( strlen( format ) +
                                 strlen( str2 ) +
                                 strlen( myerrstr ) + 1);
    sprintf( textstr, format, str2, myerrstr );
    s1 = textstr;
    _freesafe( textstr );
  
    return request( reqtype, win, s1 );
}
