/* switchbuttonbankop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "switchbuttonbankop.hh"
#include "worker.h"
#include "aguix/cyclebutton.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "aguix/lowlevelfunc.h"
#include "aguix/acontainer.h"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/util.h"
#include "datei.h"
#include "worker_locale.h"

const char *SwitchButtonBankOp::name = "SwitchButtonBankOp";

bool SwitchButtonBankOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *SwitchButtonBankOp::getName()
{
    return name;
}

SwitchButtonBankOp::SwitchButtonBankOp() : FunctionProto()
{
    m_mode = SWITCH_TO_NEXT_BANK;
    m_bank_nr = 1;

    hasConfigure = true;
}

SwitchButtonBankOp::~SwitchButtonBankOp()
{
}

SwitchButtonBankOp *SwitchButtonBankOp::duplicate() const
{
    SwitchButtonBankOp *ta = new SwitchButtonBankOp();
    ta->m_mode = m_mode;
    ta->m_bank_nr = m_bank_nr;
    return ta;
}

int SwitchButtonBankOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    switch ( m_mode ) {
        case SWITCH_TO_PREV_BANK:
            msg->getWorker()->shuffleButton( -1 );
            break;
        case SWITCH_TO_BANK_NR:
            msg->getWorker()->switchToButtonBank( m_bank_nr - 1 );
            break;
        default:
            msg->getWorker()->shuffleButton( 1 );
            break;
    }
    return 0;
}

const char *SwitchButtonBankOp::getDescription()
{
    return catalog.getLocale( 1300 );
}

int SwitchButtonBankOp::configure()
{
    AGUIX *aguix = Worker::getAGUIX();
    AWindow *win;
    AGMessage *msg;
    int endmode = -1;
    char *tstr;
    const int cincw = AContainer::ACONT_MINH +
        AContainer::ACONT_MINW +
        AContainer::ACONT_MAXH;
    const int cfix = AContainer::ACONT_MINH +
        AContainer::ACONT_MINW +
        AContainer::ACONT_MAXH +
        AContainer::ACONT_MAXW;
  
    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) +
                              strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe( tstr );

    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 3 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( 5 );
    ac1_1->setBorderWidth( 0 );

    ac1_1->add( new Text( aguix, 0, 0, catalog.getLocale( 957 ) ), 0, 0, cfix );
    CycleButton *mode_cb = (CycleButton*)ac1_1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, cincw );
    mode_cb->addOption( catalog.getLocale( 958 ) );
    mode_cb->addOption( catalog.getLocale( 959 ) );
    mode_cb->addOption( catalog.getLocale( 960 ) );
    mode_cb->resize( mode_cb->getMaxSize(), mode_cb->getHeight() );
    ac1_1->readLimits();

    switch ( m_mode ) {
        case SWITCH_TO_PREV_BANK:
            mode_cb->setOption( 1 );
            break;
        case SWITCH_TO_BANK_NR:
            mode_cb->setOption( 2 );
            break;
        default:
            mode_cb->setOption( 0 );
            break;
    }
  
    AContainer *ac1_3 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
    ac1_3->setMinSpace( 5 );
    ac1_3->setMaxSpace( 5 );
    ac1_3->setBorderWidth( 0 );

    ac1_3->add( new Text( aguix, 0, 0, catalog.getLocale( 961 ) ), 0, 0, cfix );
    std::string banknrstr = AGUIXUtils::formatStringToString( "%d", m_bank_nr );
    StringGadget *banknr_sg = (StringGadget*)ac1_3->add( new StringGadget( aguix, 0, 0, 100, banknrstr.c_str(), 0 ), 1, 0, cincw );

    AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 2 );
    ac1_2->setMinSpace( 5 );
    ac1_2->setMaxSpace( -1 );
    ac1_2->setBorderWidth( 0 );
    Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 11 ),
                                                  0 ), 0, 0, cfix );
    Button *cb = (Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 8 ),
                                                  0 ), 1, 0, cfix );
  
    win->setDoTabCycling( true );
    win->contMaximize( true );
    win->show();

    for ( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cb ) endmode = 1;
                    break;
                case AG_STRINGGADGET_OK:
                    if ( msg->stringgadget.sg == banknr_sg ) {
                        mode_cb->setOption( 2 );
                    }
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
  
    if ( endmode == 0 ) {
        // ok
        switch ( mode_cb->getSelectedOption() ) {
            case 1:
                m_mode = SWITCH_TO_PREV_BANK;
                break;
            case 2:
                m_mode = SWITCH_TO_BANK_NR;
                m_bank_nr = atoi( banknr_sg->getText() );
                break;
            default:
                m_mode = SWITCH_TO_NEXT_BANK;
                break;
        }
    }
  
    delete win;

    return endmode;
}

bool SwitchButtonBankOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;

    switch ( m_mode ) {
        case SWITCH_TO_PREV_BANK:
            fh->configPutPair( "mode", "switchtoprevbank" );
            break;
        case SWITCH_TO_BANK_NR:
            fh->configPutPair( "mode", "switchtobanknr" );
            break;
        default:
            fh->configPutPair( "mode", "switchtonextbank" );
            break;
    }

    fh->configPutPairNum( "banknr", m_bank_nr );

    return true;
}

void SwitchButtonBankOp::setSwitchMode( switch_bank_mode_t mode )
{
    m_mode = mode;
}

SwitchButtonBankOp::switch_bank_mode_t SwitchButtonBankOp::getSwitchMode() const
{
    return m_mode;
}

void SwitchButtonBankOp::setBankNr( int nr )
{
    m_bank_nr = nr;
}

int SwitchButtonBankOp::getBankNr() const
{
    return m_bank_nr;
}
