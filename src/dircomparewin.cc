/* dircomparewin.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dircomparewin.hh"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/button.h"
#include "aguix/acontainerbb.h"
#include "worker_locale.h"
#include "filenameshrinker.hh"

DirCompareWin::DirCompareWin( AGUIX *aguix,
                              bool progress_percent  ) : m_aguix( aguix ),
                                                         m_entries_to_do( 0 ),
                                                         m_entries_done( 0 ),
                                                         m_win( NULL ),
                                                         m_to_do_t( NULL ),
                                                         m_percent_t( NULL ),
                                                         m_dir_t( NULL ),
                                                         m_cancel_b( NULL ),
                                                         m_cont0( NULL ),
                                                         m_maincont( NULL ),
                                                         m_ac3( NULL ),
                                                         m_last_update( 0 ),
                                                         m_show_percent( progress_percent ),
                                                         m_percent( 0 )
{
}

DirCompareWin::~DirCompareWin()
{
    close();
}

void DirCompareWin::addEntriesToDo( size_t count )
{
    m_entries_to_do += count;

    update( false );
}

void DirCompareWin:: finishedEntry()
{
    m_entries_done++;
    m_percent = 0;

    update( false );
}

int DirCompareWin::open()
{
    m_win = new AWindow( m_aguix, 10, 10, 10, 10, catalog.getLocale( 1307 ), AWindow::AWINDOW_DIALOG );
    m_win->create();
    
    m_maincont = m_win->setContainer( new AContainer( m_win, 1, 3 + ( m_show_percent ? 1 : 0 ) ), true );
    m_maincont->setMinSpace( 5 );
    m_maincont->setMaxSpace( 5 );
    m_maincont->setBorderWidth( 5 );

    m_cont0 = m_maincont->add( new AContainer( m_win, 2, 1 ), 0, 0 );
    m_cont0->setMinSpace( 5 );
    m_cont0->setMaxSpace( 5 );
    m_cont0->setBorderWidth( 0 );
    m_cont0->addWidget( new Text( m_aguix, 0, 0, catalog.getLocale( 1188 ), 1, 0 ),
                        0, 0, AContainer::CO_FIX );
    m_to_do_t = m_cont0->addWidget( new Text( m_aguix, 0, 0, "      ", 1, 0 ),
                                    1, 0, AContainer::CO_INCW );

    if ( m_show_percent ) {
        AContainer *ac2 = m_maincont->add( new AContainer( m_win, 2, 1 ), 0, 1 );
        ac2->setMinSpace( 5 );
        ac2->setMaxSpace( 5 );
        ac2->setBorderWidth( 0 );
        ac2->addWidget( new Text( m_aguix, 0, 0, catalog.getLocale( 1189 ), 1, 0 ),
                        0, 0, AContainer::CO_FIX );
        m_percent_t = ac2->addWidget( new Text( m_aguix, 0, 0, "      ", 1, 0 ),
                                      1, 0, AContainer::CO_INCW );
    }

    m_ac3 = m_maincont->add( new AContainer( m_win, 2, 1 ), 0, m_show_percent ? 2 : 1 );
    m_ac3->setMinSpace( 5 );
    m_ac3->setMaxSpace( 5 );
    m_ac3->setBorderWidth( 0 );
    m_ac3->addWidget( new Text( m_aguix, 0, 0, catalog.getLocale( 1190 ), 1, 0 ),
                      0, 0, AContainer::CO_FIX );
    m_dir_t = m_ac3->addWidget( new Text( m_aguix, 0, 0, "", 1, 0 ),
                                1, 0, AContainer::CO_INCW );
    m_dir_t->setTextShrinker( RefCount<TextShrinker>( new FileNameShrinker() ) );

    AContainer *ac1_0 = m_maincont->add( new AContainer( m_win, 3, 1 ), 0, m_show_percent ? 3 : 2 );
    ac1_0->setMinSpace( -1 );
    ac1_0->setMaxSpace( -1 );
    ac1_0->setBorderWidth( 0 );
    m_cancel_b = ac1_0->addWidget( new Button( m_aguix,
                                               0,
                                               0,
                                               catalog.getLocale( 8 ),
                                               0 ), 1, 0, AContainer::CO_FIX );

    m_win->setDoTabCycling( true );
    m_win->contMaximize( true );
    m_win->show();

    return 0;
}

int DirCompareWin::check()
{
    AGMessage *msg;
    int res = 0;

    while ( ( msg = m_aguix->GetMessage( m_win ) ) ) {
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == m_win->getWindow() ) {
                        res = 1;
                    }
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == m_cancel_b ) {
                        res = 1;
                    }
                default:
                    break;
            }
            m_aguix->ReplyMessage( msg );
        }
    }

    return res;
}

void DirCompareWin::close()
{
    delete m_win;
    m_win = nullptr;
}

void DirCompareWin::update( bool force )
{
    time_t now = time( NULL );

    if ( force || now != m_last_update ) {
        std::string prog = AGUIXUtils::formatStringToString( catalog.getLocale( 1191 ),
                                                             m_entries_to_do - m_entries_done,
                                                             m_entries_to_do );

        m_to_do_t->setText( prog.c_str() );

        if ( ! m_current_dir.empty() ) {
            int w = m_dir_t->getWidth();
            m_dir_t->setText( m_current_dir.c_str() );

            if ( m_dir_t->getWidth() < w ) {
                m_dir_t->resize( w, m_dir_t->getHeight() );
            }
        }

        if ( m_percent_t && m_show_percent ) {
            if ( m_percent > 0 && m_current_entry == m_last_update_entry ) {
                std::string per = AGUIXUtils::formatStringToString( "%d%%", m_percent );

                m_percent_t->setText( per.c_str() );
            } else {
                m_percent_t->setText( "" );
            }
        }

        m_cont0->readLimits();
        m_ac3->readLimits();
        m_maincont->readLimits();
        m_win->contMaximize( true );

        m_last_update = now;
        m_last_update_entry = m_current_entry;
    }
}

void DirCompareWin::setCurrentEntry( const std::string &entry )
{
    m_current_entry = entry;

    update( false );
}

void DirCompareWin::setCurrentDir( const std::string &dir )
{
    m_current_dir = dir;

    update( false );
}

void DirCompareWin::setEntryPercent( int v )
{
    if ( v >= 0 && v <= 100 ) {
        m_percent = v;
    } else {
        m_percent = 0;
    }

    update( false );
}
