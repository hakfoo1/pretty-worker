/* fileentry_customcolor.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "fileentry_customcolor.hh"

FileEntryCustomColor::FileEntryCustomColor() : m_color_set( 0 )
{
    for ( int i = 0; i < 4; i++ ) {
        m_colors_fg[i] = 1;
        m_colors_bg[i] = 0;
    }
}

int FileEntryCustomColor::getColorSet() const
{
    return m_color_set;
}

void FileEntryCustomColor::setColorSet( int v )
{
    m_color_set = v & ALL_COLORS;
}

void FileEntryCustomColor::setColor( int type, int fg, int bg )
{
    switch ( type ) {
        case NORMAL:
            m_colors_fg[0] = fg;
            m_colors_bg[0] = bg;
            m_color_set |= NORMAL;
            break;
        case SELECT:
            m_colors_fg[1] = fg;
            m_colors_bg[1] = bg;
            m_color_set |= SELECT;
            break;
        case ACTIVE:
            m_colors_fg[2] = fg;
            m_colors_bg[2] = bg;
            m_color_set |= ACTIVE;
            break;
        case SELACT:
            m_colors_fg[3] = fg;
            m_colors_bg[3] = bg;
            m_color_set |= SELACT;
            break;
        default:
            break;
    }
}

int FileEntryCustomColor::getColor( int type, int *fg_return, int *bg_return ) const
{
    if ( fg_return == NULL || bg_return == NULL ) return 1;
    switch ( type ) {
        case NORMAL:
            *fg_return = m_colors_fg[0];
            *bg_return = m_colors_bg[0];
            break;
        case SELECT:
            *fg_return = m_colors_fg[1];
            *bg_return = m_colors_bg[1];
            break;
        case ACTIVE:
            *fg_return = m_colors_fg[2];
            *bg_return = m_colors_bg[2];
            break;
        case SELACT:
            *fg_return = m_colors_fg[3];
            *bg_return = m_colors_bg[3];
            break;
        default:
            break;
    }
    return 0;
}

void FileEntryCustomColor::setColors( const int fg[4], const int bg[4] )
{
    for ( int i = 0; i < 4; i++ ) {
        m_colors_fg[i] = fg[i];
        m_colors_bg[i] = bg[i];
    }
    m_color_set = ALL_COLORS;
}

const int *FileEntryCustomColor::getColors( bool bg ) const
{
    if ( bg == true ) {
        return m_colors_bg;
    } else {
        return m_colors_fg;
    }
}
