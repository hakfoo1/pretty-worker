/* getmntentmounttable.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009,2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "getmntentmounttable.hh"
#include "mounttable_entry.hh"
#include "nwc_fsentry.hh"
#include <stdio.h>
#ifdef MOUNTED_GETMNTENT1
#include <mntent.h>
#endif

typedef std::pair< mode_t, dev_t > w_osdevice_t;

static w_osdevice_t getOSDevice( const std::string &dev )
{
    NWC::FSEntry fe( dev );

    if ( ! fe.entryExists() ) {
        return std::make_pair( 0, 0 );
    }

    if ( fe.isLink() ) {
        return std::make_pair( fe.stat_dest_mode(),
                               fe.stat_dest_rdev() );
    } else {
        return std::make_pair( fe.stat_mode(),
                               fe.stat_rdev() );
    }
}

GetMntEntMountTable::GetMntEntMountTable( const std::string  &file ) : m_file( file ),
                                                                       m_last_mod( 0 )
{
}

std::list<MountTableEntry> GetMntEntMountTable::getEntries()
{
    update();
    return m_entries;
}

void GetMntEntMountTable::update()
{
#if 0
    //TODO this don't work properly as several updates per seconds are
    //     not recognized and you can't mount an already mounted volume
    //     even if it actually is no longer mounted
    //     maybe asking the OS for some signal if file changes is
    //     more appropriate
    NWC::FSEntry fe( m_file );
    time_t lastmod = 0;
    bool do_update = false;

    if ( ! fe.entryExists() ) {
        do_update = true;
    } else {
        if ( fe.isLink() ) {
            lastmod = fe.stat_dest_lastmod();
        } else {
            lastmod = fe.stat_lastmod();
        }
        if ( lastmod != m_last_mod ) {
            do_update = true;
        }
    }

    if ( do_update ) {
        m_entries.clear();

        m_entries = readEntries();
        
        m_last_mod = lastmod;
    }
#endif

    m_entries.clear();
    
    m_entries = readEntries();
}

bool GetMntEntMountTable::containsDevice( const std::string &dev )
{
    w_osdevice_t stat1 = getOSDevice( dev );
    for ( std::list<MountTableEntry>::const_iterator it1 = m_entries.begin();
          it1 != m_entries.end();
          it1++ ) {
        if ( it1->getDevice() == dev ) return true;
        if ( stat1.first && stat1.second ) {
            if ( it1->getMode() == stat1.first &&
                 it1->getRDev() == stat1.second ) return true;
        }
    }
    return false;
}

bool GetMntEntMountTable::findDevice( const std::string &dev,
                                      MountTableEntry &return_entry )
{
    w_osdevice_t stat1 = getOSDevice( dev );
    for ( std::list<MountTableEntry>::const_iterator it1 = m_entries.begin();
          it1 != m_entries.end();
          it1++ ) {
        if ( it1->getDevice() == dev ) {
            return_entry = *it1;
            return true;
        }
        if ( stat1.first && stat1.second ) {
            if ( it1->getMode() == stat1.first &&
                 it1->getRDev() == stat1.second ) {
                return_entry = *it1;
                return true;
            }
        }
    }
    return false;
}

std::list<MountTableEntry> GetMntEntMountTable::readEntries()
{
    std::list<MountTableEntry> l;

#ifdef MOUNTED_GETMNTENT1
    FILE *fp = NULL;

    fp = setmntent( m_file.c_str(), "r" );
    if ( fp != NULL ) {
        struct mntent *me = NULL;
            
        while ( ( me = getmntent( fp ) ) ) {
            if ( me->mnt_fsname != NULL ) {
                MountTableEntry mte( me->mnt_fsname );
                if ( me->mnt_dir ) mte.setMountPoint( me->mnt_dir );

                w_osdevice_t stat1 = getOSDevice( me->mnt_fsname );
                if ( stat1.first && stat1.second ) {
                    mte.setMode( stat1.first );
                    mte.setRDev( stat1.second );
                }

                l.push_back( mte );
            }
        }
            
        endmntent( fp );
    }
#endif
    return l;
}
