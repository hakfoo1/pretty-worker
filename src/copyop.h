/* copyop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COPYOP_H
#define COPYOP_H

#include "wdefines.h"
#include "functionproto.h"
#include "copy_parameters.hh"

class CopyOp:public FunctionProto
{
public:
  CopyOp();
  virtual ~CopyOp();
  CopyOp( const CopyOp &other );
  CopyOp &operator=( const CopyOp &other );

  CopyOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;

  int doconfigure(int mode);

  typedef enum {COPYOP_OVERWRITE_NORMAL=0,COPYOP_OVERWRITE_ALWAYS,COPYOP_OVERWRITE_NEVER} overwrite_t;
  void setFollowSymlinks(bool);
  void setMove(bool);
  void setRename(bool);
  void setSameDir(bool);
  void setRequestDest(bool);
  void setOverwrite(overwrite_t);
  void setPreserveAttr(bool);
  void setVdirPreserveDirStructure(bool);

  typedef enum { COPYOP_ADJUST_SYMLINK_NEVER,
                 COPYOP_ADJUST_SYMLINK_OUTSIDE,
                 COPYOP_ADJUST_SYMLINK_ALWAYS
  } adjust_relative_symlinks_t;

  void setAdjustRelativeSymlinks( adjust_relative_symlinks_t nv );
  void setEnsureFilePermissions( const CopyParams::EnsureFilePermissions &nv );
  bool isInteractiveRun() const;
  void setInteractiveRun();
protected:
  static const char *name;
  // Infos to save
  bool follow_symlinks;
  bool move;
  bool do_rename;
  bool same_dir;
  bool request_dest;
  bool preserve_attr;
  bool vdir_preserve_dir_structure;
  adjust_relative_symlinks_t adjust_relative_symlinks;
  CopyParams::EnsureFilePermissions ensure_file_permissions;

  overwrite_t overwrite;

  // temp variables
  Lister *startlister,*endlister;
  
  // temp values filled when request_flags==true
  bool tfollow_symlinks;
  bool tmove;
  bool trename;
  bool tsame_dir;
  bool trequest_dest;
  bool tpreserve_attr;
  bool tvdir_preserve_dir_structure;
  adjust_relative_symlinks_t tadjust_relative_symlinks;
  CopyParams::EnsureFilePermissions tensure_file_permissions;
  overwrite_t toverwrite;

  int normalmodecopy( ActionMessage *am );
  int requestdest( const char *defaultstr, char **dest,
                   bool show_do_not_ask, bool &do_not_ask_return );
};

#endif
