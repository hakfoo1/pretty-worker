/* wconfig_volman.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_VOLMAN_HH
#define WCONFIG_VOLMAN_HH

#include "wdefines.h"
#include "wconfig_panel.hh"

class WConfig;

class VolManPanel : public WConfigPanel
{
public:
    VolManPanel( AWindow &basewin, WConfig &baseconfig );
    ~VolManPanel();
    int create();
    int saveValues();

    /* gui elements callback */
    void run( Widget *, const AGMessage &msg ) override;
private:
    StringGadget *m_mount_sg, *m_unmount_sg;
    StringGadget *m_fstab_sg, *m_mtab_sg, *m_part_sg;
    Button *m_mount_flag_b, *m_unmount_flag_b;
    ChooseButton *m_request_action_cb;
    StringGadget *m_eject_sg, *m_closetray_sg;
    Button *m_eject_flag_b, *m_closetray_flag_b;
    CycleButton *m_udisks_version_cyb;
};
 
#endif
