/* nwc_os.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NWC_OS_HH
#define NWC_OS_HH

#include "wdefines.h"

#include <string>

namespace NWC
{
    namespace OS {
        std::string getCWD();

        char *resolveEnv( const char* );

        char *getLinkTarget( const char *symlink );
    }
}

#endif
