/* copyorder.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COPYORDER_HH
#define COPYORDER_HH

#include "wdefines.h"
#include <list>
#include <string>

#include "copy_parameters.hh"

class NM_specialsourceExt;
class CopyOpWin;

struct copyorder {
    copyorder();
    ~copyorder();

    std::string destdir;
    enum { COPY_ALLENTRIES, COPY_ONLYACTIVE, COPY_SPECIAL } source;
    std::list<NM_specialsourceExt*> sources;
    // modes:
    bool move; // true = move, false = copy
    bool follow_symlinks;
    bool do_rename;
    bool preserve_attr;
    bool vdir_preserve_dir_structure;
    enum { COPY_OVERWRITE_NORMAL, COPY_OVERWRITE_ALWAYS, COPY_OVERWRITE_NEVER } overwrite;
    /* _NORMAL: Request what to do
       _ALWAYS: overwrite without request
       _NEVER: no overwriting */

    enum { COPY_ADJUST_SYMLINK_NEVER,
           COPY_ADJUST_SYMLINK_OUTSIDE,
           COPY_ADJUST_SYMLINK_ALWAYS
    } adjust_relative_symlinks;

    CopyParams::EnsureFilePermissions ensure_file_permissions;

    CopyOpWin *cowin;

    int error;  // for returning an error
    bool ignoreLosedAttr;  // true when the user don't want to get a requester
    // when SUID/SGID bit couldn't be restored

    bool external_sources = false;
};

#endif
