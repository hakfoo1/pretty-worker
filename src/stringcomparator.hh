/* stringcomparator.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2011-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRINGCOMPARATOR_HH
#define STRINGCOMPARATOR_HH

#include "wdefines.h"
#include <string>

class StringComparator
{
public:
    enum string_comparator_modes {
        STRING_COMPARE_REGULAR,      // use strcmp
        STRING_COMPARE_VERSION,      // use strvercmp
        STRING_COMPARE_NOCASE,       // use strcasecmp with fallback
                                     // to strcmp for equal strings to
                                     // allow stable sorts
        STRING_COMPARE_STRICT_NOCASE // use strcasecmp only
    };
    
    static void setStringCompareMode( enum string_comparator_modes nv );
    static enum string_comparator_modes getStringCompareMode();

    static bool is_equal( const std::string &s1,
                          const std::string &s2 );
    static int compare( const std::string &s1,
                        const std::string &s2 );
    static int compare( const std::string &s1,
                        const std::string &s2,
                        enum string_comparator_modes mode );
    static bool is_equal( const char *s1,
                          const char *s2 );
    static int compare( const char *s1,
                        const char *s2 );
    static int compare( const char *s1,
                        const char *s2,
                        enum string_comparator_modes mode );
private:
    static enum string_comparator_modes sm_use_mode;
};

#endif
