/* worker_locale.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WORKER_LOCALE_H
#define WORKER_LOCALE_H

#include "wdefines.h"

class Datei;

class WorkerLocale {
public:
  WorkerLocale();
  ~WorkerLocale();
  WorkerLocale( const WorkerLocale &other );
  WorkerLocale &operator=( const WorkerLocale &other );

  int loadLanguage(const char*);
  void resetLanguage();
  void freeLanguage();
  const char *getLocale(int);
  const char *getLocaleFlag(int);
private:
  char **catalog;
  char **catalogflag;
  
  void initBuiltinLang();
  void parse(Datei*,int);
  int openCatFile(Datei*file,const char *name,const char *ext);
};

extern WorkerLocale catalog;

#endif

