/* wconfig_lister.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_lister.hh"
#include "wconfig.h"
#include "worker.h"
#include "aguix/acontainerbb.h"
#include "aguix/fieldlistview.h"
#include "aguix/choosebutton.h"
#include "aguix/stringgadget.h"
#include "worker_locale.h"

ListerPanel::ListerPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

ListerPanel::~ListerPanel()
{
}

int ListerPanel::create()
{
  Panel::create();

  char tstr[ A_BYTESFORNUMBER( int ) ];
  const int cmwmh = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW;

  // global container 1x1
  AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
  ac1->setMaxSpace( 5 );
  ac1->setBorderWidth( 5 );

  addMultiLineText( catalog.getLocale( 675 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  // sub container 2x2 for lister config
  AContainer *ac2 = ac1->add( new AContainer( this, 2, 2 ), 0, 1 );
  ac2->setMaxSpace( 5 );
  ac2->setBorderWidth( 0 );
  
  ac2->add( new Text( _aguix, 0, 0, catalog.getLocale( 19 ) ), 0, 0, AContainer::CO_INCWNR );
  ac2->add( new Text( _aguix, 0, 0, catalog.getLocale( 20 ) ), 1, 0, AContainer::CO_INCWNR );

  AContainerBB *ac21 = (AContainerBB*)ac2->add( new AContainerBB( this, 1, 7 ), 0, 1 );
  AContainerBB *ac22 = (AContainerBB*)ac2->add( new AContainerBB( this, 1, 7 ), 1, 1 );
  
  ac21->setMaxSpace( 5 );
  ac22->setMaxSpace( 5 );
  
  lv1 = (FieldListView*)ac21->add( new FieldListView( _aguix,
                                                      0,
                                                      0,
                                                      100,
                                                      80,
                                                      0 ), 0, 0, cmwmh );
  ac21->add( new Text( _aguix, 0, 0, catalog.getLocale( 21 ) ), 0, 1, AContainer::CO_INCWNR );

  AContainer *ac21a = ac21->add( new AContainer( this, 1, 2 ), 0, 2 );
  ac21a->setMaxSpace( 5 );

  cb1 = (ChooseButton*)ac21a->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     20,
                                                     20,
                                                     ( _baseconfig.getHBarTop( 0 ) == true ) ? 1 : 0,
                                                     catalog.getLocale( 22 ),
                                                     LABEL_LEFT,
                                                     0 ), 0, 0, AContainer::CO_INCWNR );
  cb1->connect( this );
  AContainer *ac211 = ac21a->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac211->add( new Text( _aguix, 0, 0, catalog.getLocale( 23 ) ), 0, 0, AContainer::CO_FIX );
  sprintf( tstr, "%d", (int)_baseconfig.getHBarHeight( 0 ) );
  sg1 = (StringGadget*)ac211->add( new StringGadget( _aguix,
                                                     0,
                                                     0,
                                                     50,
                                                     tstr,
                                                     0 ), 1, 0, AContainer::CO_INCW );
  sg1->connect( this );
  ac211->setBorderWidth( 0 );
  ac211->setMaxSpace( 5 );

  ac21->add( new Text( _aguix, 0, 0, catalog.getLocale( 24 ) ), 0, 3, AContainer::CO_INCWNR );

  AContainer *ac21b = ac21->add( new AContainer( this, 1, 2 ), 0, 4 );
  ac21b->setMaxSpace( 5 );

  cb2 = (ChooseButton*)ac21b->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     20,
                                                     20,
                                                     ( _baseconfig.getVBarLeft( 0 ) == true ) ? 1 : 0,
                                                     catalog.getLocale( 25 ),
                                                     LABEL_LEFT,
                                                     0 ), 0, 0, AContainer::CO_INCWNR );
  cb2->connect( this );
  AContainer *ac212 = ac21b->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac212->add( new Text( _aguix, 0, 0, catalog.getLocale( 26 ) ), 0, 0, AContainer::CO_FIX );
  sprintf( tstr, "%d", (int)_baseconfig.getVBarWidth( 0 ));
  sg2 = (StringGadget*)ac212->add( new StringGadget( _aguix,
                                                     0,
                                                     0,
                                                     50,
                                                     tstr,
                                                     0 ), 1, 0, AContainer::CO_INCW );
  sg2->connect( this );
  ac212->setBorderWidth( 0 );
  ac212->setMaxSpace( 5 );
  
  cb13 = (ChooseButton*)ac21->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     20,
                                                     20,
                                                     ( _baseconfig.getShowHeader( 0 ) == true ) ? 1 : 0,
                                                     catalog.getLocale( 554 ),
                                                     LABEL_LEFT,
                                                     0 ), 0, 5, AContainer::CO_INCWNR );
  cb13->connect( this );

  m_left_path_top_cb = ac21->addWidget( new ChooseButton( _aguix,
                                                          0,
                                                          0,
                                                          20,
                                                          20,
                                                          ( _baseconfig.getPathEntryOnTop( 0 ) == true ) ? 1 : 0,
                                                          catalog.getLocale( 1041 ),
                                                          LABEL_LEFT,
                                                          0 ), 0, 6, AContainer::CO_INCWNR );
  m_left_path_top_cb->connect( this );
  
  lv2 = (FieldListView*)ac22->add( new FieldListView( _aguix,
                                                      0,
                                                      0,
                                                      100,
                                                      80,
                                                      0 ), 0, 0, cmwmh );
  ac22->add( new Text( _aguix, 0, 0, catalog.getLocale( 21 ) ), 0, 1, AContainer::CO_INCWNR );

  AContainer *ac22a = ac22->add( new AContainer( this, 1, 2 ), 0, 2 );
  ac22a->setMaxSpace( 5 );

  cb3 = (ChooseButton*)ac22a->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     20,
                                                     20,
                                                     ( _baseconfig.getHBarTop( 1 ) == true ) ? 1 : 0,
                                                     catalog.getLocale( 22 ),
                                                     LABEL_LEFT,
                                                     0 ), 0, 0, AContainer::CO_INCWNR );
  cb3->connect( this );
  AContainer *ac221 = ac22a->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac221->add( new Text( _aguix, 0, 0, catalog.getLocale( 23 ) ), 0, 0, AContainer::CO_FIX );
  sprintf( tstr, "%d", (int)_baseconfig.getHBarHeight( 1 ) );
  sg3 = (StringGadget*)ac221->add( new StringGadget( _aguix,
                                                     0,
                                                     0,
                                                     50,
                                                     tstr,
                                                     0 ), 1, 0, AContainer::CO_INCW );
  sg3->connect( this );
  ac221->setBorderWidth( 0 );
  ac221->setMaxSpace( 5 );

  ac22->add( new Text( _aguix, 0, 0, catalog.getLocale( 24 ) ), 0, 3, AContainer::CO_INCWNR );

  AContainer *ac22b = ac22->add( new AContainer( this, 1, 2 ), 0, 4 );
  ac22b->setMaxSpace( 5 );

  cb4 = (ChooseButton*)ac22b->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     20,
                                                     20,
                                                     ( _baseconfig.getVBarLeft( 1 ) == true ) ? 1 : 0,
                                                     catalog.getLocale( 25 ),
                                                     LABEL_LEFT,
                                                     0 ), 0, 0, AContainer::CO_INCWNR );
  cb4->connect( this );
  AContainer *ac222 = ac22b->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac222->add( new Text( _aguix, 0, 0, catalog.getLocale( 26 ) ), 0, 0, AContainer::CO_FIX );
  sprintf( tstr, "%d", (int)_baseconfig.getVBarWidth( 1 ) );
  sg4 = (StringGadget*)ac222->add( new StringGadget( _aguix,
                                                     0,
                                                     0,
                                                     50,
                                                     tstr,
                                                     0 ), 1, 0, AContainer::CO_INCW );
  sg4->connect( this );
  ac222->setBorderWidth( 0 );
  ac222->setMaxSpace( 5 );

  cb23 = (ChooseButton*)ac22->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     20,
                                                     20,
                                                     ( _baseconfig.getShowHeader( 1 ) == true ) ? 1 : 0,
                                                     catalog.getLocale( 554 ),
                                                     LABEL_LEFT,
                                                     0 ), 0, 5, AContainer::CO_INCWNR );
  cb23->connect( this );
  
  m_right_path_top_cb = ac22->addWidget( new ChooseButton( _aguix,
                                                           0,
                                                           0,
                                                           20,
                                                           20,
                                                           ( _baseconfig.getPathEntryOnTop( 1 ) == true ) ? 1 : 0,
                                                           catalog.getLocale( 1041 ),
                                                           LABEL_LEFT,
                                                           0 ), 0, 6, AContainer::CO_INCWNR );
  m_right_path_top_cb->connect( this );
  
  lv1->setHBarState( ( _baseconfig.getHBarTop( 0 ) == true ) ? 1 : 2 );
  lv1->setVBarState( ( _baseconfig.getVBarLeft( 0 ) == true ) ? 1 : 2 );
  lv1->setHBarHeight( _baseconfig.getHBarHeight( 0 ) );
  lv1->setVBarWidth( _baseconfig.getVBarWidth( 0 ) );
  lv1->setShowHeader( _baseconfig.getShowHeader( 0 ) );
  lv1->setFieldText( 0, "Header" );
  lv1->addRow();
  lv1->setText( 0, 0, "                    " );
  lv1->redraw();

  lv2->setHBarState( ( _baseconfig.getHBarTop( 1 ) == true ) ? 1 : 2 );
  lv2->setVBarState( ( _baseconfig.getVBarLeft( 1 ) == true ) ? 1 : 2 );
  lv2->setHBarHeight( _baseconfig.getHBarHeight( 1 ) );
  lv2->setVBarWidth( _baseconfig.getVBarWidth( 1 ) );
  lv2->setShowHeader( _baseconfig.getShowHeader( 1 ) );
  lv2->setFieldText( 0, "Header" );
  lv2->addRow();
  lv2->setText( 0, 0, "                    " );
  lv2->redraw();

  contMaximize( true );
  return 0;
}

int ListerPanel::saveValues()
{
  _baseconfig.setHBarTop( 0, cb1->getState() );
  _baseconfig.setVBarLeft( 0, cb2->getState() );
  _baseconfig.setHBarTop( 1, cb3->getState() );
  _baseconfig.setVBarLeft( 1, cb4->getState() );
  _baseconfig.setShowHeader( 0, cb13->getState() );
  _baseconfig.setShowHeader( 1, cb23->getState() );

  _baseconfig.setPathEntryOnTop( 0, m_left_path_top_cb->getState() );
  _baseconfig.setPathEntryOnTop( 1, m_right_path_top_cb->getState() );

  int i = 0;
  sscanf( sg1->getText(), "%d", &i );
  if ( i < 5 ) i = 5;
  _baseconfig.setHBarHeight( 0, i );
  sscanf( sg2->getText(), "%d", &i );
  if ( i < 5 ) i = 5;
  _baseconfig.setVBarWidth( 0, i );
  sscanf( sg3->getText(), "%d", &i );
  if ( i < 5 ) i = 5;
  _baseconfig.setHBarHeight( 1, i );
  sscanf( sg4->getText(), "%d", &i );
  if ( i < 5 ) i = 5;
  _baseconfig.setVBarWidth( 1, i );
  return 0;
}

void ListerPanel::run( Widget *elem, const AGMessage &msg )
{
  char tstr[ A_BYTESFORNUMBER( int ) ];

  if ( msg.type == AG_CHOOSECLICKED ) {
    if ( msg.choose.button == cb1 ) {
      if ( msg.choose.state == true ) lv1->setHBarState( 1 );
      else lv1->setHBarState( 2 );
    } else if ( msg.choose.button == cb2 ) {
      if ( msg.choose.state == true ) lv1->setVBarState( 1 );
      else lv1->setVBarState( 2 );
    } else if ( msg.choose.button == cb3 ) {
      if ( msg.choose.state == true ) lv2->setHBarState( 1 );
      else lv2->setHBarState( 2 );
    } else if ( msg.choose.button == cb4 ) {
      if ( msg.choose.state == true ) lv2->setVBarState( 1 );
      else lv2->setVBarState( 2 );
    } else if ( msg.choose.button == cb13 ) {
      if ( msg.choose.state == true ) lv1->setShowHeader( true );
      else lv1->setShowHeader( false );
    } else if ( msg.choose.button == cb23 ) {
      if ( msg.choose.state == true ) lv2->setShowHeader( true );
      else lv2->setShowHeader( false );
    }
  } else if ( msg.type == AG_STRINGGADGET_DEACTIVATE ) {
    int i = 0;
    if ( msg.stringgadget.sg == sg1 ) {
      sscanf( sg1->getText(), "%d", &i );
      if ( i < 5 ) {
        i = 5;
        sprintf( tstr, "%d", i );
        sg1->setText( tstr );
      }
      lv1->setHBarHeight( i );
    } else if ( msg.stringgadget.sg == sg2 ) {
      sscanf( sg2->getText(), "%d", &i );
      if ( i < 5 ) {
        i = 5;
        sprintf( tstr, "%d", i );
        sg2->setText( tstr );
      }
      lv1->setVBarWidth( i );
    } else if ( msg.stringgadget.sg == sg3 ) {
      sscanf( sg3->getText(), "%d", &i );
      if ( i < 5 ) {
        i = 5;
        sprintf( tstr, "%d", i );
        sg3->setText( tstr );
      }
      lv2->setHBarHeight( i );
    } else if ( msg.stringgadget.sg == sg4 ) {
      sscanf( sg4->getText(), "%d", &i );
      if ( i < 5 ) {
        i = 5;
        sprintf( tstr, "%d", i );
        sg4->setText( tstr );
      }
      lv2->setVBarWidth( i );
    }
  }
}
