/* shm.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2001-2004 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: worker_shm.cc,v 1.6 2005/01/12 20:37:38 ralf Exp $ */

#include <worker_shm.h>
#ifdef HAVE_SHM
#include <sys/shm.h>
#include <sys/ipc.h>
#endif

void WorkerSHM::initSHM()
{
#ifdef HAVE_SHM
  if ( WSHMInit == false ) {
    WSHMId = shmget( IPC_PRIVATE, /*a_max( SHMMIN, 4 )*/4, 0700 );
    if ( WSHMId != -1 ) {
      WSHMPtr = (char*)shmat( WSHMId, NULL, 0 );
      if ( ( WSHMPtr != (char*)-1 ) && ( WSHMPtr != NULL ) ) {
        WSHMInit = true;
      }
    }
  }
#endif
}

void WorkerSHM::freeSHM()
{
  if ( WSHMInit == true ) {
#ifdef HAVE_SHM
    shmdt( WSHMPtr );
#endif
    WSHMInit = false;
  }
}

bool WorkerSHM::WSHMInit = false;
int WorkerSHM::WSHMId = -1;
char *WorkerSHM::WSHMPtr = NULL;

