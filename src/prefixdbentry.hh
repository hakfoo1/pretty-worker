/* prefixdbentry.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PREFIXDBENTRY_HH
#define PREFIXDBENTRY_HH

#include "wdefines.h"
#include <string>
#include <list>
#include <vector>
#include <utility>
#include <tuple>

class Datei;

class PrefixDBEntry
{
public:
    PrefixDBEntry( const std::string &prefix );
    virtual ~PrefixDBEntry();

    bool operator<( const PrefixDBEntry &rhs ) const;
    void storeAccess( const std::string &value, time_t last_use );
    std::string getBestHit() const;
    std::string getBestHit( float &count ) const;
    std::string getBestHit( float &count, time_t &last_use ) const;
    void age( float factor );

    const std::string &getPrefix() const;
    const std::list< std::tuple< std::string, float, time_t > > &getStrings() const;

    void write( Datei &fh ) const;
    bool read( FILE *fp );
private:
    std::string m_prefix;
    std::list< std::tuple< std::string, float, time_t > > m_strings;
};

#endif /* PREFIXDBENTRY_HH */
