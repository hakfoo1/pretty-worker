/* exprfilter_eval.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef EXPRFILTER_EVAL_HH
#define EXPRFILTER_EVAL_HH

#include "wdefines.h"
#include <memory>
#include <list>

class ExprFilterEvalAtom;
class NWCEntrySelectionState;

class ExprFilterEval
{
public:
    ExprFilterEval() : m_error_occured( false )
    {}
    
    // this method adds an atom during parsing phase
    void pushAtom( std::shared_ptr< ExprFilterEvalAtom > atom );

    // the following methods are used for actual evaluation
    bool popValue();
    void pushValue( bool v );

    bool eval( NWCEntrySelectionState &e );

    void setErrorOccured();
    bool getErrorOccured() const;

    template<class T> bool contains() const
    {
        for ( auto &a : m_atoms ) {
            if ( a && dynamic_cast<T*>( a.get() ) ) return true;
        }

        return false;
    }
private:
    // atoms for evaluation
    std::list< std::shared_ptr< ExprFilterEvalAtom > > m_atoms;

    // result stack
    std::list< bool > m_eval_values;

    bool m_error_occured;
};

#endif
