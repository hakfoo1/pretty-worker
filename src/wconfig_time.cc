/* wconfig_time.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_time.hh"
#include "wconfig.h"
#include "worker.h"
#include "aguix/acontainerbb.h"
#include "aguix/cyclebutton.h"
#include "aguix/choosebutton.h"
#include "aguix/stringgadget.h"
#include "worker_locale.h"

TimePanel::TimePanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

TimePanel::~TimePanel()
{
}

int TimePanel::create()
{
  Panel::create();

  int i;
  char buf[128];
  time_t curtime;
  struct tm *loctime;
  
  AContainer *ac1 = setContainer( new AContainer( this, 1, 6 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 680 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  ac1->add( new Text( _aguix, 0, 0, catalog.getLocale( 440 ) ),
            0, 1, AContainer::CO_INCWNR );
  
  AContainer *ac1_2 = ac1->add( new AContainerBB( this, 1, 3 ), 0, 2 );
  ac1_2->setBorderWidth( 5 );
  ac1_2->setMinSpace( 0 );
  ac1_2->setMaxSpace( 0 );
  
  cyc1 = (CycleButton*)ac1_2->add( new CycleButton( _aguix, 0, 0, 100, 0 ),
                                   0, 0, AContainer::CO_INCW );
  cyc1->connect( this );
  
  swin1 = new AWindow( _aguix, 0, 0, 10, 10, "" );
  ac1_2->add( swin1, 0, 1, AContainer::CO_INCW );
  swin1->create();

  AContainer *ac1_swin1 = swin1->setContainer( new AContainer( swin1, 2, 1 ), true );
  ac1_swin1->setBorderWidth( 5 );
  ac1_swin1->setMinSpace( 0 );
  ac1_swin1->setMaxSpace( 0 );

  ac1_swin1->add( new Text( _aguix, 0, 0, catalog.getLocale( 441 ) ), 0, 0, AContainer::CO_FIX );
  sg1 = (StringGadget*)ac1_swin1->add( new StringGadget( _aguix, 0, 0, 200, _baseconfig.getDateFormatString(), 0 ), 1, 0, AContainer::CO_INCW );
  swin1->contMaximize( true );
  ac1_2->readLimits();
  swin1->show();
  
  cb1 = (ChooseButton*)ac1_2->add( new ChooseButton( _aguix,
                                                     0,
                                                     0,
                                                     _baseconfig.getDateSubst(),
                                                     catalog.getLocale( 442 ),
                                                     LABEL_LEFT,
                                                     0 ),
                                   0, 2, AContainer::CO_INCWNR );
  
  curtime = time( NULL );
  loctime = localtime( &curtime );
  for ( i = 0; i < WConfig::getNrOfPreDates(); i++ ) {
    strftime( buf, 127, WConfig::getPreDate( i ), loctime );
    cyc1->addOption( buf );
  }
  cyc1->addOption( catalog.getLocale( 443 ) );
  
  ac1->add( new Text( _aguix, 0, 0, catalog.getLocale( 444 ) ),
            0, 3, AContainer::CO_INCW );
  
  AContainer *ac1_4 = ac1->add( new AContainerBB( this, 1, 2 ), 0, 4 );
  ac1_4->setBorderWidth( 5 );
  ac1_4->setMinSpace( 0 );
  ac1_4->setMaxSpace( 0 );
  
  cyc2 = (CycleButton*)ac1_4->add( new CycleButton( _aguix, 0, 0, 100, 0 ),
                                   0, 0, AContainer::CO_INCW );
  cyc2->connect( this );
  
  swin2 = new AWindow( _aguix, 0, 0, 10, 10, "" );
  ac1_4->add( swin2, 0, 1, AContainer::CO_INCW );
  swin2->create();

  AContainer *ac1_swin2 = swin2->setContainer( new AContainer( swin2, 2, 1 ), true );
  ac1_swin2->setBorderWidth( 5 );
  ac1_swin2->setMinSpace( 0 );
  ac1_swin2->setMaxSpace( 0 );

  ac1_swin2->add( new Text( _aguix, 0, 0, catalog.getLocale( 441 ) ), 0, 0, AContainer::CO_FIX );
  sg2 = (StringGadget*)ac1_swin2->add( new StringGadget( _aguix, 0, 0, 200, _baseconfig.getTimeFormatString(), 0 ),
                                       1, 0, AContainer::CO_INCW );
  swin2->contMaximize( true );
  ac1_4->readLimits();
  swin2->show();
  
  for ( i = 0; i < WConfig::getNrOfPreTimes(); i++ ) {
    strftime( buf, 127, WConfig::getPreTime( i ), loctime );
    cyc2->addOption( buf );
  }
  cyc2->addOption( catalog.getLocale( 443 ) );
  
  cb2 = (ChooseButton*)ac1->add( new ChooseButton( _aguix,
                                                 0,
                                                 0,
                                                 _baseconfig.getDateBeforeTime(),
                                                 catalog.getLocale( 445),
                                                 LABEL_LEFT,
                                                 0 ),
                               0, 5, AContainer::CO_INCWNR );

  if ( _baseconfig.getDateFormat() == -1 ) {
    cyc1->setOption( WConfig::getNrOfPreDates() );
  } else {
    if ( ( _baseconfig.getDateFormat() >= 0 ) && ( _baseconfig.getDateFormat() < WConfig::getNrOfPreDates() ) ) {
      cyc1->setOption( _baseconfig.getDateFormat() );
    } else {
      cyc1->setOption( 0 );
    }
    swin1->hide();
  }
  if ( _baseconfig.getTimeFormat() == -1 ) {
    cyc2->setOption( WConfig::getNrOfPreTimes() );
  } else {
    if ( ( _baseconfig.getTimeFormat() >= 0 ) && ( _baseconfig.getTimeFormat() < WConfig::getNrOfPreTimes() ) ) {
      cyc2->setOption( _baseconfig.getTimeFormat() );
    } else {
      cyc2->setOption( 0 );
    }
    swin2->hide();
  }

  cyc1->resize( cyc1->getMaxSize(), cyc1->getHeight() );
  ac1_2->readLimits();
  cyc2->resize( cyc2->getMaxSize(), cyc2->getHeight() );
  ac1_4->readLimits();

  contMaximize( true );
  return 0;
}

int TimePanel::saveValues()
{
  int i;
  
  i = cyc1->getSelectedOption();
  if ( i == WConfig::getNrOfPreDates() ) {
    _baseconfig.setDateFormat( -1 );
    _baseconfig.setDateFormatString( sg1->getText() );
  } else {
    _baseconfig.setDateFormat( i );
  }
  _baseconfig.setDateSubst( cb1->getState() );
  
  i = cyc2->getSelectedOption();
  if ( i == WConfig::getNrOfPreTimes() ) {
    _baseconfig.setTimeFormat( -1 );
    _baseconfig.setTimeFormatString( sg2->getText() );
  } else {
    _baseconfig.setTimeFormat( i );
  }
  _baseconfig.setDateBeforeTime( cb2->getState() );
  
  return 0;
}

void TimePanel::run( Widget *elem, const AGMessage &msg )
{
  if ( msg.type == AG_CYCLEBUTTONCLICKED ) {
    if ( msg.cyclebutton.cyclebutton == cyc1 ) {
      if ( msg.cyclebutton.option == WConfig::getNrOfPreDates() )
        swin1->show();
      else
        swin1->hide();
    } else if ( msg.cyclebutton.cyclebutton == cyc2 ) {
      if ( msg.cyclebutton.option == WConfig::getNrOfPreTimes() )
        swin2->show();
      else
        swin2->hide();
    }
  }
}
