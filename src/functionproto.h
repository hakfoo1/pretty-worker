/* functionproto.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FUNCTIONPROTO_H
#define FUNCTIONPROTO_H

#include "wdefines.h"
#include "aguix/refcount.hh"
#include <string>
#include "nwc_virtualdir.hh"
#include <memory>
#include <vector>

class Datei;
class Worker;
class AGUIX;
class Lister;
class WPUContext;
class List;
class FunctionProto;

typedef std::vector< std::shared_ptr< FunctionProto > > command_list_t;

class ActionDescr
{
 public:
    ActionDescr() {}
    virtual ~ActionDescr() {}

    virtual command_list_t get_action_list( class WCFiletype *ft ) const = 0;
};

class ActionMessage
{
public:
  ActionMessage( Worker *);
  ~ActionMessage();
  ActionMessage( const ActionMessage &other );
  ActionMessage &operator=( const ActionMessage &other );

  typedef enum { AM_MODE_NORMAL,
                 AM_MODE_ONLYACTIVE,
                 AM_MODE_DNDACTION,
                 AM_MODE_SPECIAL,
                 AM_MODE_SPECIFIC_ENTRIES,
                 AM_MODE_ON_DEMAND } am_mode_t;
  am_mode_t mode;
  Lister *startLister;
  const class DNDMsg *dndmsg;
  class WCFiletype *filetype;
  RefCount<ActionDescr> m_action_descr;
  
  Worker *getWorker() const;
  const class FileEntry *getFE() const;
  void setFE( const class FileEntry *tfe );
  void setKeyAction( bool nv );
  bool getKeyAction() const;

    void setEntriesToConsider( RefCount< NWC::VirtualDir > entries );
    RefCount< NWC::VirtualDir > getEntriesToConsider();
protected:
  Worker *worker;
  class FileEntry *fe;
  bool m_key_action;
    RefCount< NWC::VirtualDir > m_entries_to_consider;
};

class FunctionProto
{
public:
  FunctionProto();
  virtual ~FunctionProto();
  FunctionProto( const FunctionProto &other );
  FunctionProto &operator=( const FunctionProto &other );

  virtual int configure();
  virtual FunctionProto *duplicate() const;
  virtual bool isName(const char *);
  virtual const char *getName();
  virtual bool save(Datei *);
  virtual int run( std::shared_ptr< WPUContext >, ActionMessage * );
  virtual const char *getDescription();
  
  static int presave(Datei*,FunctionProto*);
  int configureWhenAvail();

  typedef enum {
      CAT_FILETYPE,
      CAT_CURSOR,
      CAT_FILELIST,
      CAT_FILEOPS,
      CAT_SELECTIONS,
      CAT_SETTINGS,
      CAT_SCRIPTING,
      CAT_OTHER
  } command_categories_t;

  static std::string getCategoryName( command_categories_t category );

  virtual command_categories_t getCategory() const;

    virtual std::string getStringRepresentation();

    virtual bool isInteractiveRun() const;
    virtual void setInteractiveRun();

    virtual bool requestParameters() const;
    virtual void setRequestParameters( bool nv );
protected:
  static const char *name;
  bool hasConfigure;
  bool m_request_parameters = false;
  command_categories_t m_category;
};

void freecoms(List*);

std::string getStringReprForList( const command_list_t &coms );

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
