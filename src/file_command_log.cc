/* file_command_log.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2021-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "file_command_log.hh"

FileCommandLog::FileCommandLog( size_t limit ) :
    m_limit( limit )
{
}

void FileCommandLog::push( const std::string &command,
                           const std::string &path,
                           const std::string &src,
                           const std::string &description )
{
    while ( m_log.size() >= m_limit ) {
        m_log.pop_back();
    }

    m_log.push_front( Entry{ command, path, src, description, time( NULL ) } );
}

const std::list< FileCommandLog::Entry > &FileCommandLog::entries() const
{
    return m_log;
}

void FileCommandLog::clear()
{
    m_log.clear();
}
