/* nmrowdata.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2003-2007 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NMROWDATA_H
#define NMROWDATA_H

#include "wdefines.h"
#include "aguix/fieldlvrowdata.h"

class FileEntry;

class NMRowData : public FieldLVRowData
{
public:
  NMRowData();
  NMRowData( int trow, const FileEntry *tfe, const char *tfullname );
  ~NMRowData();
  NMRowData( const NMRowData &other );
  NMRowData &operator=( const NMRowData &other );

  NMRowData *duplicate() const;
  int getRow() const;
  const FileEntry *getFE() const;
  const char *getFullname() const;
private:
  int row;
  FileEntry *fe;
  char *fullname;
};

#endif
