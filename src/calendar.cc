/* calendar.cc
 * This file belongs to Worker, a file manager for UNIX/X11.
 * Copyright (C) 2008-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "calendar.hh"
#include <string>
#include <sstream>
#include <iomanip>
#include "aguix/awindow.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "worker.h"
#include "worker_locale.h"

Calendar::Calendar( bool extended ) : m_extended( extended )
{
    time_t now = time( NULL );
    localtime_r( &now, &m_initial_date );
    m_current_date = m_initial_date;
    m_today = m_initial_date;

    for ( int week = 0; week < 6; week++ ) {
        for ( int day = 0; day < 7; day++ ) {
            m_day_buttons[week][day] = NULL;
        }
    }
}

Calendar::~Calendar()
{
}

void Calendar::setInitialDate( time_t t )
{
    localtime_r( &t, &m_initial_date );
}

void Calendar::setInitialDate( const std::string &t )
{
    struct tm t1 = { 0 };
    time_t ct1;
    int year = -1, month = -1, day = -1;
    int hour = -1, minute = -1, second = -1;

    //TODO use strptime if available
    //fill struct tm with negativ dst and call mktime
    
    t1.tm_sec = 0;
    t1.tm_min = 0;
    t1.tm_hour = 0;
    t1.tm_isdst = -1;

    if ( sscanf( t.c_str(), "%d-%d-%d %d:%d:%d",
                 &year, &month, &day,
                 &hour, &minute, &second ) == 6 ) {
        t1.tm_sec = second;
        t1.tm_min = minute;
        t1.tm_hour = hour;
        t1.tm_mday = day;
        t1.tm_mon = month - 1;
        t1.tm_year = year - 1900;
        
        ct1 = mktime( &t1 );
        setInitialDate( ct1 );
    } else if ( sscanf( t.c_str(), "%d-%d-%d", &year, &month, &day ) == 3 ) {
        t1.tm_mday = day;
        t1.tm_mon = month - 1;
        t1.tm_year = year - 1900;
        
        ct1 = mktime( &t1 );
        setInitialDate( ct1 );
    }
}

bool Calendar::showCalendar()
{
    AGUIX *aguix = Worker::getAGUIX();
    AGMessage *agmsg;
    AWindow *win;
    int endmode = -1;
    std::string str1;

    m_current_date = m_initial_date;

    win = new AWindow( aguix, 10, 10, 10, 10, catalog.getLocale( 819 ), AWindow::AWINDOW_DIALOG );
    win->create();
    
    AContainer *ac1 = win->setContainer( new AContainer( win, 1, m_extended ? 6 : 4 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );
    
    AContainer *ac1_0 = ac1->add( new AContainer( win, 3, 1 ), 0, 0 );
    ac1_0->setMinSpace( 0 );
    ac1_0->setMaxSpace( 0 );
    ac1_0->setBorderWidth( 0 );
    
    Button *today_b = (Button*)ac1_0->add( new Button( aguix,
                                                       0,
                                                       0,
                                                       catalog.getLocale( 437 ),
                                                       0 ), 1, 0, AContainer::CO_FIX );

    AContainer *ac1_2 = ac1->add( new AContainer( win, 7, 1 ), 0, 1 );
    ac1_2->setMinSpace( 0 );
    ac1_2->setMaxSpace( 0 );
    ac1_2->setBorderWidth( 0 );
    
    ac1_2->add( new Text( aguix,
                          1,
                          0,
                          catalog.getLocale( 1080 ) ),
                0, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );

    Button *prev_year_b = (Button*)ac1_2->add( new Button( aguix,
                                                           0,
                                                           0,
                                                           "<",
                                                           0 ), 2, 0, AContainer::CO_FIX );
    m_year_sg = (StringGadget*)ac1_2->add( new StringGadget( aguix, 0, 0, 100, "", 0 ), 3, 0, AContainer::CO_FIX );
    m_year_sg->resize( aguix->getTextWidth( "99999" ) + 20 , m_year_sg->getHeight() );
    Button *next_year_b = (Button*)ac1_2->add( new Button( aguix,
                                                           0,
                                                           0,
                                                           ">",
                                                           0 ), 4, 0, AContainer::CO_FIX );
    ac1_2->readLimits();
    ac1_2->setMinWidth( ac1_2->getMinWidth( 0, 0 ), 6, 0 );
    ac1_2->setMaxWidth( ac1_2->getMaxWidth( 0, 0 ), 6, 0 );
  
    AContainer *ac1_3 = ac1->add( new AContainer( win, 7, 1 ), 0, 2 );
    ac1_3->setMinSpace( 0 );
    ac1_3->setMaxSpace( 0 );
    ac1_3->setBorderWidth( 0 );
    
    ac1_3->add( new Text( aguix,
                          1,
                          0,
                          catalog.getLocale( 1081 ) ),
                0, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );

    Button *prev_month_b = (Button*)ac1_3->add( new Button( aguix,
                                                            0,
                                                            0,
                                                            "<",
                                                            0 ), 2, 0, AContainer::CO_FIX );
    m_month_sg = (StringGadget*)ac1_3->add( new StringGadget( aguix, 0, 0, 100, "", 0 ), 3, 0, AContainer::CO_FIX );
    m_month_sg->resize( aguix->getTextWidth( "99" ) + 20 , m_month_sg->getHeight() );
    Button *next_month_b = (Button*)ac1_3->add( new Button( aguix,
                                                            0,
                                                            0,
                                                            ">",
                                                            0 ), 4, 0, AContainer::CO_FIX );
    ac1_3->readLimits();
    ac1_3->setMinWidth( ac1_3->getMinWidth( 0, 0 ), 6, 0 );
    ac1_3->setMaxWidth( ac1_3->getMaxWidth( 0, 0 ), 6, 0 );
    
    AContainer *ac1_4 = ac1->add( new AContainer( win, 9, 7 ), 0, 3 );
    ac1_4->setMinSpace( 0 );
    ac1_4->setMaxSpace( 0 );
    ac1_4->setBorderWidth( 0 );

    for ( int week = 0; week < 6; week++ ) {
        for ( int day = 0; day < 7; day++ ) {
            m_day_buttons[week][day] = (Button*)ac1_4->add( new Button( aguix,
                                                                        0,
                                                                        0,
                                                                        "999",
                                                                        0 ), day + 1, week + 1, AContainer::CO_INCW );
        }
    }

    {
        struct tm t1 = m_initial_date;
        for ( int day = 0; day < 7; day++ ) {
            t1.tm_isdst = -1;
            if ( mktime( &t1 ) != (time_t)-1 ) {
                char weekday_str[128];
                
                strftime( weekday_str, 128, "%a", &t1 );
                ac1_4->add( new Text( aguix,
                                      0,
                                      0,
                                      weekday_str
                                      ), ( t1.tm_wday + 7 - 1 ) % 7 + 1, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
            }
            t1.tm_mday++;
        }
    }

    StringGadget *hour_sg = NULL;
    StringGadget *minute_sg = NULL;
    StringGadget *second_sg = NULL;
    Button *okb = NULL;
    Button *cb = NULL;
    
    if ( m_extended ) {
        AContainer *ac1_5 = ac1->add( new AContainer( win, 9, 1 ), 0, 4 );
        ac1_5->setMinSpace( 5 );
        ac1_5->setMaxSpace( 5 );
        ac1_5->setBorderWidth( 0 );

        ac1_5->add( new Text( aguix,
                              1,
                              0,
                              catalog.getLocale( 444 ) ),
                    0, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );

        hour_sg = ac1_5->addWidget( new StringGadget( aguix, 0, 0,
                                                      aguix->getTextWidth( "99" ) + 16,
                                                      AGUIXUtils::formatStringToString( "%02d", m_current_date.tm_hour ).c_str(), 0 ),
                                    2, 0, AContainer::CO_FIX );
        ac1_5->add( new Text( aguix,
                              1,
                              0,
                              ":" ),
                    3, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
        minute_sg = ac1_5->addWidget( new StringGadget( aguix, 0, 0,
                                                        aguix->getTextWidth( "99" ) + 16,
                                                        AGUIXUtils::formatStringToString( "%02d", m_current_date.tm_min ).c_str(), 0 ),
                                      4, 0, AContainer::CO_FIX );
        ac1_5->add( new Text( aguix,
                              1,
                              0,
                              ":" ),
                    5, 0, AContainer::CO_FIXNR + AContainer::ACONT_CENTER );
        second_sg = ac1_5->addWidget( new StringGadget( aguix, 0, 0,
                                                        aguix->getTextWidth( "99" ) + 16,
                                                        AGUIXUtils::formatStringToString( "%02d", m_current_date.tm_sec ).c_str(), 0 ),
                                      6, 0, AContainer::CO_FIX );

        ac1_5->setMinWidth( ac1_5->getMinWidth( 0, 0 ), 8, 0 );
        ac1_5->setMaxWidth( ac1_5->getMaxWidth( 0, 0 ), 8, 0 );

        AContainer *ac1_6 = ac1->add( new AContainer( win, 2, 1 ), 0, 5 );
        ac1_6->setMinSpace( 5 );
        ac1_6->setMaxSpace( -1 );
        ac1_6->setBorderWidth( 0 );
        okb =ac1_6->addWidget( new Button( aguix,
                                           0,
                                           0,
                                           catalog.getLocale( 11 ),
                                           0 ), 0, 0, AContainer::CO_FIX );
        cb = ac1_6->addWidget( new Button( aguix,
                                           0,
                                           0,
                                           catalog.getLocale( 8 ),
                                           0 ), 1, 0, AContainer::CO_FIX );
    }

    win->setDoTabCycling( true );
    win->contMaximize( true, true );

    if ( ! m_extended ) {
        win->useStippleBackground();
    }

    updateCalendar( m_current_date );

    win->show();

    for (; endmode == -1 ; ) {
        agmsg = aguix->WaitMessage( win );
        if ( agmsg != NULL ) {
            switch ( agmsg->type ) {
                case AG_CLOSEWINDOW:
                    if ( agmsg->closewindow.window == win->getWindow() ) endmode = 0;
                    break;
                case AG_BUTTONCLICKED:
                    if ( agmsg->button.button == today_b ) {
                        updateCalendar( m_today );
                    } else if ( agmsg->button.button == prev_year_b ) {
                        struct tm t1 = m_current_date;
                        t1.tm_year--;
                        t1.tm_mday = 1;
                        updateCalendar( t1 );
                    } else if ( agmsg->button.button == next_year_b ) {
                        struct tm t1 = m_current_date;
                        t1.tm_year++;
                        t1.tm_mday = 1;
                        updateCalendar( t1 );
                    } else if ( agmsg->button.button == prev_month_b ) {
                        struct tm t1 = m_current_date;
                        t1.tm_mon--;
                        t1.tm_mday = 1;
                        updateCalendar( t1 );
                    } else if ( agmsg->button.button == next_month_b ) {
                        struct tm t1 = m_current_date;
                        t1.tm_mon++;
                        t1.tm_mday = 1;
                        updateCalendar( t1 );
                    } else if ( m_extended && agmsg->button.button == okb ) {
                        endmode = 1;
                    } else if ( m_extended && agmsg->button.button == cb ) {
                        endmode = 0;
                    } else {
                        for ( int week = 0; week < 6; week++ ) {
                            for ( int day = 0; day < 7; day++ ) {
                                if ( agmsg->button.button == m_day_buttons[week][day] ) {
                                    if ( m_day_buttons[week][day]->getData() != -1 ) {
                                        m_current_date.tm_mday = m_day_buttons[week][day]->getData();
                                        m_current_date.tm_isdst = -1;
                                        if ( mktime( &m_current_date) != (time_t)-1 ) {
                                            if ( m_extended ) {
                                                updateCalendar( m_current_date );
                                            } else {
                                                endmode = 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case AG_STRINGGADGET_OK:
                    if ( agmsg->stringgadget.sg == m_year_sg ) {
                        int year = 0;
                        if ( sscanf( m_year_sg->getText(), "%d", &year ) == 1 ) {
                            struct tm t1 = m_current_date;
                            t1.tm_year = year - 1900;
                            t1.tm_mday = 1;
                            updateCalendar( t1 );
                        }
                    } else if ( agmsg->stringgadget.sg == m_month_sg ) {
                        int month = atoi( m_month_sg->getText() );
                        if ( month >= 1 && month <= 12 ) {
                            struct tm t1 = m_current_date;
                            t1.tm_mon = month - 1;
                            t1.tm_mday = 1;
                            updateCalendar( t1 );
                        }
                    }
                    break;
                case AG_STRINGGADGET_CONTENTCHANGE:
                    if ( m_extended && agmsg->stringgadget.sg == hour_sg ) {
                        int v;

                        if ( AGUIXUtils::convertFromString( hour_sg->getText(),
                                                            v ) ) {
                            if ( v >= 0 && v <= 23 ) {
                                m_current_date.tm_hour = v;
                            }
                        }
                    } else if ( m_extended && agmsg->stringgadget.sg == minute_sg ) {
                        int v;

                        if ( AGUIXUtils::convertFromString( minute_sg->getText(),
                                                            v ) ) {
                            if ( v >= 0 && v <= 59 ) {
                                m_current_date.tm_min = v;
                            }
                        }
                    } else if ( m_extended && agmsg->stringgadget.sg == second_sg ) {
                        int v;

                        if ( AGUIXUtils::convertFromString( second_sg->getText(),
                                                            v ) ) {
                            if ( v >= 0 && v <= 59 ) {
                                m_current_date.tm_sec = v;
                            }
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( agmsg );
        }
    }

    delete win;

    return ( endmode == 1 ) ? true : false;
}

std::string Calendar::getDate() const
{
    std::stringstream d;
    d << m_current_date.tm_year + 1900;
    d << "-" << std::setw( 2 ) << std::setfill( '0' ) << m_current_date.tm_mon + 1;
    d << "-" << std::setw( 2 ) << std::setfill( '0' ) << m_current_date.tm_mday;

    if ( m_extended ) {
        d << " " << std::setw( 2 ) << std::setfill( '0' ) << m_current_date.tm_hour;
        d << ":" << std::setw( 2 ) << std::setfill( '0' ) << m_current_date.tm_min;
        d << ":" << std::setw( 2 ) << std::setfill( '0' ) << m_current_date.tm_sec;
    }
    
    return d.str();
}

void Calendar::updateWindow()
{
    struct tm my_date = m_current_date;
    int offset, j;
    int my_month;

    my_month = my_date.tm_mon;

    my_date.tm_mday = 1;
    my_date.tm_isdst = -1;
    mktime( &my_date );

    offset = ( my_date.tm_wday + 7 - 1 ) % 7;

    for ( j = 0; j < offset; j++ ) {
        m_day_buttons[0][j]->hide();
        m_day_buttons[0][j]->setData( -1 );
    }

    for ( int day = 1; day < 32; day++ ) {
        std::stringstream dstr;
        dstr << day;

        my_date.tm_mday = day;
        my_date.tm_isdst = -1;
        if ( mktime( &my_date ) == (time_t)-1 ) break;
        if ( my_date.tm_mon != my_month ) break;

        m_day_buttons[offset / 7][offset % 7]->show();
        m_day_buttons[offset / 7][offset % 7]->setText( 0, dstr.str().c_str() );
        m_day_buttons[offset / 7][offset % 7]->setData( day );

        if ( m_current_date.tm_year == my_date.tm_year &&
             m_current_date.tm_mon == my_date.tm_mon &&
             m_current_date.tm_mday == my_date.tm_mday ) {
            m_day_buttons[offset / 7][offset % 7]->setFG( 0, 2 );
            m_day_buttons[offset / 7][offset % 7]->setBG( 0, 1 );
        } else {
            m_day_buttons[offset / 7][offset % 7]->setFG( 0, 1 );
            m_day_buttons[offset / 7][offset % 7]->setBG( 0, 0 );
        }

        offset++;
    }

    for ( ; offset < 6 * 7; offset++ ) {
        m_day_buttons[offset / 7][offset % 7]->hide();
        m_day_buttons[offset / 7][offset % 7]->setData( -1 );
    }
}

void Calendar::updateCalendar( const struct tm &new_time )
{
    struct tm t1 = new_time;

    t1.tm_isdst = -1;
    if ( mktime( &t1 ) != (time_t)-1 ) {
        m_current_date = t1;
    }

    std::stringstream dstr;

    dstr << m_current_date.tm_year + 1900;
    m_year_sg->setText( dstr.str().c_str() );

    dstr.str( "" );

    dstr << m_current_date.tm_mon + 1;
    m_month_sg->setText( dstr.str().c_str() );

    updateWindow();
}
