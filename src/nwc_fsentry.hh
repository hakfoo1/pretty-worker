/* nwc_fsentry.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NWC_FSENTRY_HH
#define NWC_FSENTRY_HH

#include "wdefines.h"

#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

namespace NWC
{
  
  class FSEntry
  {
  public:
    FSEntry( const std::string &fullname );
    virtual ~FSEntry();
    FSEntry( const FSEntry &other );
    FSEntry &operator=( const FSEntry &other );

    virtual bool isDir( bool follow_symlinks = false ) const;
    virtual bool isHiddenEntry() const;
    virtual const std::string &getFullname() const;
    virtual const std::string &getBasename() const;
    virtual std::string getDirname() const;
    virtual std::string getExtension() const;
    virtual bool entryExists() const;
    virtual FSEntry *clone() const;

    virtual bool isReg( bool follow_symlinks = false ) const;
    
    virtual bool isLink() const;
    virtual bool isBrokenLink() const;
    virtual bool isExe( bool fastTest = false ) const;
    virtual bool getPermissionString( std::string &res ) const;
    virtual bool getDestination( std::string &res ) const;

    virtual loff_t stat_size() const;
    virtual loff_t stat_dest_size() const;
    virtual dev_t stat_dev() const;
    virtual dev_t stat_dest_dev() const;
    virtual ino_t stat_inode() const;
    virtual ino_t stat_dest_inode() const;
    virtual time_t stat_lastmod() const;
    virtual time_t stat_dest_lastmod() const;
    virtual dev_t stat_rdev() const;
    virtual dev_t stat_dest_rdev() const;
    virtual mode_t stat_mode() const;
    virtual mode_t stat_dest_mode() const;
    virtual time_t stat_lastaccess() const;
    virtual time_t stat_dest_lastaccess() const;
    virtual time_t stat_lastchange() const;
    virtual time_t stat_dest_lastchange() const;
    virtual nlink_t stat_nlink() const;
    virtual nlink_t stat_dest_nlink() const;
    virtual uid_t stat_userid() const;
    virtual uid_t stat_dest_userid() const;
    virtual gid_t stat_groupid() const;
    virtual gid_t stat_dest_groupid() const;
    virtual loff_t stat_blocks() const;
    virtual loff_t stat_dest_blocks() const;

      virtual int changeBasename( const std::string &newbasename );
      virtual int updateStats();

      virtual void resetToDummy();
  private:
    std::string _fullname;
    mutable std::string m_basename;
    bool _exists;

    struct FSEntry_stat {
      loff_t size;
      time_t lastaccess;
      time_t lastmod;
      time_t lastchange;
      mode_t mode;
      uid_t userid;
      gid_t groupid;
      ino_t inode;
      nlink_t nlink;
      loff_t blocks;
      dev_t rdev;   // device type
      dev_t dev;    // device
    } _statbuf, _dest_statbuf;

    bool _is_link;
    bool _is_corrupt;
    mutable bool m_basename_calculated;
  };
  
}

#endif
