/* get_files_thread.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2016 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GETFILESTHREAD_HH
#define GETFILESTHREAD_HH

#include "wdefines.h"
#include <string>
#include <list>
#include "aguix/thread.hh"
#include "aguix/condvar.h"
#include "nwc_dir.hh"
#include "searchsettings.hh"
#include "nwc_file.hh"
#include <functional>

class GetFilesThread : public Thread, private NWC::DirWalkCallBack
{
public:
    GetFilesThread( std::unique_ptr<NWC::Dir> searchdir );
    ~GetFilesThread();
    GetFilesThread( const GetFilesThread &other );
    GetFilesThread &operator=( const GetFilesThread &other );

    int run();

    void setVisitCB( std::function< void( NWC::File & ) > cb );
    void setVisitEnterDirCB( std::function< bool( NWC::Dir & ) > cb );
private:
    virtual int doSearch();

    int visit( NWC::File &file, NWC::Dir::WalkControlObj &cobj );
    int visitEnterDir( NWC::Dir &dir, NWC::Dir::WalkControlObj &cobj );
    int visitLeaveDir( NWC::Dir &dir, NWC::Dir::WalkControlObj &cobj );
    int prepareDirWalk( const NWC::Dir &dir, NWC::Dir::WalkControlObj &cobj,
                        std::list< RefCount< NWC::FSEntry > > &return_add_entries,
                        std::list< RefCount< NWC::FSEntry > > &return_skip_entries );

    std::function< void( NWC::File & ) > m_visit_cb;
    std::function< bool( NWC::Dir & ) > m_visit_enter_dir_cb;

    std::unique_ptr< NWC::Dir > m_searchdir;
};

#endif
