/* magic_db.hh
 * This file is part of Worker, a file manager for UNIX/X11.
 * Copyright (C) 2008-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MAGIC_DB_HH
#define MAGIC_DB_HH

#include "wdefines.h"
#include "aguix/mutex.h"
#include <string>

#ifdef HAVE_LIBMAGIC
#  include <magic.h>
#endif

class MagicDB
{
public:
    MagicDB();
    ~MagicDB();

    static MagicDB &getInstance();

    static const int NO_FLAG = 0x0;
    static const int DECOMPRESS = 0x1;
    static const int USE_MIME_TYPE = 0x2;
    static const int USE_MIME_ENCODING = 0x4;
    static const int USE_MIME = USE_MIME_TYPE | USE_MIME_ENCODING;

    void init();
    void fini();
    std::string getInfo( const char *filename, int flags );
    std::string getInfo( const std::string &filename, int flags );
    std::string getInfo( const void *buffer, size_t len, int flags );
private:
#ifdef HAVE_LIBMAGIC
    magic_t m_magic_cookie;
#endif
    MutEx m_lock;
    bool m_magic_init;

    static MagicDB *m_instance;
};

#endif
