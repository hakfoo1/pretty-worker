/* eventqueue.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "eventqueue.hh"

EventQueue::EventQueue()
{
}

EventQueue::~EventQueue()
{
}

bool EventQueue::empty()
{
    lock();
    bool ret = m_events.empty();
    unlock();

    return ret;
}
void EventQueue::lock()
{
    m_cv.lock();
}

void EventQueue::unlock()
{
    m_cv.unlock();
}

void EventQueue::wait()
{
    m_cv.wait();
}
    
EventQueue::eq_event_t EventQueue::pop()
{
    lock();
    eq_event_t v = EQ_NONE;

    if ( ! m_events.empty() ) {
        v = m_events.front();
        m_events.pop_front();
    }
    unlock();

    return v;
}

void EventQueue::push( eq_event_t v )
{
    lock();
    m_events.push_back( v );
    m_cv.signal();
    unlock();
}
