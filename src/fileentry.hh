/* fileentry.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FILEENTRY_HH
#define FILEENTRY_HH

#include "wdefines.h"
#include "worker_types.h"
#include <list>
#include <string>
#include "fileentry_color.hh"
#include "nwc_fsentry.hh"

// how many characters are used for permission string
// NOT including the trailing null byte
#define WORKER_PERMCHARS 10

class WCFiletype;
class FieldListView;
class FieldListViewDND;

class FileEntry
{
public:
  FileEntry();
  ~FileEntry();
  FileEntry( const FileEntry &other );
  FileEntry( const NWC::FSEntry &other );
  FileEntry &operator=( const FileEntry &other );

  bool isDir() const;
  FileEntry *duplicate();
  int readInfos();
  int readInfos(bool);
  char *getDestination() const;
  const char *getPermissionString();
  bool isExe( bool fastTest = false ) const;
  bool match( const char* pattern );
  bool checkAccess( int m ) const;

  loff_t size() const;
  time_t lastaccess() const;
  time_t lastmod() const;
  time_t lastchange() const;
  mode_t mode() const;
  uid_t userid() const;
  gid_t groupid() const;
  ino_t inode() const;
  nlink_t nlink() const;
  loff_t blocks() const;
  loff_t dsize() const;
  time_t dlastaccess() const;
  time_t dlastmod() const;
  time_t dlastchange() const;
  mode_t dmode() const;
  uid_t duserid() const;
  gid_t dgroupid() const;
  ino_t dinode() const;
  nlink_t dnlink() const;
  loff_t dblocks() const;
  dev_t rdev() const;
  dev_t drdev() const;
  dev_t dev() const;
  dev_t ddev() const;
  
  char *getContentStr( int pos, int len = 1 );
  int getContentNum( int pos, int len = 1 );
  void freeContentBuf();

  struct FE_stat {
    loff_t size;
    time_t lastaccess;
    time_t lastmod;
    time_t lastchange;
    mode_t mode;
    uid_t userid;
    gid_t groupid;
    ino_t inode;
    nlink_t nlink;
    loff_t blocks;
    dev_t rdev;   // device type
    dev_t dev;    // device
  } statbuf, dstatbuf;

  char *fullname;
  char *name;
  loff_t dirsize;
  bool select;
  bool isLink;
  bool isCorrupt;
  bool exists;
  
  WCFiletype *filetype;
  bool isHidden;
  bool use;
  
  bool reclistQueued;  // for filetype recognition in normalmode
  
  void freeOutputBuf();
  const char *findOutput( const char *tcommand );
  int findReturnvalue( const char *tcommand, int *return_value );
  int findOutputAndRV( const char *tcommand, const char **return_output, int *return_value );
  void addOutput( const char *tcommand, const char *toutput, int value = 0 );
  
  bool isReg() const;

    void setColor( const FileEntryColor &c );
    const FileEntryColor &getColor() const;

    void setCustomColor( const FileEntryCustomColor &c );
    void setCustomColors( const int fg[4], const int bg[4] );
    const FileEntryCustomColor &getCustomColor() const;
    int getCustomColorSet() const;
    void clearCustomColor();

    void setOverrideColor( int type, int fg, int bg );
    
    void clearOverrideColor();

    bool isSameFile( const char *othername, bool follow_symlinks ) const;
    bool equals( const FileEntry &fe ) const;
    bool equals( const FileEntry *fe ) const;
    bool isSamePath( const FileEntry *fe ) const;
    
    int getID() const;
    
    void setMatchingLabels( const std::list<std::string> &labels );
    void clearMatchingLabels();
    const std::list<std::string> &getMatchingLabels() const;
    bool hasMatchingLabels() const;

    typedef enum {
        NOT_IN_BOOKMARKS,
        BOOKMARK_PREFIX,
        BOOKMARK
    } bookmark_match_t;

    bookmark_match_t getBookmarkMatch() const;
    void setBookmarkMatch( bookmark_match_t m );

    void setFiletypeFileOutput( const std::string &output );
    std::string getFiletypeFileOutput() const;

    void setMimeType( const std::string &mimetype );
    std::string getMimeType() const;

protected:
    friend class Verzeichnis;
    void setID( int ID );

private:
    struct {
        char *content;
        int size;
    } contentBuf;
    void readContentBuf( int len = 64 );

    class StringBuf *outputBuf;

    FileEntryColor m_colors;
    class FEBookmarkInfo *m_bookmark_info;

    int _ID;

    std::string m_filetype_file_output;
    std::string m_mime_type;
};

#endif
