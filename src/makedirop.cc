/* makedirop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "makedirop.h"
#include "listermode.h"
#include "worker.h"
#include "worker_locale.h"
#include "virtualdirmode.hh"

const char *MakeDirOp::name="MakeDirOp";

MakeDirOp::MakeDirOp() : FunctionProto()
{
    m_category = FunctionProto::CAT_FILEOPS;
}

MakeDirOp::~MakeDirOp()
{
}

MakeDirOp*
MakeDirOp::duplicate() const
{
  MakeDirOp *ta=new MakeDirOp();
  return ta;
}

bool
MakeDirOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
MakeDirOp::getName()
{
  return name;
}

int
MakeDirOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
              vdm->make_dir();
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

const char *
MakeDirOp::getDescription()
{
  return catalog.getLocale(1268);
}
