/* tabprofilesop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TABPROFILESOP_H
#define TABPROFILESOP_H

#include "wdefines.h"
#include "functionproto.h"
#include "worker_types.h"

class Worker;
class TextStorageString;
class TextView;
class FieldListView;

class TabProfilesOp : public FunctionProto
{
public:
    TabProfilesOp();
    ~TabProfilesOp();
    TabProfilesOp( const TabProfilesOp &other );
    TabProfilesOp &operator=( const TabProfilesOp &other );
    
    TabProfilesOp *duplicate() const override;
    bool isName( const char * ) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;

    const char *getDescription() override;

    static const char *name;
private:
    int gui();

    std::vector< std::string > getAvailableProfiles();
    std::pair< std::vector< std::string >, std::vector< std::string > > getProfile( const std::string &profile_name );
    Lister *m_left_l;
    Lister *m_right_l;

    void show_profile_content( const std::string &profile_name,
                               std::shared_ptr< TextStorageString > storage,
                               TextView *tv );
    int dump_tabs( const std::string &profile_name );
    int load_tabs( const std::string &profile_name );
    int filter_available_profiles( const std::vector< std::string > &profiles,
                                   const std::string &filter,
                                   FieldListView *lv );
    std::pair< std::vector< std::string >, std::vector< std::string > > get_current_tabs();
    void delete_profile( const std::string &profile_name );
};

#endif
