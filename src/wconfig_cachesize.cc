/* wconfig_cachesize.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2008,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_cachesize.hh"
#include "wconfig.h"
#include "worker_locale.h"
#include "aguix/stringgadget.h"

CacheSizePanel::CacheSizePanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

CacheSizePanel::~CacheSizePanel()
{
}

int CacheSizePanel::create()
{
  Panel::create();

  char buf[ A_BYTESFORNUMBER( int ) ];

  sprintf( buf, "%u", _baseconfig.getCacheSize() );

  AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 688 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );
  
  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  
  ac1_1->add( new Text( _aguix, 0, 0, catalog.getLocale( 233 ) ),
              0, 0, AContainer::CO_FIX );
  
  sg = (StringGadget*)ac1_1->add( new StringGadget( _aguix, 0, 0, 30, buf, 0 ),
                                  1, 0, AContainer::CO_INCW );

  contMaximize( true );
  return 0;
}

int CacheSizePanel::saveValues()
{
  int val = 0;

  if ( sscanf( sg->getText(), "%d", &val ) == 1 ) {
    if ( val >= 1 ) {
      _baseconfig.setCacheSize( val );
    }
  }
  return 0;
}
