/* run_custom_actions.hh
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2007-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef RUN_CUSTOM_ACTION_H
#define RUN_CUSTOM_ACTION_H

#include "wdefines.h"
#include "functionproto.h"
#include <string>

class RunCustomAction : public FunctionProto
{
public:
    RunCustomAction();
    ~RunCustomAction();
    RunCustomAction( const RunCustomAction &other );
    RunCustomAction &operator=( const RunCustomAction &other );

    RunCustomAction *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;

    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save( Datei* ) override;
    int configure() override;

    std::string getCustomName() const;
    void setCustomName( const std::string &newname );

    class RunCustomActionDescr : public ActionDescr
    {
    public:
        RunCustomActionDescr( const std::string &customname );
        command_list_t get_action_list( WCFiletype *ft ) const override;
    private:
        std::string m_customname;
    };

    static const char *name;
protected:
    void normalmoderuncustomaction( ActionMessage *am );

    std::string m_customname;
};

#endif
