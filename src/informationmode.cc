/* informationmode.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "informationmode.h"
#include "listermode.h"
#include "worker_locale.h"
#include "wconfig.h"
#include "worker.h"
#include "partspace.h"
#include "pdatei.h"
#include "wcfiletype.hh"
#include "fileentry.hh"
#include "nmspecialsourceext.hh"
#include "aguix/acontainer.h"
#include "aguix/fieldlistview.h"
#include "aguix/awindow.h"
#include "aguix/util.h"
#include "datei.h"
#include "nwc_path.hh"
#include "nwcentryselectionstate.hh"

const char *InformationMode::type="InformationMode";

InformationMode::InformationMode(Lister *parent):ListerMode(parent)
{
  lastactivefe=NULL;
  lv=NULL;
  blanked=false;
  nospaceinfo=false;
  m_cont = NULL;
}

InformationMode::~InformationMode()
{
  clearLastActiveFE();
}

void InformationMode::messageHandler(AGMessage *msg)
{
  bool ma=false;
  switch(msg->type) {
    case AG_SIZECHANGED:
      break;
  }
  if(ma==true) parentlister->makeActive();
}

void InformationMode::on()
{
  int side=parentlister->getSide();
  int m;

  m_cont = new AContainer( parentawindow, 1, 1 );
  m_cont->setBorderWidth( 0 );
  m_cont->setMinSpace( 0 );
  m_cont->setMaxSpace( 0 );
  parentlister->setContainer( m_cont );

  lv = (FieldListView*)m_cont->add( new FieldListView( aguix, 0, 0, 50, 50, 1 ),
                                    0, 0 );
  m=(wconfig->getHBarTop(side)==true)?1:2;
  lv->setHBarState(m);
  m=(wconfig->getVBarLeft(side)==true)?1:2;
  lv->setVBarState(m);
  m=wconfig->getHBarHeight(side);
  lv->setHBarHeight(m);
  m=wconfig->getVBarWidth(side);
  lv->setVBarWidth(m);
  lv->setFont( wconfig->getFont( 2 + side ).c_str() );
  
  lv->setSize( 23 );
  for(m=0;m<23;m++) {
    lv->setText( m, 0, "" );
    lv->setPreColors( m, FieldListView::PRECOLOR_NOTSELORACT );
  }

  parentlister->setActiveMode(this);
  setName();

  parentawindow->updateCont();
}

void InformationMode::off()
{
  delete lv;
  lv = NULL;
  parentlister->setContainer( NULL );
  delete m_cont;
  m_cont = NULL;
  parentlister->setActiveMode(NULL);
  parentlister->setName("");
  clearLastActiveFE();
}

void InformationMode::activate()
{
}

void InformationMode::deactivate()
{
}

bool InformationMode::isType(const char *str)
{
  if(strcmp(str,type)==0) return true; else return false;
}

const char *InformationMode::getType()
{
  return type;
}

const char *InformationMode::getStaticType()
{
  return type;
}


int InformationMode::configure()
{
  return 0;
}

void InformationMode::cyclicfunc(cyclicfunc_mode_t mode)
{
  update(false);
}

const char* InformationMode::getLocaleName()
{
  return getStaticLocaleName();
}

const char* InformationMode::getStaticLocaleName()
{
  return catalog.getLocale(175);
}

int InformationMode::load()
{
  return 0;
}

bool InformationMode::save(Datei *fh)
{
  return false;
}

void InformationMode::setName()
{
  parentlister->setName(catalog.getLocale(175));
}

void InformationMode::relayout()
{
    int side = parentlister->getSide();
    lv->setFont( wconfig->getFont( 2 + side ).c_str() );
}

void InformationMode::update(bool force)
{
  ListerMode *lm1=NULL;
  Lister *ol=NULL;
  const FileEntry *fe;
  
  ol=parentlister->getWorker()->getOtherLister(parentlister);
  if(ol!=NULL) {
    lm1=ol->getActiveMode();
  }
  
  if ( ( force == true ) ||
       ( nospaceinfo == true ) )
    clearLastActiveFE();
  
  if(lm1!=NULL) {
    std::list< NM_specialsourceExt > files;
    
    lm1->getSelFiles( files, ListerMode::LM_GETFILES_ONLYACTIVE );

    if ( ! files.empty() ) {
      fe = files.begin()->entry();
      if ( fe->equals( lastactivefe ) == false ) {
	clearLastActiveFE();
	if ( fe != NULL ) {
	  lastactivefe = new FileEntry( *fe );
	}
	showinfos();
      }
    } else {
      if ( ol->isActive() == true ) {
        // only clear LV when other side is active and has no active fe
	if ( lastactivefe != NULL ) {
	  clearLastActiveFE();
	  showinfos();
	}
      }
    }
  }
}

void InformationMode::showinfos()
{
  FileEntry *fe;
  int x2,len,len2;
  char buffer[1024],
       rights[3][4],
       *tstr, *str;
  char *bstr2;
  bool showno;
  loff_t size;
  const char *cstr;
  std::string bytestr, kbytestr, mbytestr, bstr1;
  
  float percent;
  struct tm *timeptr;
  time_t tvar;
  char *istr;
  std::string tstr2;
  
  showno=true;
  fe=lastactivefe;
  if(fe!=NULL) {
    showno=false;
    tstr=(char*)_allocsafe(strlen("%s: %s")+strlen(catalog.getLocale(176))+strlen(fe->name)+1);
    sprintf(tstr,"%s: %s",catalog.getLocale(176),fe->name);
    lv->setText( 1, 0, tstr);
    _freesafe(tstr);
    
    if((fe->isDir()==true)&&(fe->isCorrupt==false)) {
      if(fe->dirsize>=0) size=fe->dirsize;
      else size=fe->size();
    } else size=fe->size();

    MakeLong2NiceStr( size, bytestr );

    MakeLong2NiceStr( ( size + 512 ) / 1024, kbytestr );

    MakeLong2NiceStr( ( size + 524288 ) / 1048576, mbytestr );

    MakeLong2NiceStr( fe->blocks(), bstr1, false );
    bstr2 = (char*)_allocsafe( strlen( catalog.getLocale( 551 ) ) +
                               bstr1.length() + 1 );
    sprintf( bstr2, catalog.getLocale( 551 ), bstr1.c_str() );
    
    tstr = (char*)_allocsafe( strlen( "%s: %s B, %s KB, %s MB (%s)" ) +
			      strlen( catalog.getLocale( 177 ) ) +
			      bytestr.length() +
			      kbytestr.length() +
			      mbytestr.length() +
			      strlen( bstr2 ) + 1 );
    sprintf( tstr, "%s: %s B, %s KB, %s MB (%s)",
	     catalog.getLocale( 177 ),
	     bytestr.c_str(),
	     kbytestr.c_str(),
	     mbytestr.c_str(),
	     bstr2 );
    _freesafe( bstr2 );
    lv->setText( 2, 0, tstr);
    _freesafe(tstr);
    
    if(fe->filetype!=NULL) {
        auto fpr = std::make_shared< NWCFlagProducer >( nullptr, fe );
        FlagReplacer rpl( fpr );
        std::string filetype_str;

        if ( fe->filetype->nameContainsFlags() ) {
            filetype_str = rpl.replaceFlags( fe->filetype->getName(), false );
        } else {
            filetype_str = fe->filetype->getName();
        }

        tstr2 = AGUIXUtils::formatStringToString( "%s: %s",
                                                  catalog.getLocale( 178 ),
                                                  filetype_str.c_str() );

        if ( ! fe->getFiletypeFileOutput().empty() ) {
            tstr2 += " / ";
            tstr2 += fe->getFiletypeFileOutput();
        }

        lv->setText( 3, 0, tstr2 );
    } else {
      if(fe->isDir()==true) {
//        tstr=(char*)_allocsafe(strlen("%s: %s")+
//                               strlen(catalog.getLocale(178))+
//                               strlen(catalog.getLocale(109))+1);
//        sprintf(tstr,"%s: %s",catalog.getLocale(178),catalog.getLocale(109));
        tstr=(char*)_allocsafe(strlen("%s: %s")+
                               strlen(catalog.getLocale(178))+
                               strlen(wconfig->getdirtype()->getName().c_str())+1);
        sprintf(tstr,"%s: %s",catalog.getLocale(178),wconfig->getdirtype()->getName().c_str());
      } else {
        tstr=(char*)_allocsafe(strlen("%s: %s")+
                               strlen(catalog.getLocale(178))+
                               strlen(wconfig->getnotyettype()->getName().c_str())+1);
        sprintf(tstr,"%s: %s",catalog.getLocale(178),wconfig->getnotyettype()->getName().c_str());
      }
      lv->setText( 3, 0, tstr);
      _freesafe(tstr);
    }

    lv->setText( 4, 0, "" );
    if ( fe->isCorrupt == false ) {
      // dmode is only accessed when not corrupt
      if ( S_ISREG( ( fe->isLink == true ) ? fe->dmode() : fe->mode() ) ) {
        enum PDatei::pdatei_handler pt;
        
        pt = PDatei::getCruncher( fe->fullname );
        switch ( pt ) {
          case PDatei::PDATEI_XPK:
            tstr = (char*)_allocsafe( strlen( catalog.getLocale( 506 ) ) +
                                      strlen( "XPK" ) + 1 );
            sprintf( tstr, catalog.getLocale( 506 ), "XPK" );
            break;
          case PDatei::PDATEI_BZIP2:
            tstr = (char*)_allocsafe( strlen( catalog.getLocale( 506 ) ) +
                                      strlen( "BZIP2" ) + 1 );
            sprintf( tstr, catalog.getLocale( 506 ), "BZIP2" );
            break;
          case PDatei::PDATEI_GZIP:
            tstr = (char*)_allocsafe( strlen( catalog.getLocale( 506 ) ) +
                                      strlen( "GZIP" ) + 1 );
            sprintf( tstr, catalog.getLocale( 506 ), "GZIP" );
            break;
          case PDatei::PDATEI_NORMAL:
            tstr = dupstring( catalog.getLocale( 507 ) );
            break;
          default:
            tstr = dupstring( "???" );
            break;
        }
        lv->setText( 4, 0, tstr );
        _freesafe( tstr );
      } 
    }

    if(fe->isLink==true) {
      str=fe->getDestination();
      
      if ( str != NULL ) {
        tstr=(char*)_allocsafe(strlen("  %s: %s")+
                               strlen(catalog.getLocale(179))+
                               strlen(str)+1);
        sprintf(tstr,"  %s: %s",catalog.getLocale(179),str);
        _freesafe(str);
        lv->setText( 5, 0, tstr);
        _freesafe(tstr);
      } else lv->setText( 5, 0, "" );
    } else lv->setText( 5, 0, "");
    
    cstr = fe->getPermissionString();
    strncpy( rights[0], cstr + 1, 3 );
    rights[0][3]='\0';
    strncpy( rights[1], cstr + 4, 3 );
    rights[1][3]='\0';
    strncpy( rights[2], cstr + 7, 3 );
    rights[2][3]='\0';
    len=strlen(catalog.getLocale(180));
    len2=strlen(catalog.getLocale(214));
    for(x2=1;x2<3;x2++) if(strlen(catalog.getLocale(214+x2))>(size_t)len2) len2=strlen(catalog.getLocale(214+x2));

    std::string s1 = AGUIXUtils::formatStringToString( "%%%ds %%%ds %%s", len, len2 );
    
    std::string s2 = AGUIXUtils::formatStringToString( s1.c_str(),
                                                       catalog.getLocale( 180 ),
                                                       catalog.getLocale( 214 ),
                                                       rights[0] );
    lv->setText( 7, 0, s2 );
    s2 = AGUIXUtils::formatStringToString( s1.c_str(),
                                           "",
                                           catalog.getLocale( 215 ),
                                           rights[1] );
    lv->setText( 8, 0, s2 );
    s2 = AGUIXUtils::formatStringToString( s1.c_str(),
                                           "",
                                           catalog.getLocale( 216 ),
                                          rights[2] );
    lv->setText( 9, 0, s2 );
    
    tstr = (char*)_allocsafe( strlen( "  %s %04o" ) +
                              strlen( catalog.getLocale( 527 ) ) +
                              A_BYTESFORNUMBER( mode_t ) + 1 );
    sprintf( tstr, "  %s %04o", catalog.getLocale( 527 ), (unsigned int)( ( fe->mode() ) & 07777 ) );
    lv->setText( 10, 0, tstr );
    _freesafe( tstr );

    str = getOwnerString( fe->userid(), fe->groupid() );
    
    tstr=(char*)_allocsafe(strlen("%s: %s")+
                           strlen(catalog.getLocale(184))+
                           strlen(str)+1);
    sprintf(tstr,"%s: %s",catalog.getLocale(184),str);
    _freesafe(str);
    lv->setText( 11, 0, tstr);
    _freesafe(tstr);

    MakeLong2NiceStr( (loff_t)fe->inode(), bstr1, false );
    tstr = (char*)_allocsafe( strlen( "%s: %s" ) +
                              strlen( catalog.getLocale( 548 ) ) +
                              bstr1.length() + 1 );
    sprintf( tstr, "%s: %s", catalog.getLocale( 548 ), bstr1.c_str() );
    lv->setText( 12, 0, tstr);
    _freesafe( tstr );

    MakeLong2NiceStr( (loff_t)fe->nlink(), bstr1, false );
    tstr = (char*)_allocsafe( strlen( "%s: %s" ) +
                              strlen( catalog.getLocale( 549 ) ) +
                              bstr1.length() + 1 );
    sprintf( tstr, "%s: %s", catalog.getLocale( 549 ), bstr1.c_str() );
    lv->setText( 13, 0, tstr);
    _freesafe( tstr );

    len = strlen( catalog.getLocale( 162 ) );
    for ( x2 = 1; x2 < 3; x2++ )
      if ( strlen( catalog.getLocale( 162 + x2 ) ) > (size_t)len ) len = strlen( catalog.getLocale( 162 + x2 ) );

    tvar = fe->lastaccess();
    timeptr = localtime( &( tvar ) );
    str = asctime( timeptr );
    str[ strlen( str ) - 1 ] = 0;

    s1 = AGUIXUtils::formatStringToString( "%%%ds: %%s", len );
    
    s2 = AGUIXUtils::formatStringToString( s1.c_str(), catalog.getLocale( 162 ), str );
    lv->setText( 15, 0, s2 );

    tvar = fe->lastmod();
    timeptr = localtime( &( tvar ) );
    str = asctime( timeptr );
    str[ strlen( str ) - 1 ] = 0;

    s2 = AGUIXUtils::formatStringToString( s1.c_str(), catalog.getLocale( 163 ), str );
    lv->setText( 16, 0, s2 );

    tvar = fe->lastchange();
    timeptr = localtime( &( tvar ) );
    str = asctime( timeptr );
    str[ strlen( str ) - 1 ] = 0;

    s2 = AGUIXUtils::formatStringToString( s1.c_str(), catalog.getLocale( 164 ), str );
    lv->setText( 17, 0, s2 );

    tstr=(char*)_allocsafe(strlen("%s: %s")+
                           strlen(catalog.getLocale(185))+
                           strlen(fe->fullname)+1);
    sprintf(tstr,"%s: %s",catalog.getLocale(185),fe->fullname);
    lv->setText( 19, 0, tstr);
    _freesafe(tstr);

    tstr = realpath( fe->fullname, NULL );
    if ( tstr ) {
      s2 = AGUIXUtils::formatStringToString( "%s: %s", catalog.getLocale( 186 ), tstr );
      free( tstr );
    } else {
      s2 = AGUIXUtils::formatStringToString( "%s: ???", catalog.getLocale( 186 ) );
    }
    lv->setText( 20, 0, s2 );

    if ( fe->isCorrupt == false ) {
      istr = dupstring( fe->fullname );
    } else {
      istr = NWC::Path::parentDir( fe->fullname, NULL );
    }
    if ( parentlister->getWorker()->PS_readSpace( istr ) == 0 ) {
      if ( parentlister->getWorker()->PS_getSpace() != 0 ) {
        percent = (double)parentlister->getWorker()->PS_getFreeSpace(); 
        percent *= 100.0;
        percent /= (double)parentlister->getWorker()->PS_getSpace();
      } else percent=0.0;

      std::string spaceh, freeh;

      spaceh = parentlister->getWorker()->PS_getSpaceH();
      freeh = parentlister->getWorker()->PS_getFreeSpaceH();

      snprintf( buffer, sizeof( buffer ), "%s: %s (%2.0f %%) %s %s", catalog.getLocale( 187 ),
                freeh.c_str(),
                percent,
                catalog.getLocale( 188 ),
                spaceh.c_str() );
      buffer[ sizeof( buffer ) - 1 ] = '\0';
      nospaceinfo = false;
    } else {
      snprintf( buffer, sizeof( buffer ), "%s: ???", catalog.getLocale( 187 ) );
      nospaceinfo = true;
    }
    _freesafe( istr );
    lv->setText( 22, 0, buffer);
  }
  if(showno==true) {
    // f�r alle anderen sind keine Informationen erh�ltlich
    for(int i=0;i<lv->getElements();i++) lv->setText( i, 0, "");
    lv->setText( 2, 0, catalog.getLocale(189));
  }
  lv->redraw();
}

void InformationMode::clearLastActiveFE() {
  if ( lastactivefe != NULL ) {
    delete lastactivefe;
    lastactivefe = NULL;
  }
}
