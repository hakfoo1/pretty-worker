/* mounttable_entry.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MOUNTTABLE_ENTRY_HH
#define MOUNTTABLE_ENTRY_HH

#include "wdefines.h"
#include <string>

class MountTableEntry
{
public:
    MountTableEntry( const std::string &device = "" );

    std::string getDevice() const;
    std::string getMountPoint() const;
    mode_t getMode() const;
    dev_t getRDev() const;

    void setDevice( const std::string &device );
    void setMountPoint( const std::string &mount_point );
    void setMode( mode_t m );
    void setRDev( dev_t d );
private:
    std::string m_device;
    std::string m_mount_point;
    mode_t m_mode;
    dev_t m_rdev;
};

#endif
