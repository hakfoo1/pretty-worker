/* stringbuf.h
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2004-2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRINGBUF_H
#define STRINGBUF_H

#include "wdefines.h"
#include "aguix/avltree.hh"
#include <string>

class StringBuf
{
public:
  StringBuf();
  ~StringBuf();
  StringBuf( const StringBuf &other );
  StringBuf &operator=( const StringBuf &other );

  const char *find( const char *tkey );
  int findValue( const char *tkey, int *return_value );
  int find( const char *tkey, const char **return_str, int *return_value );
  void clear();
  void add( const char *tkey, const char *tstr, int tv = 0 );
private:
  struct sb_entry_t {
    char *str;
    int value;
  };
  AVLList<std::string,sb_entry_t> buf;
  size_t maxsize;
  void freeLast();
};

#endif

