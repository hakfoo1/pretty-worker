/* mtabmounttable.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "mtabmounttable.hh"
#include "mounttable_entry.hh"
#include "getmntentmounttable.hh"
#include "getmntinfomounttable.hh"

class DummyMountTable : public MountTable
{
public:
    DummyMountTable( const std::string &file )
    {
    }

    std::list<MountTableEntry> getEntries()
    {
        return std::list<MountTableEntry>();
    }

    bool containsDevice( const std::string &dev )
    {
        return false;
    }

    bool findDevice( const std::string &dev,
                     MountTableEntry &return_entry )
    {
        return false;
    }

    void update()
    {
    }
};

MtabMountTable::MtabMountTable( const std::string &file )
#ifdef MOUNTED_GETMNTENT1
  : m_table( new GetMntEntMountTable( file ) )
#elif defined( HAVE_GETMNTINFO_FBSD ) || defined( HAVE_GETMNTINFO_NBSD )
  : m_table( new GetMntInfoMountTable( file ) )
#else
  : m_table( new DummyMountTable( file ) )
#endif
{
    m_table->update();
}

std::list<MountTableEntry> MtabMountTable::getEntries()
{
    return m_table->getEntries();
}

bool MtabMountTable::containsDevice( const std::string &dev )
{
    return m_table->containsDevice( dev );
}

bool MtabMountTable::findDevice( const std::string &dev,
                                 MountTableEntry &return_entry )
{
    return m_table->findDevice( dev, return_entry );
}

void MtabMountTable::update()
{
    m_table->update();
}
