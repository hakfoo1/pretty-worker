/* normalops.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NORMALOPS_H
#define NORMALOPS_H

#include "wdefines.h"
#include "functionproto.h"
#include <memory>

class PersistentStringList;

class ChangeListerSetOp:public FunctionProto
{
public:
    ChangeListerSetOp();
    virtual ~ChangeListerSetOp();
    ChangeListerSetOp( const ChangeListerSetOp &other );
    ChangeListerSetOp &operator=( const ChangeListerSetOp &other );

    ChangeListerSetOp *duplicate() const override;
    int configure() override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save(Datei *) override;

    enum { CLS_LEFT_LISTER,CLS_RIGHT_LISTER,CLS_CURRENT_LISTER,CLS_OTHER_LISTER};
    void setMode(int);
protected:
    static const char *name;
    int mode;
};

class SwitchListerOp:public FunctionProto
{
public:
    SwitchListerOp();
    virtual ~SwitchListerOp();
    SwitchListerOp( const SwitchListerOp &other );
    SwitchListerOp &operator=( const SwitchListerOp &other );

    SwitchListerOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
protected:
    static const char *name;
};

class FilterSelectOp:public FunctionProto
{
public:
    FilterSelectOp();
    ~FilterSelectOp();
    FilterSelectOp( const FilterSelectOp &other );
    FilterSelectOp &operator=( const FilterSelectOp &other );

    FilterSelectOp *duplicate() const override;
    int configure() override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save(Datei *) override;
  
    bool requestfilter( const std::string &fullname );
    void setFilter( const char *str );
    void setAutoFilter( const bool nv );

    void setImmediateFilterApply( bool nv );
    bool immediateFilterApply() const;

    static std::list< std::string > getHistory();
protected:
    static const char *name;
    std::string m_filter;
    std::string current_filter;
    bool m_auto_filter = true;  // when true, use the best match from
                                // history or the file ending
    bool m_immediate_filter_apply = false;
  
    void addHistoryItem( const std::string &str );
    static std::unique_ptr< PersistentStringList > m_history;

    enum filter_select_mode {
        FILTER_SELECT,
        FILTER_DESELECT
    };
    void setMode( filter_select_mode nm );
private:
    static int instance_counter;
    static size_t history_size;
    static int lastw, lasth;
    filter_select_mode m_mode;

    void initHistory();
    void closeHistory();
};

class FilterUnSelectOp:public FilterSelectOp
{
public:
    FilterUnSelectOp();
    ~FilterUnSelectOp();
    FilterUnSelectOp( const FilterUnSelectOp &other );
    FilterUnSelectOp &operator=( const FilterUnSelectOp &other );

    FilterUnSelectOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;
    const char *getDescription() override;
protected:
    static const char *name;
};

class Path2OSideOp:public FunctionProto
{
public:
    Path2OSideOp();
    virtual ~Path2OSideOp();
    Path2OSideOp( const Path2OSideOp &other );
    Path2OSideOp &operator=( const Path2OSideOp &other );

    Path2OSideOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
protected:
    static const char *name;
};

class QuitOp:public FunctionProto
{
public:
    QuitOp();
    virtual ~QuitOp();
    QuitOp( const QuitOp &other );
    QuitOp &operator=( const QuitOp &other );

    QuitOp *duplicate() const override;
    int configure() override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
    const char *getDescription() override;
    bool save(Datei *) override;
  
    void setMode(int);
    enum {Q_NORMAL_QUIT,Q_QUICK_QUIT};
protected:
    static const char *name;
    int mode;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
