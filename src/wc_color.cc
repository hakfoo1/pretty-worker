/* wc_color.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wc_color.hh"
 
WC_Color::WC_Color()
{
  _red = _green = _blue = 0;
}

WC_Color::WC_Color( int r, int g, int b )
{
  _red = _green = _blue = 0;

  setRed( r );
  setGreen( g );
  setBlue( b );
}

WC_Color *WC_Color::duplicate() const
{
  return new WC_Color( _red, _green, _blue );
}

int WC_Color::getRed() const
{
  return _red;
}

int WC_Color::getGreen() const
{
  return _green;
}

int WC_Color::getBlue() const
{
  return _blue;
}

void WC_Color::setRed( int r )
{
  _red = r;
  if ( _red < 0 ) _red = 0;
  if ( _red > 255 ) _red = 255;
}

void WC_Color::setGreen( int g )
{
  _green = g;
  if ( _green < 0 ) _green = 0;
  if ( _green > 255 ) _green = 255;
}

void WC_Color::setBlue( int b )
{
  _blue = b;
  if ( _blue < 0 ) _blue = 0;
  if ( _blue > 255 ) _blue = 255;
}
