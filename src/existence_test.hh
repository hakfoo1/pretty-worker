/* existence_test.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EXISTENCE_TESTS_HH
#define EXISTENCE_TESTS_HH

#include "wdefines.h"
#include <string>

namespace ExistenceTest {
    typedef enum {
                  EXISTS_UNKNOWN,
                  EXISTS_YES,
                  EXISTS_NO
    } exists_tristate_t;

    typedef struct {
        exists_tristate_t exists;
        size_t path_length;
    } existence_state_t;

    existence_state_t checkPathExistence( const std::string &path );
}

#endif /* EXISTENCE_TESTS_HH */
