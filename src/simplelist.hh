/* simplelist.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef UTIL_HH
#define UTIL_HH

#include "wdefines.h"

class ListElement
{
 public:
  ListElement();
  ~ListElement();
  ListElement( const ListElement &other );
  ListElement &operator=( const ListElement &other );
  void *element;
  ListElement *next;
  ListElement *prev;
};

class List
{
public:
  List();
  ~List();
  List( const List &other );
  List &operator=( const List &other );
  int addElement(void*);
  int addElementAt(int,void *);
  int removeElement(void *);
  int removeElementAt(int);
  int removeAllElements();
  int removeLastElement();
  int removeFirstElement();
  void *exchangeElement(int,void *);
  int size() const;
  void *getFirstElement(int);
  void *getElementAt(int,int);
  void *getLastElement(int);
  void *getNextElement(int);
  bool hasNextElement(int);
  void *getFirstElement();
  void *getElementAt(int);
  void *getLastElement();
  void *getNextElement();
  int getIndex(void *) const;
  void sort(int (*relfunc)(void*,void*));
  void sort(int (*relfunc)(void*,void*,int),int);
  int initEnum();
  void closeEnum(int);
private:
  ListElement *first;
  ListElement *last;
  ListElement **acts;
  ListElement *act;
  int enums;
  int elements;
  
  void mergeSort(ListElement **liste,
                 int (*relfunc)(void*,void*));
  void mergeSort(ListElement **liste,
                 int (*relfunc)(void*,void*,int),
		 int mode);
  ListElement *merge(ListElement *list1,
                     ListElement *list2,
		     int (*relfunc)(void*,void*));
  ListElement *merge(ListElement *list1,
                     ListElement *list2,
		     int (*relfunc)(void*,void*,int),
		     int mode);
  ListElement *split(ListElement *liste);
};

class ArrayList
{
public:
  ArrayList();
  ~ArrayList();
  ArrayList( const ArrayList &other );
  ArrayList &operator=( const ArrayList &other );
  int addElement(void*);
  int addElementAt(int,void *);
  int removeElement( const void * );
  int removeElementAt(int);
  int removeAllElements();
  int removeLastElement();
  int removeFirstElement();
  void *exchangeElement(int,void *);
  int size() const;
  void *getFirstElement(int);
  void *getElementAt(int,int);
  void *getLastElement(int);
  void *getNextElement(int);
  void *getPrevElement(int);
  void *getFirstElement();
  void *getElementAt(int);
  void *getLastElement();
  void *getNextElement();
  void *getPrevElement();
  int getIndex(void *) const;
  void sort(int (*relfunc)(void*,void*));
  void sort(int (*relfunc)(void*,void*,int),int);
  int initEnum();
  void closeEnum(int);
private:
  int elements;
  int arraysize;
  void **array;
  void expandArray();
  int actindex;
  int *acts;
  int enums;
  
  void quicksort(int p,int r,int (*relfunc)(void*,void*));
  void quicksort(int p,int r,int (*relfunc)(void*,void*,int),int mode);
  int qs_partition(int p,int r,int (*relfunc)(void*,void*));
  int qs_partition(int p,int r,int (*relfunc)(void*,void*,int),int mode);
};

#endif
