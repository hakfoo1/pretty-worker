/* dircomparewin.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DIRCOMPAREWIN_H
#define DIRCOMPAREWIN_H

#include "wdefines.h"
#include <string>

class AGUIX;
class AWindow;
class Text;
class Button;
class AContainer;

class DirCompareWin
{
public:
    DirCompareWin( AGUIX *aguix,
                   bool progress_percent );
    ~DirCompareWin();

    void addEntriesToDo( size_t count );
    void finishedEntry();

    int open();
    void close();
    int check();
    void update( bool force );

    void setCurrentEntry( const std::string &entry );
    void setCurrentDir( const std::string &dir );
    void setEntryPercent( int v );
private:
    AGUIX *m_aguix;
    size_t m_entries_to_do;
    size_t m_entries_done;

    AWindow *m_win;
    Text *m_to_do_t;
    Text *m_percent_t;
    Text *m_dir_t;
    Button *m_cancel_b;
    AContainer *m_cont0;
    AContainer *m_maincont;
    AContainer *m_ac3;
    time_t m_last_update;

    bool m_show_percent;
    int m_percent;

    std::string m_current_entry;
    std::string m_last_update_entry;
    std::string m_current_dir;
};

#endif /* DIRCOMPAREWIN_H */
