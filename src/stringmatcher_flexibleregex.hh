/* stringmatcher_flexibleregex.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef STRINGMATCHER_FLEXIBLEREGEX_HH
#define STRINGMATCHER_FLEXIBLEREGEX_HH

#include "wdefines.h"
#include "stringmatcher.hh"

class StringMatcherFlexibleRegEx : public StringMatcher
{
public:
    StringMatcherFlexibleRegEx();
    ~StringMatcherFlexibleRegEx();

    bool match( const std::string &str );
    std::vector< size_t > getMatchSegments( const std::string &str );

    void setMatchString( const std::string &str );
    std::string getMatchString() const;
    void setMatchCaseSensitive( bool nv );
    bool getMatchCaseSensitive() const;
    void setMatchFlexible( bool nv );
    bool getMatchFlexible() const;
private:
    std::string m_match_string;
    bool m_match_case_sensitive;
    std::string m_reg_ex_string;
#ifdef HAVE_REGEX
    regex_t m_compiled_regex;
#endif
    bool m_compiled;
#ifdef HAVE_REGEX
    regmatch_t *m_groups;
#endif
    int m_ngroups;
    bool m_flexible_mode_enabled;

    void convertToRegEx();
};

#endif
