/* volumemanagerui.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef VOLUMEMANAGERUI_HH
#define VOLUMEMANAGERUI_HH

#include "wdefines.h"
#include <memory>
#include <string>
#include <map>
#include <list>
#include "aguix/textstorage.h"
#include "aguix/message.h"

class HWVolumeManager;
class Button;
class FieldListView;
class Text;
class AGUIX;
class SolidButton;
class AContainer;
class PersistentStringList;
class TextView;
class PopUpMenu;

class VolumeManagerUI
{
public:
    VolumeManagerUI( AGUIX &aguix, HWVolumeManager &hwman );
    ~VolumeManagerUI();
    VolumeManagerUI( const VolumeManagerUI &other );
    VolumeManagerUI &operator=( const VolumeManagerUI &other );

    int mainLoop();
    std::string getDirectoryToEnter() const;
    void setCurrentDirname( const std::string &dirname );
    void setCurrentBasename( const std::string &basename );

    static std::string getHiddenDevicesFile();
    static std::list< std::string > getListOfHiddenDevices();
private:
    AGUIX &m_aguix;
    HWVolumeManager &m_hwman;

    std::string m_dir_to_enter;
    std::string m_dirname, m_basename;

    std::shared_ptr< TextStorageString > m_status_info;
    std::unique_ptr<class AWindow> m_win;
    FieldListView *m_lv;
    Button *m_mountb, *m_unmountb, *m_hideb, *m_edit_hiddenb;
    Button *m_okb, *m_closeb;
    AContainer *m_cont2;

    std::map< int, class HWVolume > m_row_to_volume;

    std::unique_ptr< PersistentStringList > m_hide_entries;

    TextView *m_status_tv;
    Button *m_ejectb, *m_closetrayb;
    PopUpMenu *m_eject_menu;
    std::list< HWVolume > m_ejectable_devices;
    Button *m_updateb;

    typedef enum {
	VOLMANUI_MENU_EJECT,
	VOLMANUI_MENU_CLOSETRAY
    } menu_command_t;
    menu_command_t m_menu_command;
    std::string m_last_device;

    void updateLV();
    void mountRow( int row );
    void unmountRow( int row );
    void maximizeWin();
    void hide( int row );
    void editHideList();

    void buildEjectMenu( const menu_command_t &menu_command );
    void closeEjectMenu();
    void handlePopupMsg( AGMessage *msg );
    void eject( HWVolume &vol );
    void closeTray( HWVolume &vol );

    void ejectOrCloseTray( const menu_command_t &menu_command );
};

#endif
