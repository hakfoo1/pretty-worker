/* layoutsettings.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2010 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef LAYOUTSETTINGS_HH
#define LAYOUTSETTINGS_HH

#include "wdefines.h"
#include <list>

class LayoutSettings
{
public:
    LayoutSettings();

    typedef enum { LO_STATEBAR,
                   LO_CLOCKBAR,
                   LO_BUTTONS,
                   LO_LISTVIEWS,
                   LO_BLL,
                   LO_LBL,
                   LO_LLB,
                   LO_BL,
                   LO_LB,
                   LO_DUMMY } layoutID_t;

    bool operator==( const LayoutSettings &rhs );
    bool operator!=( const LayoutSettings &rhs );

    void setOrders( const std::list< layoutID_t > &orders );
    const std::list< layoutID_t > &getOrders() const;
    void clearOrders();
    void pushBackOrder( layoutID_t nv );

    void setButtonVert( bool nv );
    bool getButtonVert() const;

    void setListViewVert( bool nv );
    bool getListViewVert() const;

    void setListViewWeight( int nv );
    int getListViewWeight() const;

    void setWeightRelToActive( bool nv );
    bool getWeightRelToActive() const;

    typedef std::list< std::list< layoutID_t > > available_elements_t;

    static available_elements_t getAvailableElements( bool bv, bool lvv );
    static int countID( const available_elements_t &l, layoutID_t ID );
    static std::list< layoutID_t > fixUsedList( const available_elements_t &avail_l,
                                                const std::list< layoutID_t > &used_l );
private:
    std::list< layoutID_t > m_orders;
    bool m_button_vert;
    bool m_listview_vert;
    int m_listview_weight;
    bool m_weight_rel_to_active;
};

#endif
