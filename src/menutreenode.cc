/* menutreenode.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013,2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "menutreenode.hh"
#include <algorithm>

MenuTreeNode::MenuTreeNode( const std::string &name,
                            const std::string &help,
                            const std::string &key,
                            MenuTreeNode *parent )
    : m_parent( parent ),
      m_name( name ),
      m_help( help ),
      m_key( key ),
      m_matched( false ),
      m_visible_pos( -1 )
{
}

MenuTreeNode::~MenuTreeNode()
{
    while ( ! m_children.empty() ) {
        MenuTreeNode *child = m_children.front();
        delete child;
    }

    if ( m_parent ) {
        m_parent->removeChild( this );
    }
}

MenuTreeNode *MenuTreeNode::insertChild( const std::string &name,
                                         const std::string &help,
                                         const std::string &key )
{
    MenuTreeNode *child = new MenuTreeNode( name, help, key, this );
    m_children.push_back( child );
    return child;
}

const std::string &MenuTreeNode::getName() const
{
    return m_name;
}

const std::string &MenuTreeNode::getHelp() const
{
    return m_help;
}

const std::string &MenuTreeNode::getKey() const
{
    return m_key;
}

std::list< MenuTreeNode *>::const_iterator MenuTreeNode::begin() const
{
    return m_children.begin();
}

std::list< MenuTreeNode *>::const_iterator MenuTreeNode::end() const
{
    return m_children.end();
}

void MenuTreeNode::setMatched( bool nv )
{
    m_matched = nv;
}

bool MenuTreeNode::getMatched() const
{
    return m_matched;
}

MenuTreeNode *MenuTreeNode::getParent() const
{
    return m_parent;
}

void MenuTreeNode::clearMatchedFlag()
{
    setMatched( false );
    for ( const auto it1 : m_children ) {
        it1->clearMatchedFlag();
    }
}

void MenuTreeNode::removeChild( MenuTreeNode *child )
{
    auto it = std::find( m_children.begin(),
                         m_children.end(),
                         child );

    if ( it != m_children.end() ) {
        m_children.erase( it );
    }
}

void MenuTreeNode::setActivateFunction( std::function< void( void ) > f )
{
    m_activate_function = f;
}

std::function< void( void ) > MenuTreeNode::getActivateFunction()
{
    return m_activate_function;
}

void MenuTreeNode::setHighlightFunction( std::function< void( void ) > f )
{
    m_highlight_function = f;
}

std::function< void( void ) > MenuTreeNode::getHighlightFunction()
{
    return m_highlight_function;
}

void MenuTreeNode::setDeHighlightFunction( std::function< void( void ) > f )
{
    m_dehighlight_function = f;
}

std::function< void( void ) > MenuTreeNode::getDeHighlightFunction()
{
    return m_dehighlight_function;
}

bool MenuTreeNode::hasChildren() const
{
    return ! m_children.empty();
}

int MenuTreeNode::getDepth() const
{
    if ( getParent() != NULL ) {
        return getParent()->getDepth() + 1;
    }

    return 0;
}

void MenuTreeNode::setVisiblePos( int pos )
{
    m_visible_pos = pos;
}

int MenuTreeNode::getVisiblePos() const
{
    return m_visible_pos;
}
