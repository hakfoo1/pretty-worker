/* menutreenode.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013,2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MENUTREENODE_HH
#define MENUTREENODE_HH

#include "wdefines.h"

#include <string>
#include <list>
#include <functional>

class MenuTreeNode
{
public:
    MenuTreeNode( const std::string &name,
                  const std::string &help,
                  const std::string &key,
                  MenuTreeNode *parent = NULL );
    ~MenuTreeNode();

    MenuTreeNode *insertChild( const std::string &name,
                               const std::string &help,
                               const std::string &key );
    const std::string &getName() const;
    const std::string &getHelp() const;
    const std::string &getKey() const;

    std::list< MenuTreeNode *>::const_iterator begin() const;
    std::list< MenuTreeNode *>::const_iterator end() const;

    void setMatched( bool nv );
    bool getMatched() const;

    void setVisiblePos( int pos );
    int getVisiblePos() const;

    MenuTreeNode *getParent() const;

    void clearMatchedFlag();

    void setActivateFunction( std::function< void( void ) > f );
    std::function< void( void ) > getActivateFunction();

    void setHighlightFunction( std::function< void( void ) > f );
    std::function< void( void ) > getHighlightFunction();
    void setDeHighlightFunction( std::function< void( void ) > f );
    std::function< void( void ) > getDeHighlightFunction();

    bool hasChildren() const;
    int getDepth() const;
private:
    void removeChild( MenuTreeNode *child );

    std::list< MenuTreeNode * > m_children;
    MenuTreeNode *m_parent;
    std::string m_name;
    std::string m_help;
    std::string m_key;
    bool m_matched;
    int m_visible_pos;

    std::function< void( void ) > m_activate_function;
    std::function< void( void ) > m_highlight_function;
    std::function< void( void ) > m_dehighlight_function;
};

#endif
