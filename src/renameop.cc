/* renameop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "renameop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "worker_locale.h"
#include "renameorder.hh"
#include "virtualdirmode.hh"

const char *RenameOp::name="RenameOp";

RenameOp::RenameOp() : FunctionProto()
{
    m_category = FunctionProto::CAT_FILEOPS;
}

RenameOp::~RenameOp()
{
}

RenameOp*
RenameOp::duplicate() const
{
  RenameOp *ta=new RenameOp();
  return ta;
}

bool
RenameOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
RenameOp::getName()
{
  return name;
}

int
RenameOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode * >( lm1 ) ) {
              normalmoderename( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

const char *
RenameOp::getDescription()
{
  return catalog.getLocale(1270);
}

int
RenameOp::normalmoderename( ActionMessage *am )
{
  ListerMode *lm1=NULL;
  struct NM_renameorder renorder;
  NM_specialsourceExt *specialsource=NULL;
  
  if(startlister==NULL) return 1;
  lm1=startlister->getActiveMode();
  if(lm1==NULL) return 1;
  
  memset( &renorder, 0, sizeof( renorder ) );
  if(am->mode==am->AM_MODE_ONLYACTIVE)
    renorder.source=renorder.NM_ONLYACTIVE;
  else if(am->mode==am->AM_MODE_DNDACTION) {
    // insert DND-element into list
    renorder.source=renorder.NM_SPECIAL;
    renorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( NULL );
    //TODO: specialsource nach am besetzen (je nachdem wir ich das realisiere)
    renorder.sources->push_back(specialsource);
  } else if(am->mode==am->AM_MODE_SPECIAL) {
    renorder.source=renorder.NM_SPECIAL;
    renorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( am->getFE() );
    renorder.sources->push_back(specialsource);
  } else
    renorder.source=renorder.NM_ALLENTRIES;

  if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
      vdm->renamef( &renorder );
  }

  if(renorder.source==renorder.NM_SPECIAL) {
    if ( specialsource != NULL ) delete specialsource;
    delete renorder.sources;
  }

  return 0;
}

