/* dirsortsettings.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2018 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DIRSORTSETTINGS_HH
#define DIRSORTSETTINGS_HH

#include "wdefines.h"
#include <vector>

class DirSortSettings
{
public:
    DirSortSettings();
    ~DirSortSettings();
    
    bool check( std::vector< class NWCEntrySelectionState > &d );

    int getSerialNr() const;

    void setSortMode( int sort_mode );
    int getSortMode() const;
private:
    int _serial_nr;
    int _sort_mode;
};

#endif
