/* modifytabsop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MODIFYTABSOP_H
#define MODIFYTABSOP_H

#include "wdefines.h"
#include "functionproto.h"

class Worker;

class ModifyTabsOp : public FunctionProto
{
public:
    ModifyTabsOp();
    virtual ~ModifyTabsOp();
    ModifyTabsOp( const ModifyTabsOp &other );
    ModifyTabsOp &operator=( const ModifyTabsOp &other );
    
    ModifyTabsOp *duplicate() const override;
    bool isName(const char *) override;
    const char *getName() override;
    int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;

    bool save(Datei*) override;
    const char *getDescription() override;
    int configure() override;

    typedef enum {
        NEW_TAB = 0,
        CLOSE_CURRENT_TAB,
        NEXT_TAB,
        PREV_TAB,
        TOGGLE_LOCK_STATE,
        MOVE_TAB_LEFT,
        MOVE_TAB_RIGHT
    } modify_tab_t;
    void setTabAction( modify_tab_t a );

    static const char *name;
private:
    // Infos to save
    modify_tab_t m_tab_action;
    
    // temp variables
    Lister *startlister;
    
    void nm_tab_action( ActionMessage *am );
};

#endif
