/* nmextlist.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NMEXTLIST_HH
#define NMEXTLIST_HH

#include "wdefines.h"
#include <list>
#include <memory>
#include <string>

class NM_extern_fe;
class ActionMessage;
class List;
class FileEntry;
class NM_specialsourceExt;
class ListerMode;

class NMExtList
{
public:
  NMExtList();
  ~NMExtList();
  NMExtList( const NMExtList &other );
  NMExtList &operator=( const NMExtList &other );

  void setTakeDirs( bool nv);
  int initEnum();
  void closeEnum( int );
  NM_extern_fe *getFirstElement( int id );
  NM_extern_fe *getNextElement( int id );
  int removeElement( NM_extern_fe * );
  void createExtList( ListerMode *lm, bool rec );
  void createExtList( ListerMode *lm, ActionMessage *msg, bool rec );

    void resetModifedFlag();
    bool modified() const;
    bool empty();
    const std::string &getFirstFullname() const;
protected:
  List *extlist;
  bool take_dirs;

    bool m_list_done = false;
    bool m_list_modified = false;
    bool m_ondemand_mode = false;
    ListerMode *m_ondemand_lm = nullptr;
    int m_ondemand_state_counter = 0;
    std::string m_ondemand_active_entry;
    bool m_ondemand_rec = false;
    std::string m_first_fullname;

  int create_externfe_rec( const char *fullname,
                           bool rec,
                           const FileEntry *fe );
  int createExtList( std::list< NM_specialsourceExt > &splist, bool rec);
    void gatherNextElement();
};

#endif
