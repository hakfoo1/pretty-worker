/* reloadop.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef RELOADOP_H
#define RELOADOP_H

#include "wdefines.h"
#include "functionproto.h"

class ReloadOp:public FunctionProto
{
public:
  ReloadOp();
  virtual ~ReloadOp();
  ReloadOp( const ReloadOp &other );
  ReloadOp &operator=( const ReloadOp &other );

  ReloadOp *duplicate() const override;
  bool isName(const char *) override;
  const char *getName() override;
  int run( std::shared_ptr< WPUContext > wpu, ActionMessage* ) override;
  bool save(Datei*) override;
  const char *getDescription() override;
  int configure() override;

  typedef enum {RELOADOP_THISSIDE=0,
                RELOADOP_OTHERSIDE,
                RELOADOP_LEFTSIDE,
                RELOADOP_RIGHTSIDE} reload_t;
  void setReloadside(reload_t);
  void setResetDirSizes(bool);
  void setKeepFiletypes( bool );
protected:
  static const char *name;
  // Infos to save
  reload_t reloadside;
  bool reset_dirsizes;
  bool keep_filetypes;

  // temp variables
  Lister *startlister;
  
  void normalmodereload( ActionMessage *am );
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
