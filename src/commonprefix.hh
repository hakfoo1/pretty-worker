/* commonprefix.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COMMONPREFIX_H
#define COMMONPREFIX_H

#include "wdefines.h"
#include <vector>
#include <string>

class CommonPrefix
{
public:
    CommonPrefix() : m_first( true )
    {}

    void updateCommonPrefix( const std::string &fullname );
    std::string getCommonPrefix();
private:
    std::vector< std::string > m_components;
    bool m_first;
};

#endif /* COMMONPREFIX_H */
