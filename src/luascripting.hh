/* luascripting.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef LUASCRIPTING_HH
#define LUASCRIPTING_HH

#include "scriptinginterface.hh"

#ifdef HAVE_LUA

extern "C" {

#  include <lua.h>

};

#endif

class LUAScripting : public ScriptingInterface
{
public:
    LUAScripting();
    ~LUAScripting();

    int setIntVariable( const std::string &name, int val );
    int setBoolVariable( const std::string &name, bool val );
    int setStringVariable( const std::string &name, const std::string &val );

    int evalCommand( const std::string &cmd, int number_return_values );

    int getIntReturnValue( int &res );
    int getBoolReturnValue( bool &res );
    int getStringReturnValue( std::string &res );

    explicit operator bool() {
#ifdef HAVE_LUA
        return m_lua_state != nullptr;
#else
        return false;
#endif
    };

    std::string getErrorString();
private:
#ifdef HAVE_LUA
    void storeError( const char *msg );

    lua_State *m_lua_state;

    std::string m_last_error_string;
#endif
};

#endif
