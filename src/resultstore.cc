/* searchop_resultstore.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "resultstore.hh"

ResultStore::ResultStore( const std::string &searchname, bool follow_symlinks, bool search_vfs, bool search_same_fs )
{
  _dir = new NWC::VirtualDir( searchname, follow_symlinks, false, search_same_fs );
  _res = new std::list<SearchThread::SearchResult>;

  m_p = NULL;
}

ResultStore::~ResultStore()
{
  delete _dir;
  delete _res;
}

NWC::VirtualDir *ResultStore::getDir()
{
  //TODO returning pointer sucks
  return _dir;
}

std::list<SearchThread::SearchResult> *ResultStore::getRes()
{
  //TODO returning pointer sucks
  return _res;
}

NWC::VirtualDir *ResultStore::getClonedDir() const
{
  NWC::VirtualDir *d = NULL;
  NWC::FSEntry *fse = _dir->clone();
  
  if ( fse != NULL ) {
    d = dynamic_cast<NWC::VirtualDir*>( fse );
  }
  
  if ( d == NULL ) {
      d = new NWC::VirtualDir( "searchdir1", _dir->getFollowSymlinks(), false, _dir->getSearchSameFS() );
  }
  return d;
}

void ResultStore::setSearchSettings( const SearchSettings &sets )
{
  _settings = sets;
  _dir->setFollowSymlinks( sets.getFollowSymlinks() );
  _dir->setSearchVFS( false );
  _dir->setSearchSameFS( sets.getSearchSameFS() );
}

SearchSettings ResultStore::getSearchSettings() const
{
  return _settings;
}

void ResultStore::setActiveRow( int row )
{
  _settings.setActiveRow( row );
}

void ResultStore::removeEntries( const std::list< std::pair< std::string, int > > &entry_list )
{
    for ( std::list< std::pair< std::string, int > >::const_iterator it = entry_list.begin();
        it != entry_list.end();
        it++ ) {
      removeEntry( it->first, it->second );
  }
}

void ResultStore::removeEntry( const std::string &entry, int line )
{
    // firstly remove from _res all entries with equal fullname
    // then remove an entry in the dir with the fullname

    bool entry_with_different_line_exists = false;

    for ( std::list<SearchThread::SearchResult>::iterator it = _res->begin();
          it != _res->end(); ) {
        if ( (*it).getFullname() == entry ) {
            if ( line < 0 || it->getLineNr() == line ) {
                std::list<SearchThread::SearchResult>::iterator erase_it = it;
                it++;

                _res->erase( erase_it );
            } else {
                it++;
                entry_with_different_line_exists = true;
            }
        } else {
            it++;
        }
    }

    if ( ! entry_with_different_line_exists ) {
        // no other entry with same full name left so remove it from the list
        _dir->removeFromList( entry );
    }
}

bool ResultStore::set_exclusive( void *p )
{
    if ( m_p != NULL ) return false;

    m_p = p;

    return true;
}

void ResultStore::unset_exclusive( void *p )
{
    if ( m_p == p ) {
        m_p = NULL;
    }
}

bool ResultStore::is_exclusive() const
{
    if ( m_p ) return true;

    return false;
}
