/* nmrowdata.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2003-2007 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "nmrowdata.h"
#include "aguix/lowlevelfunc.h"
#include "fileentry.hh"

NMRowData::NMRowData() : FieldLVRowData()
{
  row = -1;
  fe = NULL;
  fullname = NULL;
}

NMRowData::NMRowData( int trow, const FileEntry *tfe, const char *tfullname )
{
  row = trow;
  if ( tfe != NULL ) {
    fe = new FileEntry( *tfe );
  } else {
    fe = NULL;
  }
  if ( tfullname != NULL ) {
    fullname = dupstring( tfullname );
  } else {
    fullname = NULL;
  }
}

NMRowData::~NMRowData()
{
  if ( fullname != NULL ) _freesafe( fullname );
  if ( fe != NULL ) delete fe;
}

NMRowData *NMRowData::duplicate() const
{
  NMRowData *rd;
  
  rd = new NMRowData( row, fe, fullname );
  return rd;
}

int NMRowData::getRow() const
{
  return row;
}

const FileEntry *NMRowData::getFE() const
{
  return fe;
}

const char *NMRowData::getFullname() const
{
  return fullname;
}

