/* processexitaction.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "processexitaction.hh"
#include "fileviewer.hh"
#include "worker.h"
#include "wpucontext.h"
#include <fstream>
#include "lister.h"
#include "virtualdirmode.hh"

class PEA_FileView_Destroyer : public FileViewerDestroyCallback
{
public:
    PEA_FileView_Destroyer( const std::string &filename ) : m_filename( filename )
    {}
    ~PEA_FileView_Destroyer()
    {
	if ( ! m_filename.empty() ) {
	    worker_unlink( m_filename.c_str() );
	}
    }
private:
    std::string m_filename;
};

ProcessExitAction::ProcessExitAction( bool show_file,
                                      const std::string &tmp_name,
                                      Worker *worker,
                                      std::shared_ptr< WPUContext > wpu )
    : m_show_file( show_file ),
      m_tmp_name( tmp_name ),
      m_worker( worker ),
      m_wpu( wpu )
{
}

ProcessExitAction::~ProcessExitAction()
{
}

void ProcessExitAction::callback( int status )
{
    if ( m_worker != NULL && status != 0 ) {
        m_worker->showCommandFailed( status );
    }

    if ( m_show_file == true &&
         ! m_tmp_name.empty() &&
         m_worker != NULL ) {
        std::list<std::string> l;
        l.push_back( m_tmp_name );
        
        FileViewer fv( m_worker );
        RefCount< PEA_FileView_Destroyer > cb( new PEA_FileView_Destroyer( m_tmp_name ) );

        fv.view( l, cb );
    }

    m_wpu = nullptr;
}

ProcessExitOutputForCustomAttribute::ProcessExitOutputForCustomAttribute( const std::string &original_fullname,
                                                                          int original_id,
                                                                          const std::string &tmp_name,
                                                                          Worker *worker,
                                                                          std::shared_ptr< WPUContext > wpu )
    : m_original_fullname( original_fullname ),
      m_original_id( original_id ),
      m_tmp_name( tmp_name ),
      m_worker( worker ),
      m_wpu( wpu )
{
}

void ProcessExitOutputForCustomAttribute::callback( int status )
{
    if ( m_worker != NULL && status != 0 ) {
        m_worker->showCommandFailed( status );
    } else {
        std::ifstream ifile( m_tmp_name );

        if ( ! ifile.is_open() ) return;

        std::string s1;

        std::getline( ifile, s1 );

        Lister *l1 = m_worker->getActiveLister();
        if ( l1 ) {
            auto lm1 = l1->getActiveMode();

            if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
                vdm->setCustomAttribute( m_original_fullname,
                                         "",
                                         m_original_id,
                                         s1 );
            }
        }
    }

    worker_unlink( m_tmp_name.c_str() );
}
