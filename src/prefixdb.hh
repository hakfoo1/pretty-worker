/* prefixdb.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PREFIXDB_HH
#define PREFIXDB_HH

#include "wdefines.h"
#include <time.h>
#include <string>
#include <list>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include "prefixdbentry.hh"

class PrefixDB
{
public:
    PrefixDB( const std::string &filename );
    virtual ~PrefixDB();

    std::string getBestHit( const std::string &prefix, time_t &return_last_use );
    void pushAccess( const std::string &prefix,
                     const std::string &value,
                     time_t last_use );
    void removeEntry( const std::string &prefix );

    std::set< std::string > getAllStrings();
    std::map< std::string, std::tuple< std::string, float, time_t > > getAllBestHits();

    loff_t getLastSize() const;
private:
    std::vector< PrefixDBEntry > m_db;
    time_t m_next_aging;

    std::string m_filename;
    time_t m_lastmod;
    loff_t m_lastsize;

    void read();
    void write();
    void age( float factor = 0.8 );
};

#endif /* PREFIXDB_HH */
