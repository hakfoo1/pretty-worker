/* stringmatcher_fnmatch.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2007-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "stringmatcher_fnmatch.hh"
#include "aguix/lowlevelfunc.h"
#include "aguix/utf8.hh"
#include "aguix/util.h"
#include <cctype>
#include <algorithm>
#include <functional>

StringMatcherFNMatch::StringMatcherFNMatch() : _match_case_sensitive( false )
{
}

StringMatcherFNMatch::~StringMatcherFNMatch()
{
}

bool StringMatcherFNMatch::match( const std::string &str )
{
    bool erg = false;
    
    if ( _match_case_sensitive == false ) {
#ifdef HAVE_GNU_FNMATCH
        if ( fnmatch( _match_string.c_str(), str.c_str(), FNM_CASEFOLD ) == 0 )
            erg = true;
#else
        char *lstr;
        int i;
        
        //TODO this is not UTF8 safe
        lstr = dupstring( str.c_str() );
        for ( i = 0; lstr[i] != '\0'; i++ )
            lstr[i] = tolower( lstr[i] );
        
        if ( fnmatch( _match_string_lowercase.c_str(), lstr, 0 ) == 0 )
            erg = true;
        
        _freesafe( lstr );
#endif
    } else {
        if ( fnmatch( _match_string.c_str(), str.c_str(), 0 ) == 0 )
            erg = true;
    }

    return erg;
}

void StringMatcherFNMatch::setMatchString( const std::string &str )
{
    _match_string = str;
    createLoweredCase();
}

std::string StringMatcherFNMatch::getMatchString() const
{
    return _match_string;
}

void StringMatcherFNMatch::setMatchCaseSensitive( bool nv )
{
    _match_case_sensitive = nv;
}

bool StringMatcherFNMatch::getMatchCaseSensitive() const
{
    return _match_case_sensitive;
}

void StringMatcherFNMatch::createLoweredCase()
{
    _match_string_lowercase = AGUIXUtils::tolower( _match_string );
}

std::string StringMatcherFNMatch::convertMatchStringToFlexibleMatch( const std::string &str )
{
    std::string res;

    for ( const char *cstr = str.c_str();
          *cstr != '\0'; ) {
        if ( *cstr != '*' ) {
            size_t l = UTF8::getLenOfCharacter( cstr );

            if ( l < 1 ) break;

            if ( ! res.empty() ) {
                res += '*';
            }

            while ( l > 0 ) {
                res += *cstr++;
                l--;
            }
        } else {
            cstr++;
        }
    }

    return res;
}

std::string StringMatcherFNMatch::cleanFlexibleMatchString( const std::string &str )
{
    std::string res;

    for ( const char *cstr = str.c_str();
          *cstr != '\0'; ) {
        if ( *cstr != '*' ) {
            size_t l = UTF8::getLenOfCharacter( cstr );

            if ( l < 1 ) break;

            while ( l > 0 ) {
                res += *cstr++;
                l--;
            }
        } else {
            cstr++;
        }
    }

    return res;
}
