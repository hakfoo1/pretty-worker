/* prefixpathtree.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PREFIXPATHTREE_HH
#define PREFIXPATHTREE_HH

#include "deeppathstore.hh"
#include <vector>

class PrefixPathTree
{
public:
    PrefixPathTree();

    enum insert_result {
        IS_NOT_FOUND,
        IS_ADDED,
        IS_IGNORED,
        IS_PREFIX,
        IS_MATCH
    };

    insert_result insert( const std::string &path );
    insert_result lookup( const std::string &path );
    std::list< std::string > getPaths();
    void clear();
private:
    insert_result storePath( DeepPathNode &node,
                             const std::vector< std::string > &path_components,
                             size_t offset );

    insert_result lookup( DeepPathNode &node,
                          const std::vector< std::string > &path_components,
                          size_t offset );

    DeepPathNode m_root;
};

#endif
