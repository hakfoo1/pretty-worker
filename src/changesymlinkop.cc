/* changesymlinkop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "changesymlinkop.h"
#include "listermode.h"
#include "worker.h"
#include "nmspecialsourceext.hh"
#include "worker_locale.h"
#include "changesymlinkorder.hh"
#include "virtualdirmode.hh"
#include "datei.h"
#include "aguix/cyclebutton.h"
#include "aguix/button.h"

const char *ChangeSymlinkOp::name="ChangeSymlinkOp";

ChangeSymlinkOp::ChangeSymlinkOp() : FunctionProto()
{
    hasConfigure = true;
    m_mode = CHANGESYMLINK_EDIT;
    m_category = FunctionProto::CAT_FILEOPS;
}

ChangeSymlinkOp::~ChangeSymlinkOp()
{
}

ChangeSymlinkOp*
ChangeSymlinkOp::duplicate() const
{
  ChangeSymlinkOp *ta=new ChangeSymlinkOp();
  ta->m_mode = m_mode;
  return ta;
}

bool
ChangeSymlinkOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
ChangeSymlinkOp::getName()
{
  return name;
}

int
ChangeSymlinkOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      lm1=startlister->getActiveMode();
      if(lm1!=NULL) {
          if(dynamic_cast< VirtualDirMode *>( lm1 ) ) {
              normalmodechangesl( msg );
          } else {
              lm1->not_supported();
          }
      }
    }
  }
  return 0;
}

const char *
ChangeSymlinkOp::getDescription()
{
  return catalog.getLocale(1278);
}

int
ChangeSymlinkOp::normalmodechangesl( ActionMessage *am )
{
  ListerMode *lm1 = NULL;
  NM_specialsourceExt *specialsource=NULL;
  struct NM_changesymlinkorder chslorder;
  
  if(startlister==NULL) return 1;
  lm1=startlister->getActiveMode();
  if(lm1==NULL) return 1;

  memset( &chslorder, 0, sizeof( chslorder ) );
  if(am->mode==am->AM_MODE_ONLYACTIVE)
    chslorder.source=chslorder.NM_ONLYACTIVE;
  else if(am->mode==am->AM_MODE_DNDACTION) {
    // insert DND-element into list
    chslorder.source=chslorder.NM_SPECIAL;
    chslorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( NULL );
    //TODO: specialsource nach am besetzen (je nachdem wir ich das realisiere)
    chslorder.sources->push_back(specialsource);
  } else if(am->mode==am->AM_MODE_SPECIAL) {
    chslorder.source=chslorder.NM_SPECIAL;
    chslorder.sources=new std::list<NM_specialsourceExt*>;
    specialsource = new NM_specialsourceExt( am->getFE() );
    chslorder.sources->push_back(specialsource);
  } else {
    chslorder.source=chslorder.NM_ALLENTRIES;
  }

  switch ( m_mode ) {
      case CHANGESYMLINK_MAKE_ABSOLUTE:
          chslorder.mode = chslorder.CSLO_MAKE_ABSOLUTE;
          break;
      case CHANGESYMLINK_MAKE_RELATIVE:
          chslorder.mode = chslorder.CSLO_MAKE_RELATIVE;
          break;
      case CHANGESYMLINK_EDIT:
          // this is the default mode, not write it
      default:
          chslorder.mode = chslorder.CSLO_EDIT;
          break;
  }
  
  if ( auto vdm = dynamic_cast< VirtualDirMode * >( lm1 ) ) {
      vdm->changesymlink( &chslorder );
  }

  if(chslorder.source==chslorder.NM_SPECIAL) {
    if ( specialsource != NULL ) delete specialsource;
    delete chslorder.sources;
  }
  return 0;
}

void ChangeSymlinkOp::setMode( changesymlink_mode_t nv )
{
    m_mode = nv;
}

bool ChangeSymlinkOp::save( Datei *fh )
{
  if ( fh == NULL ) return false;
  
  switch ( m_mode ) {
      case CHANGESYMLINK_MAKE_ABSOLUTE:
          fh->configPutPair( "mode", "makeabsolute" );
          break;
      case CHANGESYMLINK_MAKE_RELATIVE:
          fh->configPutPair( "mode", "makerelative" );
          break;
      case CHANGESYMLINK_EDIT:
          // this is the default mode, not write it
      default:
          break;
  }

  return true;
}

int ChangeSymlinkOp::configure()
{
    AGUIX *aguix = Worker::getAGUIX();
    AWindow *win;
    AGMessage *msg;
    int endmode = -1;

    char *tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );

    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe( tstr );

    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( -1 );
    ac1_1->setBorderWidth( 0 );

    ac1_1->addWidget( new Text( aguix, 0, 0, catalog.getLocale( 1092 ) ),
                      0, 0, AContainer::CO_FIX );
    CycleButton *mode_cyb = ac1_1->addWidget( new CycleButton( aguix, 0, 0, 100, 0 ),
                                              1, 0, AContainer::CO_INCW );
    mode_cyb->addOption( catalog.getLocale( 1093 ) );
    mode_cyb->addOption( catalog.getLocale( 1094 ) );
    mode_cyb->addOption( catalog.getLocale( 1095 ) );
    mode_cyb->resize( mode_cyb->getMaxSize(),
                      mode_cyb->getHeight() );
    ac1_1->readLimits();

    switch ( m_mode ) {
        case CHANGESYMLINK_MAKE_ABSOLUTE:
            mode_cyb->setOption( 1 );
            break;
        case CHANGESYMLINK_MAKE_RELATIVE:
            mode_cyb->setOption( 2 );
            break;
        default:
            mode_cyb->setOption( 0 );
            break;
    }

    AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
    ac1_2->setMinSpace( 5 );
    ac1_2->setMaxSpace( -1 );
    ac1_2->setBorderWidth( 0 );
    Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 11 ),
                                                  0 ), 0, 0, AContainer::CO_FIX );
    Button *cb = (Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 8 ),
                                                  0 ), 1, 0, AContainer::CO_FIX );
  
    okb->takeFocus();
    win->setDoTabCycling( true );
    win->contMaximize( true );
    win->show();

    for (; endmode == -1 ; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) {
                        endmode = 0;
                    } else if ( msg->button.button == cb ) endmode = 1;
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
  
    if ( endmode == 0 ) {
        // ok
        switch ( mode_cyb->getSelectedOption() ) {
            case 1:
                m_mode = CHANGESYMLINK_MAKE_ABSOLUTE;
                break;
            case 2:
                m_mode = CHANGESYMLINK_MAKE_RELATIVE;
                break;
            default:
                m_mode = CHANGESYMLINK_EDIT;
                break;
        }
    }
  
    delete win;

    return endmode;
}
