/* wconfig_palette.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_palette.hh"
#include "wconfig.h"
#include "wc_color.hh"
#include "worker.h"
#include <string>
#include <sstream>
#include "aguix/acontainer.h"
#include "aguix/acontainerbb.h"
#include "aguix/kartei.h"
#include "aguix/slider.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "aguix/solidbutton.h"
#include "worker_locale.h"
#include "simplelist.hh"

PalettePanel::PalettePanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  tempconfig = _baseconfig.duplicate();
  labelindex = -1;
  _values_changed = false;
}

PalettePanel::~PalettePanel()
{
  delete tempconfig;
}

int PalettePanel::create()
{
  int sx;
  Panel::create();
  
  AContainer *ac1 = setContainer( new AContainer( this, 1, 2 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  addMultiLineText( catalog.getLocale( 671 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 1 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );
  ac1_1->setBorderWidth( 0 );

  sx = (int)sqrt( (double)( tempconfig->getColors()->size() ) );
  if ( ( sx * sx ) < tempconfig->getColors()->size() ) sx++;
  AContainerBB *ac1_1_1 = static_cast<AContainerBB*>( ac1_1->add( new AContainerBB( this, sx, sx ), 0, 0 ) );
  ac1_1_1->setMinSpace( 0 );
  ac1_1_1->setMaxSpace( 0 );

  int th = 256 / sx;
  for ( int i = 0; i < tempconfig->getColors()->size(); i++ ) {
    colbs[i] = (Button*)ac1_1_1->add( new Button( _aguix,
						  0,
						  0,
						  th,
						  th,
						  "",
						  0,
						  i,
						  i + 1 ), i % sx, i / sx, AContainer::CO_FIX  );
    colbs[i]->connect( this );
  }

  AContainer *ac1_2 = ac1_1->add( new AContainer( this, 1, 3 ), 1, 0 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );
  ac1_2->setBorderWidth( 0 );

  AContainer *ac1_2_1 = ac1_2->add( new AContainer( this, 2, 1 ), 0, 0 );
  ac1_2_1->setMinSpace( 5 );
  ac1_2_1->setMaxSpace( 5 );
  ac1_2_1->setBorderWidth( 0 );

  ac1_2_1->add( new Text( _aguix,
                          0,
                          0,
                          catalog.getLocale(27)
                          ), 0, 0, AContainer::CO_FIX );
  palentryt = (Text*)ac1_2_1->add( new Text( _aguix,
                                             0,
                                             0,
                                             "   "
                                             ), 1, 0, AContainer::CO_INCWNR );

  AContainer *ac1_2_2 = ac1_2->add( new AContainer( this, 3, 3 ), 0, 1 );
  ac1_2_2->setMinSpace( 5 );
  ac1_2_2->setMaxSpace( 5 );
  ac1_2_2->setBorderWidth( 0 );

  ac1_2_2->add( new Text( _aguix,
                          0,
                          0,
                          catalog.getLocale(28)
                          ), 0, 0, AContainer::CO_FIX );
  redsg = (StringGadget*)ac1_2_2->add( new StringGadget( _aguix,
                                                         0,
                                                         0,
                                                         _aguix->getTextWidth( "000000" ),
                                                         "",
                                                         1 ), 1, 0, AContainer::CO_FIX );
  redsg->connect( this );
  redsl = (Slider*)ac1_2_2->add( new Slider( _aguix,
                                             0,
                                             0,
                                             100,
                                             20,
                                             false,
                                             0 ), 2, 0, AContainer::CO_INCW );
  redsl->connect( this );
  redsl->setMaxDisplay( 1 );
  redsl->setMaxLen( 256 );
  redsl->setDisplayFocus( true );
  ac1_2_2->add( new Text( _aguix,
                          0,
                          0,
                          catalog.getLocale( 29 )
                          ), 0, 1, AContainer::CO_FIX );
  greensg = (StringGadget*)ac1_2_2->add( new StringGadget( _aguix,
                                                           0,
                                                           0,
                                                           _aguix->getTextWidth( "000000" ),
                                                           "",
                                                           1 ), 1, 1, AContainer::CO_FIX );
  greensg->connect( this );
  greensl = (Slider*)ac1_2_2->add( new Slider( _aguix,
                                               0,
                                               0,
                                               100,
                                               20,
                                               false,
                                               0 ), 2, 1, AContainer::CO_INCW );
  greensl->connect( this );
  greensl->setMaxDisplay( 1 );
  greensl->setMaxLen( 256 );
  greensl->setDisplayFocus( true );
  ac1_2_2->add( new Text( _aguix,
                          0,
                          0,
                          catalog.getLocale( 30 )
                          ), 0, 2, AContainer::CO_FIX );
  bluesg = (StringGadget*)ac1_2_2->add( new StringGadget( _aguix,
                                                          0,
                                                          0,
                                                          _aguix->getTextWidth( "000000" ),
                                                          "",
                                                          1 ), 1, 2, AContainer::CO_FIX );
  bluesg->connect( this );
  bluesl = (Slider*)ac1_2_2->add( new Slider( _aguix,
                                              0,
                                              0,
                                              100,
                                              20,
                                              false,
                                              0 ), 2, 2, AContainer::CO_INCW );
  bluesl->connect( this );
  bluesl->setMaxDisplay( 1 );
  bluesl->setMaxLen( 256 );
  bluesl->setDisplayFocus( true );
  editcolsb = (SolidButton*)ac1_2->add( new SolidButton( _aguix,
                                                         0,
                                                         0,
                                                         32,
                                                         32,
                                                         "",
                                                         0,
                                                         0,
                                                         false ), 0, 2, AContainer::CO_INCW );
  
  showColor( 0 );
  contMaximize( true );
  return 0;
}

int PalettePanel::saveValues()
{
  _baseconfig.setColors( tempconfig->getColors() );
  return 0;
}

void PalettePanel::run( Widget *elem, const AGMessage &msg )
{
  List *colors = tempconfig->getColors();
  char *tstr;
  WC_Color *tc1;

  if ( _need_recreate == true ) return;

  if ( msg.type == AG_BUTTONCLICKED ) {
    if ( msg.button.button->getData() > 0 ) {
      // ein Eintrag wurde ausgewählt

      showColor( msg.button.button->getData() - 1 );
    }
  } else if(msg.type==AG_STRINGGADGET_DEACTIVATE) {
    if ( labelindex >= 0 ) {
      int val = 0;
      sscanf( msg.stringgadget.sg->getText(), "%d", &val );
      if ( val < 0 ) val = 0;
      if ( val > 255 ) val = 255;
      tstr = (char*)_allocsafe( A_BYTESFORNUMBER( int ) );
      sprintf( tstr, "%d", val );
      
      tc1=(WC_Color*)colors->getElementAt(labelindex);
      
      if ( msg.stringgadget.sg == redsg ) {
        redsg->setText(tstr);
        redsl->setOffset( val );
        tc1->setRed( val );
      } else if ( msg.stringgadget.sg == greensg ) {
        greensg->setText(tstr);
        greensl->setOffset( val );
        tc1->setGreen( val );
      } else if ( msg.stringgadget.sg == bluesg ) {
        bluesg->setText(tstr);
        bluesl->setOffset( val );
        tc1->setBlue( val );
      }
      _freesafe(tstr);
      
      _aguix->changeColor( labelindex, tc1->getRed(), tc1->getGreen(), tc1->getBlue() );
      if ( labelindex > 3 ) {
        editcolsb->redraw();
        colbs[labelindex]->redraw();
      } else redraw();

      _values_changed = true;
    } else {
      if(msg.stringgadget.sg==redsg) {
        redsg->setText("");
      } else if(msg.stringgadget.sg==greensg) {
        greensg->setText("");
      } else if(msg.stringgadget.sg==bluesg) {
        bluesg->setText("");
      }
    }
  } else if ( msg.type == AG_SLIDER_CHANGED && labelindex >= 0 ) {
    tstr = (char*)_allocsafe( A_BYTESFORNUMBER( int ) );
    sprintf( tstr, "%d", msg.slider.offset );
    
    tc1 = (WC_Color*)colors->getElementAt( labelindex );
    
    if ( msg.slider.slider == redsl ) {
      redsg->setText( tstr );
      tc1->setRed( msg.slider.offset );
    } else if ( msg.slider.slider == greensl ) {
      greensg->setText( tstr );
      tc1->setGreen( msg.slider.offset );
    } else if ( msg.slider.slider == bluesl ) {
      bluesg->setText( tstr );
      tc1->setBlue( msg.slider.offset );
    }
    
    _freesafe( tstr );
    
    _aguix->changeColor( labelindex, tc1->getRed(), tc1->getGreen(), tc1->getBlue() );
    if ( labelindex > 3 ) {
      editcolsb->redraw();
      colbs[labelindex]->redraw();
    } else redraw();
    
    _values_changed = true;
  }
}

void PalettePanel::hide()
{
  if ( _values_changed == true ) {
    if ( _conf_cb != NULL )
      _conf_cb->setColors( tempconfig->getColors() );
    _values_changed = false;
  }
  WConfigPanel::hide();
}

WConfigPanel::panel_action_t PalettePanel::setColors( List *colors )
{
  if ( colors != tempconfig->getColors() ) {
    tempconfig->setColors( colors );

    _need_recreate = true;
    return PANEL_RECREATE;
  }
  return PANEL_NOACTION;
}

void PalettePanel::showColor( int col )
{
  List *colors = tempconfig->getColors();
  WC_Color *tc1;
  
  if ( col < 0 || col >= colors->size() ) return;
  
  labelindex = col;
  
  editcolsb->setBG( labelindex );
  tc1 = (WC_Color*)colors->getElementAt( labelindex );

  {
    std::stringstream redval;
    redval << tc1->getRed();
    redsg->setText( redval.str().c_str() );
    redsl->setOffset( tc1->getRed() );
  }
  
  {
    std::stringstream greenval;
    greenval << tc1->getGreen();
    greensg->setText( greenval.str().c_str() );
    greensl->setOffset( tc1->getGreen() );
  }

  {
    std::stringstream blueval;
    blueval << tc1->getBlue();
    bluesg->setText( blueval.str().c_str() );
    bluesl->setOffset( tc1->getBlue() );
  }

  {
    std::stringstream entryval;
    entryval << labelindex;
    palentryt->setText( entryval.str().c_str() );
  }
}
