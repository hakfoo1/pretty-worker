/* wchotkey.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wchotkey.hh"
#include "wcdoubleshortkey.hh"
#include "datei.h"
#include "aguix/util.h"
#include "functionproto.h"
#include "simplelist.hh"
 
WCHotkey::WCHotkey()
{
  name=dupstring("");
}

WCHotkey::~WCHotkey()
{
  if(name!=NULL) _freesafe(name);
}

WCHotkey *WCHotkey::duplicate() const
{
  WCHotkey *thot=new WCHotkey();
  thot->setName(name);
  thot->setComs(com);
  thot->setDoubleKeys( dkeys );
  return thot;
}

void WCHotkey::setName(const char *nname)
{
  if(name!=NULL) _freesafe(name);
  if(nname==NULL) {
    name=dupstring("");
  } else {
    name=dupstring(nname);
  }
}

char *WCHotkey::getName()
{
  return name;
}

command_list_t WCHotkey::getComs()
{
  return com;
}

void WCHotkey::setComs( const command_list_t &ncom)
{
    com.clear();
    for ( auto &fp : ncom ) {
        com.push_back( std::shared_ptr< FunctionProto >( fp->duplicate() ) );
    }
}

bool WCHotkey::save(Datei *fh) const
{
  int id;
  WCDoubleShortkey *dk;

  if(fh==NULL) return false;

  id = dkeys->initEnum();

  fh->configPutPairString( "title", name );
  fh->configOpenSection( "shortkeys" );
  dk = (WCDoubleShortkey*)dkeys->getFirstElement( id );
  while ( dk != NULL ) {
    dk->save( fh );
    dk = (WCDoubleShortkey*)dkeys->getNextElement( id );
  }
  dkeys->closeEnum( id );
  fh->configCloseSection(); //shortkeys

  fh->configOpenSection( "commands" );
  for ( auto &f1 : com ) {
      FunctionProto::presave( fh, f1.get() );
  }
  fh->configCloseSection(); //commands
  return true;
}
