/* togglelistermodeop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "togglelistermodeop.h"
#include "listermode.h"
#include "worker.h"
#include "datei.h"
#include "aguix/cyclebutton.h"
#include "aguix/button.h"
#include "aguix/awindow.h"
#include "worker_locale.h"

const char *ToggleListermodeOp::name="ToggleListermodeOp";

ToggleListermodeOp::ToggleListermodeOp() : FunctionProto()
{
  mode=0;
  lastmode=-1;
  hasConfigure = true;
    m_category = FunctionProto::CAT_SETTINGS;
}

ToggleListermodeOp::~ToggleListermodeOp()
{
}

ToggleListermodeOp*
ToggleListermodeOp::duplicate() const
{
  ToggleListermodeOp *ta=new ToggleListermodeOp();
  ta->mode=mode;

  return ta;
}

bool
ToggleListermodeOp::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *
ToggleListermodeOp::getName()
{
  return name;
}

int
ToggleListermodeOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  int actmode;
  ListerMode *lm1;
  if(msg->mode!=msg->AM_MODE_DNDACTION) {
    Lister *l1 = msg->getWorker()->getActiveLister();
    if(l1!=NULL) {
      startlister=l1;
      endlister = msg->getWorker()->getOtherLister(startlister);
      lm1=endlister->getActiveMode();
      if(lm1==NULL) actmode=-1;
      else actmode = msg->getWorker()->getID4Mode(lm1);
      if(actmode<0) {
        endlister->switch2Mode(mode);
      } else {
        if(actmode!=mode) endlister->switch2Mode(mode);
        else if((lastmode>=0)&&(lastmode!=mode)) endlister->switch2Mode(lastmode);
        else endlister->switch2Mode(0);
      }
      lastmode=actmode;
    }
  }
  return 0;
}

bool
ToggleListermodeOp::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  fh->configPutPairString( "mode", Worker::getNameOfMode( mode ) );
  return true;
}

const char *
ToggleListermodeOp::getDescription()
{
  return catalog.getLocale(1280);
}

int
ToggleListermodeOp::configure()
{
  AGUIX *aguix = Worker::getAGUIX();
  AWindow *win;
  CycleButton *cyb;
  AGMessage *msg;
  int endmode=-1;
  int i;
  char *tstr;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr=(char*)_allocsafe(strlen(catalog.getLocale(293))+strlen(getDescription())+1);
  sprintf(tstr,catalog.getLocale(293),getDescription());
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, 3 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  ac1->add( new Text( aguix, 0, 0, catalog.getLocale( 112 ) ), 0, 0, cincwnr );

  cyb = (CycleButton*)ac1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 0, 1, cincw );
  for ( i = 0; i < Worker::getMaxModeNr(); i++ ) {
    cyb->addOption( Worker::getLocaleNameOfMode( i ) );
  }
  cyb->resize(cyb->getMaxSize(),cyb->getHeight());
  cyb->setOption(mode);
  ac1->readLimits();
  
  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 2 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( -1 );
  ac1_2->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_2->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) endmode=0;
          else if(msg->button.button==cb) endmode=1;
          break;
        case AG_KEYPRESSED:
            if ( win->isParent( msg->key.window, false ) == true ) {
                switch ( msg->key.key ) {
                    case XK_Escape:
                        endmode = 1;
                        break;
                }
            }
            break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  
  if(endmode==0) {
    // ok
    mode=cyb->getSelectedOption();
  }
  
  delete win;

  return endmode;
}

void ToggleListermodeOp::setMode(int nm)
{
  if ( ( nm >= 0 ) && ( nm < Worker::getMaxModeNr() ) ) {
    mode=nm;
  }
}

