/* wconfig_pathjump_allow.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2012-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_pathjump_allow.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/fieldlistview.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "datei.h"
#include "nwc_path.hh"

PathJumpAllowPanel::PathJumpAllowPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
    m_lv = NULL;
}

PathJumpAllowPanel::~PathJumpAllowPanel()
{
}

int PathJumpAllowPanel::create()
{
    Panel::create();

    int trow;

    AContainer *ac1 = setContainer( new AContainer( this, 1, 4 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    addMultiLineText( catalog.getLocale( 975 ),
                      *ac1,
                      0, 0,
                      NULL, NULL );

    m_lv =(FieldListView*)ac1->add( new FieldListView( _aguix, 0, 0, 100, 100, 0 ), 0, 1, AContainer::CO_MIN );
    m_lv->setHBarState(2);
    m_lv->setVBarState(2);
    m_lv->setAcceptFocus( true );
    m_lv->setDisplayFocus( true );

    AContainer *ac1_1 = ac1->add( new AContainer( this, 2, 1 ), 0, 2 );
    ac1_1->setMinSpace( 0 );
    ac1_1->setMaxSpace( 0 );
    ac1_1->setBorderWidth( 0 );

    m_newb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 181 ), 0 ),
                                  0, 0, AContainer::CO_INCW );
    m_newb->connect( this );

    m_delb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 183 ), 0 ),
                                  1, 0, AContainer::CO_INCW );
    m_delb->connect( this );

    m_store_files_cb = ac1->addWidget( new ChooseButton( _aguix, 0, 0,
                                                         _baseconfig.getPathJumpStoreFilesAlways(),
                                                         catalog.getLocale( 1043 ),
                                                         LABEL_RIGHT,
                                                         0 ), 0, 3, AContainer::CO_INCWNR );

    contMaximize( true );

    const std::list< std::string > &l = _baseconfig.getPathJumpAllowDirs();

    for ( std::list< std::string >::const_iterator it1 = l.begin();
          it1 != l.end();
          it1++ ) {
        trow = m_lv->addRow();
        m_lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
        m_lv->setText( trow, 0, it1->c_str() );
    }
    m_lv->redraw();

    return 0;
}

int PathJumpAllowPanel::saveValues()
{
    if ( m_lv == NULL ) return 1;
  
    std::list< std::string > l;

    int trow = 0;

    while ( m_lv->isValidRow( trow ) == true ) {
        l.push_back( m_lv->getText( trow, 0 ) );
        trow++;
    }
  
    _baseconfig.setPathJumpAllowDirs( l );

    _baseconfig.setPathJumpStoreFilesAlways( m_store_files_cb->getState() );

    return 0;
}

void PathJumpAllowPanel::run( Widget *elem, const AGMessage &msg )
{
    char *tstr, *tstr2;
    bool found;
    int trow;

    if ( msg.type == AG_BUTTONCLICKED ) {
        if ( msg.button.button == m_newb ) {
            std::string buttonstr = catalog.getLocale( 11 );
            buttonstr += "|";
            buttonstr += catalog.getLocale( 8 );

            if ( string_request( catalog.getLocale( 123 ), catalog.getLocale( 976 ), "", buttonstr.c_str(), &tstr ) == 0 ) {
                if ( strlen( tstr ) > 0 ) {
                    if ( tstr[0] == '/' ) {
                        tstr2 = NWC::Path::handlePath( tstr );
	    
                        found = false;
                        trow = 0;
                        while ( m_lv->isValidRow( trow ) == true ) {
                            if ( strcmp( m_lv->getText( trow, 0 ).c_str(), tstr2 ) == 0 ) {
                                found = true;
                                break;
                            }
                            trow++;
                        }

                        if ( found == false ) {
                            trow = m_lv->addRow();
                            m_lv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
                            m_lv->setText( trow, 0, tstr2 );
                            m_lv->setActiveRow( trow );
                            m_lv->showActive();
                            m_lv->redraw();
                        }
                        _freesafe( tstr2 );
                    }
                }
                _freesafe( tstr );
            } else _freesafe( tstr );
        } else if ( msg.button.button == m_delb ) {
            trow = m_lv->getActiveRow();
            if ( m_lv->isValidRow( trow ) == true ) {
                m_lv->deleteRow( trow );
                m_lv->redraw();
            }
        }
    }
}
