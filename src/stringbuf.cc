/* stringbuf.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2004 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* $Id: stringbuf.cc,v 1.2 2004/10/27 21:09:26 ralf Exp $ */

#include "stringbuf.h"
#include "aguix/lowlevelfunc.h"

StringBuf::StringBuf()
{
  maxsize = 1024;
}

StringBuf::~StringBuf()
{
  clear();
}

void StringBuf::clear()
{
  while ( buf.size() > 0 ) {
    freeLast();
  }
  buf.clear();
}

const char *StringBuf::find( const char *tkey )
{
  sb_entry_t *pe1;
  
  if ( tkey == NULL ) return NULL;

  pe1 = buf.findToFront( tkey );
  if ( pe1 != NULL ) {
    return pe1->str;
  }
  return NULL;
}

int StringBuf::findValue( const char *tkey, int *return_value )
{
  sb_entry_t *pe1;
  
  if ( tkey == NULL ) return 1;

  pe1 = buf.findToFront( tkey );
  if ( pe1 != NULL ) {
    if ( return_value != NULL ) *return_value = pe1->value;
    return 0;
  }
  return 1;
}

int StringBuf::find( const char *tkey, const char **return_str, int *return_value )
{
  sb_entry_t *pe1;
  
  if ( tkey == NULL ) return 1;

  pe1 = buf.findToFront( tkey );
  if ( pe1 != NULL ) {
    if ( return_str != NULL ) *return_str = pe1->str;
    if ( return_value != NULL ) *return_value = pe1->value;
    return 0;
  }
  return 1;
}

void StringBuf::add( const char *tkey, const char *tstr, int tv )
{
  sb_entry_t te;
  if ( buf.size() >= maxsize ) {
    freeLast();
  }
  te.str = dupstring( tstr );
  te.value = tv;
  buf.insert( tkey, te );
}

void StringBuf::freeLast()
{
  sb_entry_t *pe1;

  pe1 = buf.getLast();
  if ( pe1 != NULL ) {
    if ( pe1->str != NULL ) _freesafe( pe1->str );
    buf.removeLast();
  }
}

