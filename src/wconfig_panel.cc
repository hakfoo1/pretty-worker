/* wconfig_panel.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2006-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_panel.hh"
#include "wconfig.h"
#include "worker.h"

WConfigPanel::WConfigPanel( AWindow &basewin, WConfig &baseconfig ) : Panel( basewin ),
                                                                      _baseconfig( baseconfig ),
                                                                      _conf_cb( NULL ),
                                                                      _need_recreate( false )
{
}

WConfigPanel::~WConfigPanel()
{
}

WConfigPanel::panel_action_t WConfigPanel::setColors( List *colors )
{
  return PANEL_NOACTION;
}

WConfigPanel::panel_action_t WConfigPanel::setRows( int rows )
{
  return PANEL_NOACTION;
}

WConfigPanel::panel_action_t WConfigPanel::setColumns( int columns )
{
  return PANEL_NOACTION;
}

WConfigPanel::panel_action_t WConfigPanel::setFaceDB( const FaceDB &faces )
{
  return PANEL_NOACTION;
}

int WConfigPanel::addButtons( List *buttons, WConfigPanelCallBack::add_action_t action )
{
  return 0;
}

int WConfigPanel::addHotkeys( List *hotkeys, WConfigPanelCallBack::add_action_t action )
{
  return 0;
}

int WConfigPanel::addFiletypes( List *filetypes, WConfigPanelCallBack::add_action_t action )
{
  return 0;
}

void WConfigPanel::executeInitialCommand( const WConfigInitialCommand &cmd )
{
}

bool WConfigPanel::need_recreate() const
{
  return _need_recreate;
}

void WConfigPanel::setConfCB( WConfigPanelCallBack *conf_cb )
{
  _conf_cb = conf_cb;
}

bool WConfigPanel::escapeInUse()
{
    return false;
}

WConfigPanelCallBack::WConfigPanelCallBack()
{
}

WConfigPanelCallBack::~WConfigPanelCallBack()
{
}

void WConfigPanelCallBack::setColors( List *colors )
{
}

void WConfigPanelCallBack::setRows( int rows )
{
}

void WConfigPanelCallBack::setColumns( int columns )
{
}

void WConfigPanelCallBack::setFaceDB( const FaceDB &faces )
{
}

int WConfigPanelCallBack::addButtons( List *buttons, add_action_t action )
{
  return 0;
}

int WConfigPanelCallBack::addHotkeys( List *hotkeys, add_action_t action )
{
  return 0;
}

int WConfigPanelCallBack::addFiletypes( List *filetypes, add_action_t action )
{
  return 0;
}
