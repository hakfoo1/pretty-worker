/* ajson_test.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <iostream>
#include <check.h>

#include "ajson.hh"
#include "ajson_scan.hh"

using namespace AJSON;

START_TEST(json_test1)
{
	auto o1 = std::shared_ptr<JSONObject>( new JSONObject() );

    ck_assert_ptr_ne( o1.get(), NULL );

#if 0
	o1->set( "foo", make_string( "bar" ) );

	auto a2 = make_array();

	a2->append( make_string( "inner" ) );
	
	auto a1 = make_array();

	auto t1 = std::dynamic_pointer_cast< JSONType >( a1 );

	std::cout << t1.get() << std::endl;
	
	a1->append( make_string( "foo" ) );
	a1->append( make_string( "b\"ar" ) );
	a1->append( a2 );

	o1->set( "a1", a1 );
	
	dump( o1, "test.json" );

    //int res = ajson_parser( "{ \"foo\" : \"bar\" }" );
    //int res = ajson_parser( "[ \"foo\" , \"bar\" ]" );

    std::list< std::pair< enum json_token, std::string > > tokens;

    int res = ajson_scanner( "[ \"foo\" , { \"foo\" : \"bar\" } ]", tokens );

    for ( auto &t : tokens ) {
        std::cout << t.first << ": " << t.second << std::endl;
    }

    std::cout << "res:" << res << std::endl;
#endif

    JSONParser p;

    auto res2 = p.parse( "[ \"foo\" , { \"foo\" : \"b\\\"ar\", \"foo2\" : \"baz\" } ]" );

    auto res2_a = as_array( res2 );

    ck_assert_ptr_ne( res2_a.get(), NULL );

    auto res2_a1 = as_object( res2_a->get( 1 ) );

    ck_assert_ptr_ne( res2_a1.get(), NULL );

    auto res2_a1_o_s = as_string( res2_a1->get( "foo" ) );
    
    ck_assert_ptr_ne( res2_a1_o_s.get(), NULL );

    ck_assert_int_eq( strcmp( res2_a1_o_s->get_value().c_str(), "b\"ar" ), 0 );

#if 0
    dump( res2, "res2.json" );
#endif
}
END_TEST

START_TEST(json_test2)
{
    JSONParser p;

    auto res2 = p.parse( "{ \"foo\" : 12345 }" );

    auto res2_o = as_object( res2 );

    auto res2_o_n = as_number( res2_o->get( "foo" ) );
    
    ck_assert_ptr_ne( res2_o_n.get(), NULL );

    ck_assert_int_eq( res2_o_n->get_value(), 12345 );
}
END_TEST

Suite * json_suite( void )
{
    Suite *s;
    TCase *tc_json1;

    s = suite_create("json1");

    tc_json1 = tcase_create( "json1" );

    tcase_add_test( tc_json1, json_test1 );
    tcase_add_test( tc_json1, json_test2 );
    suite_add_tcase( s, tc_json1 );

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = json_suite();
    sr = srunner_create( s );

    srunner_run_all( sr, CK_NORMAL );
    number_failed = srunner_ntests_failed( sr );
    srunner_free( sr );
    return ( number_failed == 0 ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
