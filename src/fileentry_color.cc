/* fileentry_color.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "fileentry_color.hh"

FileEntryColor::FileEntryColor()
{
}

FileEntryColor::FileEntryColor( const FileEntryColor &other )
{
    if ( other.m_custom_color.get() != NULL ) {
        m_custom_color.reset( new FileEntryCustomColor( *other.m_custom_color ) );
    }

    if ( other.m_override_color.get() != NULL ) {
        m_override_color.reset( new FileEntryCustomColor( *other.m_override_color ) );
    }
}

FileEntryColor::~FileEntryColor()
{
}

FileEntryColor &FileEntryColor::operator=( const FileEntryColor &rhs )
{
    if ( this != &rhs ) {
        if ( rhs.m_custom_color.get() != NULL ) {
            m_custom_color.reset( new FileEntryCustomColor( *rhs.m_custom_color ) );
        } else {
            m_custom_color.reset();
        }

        if ( rhs.m_override_color.get() != NULL ) {
            m_override_color.reset( new FileEntryCustomColor( *rhs.m_override_color ) );
        } else {
            m_override_color.reset();
        }
    }
    return *this;
}

int FileEntryColor::getCustomColorSet() const
{
    if ( m_custom_color.get() == NULL ) return 0;
    return m_custom_color->getColorSet();
}

void FileEntryColor::setCustomColorSet( int v )
{
    if ( v != 0 ) {
        if ( m_custom_color.get() == NULL ) {
            m_custom_color = std::unique_ptr<FileEntryCustomColor>( new FileEntryCustomColor() );
        }
        m_custom_color->setColorSet( v );
    } else {
        m_custom_color.reset();
    }
}

void FileEntryColor::setCustomColor( int type, int fg, int bg )
{
    if ( m_custom_color.get() == NULL ) {
        m_custom_color = std::unique_ptr<FileEntryCustomColor>( new FileEntryCustomColor() );
    }
    m_custom_color->setColor( type, fg, bg );
}

int FileEntryColor::getCustomColor( int type, int *fg_return, int *bg_return ) const
{
    if ( m_custom_color.get() == NULL ) return 1;
    return m_custom_color->getColor( type, fg_return, bg_return );
}

void FileEntryColor::setCustomColors( const int fg[4], const int bg[4] )
{
    if ( m_custom_color.get() == NULL ) {
        m_custom_color = std::unique_ptr<FileEntryCustomColor>( new FileEntryCustomColor() );
    }
    m_custom_color->setColors( fg, bg );
}

const int *FileEntryColor::getCustomColors( bool bg ) const
{
    if ( m_custom_color.get() == NULL ) return NULL;
    return m_custom_color->getColors( bg );
}

int FileEntryColor::getOverrideColorSet() const
{
    if ( m_override_color.get() == NULL ) return 0;
    return m_override_color->getColorSet();
}

void FileEntryColor::setOverrideColorSet( int v )
{
    if ( v != 0 ) {
        if ( m_override_color.get() == NULL ) {
            m_override_color = std::unique_ptr<FileEntryCustomColor>( new FileEntryCustomColor() );
        }
        m_override_color->setColorSet( v );
    } else {
        m_override_color.reset();
    }
}

void FileEntryColor::setOverrideColor( int type, int fg, int bg )
{
    if ( m_override_color.get() == NULL ) {
        m_override_color = std::unique_ptr<FileEntryCustomColor>( new FileEntryCustomColor() );
    }
    m_override_color->setColor( type, fg, bg );
}

int FileEntryColor::getOverrideColor( int type, int *fg_return, int *bg_return ) const
{
    if ( m_override_color.get() == NULL ) return 1;
    return m_override_color->getColor( type, fg_return, bg_return );
}

void FileEntryColor::setOverrideColors( const int fg[4], const int bg[4] )
{
    if ( m_override_color.get() == NULL ) {
        m_override_color = std::unique_ptr<FileEntryCustomColor>( new FileEntryCustomColor() );
    }
    m_override_color->setColors( fg, bg );
}

const int *FileEntryColor::getOverrideColors( bool bg ) const
{
    if ( m_override_color.get() == NULL ) return NULL;
    return m_override_color->getColors( bg );
}

void FileEntryColor::setCustomColor( const FileEntryCustomColor &c )
{
    m_custom_color.reset( new FileEntryCustomColor( c ) );
}

const FileEntryCustomColor &FileEntryColor::getCustomColor() const
{
    if ( m_custom_color.get() == NULL ) throw 1;
    return *m_custom_color;
}

void FileEntryColor::setOverrideColor( const FileEntryCustomColor &c )
{
    m_override_color.reset( new FileEntryCustomColor( c ) );
}

const FileEntryCustomColor &FileEntryColor::getOverrideColor() const
{
    if ( m_override_color.get() == NULL ) throw 1;
    return *m_override_color;
}
