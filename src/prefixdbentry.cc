/* prefixdbentry.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2019 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "prefixdbentry.hh"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include "datei.h"
#include "aguix/util.h"
#include "configtokens.h"
#include "configheader.h"
#include "configparser.hh"

PrefixDBEntry::PrefixDBEntry( const std::string &prefix ) : m_prefix( prefix )
{
}

PrefixDBEntry::~PrefixDBEntry()
{
}

bool PrefixDBEntry::operator<( const PrefixDBEntry &rhs ) const
{
    return m_prefix < rhs.m_prefix;
}

void PrefixDBEntry::storeAccess( const std::string &value, time_t last_use )
{
    std::list< std::tuple< std::string, float, time_t > >::iterator it1;

    for ( it1 = m_strings.begin();
          it1 != m_strings.end();
          it1++ ) {
        if ( std::get<0>( *it1 ) == value ) break;
    }
    
    if ( it1 != m_strings.end() ) {
        std::get<1>( *it1 )++;
        std::get<2>( *it1 ) = last_use;
    } else {
        m_strings.push_back( std::make_tuple( value, 1.0, last_use ) );
    }
}

std::string PrefixDBEntry::getBestHit() const
{
    float t1;
    return getBestHit( t1 );
}

std::string PrefixDBEntry::getBestHit( float &count ) const
{
    time_t t1;
    return getBestHit( count, t1 );
}

std::string PrefixDBEntry::getBestHit( float &count, time_t &last_use ) const
{
    std::list< std::tuple< std::string, float, time_t > >::const_iterator it1, best_hit;

    if ( m_strings.empty() ) return "";

    best_hit = m_strings.begin();

    for ( it1 = m_strings.begin();
          it1 != m_strings.end();
          it1++ ) {
        if ( std::get<1>( *it1 ) > std::get<1>( *best_hit ) ) {
            best_hit = it1;
        }
    }

    count = std::get<1>( *best_hit );
    last_use = std::get<2>( *best_hit );
    return std::get<0>( *best_hit );
}

const std::string &PrefixDBEntry::getPrefix() const
{
    return m_prefix;
}

const std::list< std::tuple< std::string, float, time_t > > &PrefixDBEntry::getStrings() const
{
    return m_strings;
}

void PrefixDBEntry::age( float factor )
{
    for ( auto it1 = m_strings.begin();
          it1 != m_strings.end();
          it1++ ) {
        std::get<1>( *it1 ) *= factor;
    }
}

void PrefixDBEntry::write( Datei &fh ) const
{
    fh.configPutPairString( "prefix", m_prefix.c_str() );

    std::list< std::tuple< std::string, float, time_t > >::const_iterator it1;

    for ( it1 = m_strings.begin();
          it1 != m_strings.end();
          it1++ ) {
        std::string f1 = AGUIXUtils::formatStringToString( "%f", std::get<1>( *it1 ) );
        if ( std::get<2>( *it1 ) != 0 ) {
            fh.configPutPairString( "value",
                                    AGUIXUtils::formatStringToString( "lastuse:%lu", std::get<2>( *it1 ) ).c_str() );
        }
        fh.configPutPairString( "value", f1.c_str() );
        fh.configPutPairString( "name", std::get<0>( *it1 ).c_str() );
    }
}

bool PrefixDBEntry::read( FILE *fp )
{
    if ( fp == NULL ) return true;
    std::string line;

    m_strings.clear();
    m_prefix = "";

    int found_error = 0;
    std::string str1;
    float value = -1.0;
    time_t last_use = 0;

    for (;;) {
        if ( worker_token == PREFIX_WCP ) {
            readtoken();

            if ( worker_token != '=' ) {
                found_error = 1;
                break;
            }
            readtoken();

            if ( worker_token != STRING_WCP ) {
                found_error = 1;
                break;
            }

            str1 = yylval.strptr;
            m_prefix = str1;

            readtoken();

            if ( worker_token != ';' ) {
                found_error = 1;
                break;
            }
            readtoken();
        } else if ( worker_token == VALUE_WCP ) {
            readtoken();

            if ( worker_token != '=' ) {
                found_error = 1;
                break;
            }
            readtoken();
            if ( worker_token != STRING_WCP ) {
                found_error = 1;
                break;
            }

            str1 = yylval.strptr;
            if ( sscanf( yylval.strptr, "%f", &value ) == 1 ) {
            } else {
                // this is an ugly workaround to stay compatible with
                // older version
                if ( AGUIXUtils::starts_with( str1, "lastuse:" ) ) {
                    std::string str2( str1, strlen( "lastuse:" ) );
                    if ( sscanf( str2.c_str(), "%lu", &last_use ) != 1 ) {
                        last_use = 0;
                    }
                } else {
                    value = -1.0;
                }
            }
            readtoken();

            if ( worker_token != ';' ) {
                found_error = 1;
                break;
            }
            readtoken();
        } else if ( worker_token == NAME_WCP ) {
            readtoken();

            if ( worker_token != '=' ) {
                found_error = 1;
                break;
            }
            readtoken();

            if ( worker_token != STRING_WCP ) {
                found_error = 1;
                break;
            }

            str1 = yylval.strptr;
            readtoken();

            if ( worker_token != ';' ) {
                found_error = 1;
                break;
            }
            readtoken();

            if ( value > 0.0 ) {
                m_strings.push_back( std::make_tuple( str1, value, last_use ) );
                last_use = 0;
            }
        } else {
            break;
        }
    }

    return found_error > 0;
}
