/* showimagemode.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "showimagemode.h"
#include "listermode.h"
#include "worker_locale.h"
#include "wconfig.h"
#include "wc_color.hh"
#include "worker.h"
#include "configheader.h"
#include "configparser.hh"
#include "fileentry.hh"
#include "nmspecialsourceext.hh"
#include "aguix/stringgadget.h"
#include "aguix/button.h"
#include "aguix/fieldlistview.h"
#include "aguix/awindow.h"
#include "datei.h"
#include "simplelist.hh"

const char *defaultviewers[] = { "xliwrapper_worker",
                                 "displaywrapper_worker" };

const char *ShowImageMode::type="ShowImageMode";

ShowImageMode::ShowImageMode(Lister *parent):ListerMode(parent)
{
  x=0;
  y=0;
  w=1;
  h=1;
  imgwin=NULL;
  lastactivefe=NULL;
  show_program = dupstring( defaultviewers[0] );
  blanked=false;
}

ShowImageMode::~ShowImageMode()
{
  clearLastActiveFE();
  _freesafe(show_program);
}

void ShowImageMode::messageHandler(AGMessage *msg)
{
  int tx,ty,tw,th;
  bool ma=false;
  switch(msg->type) {
    case AG_SIZECHANGED:
      parentlister->getGeometry(&tx,&ty,&tw,&th);
      reconf(tx,ty,tw,th);
      break;
  }
  if(ma==true) parentlister->makeActive();
}

void ShowImageMode::on()
{
  parentlister->getGeometry(&x,&y,&w,&h);

  if ( w < 10 ) w = 10;
  if ( h < 10 ) h = 10;
  imgwin = new AWindow( aguix, x, y, w, h, "" );
  parentawindow->add( imgwin );
  imgwin->create();
  imgwin->show();
  
  parentlister->setActiveMode(this);
  setName();
}

/* this is optional code to free X resources created by
 * xli. It's kind of a hack, but should be safe since
 * pretty much no-one else set the necessary property
 * in our own window */
#define FREE_XLI_RESOURCE

#ifdef FREE_XLI_RESOURCE

#define RETAIN_PROP_NAME "_XSETROOT_ID"

// that's a ugly hack from xli to free its pixmap resource
static void free_previous_xli_resource( Display *dsp, Window win )
{
    Pixmap *pm;
    Atom actual_type;
    int format;
    unsigned long nitems;
    unsigned long bytes_after;

    Atom atom = XInternAtom( dsp, RETAIN_PROP_NAME, 0 );

    if ( XGetWindowProperty( dsp, win, atom, 0, 1, 1,
                             AnyPropertyType,
                             &actual_type,
                             &format,
                             &nitems,
                             &bytes_after,
                             (unsigned char**)&pm) == Success ) {

        if ( actual_type == XA_PIXMAP &&
             format == 32 &&
             nitems == 1 &&
             bytes_after == 0 &&
             pm != NULL ) {

            XKillClient( dsp, (XID)*pm );
            XFree( pm );
        }
    }
}
#endif

void ShowImageMode::off()
{
  parentlister->setActiveMode(NULL);
  parentlister->setName("");
#ifdef FREE_XLI_RESOURCE
  free_previous_xli_resource( aguix->getDisplay(), imgwin->getWindow() );
#endif
  delete imgwin;
  imgwin=NULL;
  clearLastActiveFE();
}

void ShowImageMode::reconf(int tx,int ty,int tw,int th)
{
  if((tw!=w)||(th!=h)||(tx!=x)||(ty!=y)) {
    w=tw;
    h=th;
    if ( w < 10 ) w = 10;
    if ( h < 10 ) h = 10;
    x=tx;
    y=ty;
    imgwin->resize(w,h);
    imgwin->move(x,y);
    update(true);
  }
}

void ShowImageMode::activate()
{
}

void ShowImageMode::deactivate()
{
}

bool ShowImageMode::isType(const char *str)
{
  if(strcmp(str,type)==0) return true; else return false;
}

const char *ShowImageMode::getType()
{
  return type;
}

const char *ShowImageMode::getStaticType()
{
  return type;
}

int ShowImageMode::configure()
{
  Button *db;
  AWindow *win;
  StringGadget *sg;
  AGMessage *msg;
  int endmode=-1;
  char *tstr;
  char **liness;
  int lines;
  Requester *req;
  int i;
  const int cincw = AContainer::ACONT_MINH +
                    AContainer::ACONT_MINW +
                    AContainer::ACONT_MAXH;
  const int cincwnr = cincw +
                      AContainer::ACONT_NORESIZE;
  const int cfix = AContainer::ACONT_MINH +
                   AContainer::ACONT_MINW +
                   AContainer::ACONT_MAXH +
                   AContainer::ACONT_MAXW;
  
  tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getLocaleName() ) + 1 );
  sprintf( tstr, catalog.getLocale( 293 ), getLocaleName() );
  win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
  win->create();
  _freesafe(tstr);

  lines=createLines(catalog.getLocale(521),&liness);

  AContainer *ac1 = win->setContainer( new AContainer( win, 1, lines + 3 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  for(i=0;i<lines;i++) {
    ac1->add( new Text( aguix, 0, 0, liness[i] ), 0, i, cincwnr );
  }

  sg = (StringGadget*)ac1->add( new StringGadget( aguix, 0, 0, 100, show_program, 0 ), 0, lines, cincw );
  
  db = (Button*)ac1->add( new Button( aguix, 0, 0, 100, catalog.getLocale( 110 ), 0 ), 0, lines + 1, cincw );

  AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, lines + 2 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( -1 );
  ac1_2->setBorderWidth( 0 );
  Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                0,
                                                0,
                                                catalog.getLocale( 11 ),
                                                0 ), 0, 0, cfix );
  Button *cb = (Button*)ac1_2->add( new Button( aguix,
						0,
						0,
						catalog.getLocale( 8 ),
						0 ), 1, 0, cfix );
  
  win->setDoTabCycling( true );
  win->contMaximize( true );
  win->show();
  req=new Requester(aguix);
  for(;endmode==-1;) {
    msg=aguix->WaitMessage(win);
    if(msg!=NULL) {
      switch(msg->type) {
        case AG_CLOSEWINDOW:
          if(msg->closewindow.window==win->getWindow()) endmode=1;
          break;
        case AG_BUTTONCLICKED:
          if(msg->button.button==okb) {
            endmode = 0;
          } else if(msg->button.button==cb) endmode=1;
          else if(msg->button.button==db) {
            tstr = requestDefaultViewer();
            if ( tstr != NULL ) {
              sg->setText( tstr );
              _freesafe( tstr );
            }
          }
          break;
      }
      aguix->ReplyMessage(msg);
    }
  }
  delete req;
  
  if(endmode==0) {
    // ok
    _freesafe(show_program);
    show_program=dupstring(sg->getText());
  }
  
  delete win;

  for(i=0;i<lines;i++) _freesafe(liness[i]);
  _freesafe(liness);

  return endmode;
}

void ShowImageMode::cyclicfunc(cyclicfunc_mode_t mode)
{
  update(false);
}

const char* ShowImageMode::getLocaleName()
{
  return getStaticLocaleName();
}

const char* ShowImageMode::getStaticLocaleName()
{
  return catalog.getLocale(174);
}

int ShowImageMode::load()
{
  if ( worker_token == VIEWSTR_WCP ) {
    readtoken();

    if ( worker_token != '=' ) return 1;
    readtoken();

    if ( worker_token != STRING_WCP ) return 1;
    if ( show_program != NULL ) _freesafe( show_program );
    show_program = dupstring( yylval.strptr );
    readtoken();

    if ( worker_token != ';' ) return 1;
    readtoken();
  } else return 1; //parse error
  return 0;
}

bool ShowImageMode::save(Datei *fh)
{
  if ( fh == NULL ) return false;
  
  fh->configPutPairString( "viewstr", show_program );

  return false;
}

void ShowImageMode::setName()
{
  parentlister->setName(catalog.getLocale(174));
}

void ShowImageMode::relayout()
{
}

void ShowImageMode::update(bool force)
{
  ListerMode *lm1=NULL;
  Lister *ol=NULL;
  List *colors;
  WC_Color *bgcol;
  const FileEntry *fe;
  char *buf,*colbuf, *buf2;
  std::string str1;
  
  ol=parentlister->getWorker()->getOtherLister(parentlister);
  if(ol!=NULL) {
    lm1=ol->getActiveMode();
  }
  
  if(force==true) clearLastActiveFE();
  
  if(lm1!=NULL) {
    std::list< NM_specialsourceExt > files;
    
    lm1->getSelFiles( files, ListerMode::LM_GETFILES_ONLYACTIVE );
    if ( ! files.empty() ) {
      fe = files.begin()->entry();
      if ( fe->equals( lastactivefe ) == false ) {
	clearLastActiveFE();
	if ( fe != NULL ) {
	  lastactivefe = new FileEntry( *fe );
	}
        if(lastactivefe!=NULL) {
          // first get the background color in the X format for xli
          colors=(List*)wconfig->getColors();
          bgcol=(WC_Color*)colors->getElementAt(0);
          colbuf = (char*)_allocsafe( strlen( "rgb://" ) + ( 3 * A_BYTESFORNUMBER( int ) ) + 1 );
          sprintf( colbuf, "rgb:%x/%x/%x", bgcol->getRed(), bgcol->getGreen(), bgcol->getBlue() );
        
          if ( show_program[0] != '/' ) {
            // no absolute path so check if we find
            // the file in the script directory
            str1 = Worker::getDataDir();
            str1 += "/scripts/";
            str1 += show_program;
            if ( Datei::fileExistsExt( str1.c_str() ) != Datei::D_FE_FILE ) {
              // not found so let the shell find it
              str1 = show_program;
            }
          } else {
            str1 = show_program;
          }

          buf = (char*)_allocsafe( strlen( "%s %d %d %d %d %x " ) +
                                   strlen( str1.c_str() ) +
                                   4 * A_BYTESFORNUMBER( int ) + 
                                   A_BYTESFORNUMBER( Window ) + 1 );
          sprintf( buf, "%s %d %d %d %d %x ", str1.c_str(), 0, 0, w, h, (unsigned int)imgwin->getWindow() );
          buf2 = AGUIX_catTrustedAndUnTrusted( buf, lastactivefe->fullname );
          _freesafe( buf );
          buf = catstring( buf2, " " );
          _freesafe( buf2 );
          buf2 = catstring( buf, colbuf );
          _freesafe( buf );
          buf = buf2;
          blank();
          int tres __attribute__((unused)) = system( buf );
          blanked=false;
          
          _freesafe(buf);
          _freesafe(colbuf);
        }
      }
    } else {
      clearLastActiveFE();
      blank();
    }
  }
}

void ShowImageMode::blank()
{
  if(blanked==false) {
    XSetWindowAttributes attr;
    attr.background_pixel=aguix->getPixel(0);
    attr.background_pixmap=None;
    XChangeWindowAttributes(aguix->getDisplay(),imgwin->getWindow(),CWBackPixel|CWBackPixmap,&attr);
    XClearWindow(aguix->getDisplay(),imgwin->getWindow());
    aguix->Flush();
  }
  blanked=true;
}

char *ShowImageMode::requestDefaultViewer()
{
  Button *okb,*cb;
  AWindow *win;
  int tw,ttw,tth,ttx,tty;
  AGMessage *msg;
  int endmode=-1;
  GUIElement *bs[2];
  FieldListView *lv;
  unsigned int i;
  int row;
  char *result;
  std::string str1;
  
  ttw = tth = 10;
  ttx = tty = 5;

  win = new AWindow( aguix, 10, 10, ttw, tth, catalog.getLocale( 534 ), AWindow::AWINDOW_DIALOG );
  win->create();

  win->addTextFromString( catalog.getLocale( 535 ),
                          ttx, tty, 5,
                          NULL, NULL, &tty );
  tty += 5;

  lv = (FieldListView*)win->add( new FieldListView( aguix,
                                                   ttx, tty,
                                                   10, 6 * aguix->getCharHeight(),
                                                   0 ) );
  for ( i = 0; i < ( sizeof( defaultviewers ) / sizeof( defaultviewers[0] ) ); i++ ) {
    row = lv->addRow();
    lv->setText( row, 0, defaultviewers[i] );
    lv->setPreColors( row, FieldListView::PRECOLOR_ONLYACTIVE );
    if ( i == 0 ) lv->setActiveRow( row );
  }
  tty += lv->getHeight() + 5;

  lv->setVBarState( 2 );
  lv->setHBarState( 0 );
  lv->setDisplayFocus( true );
  lv->setAcceptFocus( true );
  lv->maximizeX();

  okb = (Button*)win->add( new Button( aguix,
                                       5,
                                       tty,
                                       catalog.getLocale( 11 ),
                                       0 ) );
  cb = (Button*)win->add( new Button( aguix,
                                      5,
                                      tty,
                                      catalog.getLocale( 8 ),
                                      0 ) );
  tty += okb->getHeight() + 5;

  win->maximizeX();
  
  ttw = win->getWidth();
  
  bs[0] = okb;
  bs[1] = cb;
  tw = AGUIX::scaleElementsW( ttw, 5, 5, -1, false, false, bs, NULL, 2 );

  win->resize( ( tw > ttw ) ? tw : ttw, tty );
  lv->resize( win->getWidth() - 2 * win->getBorderWidth(), lv->getHeight() );

  win->setDoTabCycling( true );
  win->setMaxSize( win->getWidth(), win->getHeight() );
  win->setMinSize( win->getWidth(), win->getHeight() );
  win->show();
  
  result = NULL;
  
  for(;endmode == -1;) {
    msg = aguix->WaitMessage( win );
    if ( msg != NULL ) {
      switch ( msg->type ) {
        case AG_CLOSEWINDOW:
          if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
          break;
        case AG_BUTTONCLICKED:
          if ( msg->button.button == okb ) endmode = 0;
          else if ( msg->button.button == cb ) endmode = 1;
          break;
      }
      aguix->ReplyMessage( msg );
    }
  }
  
  if ( endmode == 0 ) {
    // ok
    row = lv->getActiveRow();
    if ( lv->isValidRow( row ) == true ) {
      str1 = lv->getText( row, 0 );
      if ( str1.length() > 0 ) {
        result = dupstring( str1.c_str() );
      }
    }
  }
  
  delete win;

  return result;
}

void ShowImageMode::clearLastActiveFE() {
  if ( lastactivefe != NULL ) {
    delete lastactivefe;
    lastactivefe = NULL;
  }
}
