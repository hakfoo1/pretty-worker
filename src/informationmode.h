/* informationmode.h
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2004,2009,2011,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef INFORMATIONMODE_H
#define INFORMATIONMODE_H

#include "wdefines.h"
#include "listermode.h"

class Lister;
class AContainer;
class Datei;
class FileEntry;

class InformationMode:public ListerMode
{
public:
  InformationMode(Lister *);
  virtual ~InformationMode();
  InformationMode( const InformationMode &other );
  InformationMode &operator=( const InformationMode &other );

  virtual void messageHandler(AGMessage *);
  virtual void on();
  virtual void off();
  virtual void activate();
  virtual void deactivate();
  virtual bool isType(const char *);
  virtual const char *getType();
  virtual int configure();
  virtual void cyclicfunc(cyclicfunc_mode_t mode);
  virtual const char* getLocaleName();
  virtual int load();
  virtual bool save(Datei *);
  virtual void relayout();
  
  static const char *getStaticLocaleName();
  static const char *getStaticType();
private:
  static const char *type;
  void setName();
  FieldListView *lv;
  
  FileEntry *lastactivefe;
  
  void update(bool force);
  bool blanked;
  void showinfos();
  
  bool nospaceinfo;
  void clearLastActiveFE();

  AContainer *m_cont;
};

#endif

/* Local Variables: */
/* mode:c++ */
/* End: */
