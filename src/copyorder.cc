/* copyorder.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014-2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "copyorder.hh"
#include "aguix/lowlevelfunc.h"
#include "nmspecialsourceext.hh"
#include "copyopwin.hh"

copyorder::copyorder()
    : source( COPY_ALLENTRIES ),
      move( false ),
      follow_symlinks( false ),
      do_rename( false ),
      preserve_attr( true ),
      vdir_preserve_dir_structure( false ),
      overwrite( COPY_OVERWRITE_NORMAL ),
      adjust_relative_symlinks( COPY_ADJUST_SYMLINK_NEVER ),
      ensure_file_permissions( CopyParams::LEAVE_PERMISSIONS_UNMODIFIED ),
      cowin( NULL ),
      error( 0 ),
      ignoreLosedAttr( false )
{
}

copyorder::~copyorder()
{
    for ( auto &el : sources ) {
        delete el;
    }

    delete cowin;
}
