/* dirbookmarkssettings.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2008,2011,2013 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "dirbookmarkssettings.hh"
#include "verzeichnis.hh"
#include "worker.h"
#include "bookmarkdbproxy.hh"
#include "bookmarkdbentry.hh"
#include <algorithm>
#include <string>
#include <list>
#include "fileentry.hh"
#include "nwcentryselectionstate.hh"

DirBookmarksSettings::DirBookmarksSettings() : m_serial_nr( 0 )
{
}

DirBookmarksSettings::~DirBookmarksSettings()
{
}

struct copy_category
{
    std::list<std::string> &m_l;

    copy_category( std::list<std::string> &l ) : m_l( l )
    {
    }

    void operator() (const BookmarkDBEntry& e) const 
    {
        return m_l.push_back( e.getCategory() );
    }
};

bool DirBookmarksSettings::checkEntry( BookmarkDBProxy &bookmarks,
                                       FileEntry *fe, NWCEntrySelectionState *es )
{
    if ( fe == NULL && es == NULL ) return false;

    std::list< BookmarkDBEntry > bookmark_entries;

    NWC::FSEntry *fse = NULL;

    if ( es ) {
        fse = es->getNWCEntry();

        if ( ! fse ) return false;
    }

    BookmarkDBProxy::check_path_res_t res = bookmarks.checkPath( fe ? fe->fullname : fse->getFullname(),
                                                                 bookmark_entries );

    std::list<std::string> matching_labels;
    std::list<std::string>::iterator full_hit_it = matching_labels.end();

    if ( bookmark_entries.empty() == true ) {
        res = BookmarkDBProxy::NO_HIT;
    }

    std::for_each( bookmark_entries.begin(),
                   bookmark_entries.end(),
                   copy_category( matching_labels ) );

    if ( res == BookmarkDBProxy::FULL_HIT ) {
        full_hit_it = matching_labels.begin();
    }

    matching_labels.sort();
    matching_labels.unique();

    if ( full_hit_it != matching_labels.begin() &&
         full_hit_it != matching_labels.end() ) {
        matching_labels.push_front( *full_hit_it );
        matching_labels.erase( full_hit_it );
    }

    if ( ( res == BookmarkDBProxy::FULL_HIT || res == BookmarkDBProxy::PREFIX_HIT ) &&
         matching_labels.empty() == false ) {
        if ( res == BookmarkDBProxy::FULL_HIT ) {
            if ( fe ) {
                fe->setBookmarkMatch( FileEntry::BOOKMARK );
            } else {
                es->setBookmarkMatch( FileEntry::BOOKMARK );
            }
        } else {
            if ( fe ) {
                fe->setBookmarkMatch( FileEntry::BOOKMARK_PREFIX );
            } else {
                es->setBookmarkMatch( FileEntry::BOOKMARK_PREFIX );
            }
        }
        if ( fe ) {
            fe->setMatchingLabels( matching_labels );
        } else {
            es->setMatchingLabels( matching_labels );
        }
    } else {
        if ( fe ) {
            fe->clearMatchingLabels();
        } else {
            es->clearMatchingLabels();
        }
    }

    return true;
}

bool DirBookmarksSettings::check( Verzeichnis &v )
{
    BookmarkDBProxy &bookmarks = Worker::getBookmarkDBInstance();

    for ( Verzeichnis::verz_it it1 = v.begin();
          it1 != v.end();
          it1++ ) {
        FileEntry *fe = *it1;

        checkEntry( bookmarks, fe, NULL );
    }
    return true;
}

bool DirBookmarksSettings::check( std::vector< NWCEntrySelectionState > &d )
{
    BookmarkDBProxy &bookmarks = Worker::getBookmarkDBInstance();

    for ( auto &es : d ) {
        checkEntry( bookmarks, NULL, &es );
    }

    return true;
}

int DirBookmarksSettings::getSerialNr() const
{
    //TODO check bookmarks serial id to be able to notice changes without explicit notification?
    return m_serial_nr;
}

void DirBookmarksSettings::bookmarksChanged()
{
    m_serial_nr++;
}
