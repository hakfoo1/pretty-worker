/* functionproto.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "functionproto.h"
#include "worker.h"
#include "aguix/util.h"
#include "fileentry.hh"
#include "worker_locale.h"
#include "aguix/request.h"
#include "simplelist.hh"
#include "datei.h"

const char *FunctionProto::name="FunctionProto";

bool FunctionProto::isName(const char *str)
{
  if(strcmp(str,name)==0) return true; else return false;
}

const char *FunctionProto::getName()
{
  return name;
}

FunctionProto::FunctionProto()
{
  hasConfigure = false;
  m_category = CAT_OTHER;
}

FunctionProto::~FunctionProto()
{
}

int FunctionProto::configure()
{
  AGUIX *aguix = Worker::getAGUIX();
  Requester *req;
  const char *textstr,*buttonstr;
  
  req=new Requester(aguix);
  textstr=catalog.getLocale(292);
  buttonstr=catalog.getLocale(11);
  req->request(catalog.getLocale(124),textstr,buttonstr);
  delete req;

  return 0;
}

FunctionProto *FunctionProto::duplicate() const
{
  return NULL;
}

bool FunctionProto::save(Datei *fh)
{
  return false;
}

int FunctionProto::presave(Datei *fh,FunctionProto *fp)
{
  if ( ( fh == NULL ) || ( fp == NULL ) ) return 1;
  fh->configOpenSection( fp->getName() );
  fp->save( fh );
  fh->configCloseSection();
  return 0;
}

int FunctionProto::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
  return 0;
}

ActionMessage::ActionMessage( Worker *w)
{
  mode=AM_MODE_NORMAL;
  startLister=NULL;
  dndmsg = NULL;
  fe = NULL;
  worker = w;

  filetype = NULL;

  m_key_action = false;
}

ActionMessage::~ActionMessage()
{
  if ( fe != NULL ) delete fe;
}

Worker *ActionMessage::getWorker() const
{
  return worker;
}

const FileEntry *ActionMessage::getFE() const
{
  return fe;
}

void ActionMessage::setFE( const FileEntry *tfe )
{
  if ( fe != NULL ) delete fe;
  if ( tfe != NULL ) {
    fe = new FileEntry( *tfe );
  } else {
    fe = NULL;
  }
}

void ActionMessage::setKeyAction( bool nv )
{
    m_key_action = nv;
}

bool ActionMessage::getKeyAction() const
{
    return m_key_action;
}

const char *FunctionProto::getDescription()
{
    return "prototype of an action";
}

int FunctionProto::configureWhenAvail()
{
  if ( hasConfigure == false ) return -1;
  return configure();
}

void freecoms(List *com)
{
  FunctionProto *fp;
  int id=com->initEnum();
  fp=(FunctionProto*)com->getFirstElement(id);
  while(fp!=NULL) {
    delete fp;
    fp=(FunctionProto*)com->getNextElement(id);
  }
  com->removeAllElements();
  com->closeEnum(id);
}

std::string FunctionProto::getCategoryName( command_categories_t category )
{
    switch ( category ) {
        case CAT_FILETYPE:
            return catalog.getLocale( 942 );
            break;
        case CAT_CURSOR:
            return catalog.getLocale( 943 );
            break;
        case CAT_FILELIST:
            return catalog.getLocale( 944 );
            break;
        case CAT_FILEOPS:
            return catalog.getLocale( 945 );
            break;
        case CAT_SELECTIONS:
            return catalog.getLocale( 946 );
            break;
        case CAT_SETTINGS:
            return catalog.getLocale( 947 );
            break;
        case CAT_SCRIPTING:
            return catalog.getLocale( 948 );
            break;
        default:
            return catalog.getLocale( 949 );
            break;
    }
}

FunctionProto::command_categories_t FunctionProto::getCategory() const
{
    return m_category;
}

std::string FunctionProto::getStringRepresentation()
{
    std::string res = getDescription();

    if ( m_request_parameters ) {
        res += " (";
        res += catalog.getLocale( 1375 );
        res += ")";
    }

    return res;
}

void ActionMessage::setEntriesToConsider( RefCount< NWC::VirtualDir > entries )
{
    m_entries_to_consider = entries;
    mode = AM_MODE_SPECIFIC_ENTRIES;
}

RefCount< NWC::VirtualDir > ActionMessage::getEntriesToConsider()
{
    return m_entries_to_consider;
}

std::string getStringReprForList( const command_list_t &coms )
{
    std::string res;

    for ( auto &fp : coms ) {
        if ( ! res.empty() ) {
            res += "; ";
        }

        res += fp->getStringRepresentation();
    }

    return res;
}

bool FunctionProto::isInteractiveRun() const
{
    return false;
}

void FunctionProto::setInteractiveRun()
{
}

bool FunctionProto::requestParameters() const
{
    return m_request_parameters;
}

void FunctionProto::setRequestParameters( bool nv )
{
    m_request_parameters = nv;
}
