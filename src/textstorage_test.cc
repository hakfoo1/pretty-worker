#include <check.h>
#include <stdlib.h>
#include "aguix/textstorage.h"

START_TEST(rawtest1)
{
    RefCount<ACharWidth> lencalc( new ACharWidth() );
    auto ts = TextStorageString( "foo\tbar\n",
                                 lencalc,
                                 -1,
                                 TextStorageString::CONVERT_NON_PRINTABLE_SIMPLE );

    std::string res;

    ck_assert( ts.getUnwrappedLine( 0, 0, 10, res ) == 0 );
    ck_assert( res == "foo     ba" );
    ck_assert( ts.getUnwrappedLine( 0, 0, 3, res ) == 0 );
    ck_assert( res == "foo" );
    ck_assert( ts.getUnwrappedLine( 0, 0, 4, res ) == 0 );
    ck_assert( res == "foo " );
    ck_assert( ts.getUnwrappedLine( 0, 4, 6, res ) == 0 );
    ck_assert( res == "    ba" );

    ck_assert( ts.getUnwrappedLineRaw( 0, 0, 10, res ) == 0 );
    ck_assert( res == "foo\tba" );
    ck_assert( ts.getUnwrappedLineRaw( 0, 0, 3, res ) == 0 );
    ck_assert( res == "foo" );
    ck_assert( ts.getUnwrappedLineRaw( 0, 0, 4, res ) == 0 );
    ck_assert( res == "foo" );
    ck_assert( ts.getUnwrappedLineRaw( 0, 4, 6, res ) == 0 );
    ck_assert( res == "\tba" );
}
END_TEST

START_TEST(rawtest2)
{
    RefCount<ACharWidth> lencalc( new ACharWidth() );
    auto ts = TextStorageString( "begin\nfoo\t\nbar\n",
                                 lencalc,
                                 -1,
                                 TextStorageString::CONVERT_NON_PRINTABLE_SIMPLE );

    std::string res;

    ck_assert( ts.getUnwrappedLine( 1, 4, -1, res ) == 0 );
    ck_assert( res == "    " );
    ck_assert( ts.getUnwrappedLine( 2, 1, 2, res ) == 0 );
    ck_assert( res == "ar" );

    ck_assert( ts.getUnwrappedLineRaw( 0, 0, -1, res ) == 0 );
    ck_assert( res == "begin" );
    ck_assert( ts.getUnwrappedLineRaw( 1, 0, -1, res ) == 0 );
    ck_assert( res == "foo\t" );
    ck_assert( ts.getUnwrappedLineRaw( 1, 5, -1, res ) == 0 );
    ck_assert( res == "\t" );
    ck_assert( ts.getUnwrappedLineRaw( 2, 0, -1, res ) == 0 );
    ck_assert( res == "bar" );
}
END_TEST

Suite * textstorage_suite( void )
{
    Suite *s;
    TCase *tc_prefix1;

    s = suite_create("TextStorage");

    /* Core test case */
    tc_prefix1 = tcase_create( "rawtest" );

    tcase_add_test( tc_prefix1, rawtest1 );
    tcase_add_test( tc_prefix1, rawtest2 );
    suite_add_tcase( s, tc_prefix1 );

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = textstorage_suite();
    sr = srunner_create( s );

    srunner_run_all( sr, CK_NORMAL );
    number_failed = srunner_ntests_failed( sr );
    srunner_free( sr );
    return ( number_failed == 0 ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
