/* worker_types.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "worker_time.hh"
#include "aguix/util.h"
#include "worker_locale.h"

std::string WorkerTime::convert_time_diff_to_string( time_t diff )
{
    std::string t;

    if ( diff < 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1104 ), (int)( diff / 60 ) );
    } else if ( diff < 2 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1103 ), (int)( diff / 60 ) );
    } else if ( diff < 1 * 60 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1104 ), (int)( diff / 60 ) );
    } else if ( diff < 2 * 60 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1105 ), (int)( diff / 60 / 60 ) );
    } else if ( diff < 24 * 60 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1106 ), (int)( diff / 60 / 60 ) );
    } else if ( diff < 2 * 24 * 60 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1107 ), (int)( diff / 60 / 60 / 24 ) );
    } else if ( diff < 7 * 24 * 60 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1108 ), (int)( diff / 60 / 60 / 24 ) );
    } else if ( diff < 2 * 7 * 24 * 60 * 60 ) {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1109 ), (int)( diff / 60 / 60 / 24 / 7 ) );
    } else {
        t = AGUIXUtils::formatStringToString( catalog.getLocale( 1110 ), (int)( diff / 60 / 60 / 24 / 7 ) );
    }

    return t;
}
