/* commandmenuop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2013-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "commandmenuop.hh"
#include "worker.h"
#include "wpucontext.h"
#include "worker_locale.h"
#include "aguix/aguix.h"
#include "aguix/choosebutton.h"
#include "aguix/button.h"
#include "aguix/acontainer.h"
#include "aguix/awindow.h"
#include "aguix/fieldlistview.h"

const char *CommandMenuOp::name = "CommandMenuOp";

CommandMenuOp::CommandMenuOp() : FunctionProto(),
                                 m_show_recently_used( false )
{
    m_category = FunctionProto::CAT_OTHER;
    hasConfigure = true;
}

CommandMenuOp::~CommandMenuOp()
{
}

CommandMenuOp* CommandMenuOp::duplicate() const
{
    CommandMenuOp *ta = new CommandMenuOp();
    ta->m_show_recently_used = m_show_recently_used;
    ta->m_start_node = m_start_node;
    return ta;
}

bool CommandMenuOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *CommandMenuOp::getName()
{
    return name;
}

int CommandMenuOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    msg->getWorker()->openCommandMenu( m_show_recently_used, m_start_node );

    return 0;
}

const char *CommandMenuOp::getDescription()
{
    return catalog.getLocale( 1303 );
}

int CommandMenuOp::configure()
{
    AGUIX *aguix = Worker::getAGUIX();
    AWindow *win;
    AGMessage *msg;
    int endmode=-1;
    char *tstr;
  
    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) + strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe( tstr );

    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 3 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );

    ChooseButton *recently_cb = ac1->addWidget( new ChooseButton( aguix, 0, 0,
                                                                  m_show_recently_used,
                                                                  catalog.getLocale( 1169 ),
                                                                  LABEL_LEFT, 0 ),
                                                0, 0, AContainer::CO_INCWNR );

    AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( 5 );
    ac1_1->setBorderWidth( 0 );
    ac1_1->addWidget( new Text( aguix, 0, 0, "Initial menu entry:" ),
                      0, 0, AContainer::ACONT_MINH + AContainer::ACONT_MINW + AContainer::ACONT_MAXW );
    auto *lv = ac1_1->addWidget( new FieldListView( aguix, 0, 0, 50, 50, 0 ),
                                 1, 0, AContainer::CO_MIN );
    lv->setDefaultColorMode( FieldListView::PRECOLOR_ONLYACTIVE );
    lv->setHBarState(2);
    lv->setVBarState(2);
    lv->setDisplayFocus( true );
    lv->setAcceptFocus( true );
    
    int row = lv->addRow();
    lv->setText( row, 0, catalog.getLocale( 1498 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 787 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 558 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 91 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 1006 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 1016 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 1499 ) );
    lv->setText( lv->addRow(), 0, catalog.getLocale( 245 ) );

    switch ( m_start_node ) {
        case WorkerTypes::TOP_LEVEL:
            lv->setActiveRow( 0 );
            break;
        case WorkerTypes::MENUS:
            lv->setActiveRow( 1 );
            break;
        case WorkerTypes::BUTTONS:
            lv->setActiveRow( 2 );
            break;
        case WorkerTypes::HOTKEYS:
            lv->setActiveRow( 3 );
            break;
        case WorkerTypes::PATHS:
            lv->setActiveRow( 4 );
            break;
        case WorkerTypes::LV_MODES:
            lv->setActiveRow( 5 );
            break;
        case WorkerTypes::LV_COMMANDS:
            lv->setActiveRow( 6 );
            break;
        case WorkerTypes::COMMANDS:
            lv->setActiveRow( 7 );
            break;
    }

    lv->maximizeX();
    lv->maximizeY();

    ac1_1->readLimits();

    AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 2 );
    ac1_2->setMinSpace( 5 );
    ac1_2->setMaxSpace( -1 );
    ac1_2->setBorderWidth( 0 );
    Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 11 ),
                                                  0 ), 0, 0, AContainer::CO_FIX );
    Button *cb = (Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 8 ),
                                                  0 ), 1, 0, AContainer::CO_FIX );
  
    win->setDoTabCycling( true );
    win->contMaximize( true );
    win->show();

    for ( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cb ) endmode = 1;
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
  
    if ( endmode == 0 ) {
        // ok
        if ( recently_cb->getState() ) {
            m_show_recently_used = true;
        } else {
            m_show_recently_used = false;
        }

        switch ( lv->getActiveRow() ) {
            case 1:
                m_start_node = WorkerTypes::MENUS;
                break;
            case 2:
                m_start_node = WorkerTypes::BUTTONS;
                break;
            case 3:
                m_start_node = WorkerTypes::HOTKEYS;
                break;
            case 4:
                m_start_node = WorkerTypes::PATHS;
                break;
            case 5:
                m_start_node = WorkerTypes::LV_MODES;
                break;
            case 6:
                m_start_node = WorkerTypes::LV_COMMANDS;
                break;
            case 7:
                m_start_node = WorkerTypes::COMMANDS;
                break;
            default:
                m_start_node = WorkerTypes::TOP_LEVEL;
                break;
        }
    }
  
    delete win;

    return endmode;
}

bool CommandMenuOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;

    if ( m_show_recently_used ) {
        fh->configPutPairBool( "showrecent", m_show_recently_used );
    }

    if ( m_start_node != WorkerTypes::TOP_LEVEL ) {
        switch ( m_start_node ) {
            case WorkerTypes::TOP_LEVEL:
                break;
            case WorkerTypes::MENUS:
                fh->configPutPair( "startnode", "menus" );
                break;
            case WorkerTypes::BUTTONS:
                fh->configPutPair( "startnode", "buttons" );
                break;
            case WorkerTypes::HOTKEYS:
                fh->configPutPair( "startnode", "hotkeys" );
                break;
            case WorkerTypes::PATHS:
                fh->configPutPair( "startnode", "paths" );
                break;
            case WorkerTypes::LV_MODES:
                fh->configPutPair( "startnode", "lvmodes" );
                break;
            case WorkerTypes::LV_COMMANDS:
                fh->configPutPair( "startnode", "lvcommands" );
                break;
            case WorkerTypes::COMMANDS:
                fh->configPutPair( "startnode", "commands" );
                break;
        }
    }

    return true;
}

void CommandMenuOp::setShowRecentlyUsed( bool nv )
{
    m_show_recently_used = nv;
}

void CommandMenuOp::setStartNode( WorkerTypes::command_menu_node nv )
{
    m_start_node = nv;
}
