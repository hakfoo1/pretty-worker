/* nmfilter.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2001-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "nmfilter.hh"
#include "configheader.h"
#include "configparser.hh"
#include "wconfig.h"
#include "datei.h"
#include "aguix/lowlevelfunc.h"

NM_Filter::NM_Filter( const NM_Filter &other )
{
    setPattern( other.getPattern() );
    setCheck( other.getCheck() );
}

NM_Filter &NM_Filter::operator=( const NM_Filter &other )
{
    if ( this != &other ) {
        setPattern( other.getPattern() );
        setCheck( other.getCheck() );
    }
    return *this;
}

void NM_Filter::setPattern(const char *np)
{
    if ( np ) {
        pattern = np;
    } else {
        pattern.clear();
    }
}

const char *NM_Filter::getPattern() const
{
    return pattern.c_str();
}

void NM_Filter::setCheck( check_t mode )
{
    _check = mode;
}

NM_Filter::check_t NM_Filter::getCheck() const
{
    return _check;
}

int NM_Filter::load()
{
  int found_error = 0;

  setCheck( INACTIVE );
  for (;;) {
    if ( worker_token == PATTERN_WCP ) {
      readtoken();

      if ( worker_token != '=' ) {
        found_error = 1;
        break;
      }      
      readtoken();

      if ( worker_token == STRING_WCP ) {
        pattern = yylval.strptr;
      } else {
        found_error = 1;
        break;
      }      
      readtoken();

      if ( worker_token != ';' ) {
        found_error = 1;
        break;
      }
      readtoken();
    } else if ( worker_token == MODE_WCP ) {
      readtoken();

      if ( worker_token != '=' ) {
        found_error = 1;
        break;
      }      
      readtoken();

      if ( worker_token == INCLUDE_WCP ) {
        setCheck( INCLUDE );
      } else if ( worker_token == EXCLUDE_WCP ) {
        setCheck( EXCLUDE );
      } else {
        found_error = 1;
        break;
      }      
      readtoken();

      if ( worker_token != ';' ) {
        found_error = 1;
        break;
      }
      readtoken();
    } else {
      break;
    }
  }
  
  return found_error;
}

int NM_Filter::save(Datei *fh)
{
  if ( fh == NULL ) return 1;

  fh->configPutPairString( "pattern", pattern.c_str() );
  switch ( _check ) {
    case INCLUDE:
      fh->configPutPair( "mode", "include" );
      break;
    case EXCLUDE:
      fh->configPutPair( "mode", "exclude" );
      break;
    default:
      break;
  }
  return 0;
}

NM_Filter *NM_Filter::duplicate()
{
    NM_Filter *fi = new NM_Filter( *this);
    return fi;
}

bool NM_Filter::operator==( const NM_Filter &other ) const
{
    if ( _check != other._check ) return false;

    if ( pattern == other.pattern ) return true;

    return false;
}

bool NM_Filter::operator!=( const NM_Filter &other ) const
{
    return ! ( *this == other );
}

bool NM_Filter::operator<( const NM_Filter &other ) const
{
    if ( _check == EXCLUDE && other._check != EXCLUDE ) return true;
    if ( _check == INCLUDE && other._check == EXCLUDE ) return false;
    if ( _check == INCLUDE && other._check != INCLUDE ) return true;
    if ( _check == INACTIVE && other._check != INACTIVE ) return false;

    if ( pattern < other.pattern ) return true;

    return false;
}
