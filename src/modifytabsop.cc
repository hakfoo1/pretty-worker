/* modifytabsop.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "modifytabsop.hh"
#include "listermode.h"
#include "worker.h"
#include "aguix/awindow.h"
#include "aguix/text.h"
#include "aguix/solidbutton.h"
#include "aguix/button.h"
#include "aguix/cyclebutton.h"
#include "worker_locale.h"
#include "datei.h"
#include "argclass.hh"
#include "virtualdirmode.hh"

const char *ModifyTabsOp::name = "ModifyTabsOp";

ModifyTabsOp::ModifyTabsOp() : FunctionProto()
{
    m_tab_action = NEW_TAB;
    hasConfigure = true;
}

ModifyTabsOp::~ModifyTabsOp()
{
}

ModifyTabsOp*
ModifyTabsOp::duplicate() const
{
    ModifyTabsOp *ta = new ModifyTabsOp();
    ta->m_tab_action = m_tab_action;
    return ta;
}

bool
ModifyTabsOp::isName( const char *str )
{
    if ( strcmp( str, name ) == 0 ) return true; else return false;
}

const char *
ModifyTabsOp::getName()
{
    return name;
}

int
ModifyTabsOp::run( std::shared_ptr< WPUContext > wpu, ActionMessage *msg )
{
    ListerMode *lm1;
    if ( msg->mode != msg->AM_MODE_DNDACTION ) {
        Lister *l1 = msg->getWorker()->getActiveLister();
        if ( l1 != NULL ) {
            startlister = l1;
            lm1 = startlister->getActiveMode();
            if ( lm1 != NULL ) {
                if ( dynamic_cast< VirtualDirMode *>( lm1 ) ) {
                    nm_tab_action( msg );
                } else {
                    lm1->not_supported();
                }
            }
        }
    }
    return 0;
}

bool
ModifyTabsOp::save( Datei *fh )
{
    if ( fh == NULL ) return false;
    switch ( m_tab_action ) {
        case CLOSE_CURRENT_TAB:
            fh->configPutPair( "tabaction", "closecurrenttab" );
            break;
        case NEXT_TAB:
            fh->configPutPair( "tabaction", "nexttab" );
            break;
        case PREV_TAB:
            fh->configPutPair( "tabaction", "prevtab" );
            break;
        case TOGGLE_LOCK_STATE:
            fh->configPutPair( "tabaction", "togglelockstate" );
            break;
        case MOVE_TAB_LEFT:
            fh->configPutPair( "tabaction", "movetableft" );
            break;
        case MOVE_TAB_RIGHT:
            fh->configPutPair( "tabaction", "movetabright" );
            break;
        default:
            fh->configPutPair( "tabaction", "newtab" );
            break;
    }
    return true;
}

const char *
ModifyTabsOp::getDescription()
{
    return catalog.getLocale( 1297 );
}

void
ModifyTabsOp::nm_tab_action( ActionMessage *am )
{
    ListerMode *lm1 = NULL;

    if ( startlister != NULL ) {
        lm1 = startlister->getActiveMode();
        if ( lm1 != NULL ) {
            std::list< RefCount< ArgClass > > args;
            switch ( m_tab_action ) {
                case CLOSE_CURRENT_TAB:
                    lm1->runCommand( "close_current_tab" );
                    break;
                case NEXT_TAB:
                    args.push_back( RefCount< ArgClass >( new IntArg( 1 ) ) );
                    lm1->runCommand( "switch_to_tab_direction", args );
                    break;
                case PREV_TAB:
                    args.push_back( RefCount< ArgClass >( new IntArg( -1 ) ) );
                    lm1->runCommand( "switch_to_tab_direction", args );
                    break;
                case TOGGLE_LOCK_STATE:
                    lm1->runCommand( "toggle_lock_current_tab" );
                    break;
                case MOVE_TAB_LEFT:
                    args.push_back( RefCount< ArgClass >( new IntArg( -1 ) ) );
                    lm1->runCommand( "move_tab_direction", args );
                    break;
                case MOVE_TAB_RIGHT:
                    args.push_back( RefCount< ArgClass >( new IntArg( 1 ) ) );
                    lm1->runCommand( "move_tab_direction", args );
                    break;
                default:
                    lm1->runCommand( "new_tab" );
                    break;
            }
        }
    }
}

int
ModifyTabsOp::configure()
{
    AGUIX *aguix = Worker::getAGUIX();
    AWindow *win;
    CycleButton *rcyb;
    AGMessage *msg;
    int endmode = -1;
    char *tstr;
  
    tstr = (char*)_allocsafe( strlen( catalog.getLocale( 293 ) ) +
                              strlen( getDescription() ) + 1 );
    sprintf( tstr, catalog.getLocale( 293 ), getDescription() );
    win = new AWindow( aguix, 10, 10, 10, 10, tstr, AWindow::AWINDOW_DIALOG );
    win->create();
    _freesafe(tstr);
    
    AContainer *ac1 = win->setContainer( new AContainer( win, 1, 2 ), true );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );
    
    AContainer *ac1_1 = ac1->add( new AContainer( win, 2, 1 ), 0, 0 );
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( 5 );
    ac1_1->setBorderWidth( 0 );
    
    ac1_1->add( new Text( aguix, 0, 0, catalog.getLocale( 851 ) ), 0, 0, AContainer::CO_FIX );
    rcyb = (CycleButton*)ac1_1->add( new CycleButton( aguix, 0, 0, 100, 0 ), 1, 0, AContainer::CO_INCWNR );
    rcyb->addOption( catalog.getLocale( 852 ) );
    rcyb->addOption( catalog.getLocale( 853 ) );
    rcyb->addOption( catalog.getLocale( 854 ) );
    rcyb->addOption( catalog.getLocale( 855 ) );
    rcyb->addOption( catalog.getLocale( 1212 ) );
    rcyb->addOption( catalog.getLocale( 1215 ) );
    rcyb->addOption( catalog.getLocale( 1216 ) );
    rcyb->resize( rcyb->getMaxSize(), rcyb->getHeight() );
    ac1_1->readLimits();
    switch ( m_tab_action ) {
        case CLOSE_CURRENT_TAB:
            rcyb->setOption( 1 );
            break;
        case NEXT_TAB:
            rcyb->setOption( 2 );
            break;
        case PREV_TAB:
            rcyb->setOption( 3 );
            break;
        case TOGGLE_LOCK_STATE:
            rcyb->setOption( 4 );
            break;
        case MOVE_TAB_LEFT:
            rcyb->setOption( 5 );
            break;
        case MOVE_TAB_RIGHT:
            rcyb->setOption( 6 );
            break;
        default:
            rcyb->setOption( 0 );
            break;
    }
    
    AContainer *ac1_2 = ac1->add( new AContainer( win, 2, 1 ), 0, 1 );
    ac1_2->setMinSpace( 5 );
    ac1_2->setMaxSpace( -1 );
    ac1_2->setBorderWidth( 0 );
    Button *okb =(Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 11 ),
                                                  0 ), 0, 0, AContainer::CO_FIX );
    Button *cb = (Button*)ac1_2->add( new Button( aguix,
                                                  0,
                                                  0,
                                                  catalog.getLocale( 8 ),
                                                  0 ), 1, 0, AContainer::CO_FIX );
    
    win->setDoTabCycling( true );
    win->contMaximize( true );
    win->show();

    for ( ; endmode == -1; ) {
        msg = aguix->WaitMessage( win );
        if ( msg != NULL ) {
            switch ( msg->type ) {
                case AG_CLOSEWINDOW:
                    if ( msg->closewindow.window == win->getWindow() ) endmode = 1;
                    break;
                case AG_BUTTONCLICKED:
                    if ( msg->button.button == okb ) endmode = 0;
                    else if ( msg->button.button == cb ) endmode = 1;
                    break;
                case AG_KEYPRESSED:
                    if ( win->isParent( msg->key.window, false ) == true ) {
                        switch ( msg->key.key ) {
                            case XK_Escape:
                                endmode = 1;
                                break;
                        }
                    }
                    break;
            }
            aguix->ReplyMessage( msg );
        }
    }
    
    if ( endmode == 0 ) {
        // ok
        switch ( rcyb->getSelectedOption() ) {
            case 1:
                m_tab_action = CLOSE_CURRENT_TAB;
                break;
            case 2:
                m_tab_action = NEXT_TAB;
                break;
            case 3:
                m_tab_action = PREV_TAB;
                break;
            case 4:
                m_tab_action = TOGGLE_LOCK_STATE;
                break;
            case 5:
                m_tab_action = MOVE_TAB_LEFT;
                break;
            case 6:
                m_tab_action = MOVE_TAB_RIGHT;
                break;
            default:
                m_tab_action = NEW_TAB;
                break;
        }
    }
    
    delete win;
    
    return endmode;
}

void ModifyTabsOp::setTabAction( modify_tab_t a )
{
    m_tab_action = a;
}
