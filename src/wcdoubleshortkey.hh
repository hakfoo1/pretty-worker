/* wcdoubleshortkey.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006,2011-2012 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCDOUBLESHORTKEY_HH
#define WCDOUBLESHORTKEY_HH

#include "wdefines.h"
#include "aguix/aguix.h"
#include <string>

class WCDoubleShortkey
{
public:
  WCDoubleShortkey();
  ~WCDoubleShortkey();
  WCDoubleShortkey( const WCDoubleShortkey &other );
  WCDoubleShortkey &operator=( const WCDoubleShortkey &other );

  WCDoubleShortkey *duplicate() const;
  void setKeySym( KeySym k, int pos );
  KeySym getKeySym( int pos ) const;

  void setMod( unsigned int m, int pos);
  unsigned int getMod( int pos ) const;

  bool isShortkey( KeySym k, unsigned int m ) const;
  bool isShortkey( KeySym k1, unsigned int m1, KeySym k2, unsigned int m2 ) const;
  bool save( class Datei* ) const;
  bool isReal() const;

  enum shortkey_type { WCDS_NORMAL, WCDS_DOUBLE };

  void setType( enum shortkey_type nv );
  enum shortkey_type getType() const;

  std::string getName( AGUIX *aguix );
private:
  KeySym _key[2];
  unsigned int _mod[2];
  enum shortkey_type _type;
};

#endif
