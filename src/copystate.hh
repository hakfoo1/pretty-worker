/* copystate.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef COPYSTATE_HH
#define COPYSTATE_HH

#include "wdefines.h"
#include <memory>
#include <list>
#include <vector>
#include <string>
#include <tuple>

class CopyCore;
class VirtualDirMode;

class CopyState
{
public:
    CopyState( std::shared_ptr< CopyCore > cc )
        : m_cc( cc ),
          m_row_adjustment( 0 ),
          m_updatevdm( NULL ),
          m_detached( false ),
          m_id( m_next_id++ ),
          m_insert_elements_into_vdm( false )
    {}
    CopyState( const CopyState &rhs ) = delete;
    CopyState &operator=( const CopyState &rhs ) = delete;

    std::shared_ptr< CopyCore > m_cc;
    int m_row_adjustment;
    std::vector< int > m_ids_to_remove;
    std::vector< std::tuple< int, int, std::string > > m_entries_to_delete;
    VirtualDirMode *m_updatevdm;
    std::list< std::string > m_inserts;
    std::list< std::string > m_entries_to_deselect;

    bool m_detached;

    int m_id;

    bool m_insert_elements_into_vdm;
private:
    static int m_next_id;
};

#endif /* COPYSTATE_HH */
