/* luascripting.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "luascripting.hh"

#ifdef HAVE_LUA

extern "C" {

#  include <lauxlib.h>
#  include <lualib.h> 

};

#endif

#include "aguix/util.h"

LUAScripting::LUAScripting()
{
#ifdef HAVE_LUA
    m_lua_state = luaL_newstate();
    luaL_openlibs( m_lua_state );
#endif
}

LUAScripting::~LUAScripting()
{
#ifdef HAVE_LUA
    if ( m_lua_state ) {
        lua_close( m_lua_state );
    }
#endif
}

int LUAScripting::setIntVariable( const std::string &name, int val )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    std::string cmd;

    cmd = AGUIXUtils::formatStringToString( "%s=%d", name.c_str(), val );

    int error = luaL_loadbuffer( m_lua_state,
                                 cmd.c_str(), cmd.length(),
                                 "line" ) ||
        lua_pcall( m_lua_state, 0, 0, 0 );

    if ( error ) {
        storeError( "lua_pcall() failed" );
        return -2;
    }

    return 0;
#else
    return -1;
#endif
}

int LUAScripting::setBoolVariable( const std::string &name, bool val )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    std::string cmd;

    if ( val ) {
        cmd = AGUIXUtils::formatStringToString( "%s=true", name.c_str() );
    } else {
        cmd = AGUIXUtils::formatStringToString( "%s=false", name.c_str() );
    }

    int error = luaL_loadbuffer( m_lua_state,
                                 cmd.c_str(), cmd.length(),
                                 "line" ) ||
        lua_pcall( m_lua_state, 0, 0, 0 );

    if ( error ) {
        storeError( "lua_pcall() failed" );
        return -2;
    }

    return 0;
#else
    return -1;
#endif
}

int LUAScripting::setStringVariable( const std::string &name, const std::string &val )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    std::string cmd;

    //TODO do correct quoting
    cmd = AGUIXUtils::formatStringToString( "%s=[[%s]]", name.c_str(), val.c_str() );

    int error = luaL_loadbuffer( m_lua_state,
                                 cmd.c_str(), cmd.length(),
                                 "line" ) ||
        lua_pcall( m_lua_state, 0, 0, 0 );

    if ( error ) {
        storeError( "lua_pcall() failed" );
        return -2;
    }

    return 0;
#else
    return -1;
#endif
}

int LUAScripting::evalCommand( const std::string &cmd, int number_return_values )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    int error = luaL_loadbuffer( m_lua_state,
        cmd.c_str(), cmd.length(), "line" ) ||
        lua_pcall( m_lua_state, 0, number_return_values, 0 );

    if ( error ) {
        storeError( "lua_pcall() failed" );
        return -2;
    }

    return 0;
#else
    return -1;
#endif
}

int LUAScripting::getIntReturnValue( int &res )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    int t = lua_type( m_lua_state, -1 );

    if ( t == LUA_TNUMBER ) {
        res = lua_tonumber( m_lua_state, -1 );
        return 0;
    }

    return -2;
#else
    return -1;
#endif
}

int LUAScripting::getBoolReturnValue( bool &res )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    int t = lua_type( m_lua_state, -1 );

    if ( t == LUA_TBOOLEAN ) {
        res = lua_toboolean( m_lua_state, -1 ) ? true : false;
        return 0;
    }

    return -2;
#else
    return -1;
#endif
}

int LUAScripting::getStringReturnValue( std::string &res )
{
#ifdef HAVE_LUA
    if ( ! m_lua_state ) return -1;

    int t = lua_type( m_lua_state, -1 );

    if ( t == LUA_TSTRING ) {
        res = lua_tostring( m_lua_state, -1 );
        return 0;
    }

    return -2;
#else
    return -1;
#endif
}

#ifdef HAVE_LUA
void LUAScripting::storeError( const char *msg )
{
    m_last_error_string = AGUIXUtils::formatStringToString( "LUA error: %s: %s",
                                                            msg, lua_tostring( m_lua_state, -1 ) );
}
#endif

std::string LUAScripting::getErrorString()
{
#ifdef HAVE_LUA
    return m_last_error_string;
#else
    return "";
#endif
}
