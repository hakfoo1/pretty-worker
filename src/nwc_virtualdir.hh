/* nwc_virtualdir.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2008,2011 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NWC_VIRTUALDIR_HH
#define NWC_VIRTUALDIR_HH

#include "wdefines.h"

#include "nwc_dir.hh"

namespace NWC
{
  
  class VirtualDir : public Dir
  {
  public:
    VirtualDir( const std::string &fullname, bool follow_symlinks = false, bool search_vfs = false, bool search_same_fs = false );
    VirtualDir( const FSEntry &other, bool follow_symlinks = false, bool search_vfs = false, bool search_same_fs = false );
    ~VirtualDir();

    File  *createFile( const FSEntry &entry );
    Dir   *createDir( const FSEntry &entry );
    
    FSEntry       *clone() const;
    virtual void setSearchVFS( bool nv );
    virtual bool getSearchVFS() const;
    virtual void setSearchSameFS( bool nv );
    virtual bool getSearchSameFS() const;
      virtual void setVirtualDepthCount( int depth );
      virtual int getVirtualDepthCount() const;
  protected:
    void handleDirEntry( const std::string &filename );
  private:
    bool _search_vfs;
    bool _search_same_fs;
      int m_virtual_depth_count;
  };

}

#endif
