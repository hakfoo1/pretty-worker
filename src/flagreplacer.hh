/* flagreplacer.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2021 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FLAGREPLACER_HH
#define FLAGREPLACER_HH

#include "wdefines.h"
#include <string>
#include <list>
#include <map>
#include <memory>
#include <functional>

class FlagReplacer
{
public:
  class FlagProducer
  {
  public:
    virtual ~FlagProducer() {}
    virtual std::string getFlagReplacement( const std::string &flag ) = 0;
    virtual bool hasFlag( const std::string &str ) = 0;
  };
  
  FlagReplacer( std::shared_ptr< FlagProducer > flagstore );
  virtual ~FlagReplacer();
  FlagReplacer( const FlagReplacer &other );
  FlagReplacer &operator=( const FlagReplacer &other );

  std::string replaceFlags( const std::string &str, bool quote = true );
  int failed_flag_replacements();

  class FlagProducerMap : public FlagProducer
  {
  public:
    FlagProducerMap();
    ~FlagProducerMap();
    FlagProducerMap( const FlagProducerMap &other );
    FlagProducerMap &operator=( const FlagProducerMap &other );

    std::string getFlagReplacement( const std::string &flag ) override;
    bool hasFlag( const std::string &flag ) override;

    void registerFlag( const std::string &flag,
                       const std::string &value );
  private:
    std::map<std::string, std::string> _flagvalue;
  };

  class FlagProducerCallback : public FlagProducer
  {
  public:
      FlagProducerCallback( void *user_data );
      ~FlagProducerCallback();
      FlagProducerCallback( const FlagProducerCallback &other );
      FlagProducerCallback &operator=( const FlagProducerCallback &other );

      std::string getFlagReplacement( const std::string &flag ) override;
      bool hasFlag( const std::string &flag ) override;

      void registerFlag( const std::string &flag,
                         std::function< std::string( void *user_data ) > callback );
  private:
      std::map< std::string, std::function< std::string( void *user_data ) > > m_flag_values;
      void *m_user_data;
  };

  class FlagHelp
  {
  public:
    FlagHelp();
    virtual ~FlagHelp();

    virtual void registerFlag( const std::string &flag,
                               const std::string &description );
    virtual std::list<std::string> getListOfFlags() const;
    virtual std::string getFlagDescription( const std::string &flag ) const;
  private:
    std::map<std::string, std::string> _flagdescr;
  };
  
  static std::string requestFlag( const FlagHelp &flags );
private:
    std::shared_ptr< FlagProducer > m_flagstore;
    int m_replace_errors;
};

#endif
