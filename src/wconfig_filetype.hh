/* wconfig_filetype.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2020 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef WCONFIG_FILETYPE_HH
#define WCONFIG_FILETYPE_HH

#include "wdefines.h"
#include "wconfig_panel.hh"
#include <string>

class WConfig;
class WCFiletype;
class FlatTypeList;

class FiletypePanel : public WConfigPanel
{
public:
  FiletypePanel( AWindow &basewin, WConfig &baseconfig );
  ~FiletypePanel();
  int create();
  int saveValues();

  /* gui elements callback */
  void run( Widget *, const AGMessage &msg ) override;

  /* we are interested in updated colors */
  panel_action_t setColors( List *colors );
  void setAllowRemoveUnique( bool nv );
  int addFiletypes( List *filetypes, WConfigPanelCallBack::add_action_t action );
  void executeInitialCommand( const WConfigInitialCommand &cmd );
protected:
  WConfig *tempconfig;
  bool allowRemoveUnique;
  
  Button *newb, *dupb, *delb, *editb, *newchildb, *cutb, *pasteb, *pastechildb;
  FieldListView *lv;
  WCFiletype *cutft;
  StringGadget *m_filter_sg;
  Button *m_clear_filter_b;

  FlatTypeList *l1;
  void updateConf( List *filetypes,
		   FlatTypeList **l1,
		   FieldListView *lv,
		   WCFiletype *highlightft,
                   const std::string &filter = "" );
};
 
#endif
