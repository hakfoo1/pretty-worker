/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "configparser.yy"

/* parser for config file (bison generated)
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2003-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wdefines.h"
#include "wconfig.h"
#include "wcdoubleshortkey.hh"
#include "wcpath.hh"
#include "wcfiletype.hh"
#include "wchotkey.hh"
#include "wcbutton.hh"
#include <stdlib.h>
#include <string.h>
#include "worker_commands.h"
#include "configheader.h"
#include <list>
#include "verzeichnis.hh"
#include <map>
#include "dirfiltersettings.hh"
#include "layoutsettings.hh"
#include "simplelist.hh"
#include "worker_types.h"
#include "wconfig_types.hh"

using namespace CopyParams;

int lconfig_side;
int lconfig_pos;
List *lconfig_listp,
     *lconfig_listb,
     *lconfig_listft,
     *lconfig_listh,
     *lconfig_listsk;
std::list< WConfigTypes::dont_check_dir > lconfig_ignorelist;
WConfigTypes::dont_check_dir lconfig_ignore_dir;
command_list_t lconfig_listcom;
std::list< std::string > lconfig_pathjump_allow;
std::map< std::string, struct directory_presets > dir_presets;
std::string path;
struct directory_presets dir_preset;
NM_Filter dir_preset_filter;
WCButton *lconfig_bt1;
WCFiletype *lconfig_ft1, *old_lconfig_ft1;
std::list<WCFiletype*> filetypehier;
WCHotkey *lconfig_hk1;
WCDoubleShortkey *lconfig_dk;
WCPath *lconfig_p1;
int lconfig_id;
KeySym lconfig_nkeysym;
unsigned int lconfig_keymod;
char *lconfig_tstr;
std::shared_ptr< FunctionProto > lconfig_fp1;
char lconfig_error[256];
std::map<std::string, WConfig::ColorDef::label_colors_t> lconfig_labelcolors;
WConfig::ColorDef::label_colors_t lconfig_labelcolor;
std::string lconfig_labelcolor_name;
LayoutSettings layout_sets;
std::string lconfig_face_name;
int lconfig_face_color;
std::list< std::pair< std::string, int > > lconfig_faces_list;
std::vector< WorkerTypes::listcol_t > columns;

extern WConfig *lconfig;

extern int yyerror( const char *s );

void lconfig_parseinit(void)
{
  lconfig_listp = NULL;
  lconfig_listb = NULL;
  lconfig_listft = NULL;
  lconfig_listh = NULL;
  lconfig_listsk = NULL;
  lconfig_listcom = {};
  lconfig_ignorelist.clear();
  lconfig_bt1 = NULL;
  lconfig_ft1 = NULL;
  lconfig_hk1 = NULL;
  lconfig_dk = NULL;
  lconfig_p1 = NULL;
  lconfig_tstr = NULL;
  lconfig_fp1 = NULL;
}

void lconfig_cleanup(void)
{
  // free all stuff in the list
  if ( lconfig_fp1 ) {
    lconfig_fp1.reset();
  }
  if ( lconfig_tstr != NULL ) {
    _freesafe( lconfig_tstr );
    lconfig_tstr = NULL;
  }
  if ( lconfig_p1 != NULL ) {
    delete lconfig_p1;
    lconfig_p1 = NULL;
  }
  if ( lconfig_dk != NULL ) {
    delete lconfig_dk;
    lconfig_dk = NULL;
  }
  if ( lconfig_hk1 != NULL ) {
    delete lconfig_hk1;
    lconfig_hk1 = NULL;
  }
  if ( lconfig_ft1 != NULL ) {
    delete lconfig_ft1;
    lconfig_ft1 = NULL;
  }
  if ( lconfig_bt1 != NULL ) {
    delete lconfig_bt1;
    lconfig_bt1 = NULL;
  }
  if ( lconfig_listp != NULL ) {
    lconfig_id = lconfig_listp->initEnum();
    lconfig_p1 = (WCPath*)lconfig_listp->getFirstElement( lconfig_id );
    while ( lconfig_p1 != NULL ) {
      delete lconfig_p1;
      lconfig_p1 = (WCPath*)lconfig_listp->getNextElement( lconfig_id );
    }
    lconfig_listp->closeEnum( lconfig_id );
    delete lconfig_listp;
    lconfig_listp = NULL;
  }
  if ( lconfig_listb != NULL ) {
    lconfig_id = lconfig_listb->initEnum();
    lconfig_bt1 = (WCButton*)lconfig_listb->getFirstElement( lconfig_id );
    while ( lconfig_bt1 != NULL ) {
      delete lconfig_bt1;
      lconfig_bt1 = (WCButton*)lconfig_listb->getNextElement( lconfig_id );
    }
    lconfig_listb->closeEnum( lconfig_id );
    delete lconfig_listb;
    lconfig_listb = NULL;
  }
  if ( lconfig_listft != NULL ) {
    lconfig_id = lconfig_listft->initEnum();
    lconfig_ft1 = (WCFiletype*)lconfig_listft->getFirstElement( lconfig_id );
    while ( lconfig_ft1 != NULL ) {
      delete lconfig_ft1;
      lconfig_ft1 = (WCFiletype*)lconfig_listft->getNextElement( lconfig_id );
    }
    lconfig_listft->closeEnum( lconfig_id );
    delete lconfig_listft;
    lconfig_listft = NULL;
  }

  while ( filetypehier.size() > 0 ) {
      lconfig_ft1 = filetypehier.back();
      filetypehier.pop_back();
      delete lconfig_ft1;
      lconfig_ft1 = NULL;
  }

  if ( lconfig_listh != NULL ) {
    lconfig_id = lconfig_listh->initEnum();
    lconfig_hk1 = (WCHotkey*)lconfig_listh->getFirstElement( lconfig_id );
    while ( lconfig_hk1 != NULL ) {
      delete lconfig_hk1;
      lconfig_hk1 = (WCHotkey*)lconfig_listh->getNextElement( lconfig_id );
    }
    lconfig_listh->closeEnum( lconfig_id );
    delete lconfig_listh;
    lconfig_listh = NULL;
  }
  if ( lconfig_listsk != NULL ) {
    lconfig_id = lconfig_listsk->initEnum();
    lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
    while ( lconfig_dk != NULL ) {
      delete lconfig_dk;
      lconfig_listsk->removeFirstElement();
      lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
    }
    lconfig_listsk->closeEnum( lconfig_id );
    delete lconfig_listsk;
    lconfig_listsk = NULL;
  }
  lconfig_listcom.clear();
  lconfig_ignorelist.clear();
}

/*#define YYERROR_VERBOSE*/


#line 275 "configparser.cc"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_CONFIGPARSER_HH_INCLUDED
# define YY_YY_CONFIGPARSER_HH_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    LEFTBRACE_WCP = 258,           /* LEFTBRACE_WCP  */
    RIGHTBRACE_WCP = 259,          /* RIGHTBRACE_WCP  */
    GLOBAL_WCP = 260,              /* GLOBAL_WCP  */
    COLORS_WCP = 261,              /* COLORS_WCP  */
    LANG_WCP = 262,                /* LANG_WCP  */
    PALETTE_WCP = 263,             /* PALETTE_WCP  */
    OWNOP_WCP = 264,               /* OWNOP_WCP  */
    LISTERSETS_WCP = 265,          /* LISTERSETS_WCP  */
    YES_WCP = 266,                 /* YES_WCP  */
    NO_WCP = 267,                  /* NO_WCP  */
    LEFT_WCP = 268,                /* LEFT_WCP  */
    RIGHT_WCP = 269,               /* RIGHT_WCP  */
    HBARTOP_WCP = 270,             /* HBARTOP_WCP  */
    HBARHEIGHT_WCP = 271,          /* HBARHEIGHT_WCP  */
    VBARLEFT_WCP = 272,            /* VBARLEFT_WCP  */
    VBARWIDTH_WCP = 273,           /* VBARWIDTH_WCP  */
    DISPLAYSETS_WCP = 274,         /* DISPLAYSETS_WCP  */
    NAME_WCP = 275,                /* NAME_WCP  */
    SIZE_WCP = 276,                /* SIZE_WCP  */
    TYPE_WCP = 277,                /* TYPE_WCP  */
    PERMISSION_WCP = 278,          /* PERMISSION_WCP  */
    OWNER_WCP = 279,               /* OWNER_WCP  */
    DESTINATION_WCP = 280,         /* DESTINATION_WCP  */
    MODTIME_WCP = 281,             /* MODTIME_WCP  */
    ACCTIME_WCP = 282,             /* ACCTIME_WCP  */
    CHGTIME_WCP = 283,             /* CHGTIME_WCP  */
    ROWS_WCP = 284,                /* ROWS_WCP  */
    COLUMNS_WCP = 285,             /* COLUMNS_WCP  */
    CACHESIZE_WCP = 286,           /* CACHESIZE_WCP  */
    OWNERSTYLE_WCP = 287,          /* OWNERSTYLE_WCP  */
    TERMINAL_WCP = 288,            /* TERMINAL_WCP  */
    USESTRINGFORDIRSIZE_WCP = 289, /* USESTRINGFORDIRSIZE_WCP  */
    TIMESETS_WCP = 290,            /* TIMESETS_WCP  */
    STYLE1_WCP = 291,              /* STYLE1_WCP  */
    STYLE2_WCP = 292,              /* STYLE2_WCP  */
    STYLE3_WCP = 293,              /* STYLE3_WCP  */
    DATE_WCP = 294,                /* DATE_WCP  */
    TIME_WCP = 295,                /* TIME_WCP  */
    DATESUBSTITUTION_WCP = 296,    /* DATESUBSTITUTION_WCP  */
    DATEBEFORETIME_WCP = 297,      /* DATEBEFORETIME_WCP  */
    STATEBAR_WCP = 298,            /* STATEBAR_WCP  */
    SELLVBAR_WCP = 299,            /* SELLVBAR_WCP  */
    UNSELLVBAR_WCP = 300,          /* UNSELLVBAR_WCP  */
    CLOCKBAR_WCP = 301,            /* CLOCKBAR_WCP  */
    REQUESTER_WCP = 302,           /* REQUESTER_WCP  */
    UNSELDIR_WCP = 303,            /* UNSELDIR_WCP  */
    SELDIR_WCP = 304,              /* SELDIR_WCP  */
    UNSELFILE_WCP = 305,           /* UNSELFILE_WCP  */
    SELFILE_WCP = 306,             /* SELFILE_WCP  */
    UNSELACTDIR_WCP = 307,         /* UNSELACTDIR_WCP  */
    SELACTDIR_WCP = 308,           /* SELACTDIR_WCP  */
    UNSELACTFILE_WCP = 309,        /* UNSELACTFILE_WCP  */
    SELACTFILE_WCP = 310,          /* SELACTFILE_WCP  */
    LVBG_WCP = 311,                /* LVBG_WCP  */
    STARTUP_WCP = 312,             /* STARTUP_WCP  */
    BUTTON_WCP = 313,              /* BUTTON_WCP  */
    BUTTONS_WCP = 314,             /* BUTTONS_WCP  */
    POSITION_WCP = 315,            /* POSITION_WCP  */
    TITLE_WCP = 316,               /* TITLE_WCP  */
    COMMANDS_WCP = 317,            /* COMMANDS_WCP  */
    SHORTKEYS_WCP = 318,           /* SHORTKEYS_WCP  */
    PATHS_WCP = 319,               /* PATHS_WCP  */
    PATH_WCP = 320,                /* PATH_WCP  */
    FILETYPES_WCP = 321,           /* FILETYPES_WCP  */
    FILETYPE_WCP = 322,            /* FILETYPE_WCP  */
    NORMAL_WCP = 323,              /* NORMAL_WCP  */
    NOTYETCHECKED_WCP = 324,       /* NOTYETCHECKED_WCP  */
    UNKNOWN_WCP = 325,             /* UNKNOWN_WCP  */
    NOSELECT_WCP = 326,            /* NOSELECT_WCP  */
    DIR_WCP = 327,                 /* DIR_WCP  */
    USEPATTERN_WCP = 328,          /* USEPATTERN_WCP  */
    USECONTENT_WCP = 329,          /* USECONTENT_WCP  */
    PATTERN_WCP = 330,             /* PATTERN_WCP  */
    CONTENT_WCP = 331,             /* CONTENT_WCP  */
    FTCOMMANDS_WCP = 332,          /* FTCOMMANDS_WCP  */
    DND_WCP = 333,                 /* DND_WCP  */
    DC_WCP = 334,                  /* DC_WCP  */
    SHOW_WCP = 335,                /* SHOW_WCP  */
    RAWSHOW_WCP = 336,             /* RAWSHOW_WCP  */
    USER_WCP = 337,                /* USER_WCP  */
    FLAGS_WCP = 338,               /* FLAGS_WCP  */
    IGNOREDIRS_WCP = 339,          /* IGNOREDIRS_WCP  */
    HOTKEYS_WCP = 340,             /* HOTKEYS_WCP  */
    HOTKEY_WCP = 341,              /* HOTKEY_WCP  */
    FONTS_WCP = 342,               /* FONTS_WCP  */
    GLOBALFONT_WCP = 343,          /* GLOBALFONT_WCP  */
    BUTTONFONT_WCP = 344,          /* BUTTONFONT_WCP  */
    LEFTFONT_WCP = 345,            /* LEFTFONT_WCP  */
    RIGHTFONT_WCP = 346,           /* RIGHTFONT_WCP  */
    TEXTVIEWFONT_WCP = 347,        /* TEXTVIEWFONT_WCP  */
    STATEBARFONT_WCP = 348,        /* STATEBARFONT_WCP  */
    TEXTVIEWALTFONT_WCP = 349,     /* TEXTVIEWALTFONT_WCP  */
    CLOCKBARSETS_WCP = 350,        /* CLOCKBARSETS_WCP  */
    MODUS_WCP = 351,               /* MODUS_WCP  */
    TIMESPACE_WCP = 352,           /* TIMESPACE_WCP  */
    VERSION_WCP = 353,             /* VERSION_WCP  */
    EXTERN_WCP = 354,              /* EXTERN_WCP  */
    UPDATETIME_WCP = 355,          /* UPDATETIME_WCP  */
    PROGRAM_WCP = 356,             /* PROGRAM_WCP  */
    SHOWHINTS_WCP = 357,           /* SHOWHINTS_WCP  */
    KEY_WCP = 358,                 /* KEY_WCP  */
    DOUBLE_WCP = 359,              /* DOUBLE_WCP  */
    MOD_WCP = 360,                 /* MOD_WCP  */
    CONTROL_WCP = 361,             /* CONTROL_WCP  */
    SHIFT_WCP = 362,               /* SHIFT_WCP  */
    LOCK_WCP = 363,                /* LOCK_WCP  */
    MOD1_WCP = 364,                /* MOD1_WCP  */
    MOD2_WCP = 365,                /* MOD2_WCP  */
    MOD3_WCP = 366,                /* MOD3_WCP  */
    MOD4_WCP = 367,                /* MOD4_WCP  */
    MOD5_WCP = 368,                /* MOD5_WCP  */
    DNDACTION_WCP = 369,           /* DNDACTION_WCP  */
    DCACTION_WCP = 370,            /* DCACTION_WCP  */
    SHOWACTION_WCP = 371,          /* SHOWACTION_WCP  */
    RSHOWACTION_WCP = 372,         /* RSHOWACTION_WCP  */
    USERACTION_WCP = 373,          /* USERACTION_WCP  */
    ROWUP_WCP = 374,               /* ROWUP_WCP  */
    ROWDOWN_WCP = 375,             /* ROWDOWN_WCP  */
    CHANGEHIDDENFLAG_WCP = 376,    /* CHANGEHIDDENFLAG_WCP  */
    COPYOP_WCP = 377,              /* COPYOP_WCP  */
    FIRSTROW_WCP = 378,            /* FIRSTROW_WCP  */
    LASTROW_WCP = 379,             /* LASTROW_WCP  */
    PAGEUP_WCP = 380,              /* PAGEUP_WCP  */
    PAGEDOWN_WCP = 381,            /* PAGEDOWN_WCP  */
    SELECTOP_WCP = 382,            /* SELECTOP_WCP  */
    SELECTALLOP_WCP = 383,         /* SELECTALLOP_WCP  */
    SELECTNONEOP_WCP = 384,        /* SELECTNONEOP_WCP  */
    INVERTALLOP_WCP = 385,         /* INVERTALLOP_WCP  */
    PARENTDIROP_WCP = 386,         /* PARENTDIROP_WCP  */
    ENTERDIROP_WCP = 387,          /* ENTERDIROP_WCP  */
    CHANGELISTERSETOP_WCP = 388,   /* CHANGELISTERSETOP_WCP  */
    SWITCHLISTEROP_WCP = 389,      /* SWITCHLISTEROP_WCP  */
    FILTERSELECTOP_WCP = 390,      /* FILTERSELECTOP_WCP  */
    FILTERUNSELECTOP_WCP = 391,    /* FILTERUNSELECTOP_WCP  */
    PATHTOOTHERSIDEOP_WCP = 392,   /* PATHTOOTHERSIDEOP_WCP  */
    QUITOP_WCP = 393,              /* QUITOP_WCP  */
    DELETEOP_WCP = 394,            /* DELETEOP_WCP  */
    RELOADOP_WCP = 395,            /* RELOADOP_WCP  */
    MAKEDIROP_WCP = 396,           /* MAKEDIROP_WCP  */
    RENAMEOP_WCP = 397,            /* RENAMEOP_WCP  */
    DIRSIZEOP_WCP = 398,           /* DIRSIZEOP_WCP  */
    SIMDDOP_WCP = 399,             /* SIMDDOP_WCP  */
    STARTPROGOP_WCP = 400,         /* STARTPROGOP_WCP  */
    SEARCHENTRYOP_WCP = 401,       /* SEARCHENTRYOP_WCP  */
    ENTERPATHOP_WCP = 402,         /* ENTERPATHOP_WCP  */
    SCROLLLISTEROP_WCP = 403,      /* SCROLLLISTEROP_WCP  */
    CREATESYMLINKOP_WCP = 404,     /* CREATESYMLINKOP_WCP  */
    CHANGESYMLINKOP_WCP = 405,     /* CHANGESYMLINKOP_WCP  */
    CHMODOP_WCP = 406,             /* CHMODOP_WCP  */
    TOGGLELISTERMODEOP_WCP = 407,  /* TOGGLELISTERMODEOP_WCP  */
    SETSORTMODEOP_WCP = 408,       /* SETSORTMODEOP_WCP  */
    SETFILTEROP_WCP = 409,         /* SETFILTEROP_WCP  */
    SHORTKEYFROMLISTOP_WCP = 410,  /* SHORTKEYFROMLISTOP_WCP  */
    CHOWNOP_WCP = 411,             /* CHOWNOP_WCP  */
    WORKERCONFIG_WCP = 412,        /* WORKERCONFIG_WCP  */
    USERSTYLE_WCP = 413,           /* USERSTYLE_WCP  */
    DATESTRING_WCP = 414,          /* DATESTRING_WCP  */
    TIMESTRING_WCP = 415,          /* TIMESTRING_WCP  */
    COLOR_WCP = 416,               /* COLOR_WCP  */
    PATTERNIGNORECASE_WCP = 417,   /* PATTERNIGNORECASE_WCP  */
    PATTERNUSEREGEXP_WCP = 418,    /* PATTERNUSEREGEXP_WCP  */
    PATTERNUSEFULLNAME_WCP = 419,  /* PATTERNUSEFULLNAME_WCP  */
    COM_WCP = 420,                 /* COM_WCP  */
    SEPARATEEACHENTRY_WCP = 421,   /* SEPARATEEACHENTRY_WCP  */
    RECURSIVE_WCP = 422,           /* RECURSIVE_WCP  */
    START_WCP = 423,               /* START_WCP  */
    TERMINALWAIT_WCP = 424,        /* TERMINALWAIT_WCP  */
    SHOWOUTPUT_WCP = 425,          /* SHOWOUTPUT_WCP  */
    VIEWSTR_WCP = 426,             /* VIEWSTR_WCP  */
    SHOWOUTPUTINT_WCP = 427,       /* SHOWOUTPUTINT_WCP  */
    SHOWOUTPUTOTHERSIDE_WCP = 428, /* SHOWOUTPUTOTHERSIDE_WCP  */
    SHOWOUTPUTCUSTOMATTRIBUTE_WCP = 429, /* SHOWOUTPUTCUSTOMATTRIBUTE_WCP  */
    INBACKGROUND_WCP = 430,        /* INBACKGROUND_WCP  */
    TAKEDIRS_WCP = 431,            /* TAKEDIRS_WCP  */
    ACTIONNUMBER_WCP = 432,        /* ACTIONNUMBER_WCP  */
    HIDDENFILES_WCP = 433,         /* HIDDENFILES_WCP  */
    HIDE_WCP = 434,                /* HIDE_WCP  */
    TOGGLE_WCP = 435,              /* TOGGLE_WCP  */
    FOLLOWSYMLINKS_WCP = 436,      /* FOLLOWSYMLINKS_WCP  */
    MOVE_WCP = 437,                /* MOVE_WCP  */
    RENAME_WCP = 438,              /* RENAME_WCP  */
    SAMEDIR_WCP = 439,             /* SAMEDIR_WCP  */
    REQUESTDEST_WCP = 440,         /* REQUESTDEST_WCP  */
    REQUESTFLAGS_WCP = 441,        /* REQUESTFLAGS_WCP  */
    OVERWRITE_WCP = 442,           /* OVERWRITE_WCP  */
    ALWAYS_WCP = 443,              /* ALWAYS_WCP  */
    NEVER_WCP = 444,               /* NEVER_WCP  */
    COPYMODE_WCP = 445,            /* COPYMODE_WCP  */
    FAST_WCP = 446,                /* FAST_WCP  */
    PRESERVEATTR_WCP = 447,        /* PRESERVEATTR_WCP  */
    MODE_WCP = 448,                /* MODE_WCP  */
    ACTIVE_WCP = 449,              /* ACTIVE_WCP  */
    ACTIVE2OTHER_WCP = 450,        /* ACTIVE2OTHER_WCP  */
    SPECIAL_WCP = 451,             /* SPECIAL_WCP  */
    REQUEST_WCP = 452,             /* REQUEST_WCP  */
    CURRENT_WCP = 453,             /* CURRENT_WCP  */
    OTHER_WCP = 454,               /* OTHER_WCP  */
    FILTER_WCP = 455,              /* FILTER_WCP  */
    QUICK_WCP = 456,               /* QUICK_WCP  */
    ALSOACTIVE_WCP = 457,          /* ALSOACTIVE_WCP  */
    RESETDIRSIZES_WCP = 458,       /* RESETDIRSIZES_WCP  */
    KEEPFILETYPES_WCP = 459,       /* KEEPFILETYPES_WCP  */
    RELATIVE_WCP = 460,            /* RELATIVE_WCP  */
    ONFILES_WCP = 461,             /* ONFILES_WCP  */
    ONDIRS_WCP = 462,              /* ONDIRS_WCP  */
    SORTBY_WCP = 463,              /* SORTBY_WCP  */
    SORTFLAG_WCP = 464,            /* SORTFLAG_WCP  */
    REVERSE_WCP = 465,             /* REVERSE_WCP  */
    DIRLAST_WCP = 466,             /* DIRLAST_WCP  */
    DIRMIXED_WCP = 467,            /* DIRMIXED_WCP  */
    EXCLUDE_WCP = 468,             /* EXCLUDE_WCP  */
    INCLUDE_WCP = 469,             /* INCLUDE_WCP  */
    UNSET_WCP = 470,               /* UNSET_WCP  */
    UNSETALL_WCP = 471,            /* UNSETALL_WCP  */
    SCRIPTOP_WCP = 472,            /* SCRIPTOP_WCP  */
    NOP_WCP = 473,                 /* NOP_WCP  */
    PUSH_WCP = 474,                /* PUSH_WCP  */
    LABEL_WCP = 475,               /* LABEL_WCP  */
    IF_WCP = 476,                  /* IF_WCP  */
    END_WCP = 477,                 /* END_WCP  */
    POP_WCP = 478,                 /* POP_WCP  */
    SETTINGS_WCP = 479,            /* SETTINGS_WCP  */
    WINDOW_WCP = 480,              /* WINDOW_WCP  */
    GOTO_WCP = 481,                /* GOTO_WCP  */
    PUSHUSEOUTPUT_WCP = 482,       /* PUSHUSEOUTPUT_WCP  */
    DODEBUG_WCP = 483,             /* DODEBUG_WCP  */
    WPURECURSIVE_WCP = 484,        /* WPURECURSIVE_WCP  */
    WPUTAKEDIRS_WCP = 485,         /* WPUTAKEDIRS_WCP  */
    STACKNR_WCP = 486,             /* STACKNR_WCP  */
    PUSHSTRING_WCP = 487,          /* PUSHSTRING_WCP  */
    PUSHOUTPUTRETURNCODE_WCP = 488, /* PUSHOUTPUTRETURNCODE_WCP  */
    IFTEST_WCP = 489,              /* IFTEST_WCP  */
    IFLABEL_WCP = 490,             /* IFLABEL_WCP  */
    WINTYPE_WCP = 491,             /* WINTYPE_WCP  */
    OPEN_WCP = 492,                /* OPEN_WCP  */
    CLOSE_WCP = 493,               /* CLOSE_WCP  */
    LEAVE_WCP = 494,               /* LEAVE_WCP  */
    CHANGEPROGRESS_WCP = 495,      /* CHANGEPROGRESS_WCP  */
    CHANGETEXT_WCP = 496,          /* CHANGETEXT_WCP  */
    PROGRESSUSEOUTPUT_WCP = 497,   /* PROGRESSUSEOUTPUT_WCP  */
    WINTEXTUSEOUTPUT_WCP = 498,    /* WINTEXTUSEOUTPUT_WCP  */
    PROGRESS_WCP = 499,            /* PROGRESS_WCP  */
    WINTEXT_WCP = 500,             /* WINTEXT_WCP  */
    SHOWDIRCACHEOP_WCP = 501,      /* SHOWDIRCACHEOP_WCP  */
    INODE_WCP = 502,               /* INODE_WCP  */
    NLINK_WCP = 503,               /* NLINK_WCP  */
    BLOCKS_WCP = 504,              /* BLOCKS_WCP  */
    SHOWHEADER_WCP = 505,          /* SHOWHEADER_WCP  */
    LVHEADER_WCP = 506,            /* LVHEADER_WCP  */
    IGNORECASE_WCP = 507,          /* IGNORECASE_WCP  */
    LISTVIEWS_WCP = 508,           /* LISTVIEWS_WCP  */
    BLL_WCP = 509,                 /* BLL_WCP  */
    LBL_WCP = 510,                 /* LBL_WCP  */
    LLB_WCP = 511,                 /* LLB_WCP  */
    BL_WCP = 512,                  /* BL_WCP  */
    LB_WCP = 513,                  /* LB_WCP  */
    LAYOUT_WCP = 514,              /* LAYOUT_WCP  */
    BUTTONSVERT_WCP = 515,         /* BUTTONSVERT_WCP  */
    LISTVIEWSVERT_WCP = 516,       /* LISTVIEWSVERT_WCP  */
    LISTVIEWWEIGHT_WCP = 517,      /* LISTVIEWWEIGHT_WCP  */
    WEIGHTTOACTIVE_WCP = 518,      /* WEIGHTTOACTIVE_WCP  */
    EXTCOND_WCP = 519,             /* EXTCOND_WCP  */
    USEEXTCOND_WCP = 520,          /* USEEXTCOND_WCP  */
    SUBTYPE_WCP = 521,             /* SUBTYPE_WCP  */
    PARENTACTIONOP_WCP = 522,      /* PARENTACTIONOP_WCP  */
    NOOPERATIONOP_WCP = 523,       /* NOOPERATIONOP_WCP  */
    COLORMODE_WCP = 524,           /* COLORMODE_WCP  */
    DEFAULT_WCP = 525,             /* DEFAULT_WCP  */
    CUSTOM_WCP = 526,              /* CUSTOM_WCP  */
    UNSELECTCOLOR_WCP = 527,       /* UNSELECTCOLOR_WCP  */
    SELECTCOLOR_WCP = 528,         /* SELECTCOLOR_WCP  */
    UNSELECTACTIVECOLOR_WCP = 529, /* UNSELECTACTIVECOLOR_WCP  */
    SELECTACTIVECOLOR_WCP = 530,   /* SELECTACTIVECOLOR_WCP  */
    COLOREXTERNPROG_WCP = 531,     /* COLOREXTERNPROG_WCP  */
    PARENT_WCP = 532,              /* PARENT_WCP  */
    DONTCD_WCP = 533,              /* DONTCD_WCP  */
    DONTCHECKVIRTUAL_WCP = 534,    /* DONTCHECKVIRTUAL_WCP  */
    GOFTPOP_WCP = 535,             /* GOFTPOP_WCP  */
    HOSTNAME_WCP = 536,            /* HOSTNAME_WCP  */
    USERNAME_WCP = 537,            /* USERNAME_WCP  */
    PASSWORD_WCP = 538,            /* PASSWORD_WCP  */
    DONTENTERFTP_WCP = 539,        /* DONTENTERFTP_WCP  */
    ALWAYSSTOREPW_WCP = 540,       /* ALWAYSSTOREPW_WCP  */
    INTERNALVIEWOP_WCP = 541,      /* INTERNALVIEWOP_WCP  */
    CUSTOMFILES_WCP = 542,         /* CUSTOMFILES_WCP  */
    SHOWMODE_WCP = 543,            /* SHOWMODE_WCP  */
    SELECTED_WCP = 544,            /* SELECTED_WCP  */
    REVERSESEARCH_WCP = 545,       /* REVERSESEARCH_WCP  */
    MOUSECONF_WCP = 546,           /* MOUSECONF_WCP  */
    SELECTBUTTON_WCP = 547,        /* SELECTBUTTON_WCP  */
    ACTIVATEBUTTON_WCP = 548,      /* ACTIVATEBUTTON_WCP  */
    SCROLLBUTTON_WCP = 549,        /* SCROLLBUTTON_WCP  */
    SELECTMETHOD_WCP = 550,        /* SELECTMETHOD_WCP  */
    ALTERNATIVE_WCP = 551,         /* ALTERNATIVE_WCP  */
    SEARCHOP_WCP = 552,            /* SEARCHOP_WCP  */
    EDITCOMMAND_WCP = 553,         /* EDITCOMMAND_WCP  */
    SHOWPREVRESULTS_WCP = 554,     /* SHOWPREVRESULTS_WCP  */
    TEXTVIEW_WCP = 555,            /* TEXTVIEW_WCP  */
    TEXTVIEWHIGHLIGHTED_WCP = 556, /* TEXTVIEWHIGHLIGHTED_WCP  */
    TEXTVIEWSELECTION_WCP = 557,   /* TEXTVIEWSELECTION_WCP  */
    DIRBOOKMARKOP_WCP = 558,       /* DIRBOOKMARKOP_WCP  */
    OPENCONTEXTMENUOP_WCP = 559,   /* OPENCONTEXTMENUOP_WCP  */
    RUNCUSTOMACTION_WCP = 560,     /* RUNCUSTOMACTION_WCP  */
    CUSTOMNAME_WCP = 561,          /* CUSTOMNAME_WCP  */
    CONTEXTBUTTON_WCP = 562,       /* CONTEXTBUTTON_WCP  */
    ACTIVATEMOD_WCP = 563,         /* ACTIVATEMOD_WCP  */
    SCROLLMOD_WCP = 564,           /* SCROLLMOD_WCP  */
    CONTEXTMOD_WCP = 565,          /* CONTEXTMOD_WCP  */
    NONE_WCP = 566,                /* NONE_WCP  */
    CUSTOMACTION_WCP = 567,        /* CUSTOMACTION_WCP  */
    SAVE_WORKER_STATE_ON_EXIT_WCP = 568, /* SAVE_WORKER_STATE_ON_EXIT_WCP  */
    OPENWORKERMENUOP_WCP = 569,    /* OPENWORKERMENUOP_WCP  */
    CHANGELABELOP_WCP = 570,       /* CHANGELABELOP_WCP  */
    ASKFORLABEL_WCP = 571,         /* ASKFORLABEL_WCP  */
    LABELCOLORS_WCP = 572,         /* LABELCOLORS_WCP  */
    BOOKMARKLABEL_WCP = 573,       /* BOOKMARKLABEL_WCP  */
    BOOKMARKFILTER_WCP = 574,      /* BOOKMARKFILTER_WCP  */
    SHOWONLYBOOKMARKS_WCP = 575,   /* SHOWONLYBOOKMARKS_WCP  */
    SHOWONLYLABEL_WCP = 576,       /* SHOWONLYLABEL_WCP  */
    SHOWALL_WCP = 577,             /* SHOWALL_WCP  */
    OPTIONMODE_WCP = 578,          /* OPTIONMODE_WCP  */
    INVERT_WCP = 579,              /* INVERT_WCP  */
    SET_WCP = 580,                 /* SET_WCP  */
    CHANGEFILTERS_WCP = 581,       /* CHANGEFILTERS_WCP  */
    CHANGEBOOKMARKS_WCP = 582,     /* CHANGEBOOKMARKS_WCP  */
    QUERYLABEL_WCP = 583,          /* QUERYLABEL_WCP  */
    MODIFYTABSOP_WCP = 584,        /* MODIFYTABSOP_WCP  */
    TABACTION_WCP = 585,           /* TABACTION_WCP  */
    NEWTAB_WCP = 586,              /* NEWTAB_WCP  */
    CLOSECURRENTTAB_WCP = 587,     /* CLOSECURRENTTAB_WCP  */
    NEXTTAB_WCP = 588,             /* NEXTTAB_WCP  */
    PREVTAB_WCP = 589,             /* PREVTAB_WCP  */
    TOGGLELOCKSTATE_WCP = 590,     /* TOGGLELOCKSTATE_WCP  */
    MOVETABLEFT_WCP = 591,         /* MOVETABLEFT_WCP  */
    MOVETABRIGHT_WCP = 592,        /* MOVETABRIGHT_WCP  */
    CHANGELAYOUTOP_WCP = 593,      /* CHANGELAYOUTOP_WCP  */
    INFIXSEARCH_WCP = 594,         /* INFIXSEARCH_WCP  */
    VOLUMEMANAGEROP_WCP = 595,     /* VOLUMEMANAGEROP_WCP  */
    ERROR = 596,                   /* ERROR  */
    ACTIVESIDE_WCP = 597,          /* ACTIVESIDE_WCP  */
    SHOWFREESPACE_WCP = 598,       /* SHOWFREESPACE_WCP  */
    ACTIVEMODE_WCP = 599,          /* ACTIVEMODE_WCP  */
    ASK_WCP = 600,                 /* ASK_WCP  */
    SSHALLOW_WCP = 601,            /* SSHALLOW_WCP  */
    FIELD_WIDTH_WCP = 602,         /* FIELD_WIDTH_WCP  */
    QUICKSEARCHENABLED_WCP = 603,  /* QUICKSEARCHENABLED_WCP  */
    FILTEREDSEARCHENABLED_WCP = 604, /* FILTEREDSEARCHENABLED_WCP  */
    XFTFONTS_WCP = 605,            /* XFTFONTS_WCP  */
    USEMAGIC_WCP = 606,            /* USEMAGIC_WCP  */
    MAGICPATTERN_WCP = 607,        /* MAGICPATTERN_WCP  */
    MAGICCOMPRESSED_WCP = 608,     /* MAGICCOMPRESSED_WCP  */
    MAGICMIME_WCP = 609,           /* MAGICMIME_WCP  */
    VOLUMEMANAGER_WCP = 610,       /* VOLUMEMANAGER_WCP  */
    MOUNTCOMMAND_WCP = 611,        /* MOUNTCOMMAND_WCP  */
    UNMOUNTCOMMAND_WCP = 612,      /* UNMOUNTCOMMAND_WCP  */
    FSTABFILE_WCP = 613,           /* FSTABFILE_WCP  */
    MTABFILE_WCP = 614,            /* MTABFILE_WCP  */
    PARTFILE_WCP = 615,            /* PARTFILE_WCP  */
    REQUESTACTION_WCP = 616,       /* REQUESTACTION_WCP  */
    SIZEH_WCP = 617,               /* SIZEH_WCP  */
    NEXTAGING_WCP = 618,           /* NEXTAGING_WCP  */
    ENTRY_WCP = 619,               /* ENTRY_WCP  */
    PREFIX_WCP = 620,              /* PREFIX_WCP  */
    VALUE_WCP = 621,               /* VALUE_WCP  */
    EXTENSION_WCP = 622,           /* EXTENSION_WCP  */
    EJECTCOMMAND_WCP = 623,        /* EJECTCOMMAND_WCP  */
    CLOSETRAYCOMMAND_WCP = 624,    /* CLOSETRAYCOMMAND_WCP  */
    AVFSMODULE_WCP = 625,          /* AVFSMODULE_WCP  */
    FLEXIBLEMATCH_WCP = 626,       /* FLEXIBLEMATCH_WCP  */
    USE_VERSION_STRING_COMPARE_WCP = 627, /* USE_VERSION_STRING_COMPARE_WCP  */
    SWITCHBUTTONBANKOP_WCP = 628,  /* SWITCHBUTTONBANKOP_WCP  */
    SWITCHTONEXTBANK_WCP = 629,    /* SWITCHTONEXTBANK_WCP  */
    SWITCHTOPREVBANK_WCP = 630,    /* SWITCHTOPREVBANK_WCP  */
    SWITCHTOBANKNR_WCP = 631,      /* SWITCHTOBANKNR_WCP  */
    BANKNR_WCP = 632,              /* BANKNR_WCP  */
    USE_VIRTUAL_TEMP_COPIES_WCP = 633, /* USE_VIRTUAL_TEMP_COPIES_WCP  */
    PATHJUMPOP_WCP = 634,          /* PATHJUMPOP_WCP  */
    PATHJUMPALLOWDIRS_WCP = 635,   /* PATHJUMPALLOWDIRS_WCP  */
    CLIPBOARDOP_WCP = 636,         /* CLIPBOARDOP_WCP  */
    CLIPBOARDSTRING_WCP = 637,     /* CLIPBOARDSTRING_WCP  */
    COMMANDSTRING_WCP = 638,       /* COMMANDSTRING_WCP  */
    EVALCOMMAND_WCP = 639,         /* EVALCOMMAND_WCP  */
    WATCHMODE_WCP = 640,           /* WATCHMODE_WCP  */
    APPLY_WINDOW_DIALOG_TYPE_WCP = 641, /* APPLY_WINDOW_DIALOG_TYPE_WCP  */
    FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP = 642, /* FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP  */
    COMMANDMENUOP_WCP = 643,       /* COMMANDMENUOP_WCP  */
    SEARCHMODEONKEYPRESS_WCP = 644, /* SEARCHMODEONKEYPRESS_WCP  */
    PATHENTRYONTOP_WCP = 645,      /* PATHENTRYONTOP_WCP  */
    STORE_FILES_ALWAYS_WCP = 646,  /* STORE_FILES_ALWAYS_WCP  */
    PATHJUMPSETS_WCP = 647,        /* PATHJUMPSETS_WCP  */
    SHOWDOTDOT_WCP = 648,          /* SHOWDOTDOT_WCP  */
    SHOWBREADCRUMB_WCP = 649,      /* SHOWBREADCRUMB_WCP  */
    FACES_WCP = 650,               /* FACES_WCP  */
    FACE_WCP = 651,                /* FACE_WCP  */
    ENABLE_INFO_LINE_WCP = 652,    /* ENABLE_INFO_LINE_WCP  */
    INFO_LINE_LUA_MODE_WCP = 653,  /* INFO_LINE_LUA_MODE_WCP  */
    INFO_LINE_CONTENT_WCP = 654,   /* INFO_LINE_CONTENT_WCP  */
    RESTORE_TABS_MODE_WCP = 655,   /* RESTORE_TABS_MODE_WCP  */
    STORE_TABS_MODE_WCP = 656,     /* STORE_TABS_MODE_WCP  */
    AS_EXIT_STATE_WCP = 657,       /* AS_EXIT_STATE_WCP  */
    USE_EXTENDED_REGEX_WCP = 658,  /* USE_EXTENDED_REGEX_WCP  */
    HIGHLIGHT_USER_ACTION_WCP = 659, /* HIGHLIGHT_USER_ACTION_WCP  */
    CHTIMEOP_WCP = 660,            /* CHTIMEOP_WCP  */
    ADJUSTRELATIVESYMLINKS_WCP = 661, /* ADJUSTRELATIVESYMLINKS_WCP  */
    OUTSIDE_WCP = 662,             /* OUTSIDE_WCP  */
    EDIT_WCP = 663,                /* EDIT_WCP  */
    MAKEABSOLUTE_WCP = 664,        /* MAKEABSOLUTE_WCP  */
    MAKERELATIVE_WCP = 665,        /* MAKERELATIVE_WCP  */
    MAGICIGNORECASE_WCP = 666,     /* MAGICIGNORECASE_WCP  */
    ENSURE_FILE_PERMISSIONS_WCP = 667, /* ENSURE_FILE_PERMISSIONS_WCP  */
    USER_RW_WCP = 668,             /* USER_RW_WCP  */
    USER_RW_GROUP_R_WCP = 669,     /* USER_RW_GROUP_R_WCP  */
    USER_RW_ALL_R_WCP = 670,       /* USER_RW_ALL_R_WCP  */
    LEAVE_UNMODIFIED_WCP = 671,    /* LEAVE_UNMODIFIED_WCP  */
    PREFERUDISKSVERSION_WCP = 672, /* PREFERUDISKSVERSION_WCP  */
    CHANGECOLUMNSOP_WCP = 673,     /* CHANGECOLUMNSOP_WCP  */
    PATTERNISCOMMASEPARATED_WCP = 674, /* PATTERNISCOMMASEPARATED_WCP  */
    STRCASECMP_WCP = 675,          /* STRCASECMP_WCP  */
    USE_STRING_COMPARE_MODE_WCP = 676, /* USE_STRING_COMPARE_MODE_WCP  */
    VIEWNEWESTFILESOP_WCP = 677,   /* VIEWNEWESTFILESOP_WCP  */
    SHOWRECENT_WCP = 678,          /* SHOWRECENT_WCP  */
    DIRCOMPAREOP_WCP = 679,        /* DIRCOMPAREOP_WCP  */
    TABPROFILESOP_WCP = 680,       /* TABPROFILESOP_WCP  */
    EXTERNALVDIROP_WCP = 681,      /* EXTERNALVDIROP_WCP  */
    VDIR_PRESERVE_DIR_STRUCTURE_WCP = 682, /* VDIR_PRESERVE_DIR_STRUCTURE_WCP  */
    OPENTABMENUOP_WCP = 683,       /* OPENTABMENUOP_WCP  */
    INITIALTAB_WCP = 684,          /* INITIALTAB_WCP  */
    SHOWBYTIME_WCP = 685,          /* SHOWBYTIME_WCP  */
    SHOWBYFILTER_WCP = 686,        /* SHOWBYFILTER_WCP  */
    SHOWBYPROGRAM_WCP = 687,       /* SHOWBYPROGRAM_WCP  */
    TERMINAL_RETURNS_EARLY_WCP = 688, /* TERMINAL_RETURNS_EARLY_WCP  */
    DIRECTORYPRESETS_WCP = 689,    /* DIRECTORYPRESETS_WCP  */
    DIRECTORYPRESET_WCP = 690,     /* DIRECTORYPRESET_WCP  */
    HELPOP_WCP = 691,              /* HELPOP_WCP  */
    PRIORITY_WCP = 692,            /* PRIORITY_WCP  */
    AUTOFILTER_WCP = 693,          /* AUTOFILTER_WCP  */
    ENABLE_CUSTOM_LVB_LINE_WCP = 694, /* ENABLE_CUSTOM_LVB_LINE_WCP  */
    CUSTOM_LVB_LINE_WCP = 695,     /* CUSTOM_LVB_LINE_WCP  */
    CUSTOM_DIRECTORY_INFO_COMMAND_WCP = 696, /* CUSTOM_DIRECTORY_INFO_COMMAND_WCP  */
    IMMEDIATE_FILTER_APPLY_WCP = 697, /* IMMEDIATE_FILTER_APPLY_WCP  */
    DISABLEBGCHECKPREFIX_WCP = 698, /* DISABLEBGCHECKPREFIX_WCP  */
    DISPLAYMODE_WCP = 699,         /* DISPLAYMODE_WCP  */
    SHOWSUBDIRS_WCP = 700,         /* SHOWSUBDIRS_WCP  */
    SHOWDIRECTSUBDIRS_WCP = 701,   /* SHOWDIRECTSUBDIRS_WCP  */
    INCLUDEALL_WCP = 702,          /* INCLUDEALL_WCP  */
    APPLYMODE_WCP = 703,           /* APPLYMODE_WCP  */
    ADD_WCP = 704,                 /* ADD_WCP  */
    REMOVE_WCP = 705,              /* REMOVE_WCP  */
    HIDENONEXISTING_WCP = 706,     /* HIDENONEXISTING_WCP  */
    VIEWCOMMANDLOGOP_WCP = 707,    /* VIEWCOMMANDLOGOP_WCP  */
    LOCKONCURRENTDIR_WCP = 708,    /* LOCKONCURRENTDIR_WCP  */
    STARTNODE_WCP = 709,           /* STARTNODE_WCP  */
    MENUS_WCP = 710,               /* MENUS_WCP  */
    LVMODES_WCP = 711,             /* LVMODES_WCP  */
    LVCOMMANDS_WCP = 712,          /* LVCOMMANDS_WCP  */
    TOPLEVEL_WCP = 713,            /* TOPLEVEL_WCP  */
    FOLLOWACTIVE_WCP = 714,        /* FOLLOWACTIVE_WCP  */
    WATCHFILE_WCP = 715,           /* WATCHFILE_WCP  */
    ACTIVATETEXTVIEWMODEOP_WCP = 716, /* ACTIVATETEXTVIEWMODEOP_WCP  */
    SOURCEMODE_WCP = 717,          /* SOURCEMODE_WCP  */
    ACTIVEFILE_WCP = 718,          /* ACTIVEFILE_WCP  */
    FILETYPEACTION_WCP = 719,      /* FILETYPEACTION_WCP  */
    IGNOREDIRS2_WCP = 720,         /* IGNOREDIRS2_WCP  */
    IGNOREDIR_WCP = 721,           /* IGNOREDIR_WCP  */
    CUSTOMATTRIBUTE_WCP = 722,     /* CUSTOMATTRIBUTE_WCP  */
    STRING_WCP = 723,              /* STRING_WCP  */
    NUM_WCP = 724                  /* NUM_WCP  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define LEFTBRACE_WCP 258
#define RIGHTBRACE_WCP 259
#define GLOBAL_WCP 260
#define COLORS_WCP 261
#define LANG_WCP 262
#define PALETTE_WCP 263
#define OWNOP_WCP 264
#define LISTERSETS_WCP 265
#define YES_WCP 266
#define NO_WCP 267
#define LEFT_WCP 268
#define RIGHT_WCP 269
#define HBARTOP_WCP 270
#define HBARHEIGHT_WCP 271
#define VBARLEFT_WCP 272
#define VBARWIDTH_WCP 273
#define DISPLAYSETS_WCP 274
#define NAME_WCP 275
#define SIZE_WCP 276
#define TYPE_WCP 277
#define PERMISSION_WCP 278
#define OWNER_WCP 279
#define DESTINATION_WCP 280
#define MODTIME_WCP 281
#define ACCTIME_WCP 282
#define CHGTIME_WCP 283
#define ROWS_WCP 284
#define COLUMNS_WCP 285
#define CACHESIZE_WCP 286
#define OWNERSTYLE_WCP 287
#define TERMINAL_WCP 288
#define USESTRINGFORDIRSIZE_WCP 289
#define TIMESETS_WCP 290
#define STYLE1_WCP 291
#define STYLE2_WCP 292
#define STYLE3_WCP 293
#define DATE_WCP 294
#define TIME_WCP 295
#define DATESUBSTITUTION_WCP 296
#define DATEBEFORETIME_WCP 297
#define STATEBAR_WCP 298
#define SELLVBAR_WCP 299
#define UNSELLVBAR_WCP 300
#define CLOCKBAR_WCP 301
#define REQUESTER_WCP 302
#define UNSELDIR_WCP 303
#define SELDIR_WCP 304
#define UNSELFILE_WCP 305
#define SELFILE_WCP 306
#define UNSELACTDIR_WCP 307
#define SELACTDIR_WCP 308
#define UNSELACTFILE_WCP 309
#define SELACTFILE_WCP 310
#define LVBG_WCP 311
#define STARTUP_WCP 312
#define BUTTON_WCP 313
#define BUTTONS_WCP 314
#define POSITION_WCP 315
#define TITLE_WCP 316
#define COMMANDS_WCP 317
#define SHORTKEYS_WCP 318
#define PATHS_WCP 319
#define PATH_WCP 320
#define FILETYPES_WCP 321
#define FILETYPE_WCP 322
#define NORMAL_WCP 323
#define NOTYETCHECKED_WCP 324
#define UNKNOWN_WCP 325
#define NOSELECT_WCP 326
#define DIR_WCP 327
#define USEPATTERN_WCP 328
#define USECONTENT_WCP 329
#define PATTERN_WCP 330
#define CONTENT_WCP 331
#define FTCOMMANDS_WCP 332
#define DND_WCP 333
#define DC_WCP 334
#define SHOW_WCP 335
#define RAWSHOW_WCP 336
#define USER_WCP 337
#define FLAGS_WCP 338
#define IGNOREDIRS_WCP 339
#define HOTKEYS_WCP 340
#define HOTKEY_WCP 341
#define FONTS_WCP 342
#define GLOBALFONT_WCP 343
#define BUTTONFONT_WCP 344
#define LEFTFONT_WCP 345
#define RIGHTFONT_WCP 346
#define TEXTVIEWFONT_WCP 347
#define STATEBARFONT_WCP 348
#define TEXTVIEWALTFONT_WCP 349
#define CLOCKBARSETS_WCP 350
#define MODUS_WCP 351
#define TIMESPACE_WCP 352
#define VERSION_WCP 353
#define EXTERN_WCP 354
#define UPDATETIME_WCP 355
#define PROGRAM_WCP 356
#define SHOWHINTS_WCP 357
#define KEY_WCP 358
#define DOUBLE_WCP 359
#define MOD_WCP 360
#define CONTROL_WCP 361
#define SHIFT_WCP 362
#define LOCK_WCP 363
#define MOD1_WCP 364
#define MOD2_WCP 365
#define MOD3_WCP 366
#define MOD4_WCP 367
#define MOD5_WCP 368
#define DNDACTION_WCP 369
#define DCACTION_WCP 370
#define SHOWACTION_WCP 371
#define RSHOWACTION_WCP 372
#define USERACTION_WCP 373
#define ROWUP_WCP 374
#define ROWDOWN_WCP 375
#define CHANGEHIDDENFLAG_WCP 376
#define COPYOP_WCP 377
#define FIRSTROW_WCP 378
#define LASTROW_WCP 379
#define PAGEUP_WCP 380
#define PAGEDOWN_WCP 381
#define SELECTOP_WCP 382
#define SELECTALLOP_WCP 383
#define SELECTNONEOP_WCP 384
#define INVERTALLOP_WCP 385
#define PARENTDIROP_WCP 386
#define ENTERDIROP_WCP 387
#define CHANGELISTERSETOP_WCP 388
#define SWITCHLISTEROP_WCP 389
#define FILTERSELECTOP_WCP 390
#define FILTERUNSELECTOP_WCP 391
#define PATHTOOTHERSIDEOP_WCP 392
#define QUITOP_WCP 393
#define DELETEOP_WCP 394
#define RELOADOP_WCP 395
#define MAKEDIROP_WCP 396
#define RENAMEOP_WCP 397
#define DIRSIZEOP_WCP 398
#define SIMDDOP_WCP 399
#define STARTPROGOP_WCP 400
#define SEARCHENTRYOP_WCP 401
#define ENTERPATHOP_WCP 402
#define SCROLLLISTEROP_WCP 403
#define CREATESYMLINKOP_WCP 404
#define CHANGESYMLINKOP_WCP 405
#define CHMODOP_WCP 406
#define TOGGLELISTERMODEOP_WCP 407
#define SETSORTMODEOP_WCP 408
#define SETFILTEROP_WCP 409
#define SHORTKEYFROMLISTOP_WCP 410
#define CHOWNOP_WCP 411
#define WORKERCONFIG_WCP 412
#define USERSTYLE_WCP 413
#define DATESTRING_WCP 414
#define TIMESTRING_WCP 415
#define COLOR_WCP 416
#define PATTERNIGNORECASE_WCP 417
#define PATTERNUSEREGEXP_WCP 418
#define PATTERNUSEFULLNAME_WCP 419
#define COM_WCP 420
#define SEPARATEEACHENTRY_WCP 421
#define RECURSIVE_WCP 422
#define START_WCP 423
#define TERMINALWAIT_WCP 424
#define SHOWOUTPUT_WCP 425
#define VIEWSTR_WCP 426
#define SHOWOUTPUTINT_WCP 427
#define SHOWOUTPUTOTHERSIDE_WCP 428
#define SHOWOUTPUTCUSTOMATTRIBUTE_WCP 429
#define INBACKGROUND_WCP 430
#define TAKEDIRS_WCP 431
#define ACTIONNUMBER_WCP 432
#define HIDDENFILES_WCP 433
#define HIDE_WCP 434
#define TOGGLE_WCP 435
#define FOLLOWSYMLINKS_WCP 436
#define MOVE_WCP 437
#define RENAME_WCP 438
#define SAMEDIR_WCP 439
#define REQUESTDEST_WCP 440
#define REQUESTFLAGS_WCP 441
#define OVERWRITE_WCP 442
#define ALWAYS_WCP 443
#define NEVER_WCP 444
#define COPYMODE_WCP 445
#define FAST_WCP 446
#define PRESERVEATTR_WCP 447
#define MODE_WCP 448
#define ACTIVE_WCP 449
#define ACTIVE2OTHER_WCP 450
#define SPECIAL_WCP 451
#define REQUEST_WCP 452
#define CURRENT_WCP 453
#define OTHER_WCP 454
#define FILTER_WCP 455
#define QUICK_WCP 456
#define ALSOACTIVE_WCP 457
#define RESETDIRSIZES_WCP 458
#define KEEPFILETYPES_WCP 459
#define RELATIVE_WCP 460
#define ONFILES_WCP 461
#define ONDIRS_WCP 462
#define SORTBY_WCP 463
#define SORTFLAG_WCP 464
#define REVERSE_WCP 465
#define DIRLAST_WCP 466
#define DIRMIXED_WCP 467
#define EXCLUDE_WCP 468
#define INCLUDE_WCP 469
#define UNSET_WCP 470
#define UNSETALL_WCP 471
#define SCRIPTOP_WCP 472
#define NOP_WCP 473
#define PUSH_WCP 474
#define LABEL_WCP 475
#define IF_WCP 476
#define END_WCP 477
#define POP_WCP 478
#define SETTINGS_WCP 479
#define WINDOW_WCP 480
#define GOTO_WCP 481
#define PUSHUSEOUTPUT_WCP 482
#define DODEBUG_WCP 483
#define WPURECURSIVE_WCP 484
#define WPUTAKEDIRS_WCP 485
#define STACKNR_WCP 486
#define PUSHSTRING_WCP 487
#define PUSHOUTPUTRETURNCODE_WCP 488
#define IFTEST_WCP 489
#define IFLABEL_WCP 490
#define WINTYPE_WCP 491
#define OPEN_WCP 492
#define CLOSE_WCP 493
#define LEAVE_WCP 494
#define CHANGEPROGRESS_WCP 495
#define CHANGETEXT_WCP 496
#define PROGRESSUSEOUTPUT_WCP 497
#define WINTEXTUSEOUTPUT_WCP 498
#define PROGRESS_WCP 499
#define WINTEXT_WCP 500
#define SHOWDIRCACHEOP_WCP 501
#define INODE_WCP 502
#define NLINK_WCP 503
#define BLOCKS_WCP 504
#define SHOWHEADER_WCP 505
#define LVHEADER_WCP 506
#define IGNORECASE_WCP 507
#define LISTVIEWS_WCP 508
#define BLL_WCP 509
#define LBL_WCP 510
#define LLB_WCP 511
#define BL_WCP 512
#define LB_WCP 513
#define LAYOUT_WCP 514
#define BUTTONSVERT_WCP 515
#define LISTVIEWSVERT_WCP 516
#define LISTVIEWWEIGHT_WCP 517
#define WEIGHTTOACTIVE_WCP 518
#define EXTCOND_WCP 519
#define USEEXTCOND_WCP 520
#define SUBTYPE_WCP 521
#define PARENTACTIONOP_WCP 522
#define NOOPERATIONOP_WCP 523
#define COLORMODE_WCP 524
#define DEFAULT_WCP 525
#define CUSTOM_WCP 526
#define UNSELECTCOLOR_WCP 527
#define SELECTCOLOR_WCP 528
#define UNSELECTACTIVECOLOR_WCP 529
#define SELECTACTIVECOLOR_WCP 530
#define COLOREXTERNPROG_WCP 531
#define PARENT_WCP 532
#define DONTCD_WCP 533
#define DONTCHECKVIRTUAL_WCP 534
#define GOFTPOP_WCP 535
#define HOSTNAME_WCP 536
#define USERNAME_WCP 537
#define PASSWORD_WCP 538
#define DONTENTERFTP_WCP 539
#define ALWAYSSTOREPW_WCP 540
#define INTERNALVIEWOP_WCP 541
#define CUSTOMFILES_WCP 542
#define SHOWMODE_WCP 543
#define SELECTED_WCP 544
#define REVERSESEARCH_WCP 545
#define MOUSECONF_WCP 546
#define SELECTBUTTON_WCP 547
#define ACTIVATEBUTTON_WCP 548
#define SCROLLBUTTON_WCP 549
#define SELECTMETHOD_WCP 550
#define ALTERNATIVE_WCP 551
#define SEARCHOP_WCP 552
#define EDITCOMMAND_WCP 553
#define SHOWPREVRESULTS_WCP 554
#define TEXTVIEW_WCP 555
#define TEXTVIEWHIGHLIGHTED_WCP 556
#define TEXTVIEWSELECTION_WCP 557
#define DIRBOOKMARKOP_WCP 558
#define OPENCONTEXTMENUOP_WCP 559
#define RUNCUSTOMACTION_WCP 560
#define CUSTOMNAME_WCP 561
#define CONTEXTBUTTON_WCP 562
#define ACTIVATEMOD_WCP 563
#define SCROLLMOD_WCP 564
#define CONTEXTMOD_WCP 565
#define NONE_WCP 566
#define CUSTOMACTION_WCP 567
#define SAVE_WORKER_STATE_ON_EXIT_WCP 568
#define OPENWORKERMENUOP_WCP 569
#define CHANGELABELOP_WCP 570
#define ASKFORLABEL_WCP 571
#define LABELCOLORS_WCP 572
#define BOOKMARKLABEL_WCP 573
#define BOOKMARKFILTER_WCP 574
#define SHOWONLYBOOKMARKS_WCP 575
#define SHOWONLYLABEL_WCP 576
#define SHOWALL_WCP 577
#define OPTIONMODE_WCP 578
#define INVERT_WCP 579
#define SET_WCP 580
#define CHANGEFILTERS_WCP 581
#define CHANGEBOOKMARKS_WCP 582
#define QUERYLABEL_WCP 583
#define MODIFYTABSOP_WCP 584
#define TABACTION_WCP 585
#define NEWTAB_WCP 586
#define CLOSECURRENTTAB_WCP 587
#define NEXTTAB_WCP 588
#define PREVTAB_WCP 589
#define TOGGLELOCKSTATE_WCP 590
#define MOVETABLEFT_WCP 591
#define MOVETABRIGHT_WCP 592
#define CHANGELAYOUTOP_WCP 593
#define INFIXSEARCH_WCP 594
#define VOLUMEMANAGEROP_WCP 595
#define ERROR 596
#define ACTIVESIDE_WCP 597
#define SHOWFREESPACE_WCP 598
#define ACTIVEMODE_WCP 599
#define ASK_WCP 600
#define SSHALLOW_WCP 601
#define FIELD_WIDTH_WCP 602
#define QUICKSEARCHENABLED_WCP 603
#define FILTEREDSEARCHENABLED_WCP 604
#define XFTFONTS_WCP 605
#define USEMAGIC_WCP 606
#define MAGICPATTERN_WCP 607
#define MAGICCOMPRESSED_WCP 608
#define MAGICMIME_WCP 609
#define VOLUMEMANAGER_WCP 610
#define MOUNTCOMMAND_WCP 611
#define UNMOUNTCOMMAND_WCP 612
#define FSTABFILE_WCP 613
#define MTABFILE_WCP 614
#define PARTFILE_WCP 615
#define REQUESTACTION_WCP 616
#define SIZEH_WCP 617
#define NEXTAGING_WCP 618
#define ENTRY_WCP 619
#define PREFIX_WCP 620
#define VALUE_WCP 621
#define EXTENSION_WCP 622
#define EJECTCOMMAND_WCP 623
#define CLOSETRAYCOMMAND_WCP 624
#define AVFSMODULE_WCP 625
#define FLEXIBLEMATCH_WCP 626
#define USE_VERSION_STRING_COMPARE_WCP 627
#define SWITCHBUTTONBANKOP_WCP 628
#define SWITCHTONEXTBANK_WCP 629
#define SWITCHTOPREVBANK_WCP 630
#define SWITCHTOBANKNR_WCP 631
#define BANKNR_WCP 632
#define USE_VIRTUAL_TEMP_COPIES_WCP 633
#define PATHJUMPOP_WCP 634
#define PATHJUMPALLOWDIRS_WCP 635
#define CLIPBOARDOP_WCP 636
#define CLIPBOARDSTRING_WCP 637
#define COMMANDSTRING_WCP 638
#define EVALCOMMAND_WCP 639
#define WATCHMODE_WCP 640
#define APPLY_WINDOW_DIALOG_TYPE_WCP 641
#define FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP 642
#define COMMANDMENUOP_WCP 643
#define SEARCHMODEONKEYPRESS_WCP 644
#define PATHENTRYONTOP_WCP 645
#define STORE_FILES_ALWAYS_WCP 646
#define PATHJUMPSETS_WCP 647
#define SHOWDOTDOT_WCP 648
#define SHOWBREADCRUMB_WCP 649
#define FACES_WCP 650
#define FACE_WCP 651
#define ENABLE_INFO_LINE_WCP 652
#define INFO_LINE_LUA_MODE_WCP 653
#define INFO_LINE_CONTENT_WCP 654
#define RESTORE_TABS_MODE_WCP 655
#define STORE_TABS_MODE_WCP 656
#define AS_EXIT_STATE_WCP 657
#define USE_EXTENDED_REGEX_WCP 658
#define HIGHLIGHT_USER_ACTION_WCP 659
#define CHTIMEOP_WCP 660
#define ADJUSTRELATIVESYMLINKS_WCP 661
#define OUTSIDE_WCP 662
#define EDIT_WCP 663
#define MAKEABSOLUTE_WCP 664
#define MAKERELATIVE_WCP 665
#define MAGICIGNORECASE_WCP 666
#define ENSURE_FILE_PERMISSIONS_WCP 667
#define USER_RW_WCP 668
#define USER_RW_GROUP_R_WCP 669
#define USER_RW_ALL_R_WCP 670
#define LEAVE_UNMODIFIED_WCP 671
#define PREFERUDISKSVERSION_WCP 672
#define CHANGECOLUMNSOP_WCP 673
#define PATTERNISCOMMASEPARATED_WCP 674
#define STRCASECMP_WCP 675
#define USE_STRING_COMPARE_MODE_WCP 676
#define VIEWNEWESTFILESOP_WCP 677
#define SHOWRECENT_WCP 678
#define DIRCOMPAREOP_WCP 679
#define TABPROFILESOP_WCP 680
#define EXTERNALVDIROP_WCP 681
#define VDIR_PRESERVE_DIR_STRUCTURE_WCP 682
#define OPENTABMENUOP_WCP 683
#define INITIALTAB_WCP 684
#define SHOWBYTIME_WCP 685
#define SHOWBYFILTER_WCP 686
#define SHOWBYPROGRAM_WCP 687
#define TERMINAL_RETURNS_EARLY_WCP 688
#define DIRECTORYPRESETS_WCP 689
#define DIRECTORYPRESET_WCP 690
#define HELPOP_WCP 691
#define PRIORITY_WCP 692
#define AUTOFILTER_WCP 693
#define ENABLE_CUSTOM_LVB_LINE_WCP 694
#define CUSTOM_LVB_LINE_WCP 695
#define CUSTOM_DIRECTORY_INFO_COMMAND_WCP 696
#define IMMEDIATE_FILTER_APPLY_WCP 697
#define DISABLEBGCHECKPREFIX_WCP 698
#define DISPLAYMODE_WCP 699
#define SHOWSUBDIRS_WCP 700
#define SHOWDIRECTSUBDIRS_WCP 701
#define INCLUDEALL_WCP 702
#define APPLYMODE_WCP 703
#define ADD_WCP 704
#define REMOVE_WCP 705
#define HIDENONEXISTING_WCP 706
#define VIEWCOMMANDLOGOP_WCP 707
#define LOCKONCURRENTDIR_WCP 708
#define STARTNODE_WCP 709
#define MENUS_WCP 710
#define LVMODES_WCP 711
#define LVCOMMANDS_WCP 712
#define TOPLEVEL_WCP 713
#define FOLLOWACTIVE_WCP 714
#define WATCHFILE_WCP 715
#define ACTIVATETEXTVIEWMODEOP_WCP 716
#define SOURCEMODE_WCP 717
#define ACTIVEFILE_WCP 718
#define FILETYPEACTION_WCP 719
#define IGNOREDIRS2_WCP 720
#define IGNOREDIR_WCP 721
#define CUSTOMATTRIBUTE_WCP 722
#define STRING_WCP 723
#define NUM_WCP 724

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 205 "configparser.yy"

  int num;
  char *strptr;

#line 1271 "configparser.cc"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_CONFIGPARSER_HH_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_LEFTBRACE_WCP = 3,              /* LEFTBRACE_WCP  */
  YYSYMBOL_RIGHTBRACE_WCP = 4,             /* RIGHTBRACE_WCP  */
  YYSYMBOL_GLOBAL_WCP = 5,                 /* GLOBAL_WCP  */
  YYSYMBOL_COLORS_WCP = 6,                 /* COLORS_WCP  */
  YYSYMBOL_LANG_WCP = 7,                   /* LANG_WCP  */
  YYSYMBOL_PALETTE_WCP = 8,                /* PALETTE_WCP  */
  YYSYMBOL_OWNOP_WCP = 9,                  /* OWNOP_WCP  */
  YYSYMBOL_LISTERSETS_WCP = 10,            /* LISTERSETS_WCP  */
  YYSYMBOL_YES_WCP = 11,                   /* YES_WCP  */
  YYSYMBOL_NO_WCP = 12,                    /* NO_WCP  */
  YYSYMBOL_LEFT_WCP = 13,                  /* LEFT_WCP  */
  YYSYMBOL_RIGHT_WCP = 14,                 /* RIGHT_WCP  */
  YYSYMBOL_HBARTOP_WCP = 15,               /* HBARTOP_WCP  */
  YYSYMBOL_HBARHEIGHT_WCP = 16,            /* HBARHEIGHT_WCP  */
  YYSYMBOL_VBARLEFT_WCP = 17,              /* VBARLEFT_WCP  */
  YYSYMBOL_VBARWIDTH_WCP = 18,             /* VBARWIDTH_WCP  */
  YYSYMBOL_DISPLAYSETS_WCP = 19,           /* DISPLAYSETS_WCP  */
  YYSYMBOL_NAME_WCP = 20,                  /* NAME_WCP  */
  YYSYMBOL_SIZE_WCP = 21,                  /* SIZE_WCP  */
  YYSYMBOL_TYPE_WCP = 22,                  /* TYPE_WCP  */
  YYSYMBOL_PERMISSION_WCP = 23,            /* PERMISSION_WCP  */
  YYSYMBOL_OWNER_WCP = 24,                 /* OWNER_WCP  */
  YYSYMBOL_DESTINATION_WCP = 25,           /* DESTINATION_WCP  */
  YYSYMBOL_MODTIME_WCP = 26,               /* MODTIME_WCP  */
  YYSYMBOL_ACCTIME_WCP = 27,               /* ACCTIME_WCP  */
  YYSYMBOL_CHGTIME_WCP = 28,               /* CHGTIME_WCP  */
  YYSYMBOL_ROWS_WCP = 29,                  /* ROWS_WCP  */
  YYSYMBOL_COLUMNS_WCP = 30,               /* COLUMNS_WCP  */
  YYSYMBOL_CACHESIZE_WCP = 31,             /* CACHESIZE_WCP  */
  YYSYMBOL_OWNERSTYLE_WCP = 32,            /* OWNERSTYLE_WCP  */
  YYSYMBOL_TERMINAL_WCP = 33,              /* TERMINAL_WCP  */
  YYSYMBOL_USESTRINGFORDIRSIZE_WCP = 34,   /* USESTRINGFORDIRSIZE_WCP  */
  YYSYMBOL_TIMESETS_WCP = 35,              /* TIMESETS_WCP  */
  YYSYMBOL_STYLE1_WCP = 36,                /* STYLE1_WCP  */
  YYSYMBOL_STYLE2_WCP = 37,                /* STYLE2_WCP  */
  YYSYMBOL_STYLE3_WCP = 38,                /* STYLE3_WCP  */
  YYSYMBOL_DATE_WCP = 39,                  /* DATE_WCP  */
  YYSYMBOL_TIME_WCP = 40,                  /* TIME_WCP  */
  YYSYMBOL_DATESUBSTITUTION_WCP = 41,      /* DATESUBSTITUTION_WCP  */
  YYSYMBOL_DATEBEFORETIME_WCP = 42,        /* DATEBEFORETIME_WCP  */
  YYSYMBOL_STATEBAR_WCP = 43,              /* STATEBAR_WCP  */
  YYSYMBOL_SELLVBAR_WCP = 44,              /* SELLVBAR_WCP  */
  YYSYMBOL_UNSELLVBAR_WCP = 45,            /* UNSELLVBAR_WCP  */
  YYSYMBOL_CLOCKBAR_WCP = 46,              /* CLOCKBAR_WCP  */
  YYSYMBOL_REQUESTER_WCP = 47,             /* REQUESTER_WCP  */
  YYSYMBOL_UNSELDIR_WCP = 48,              /* UNSELDIR_WCP  */
  YYSYMBOL_SELDIR_WCP = 49,                /* SELDIR_WCP  */
  YYSYMBOL_UNSELFILE_WCP = 50,             /* UNSELFILE_WCP  */
  YYSYMBOL_SELFILE_WCP = 51,               /* SELFILE_WCP  */
  YYSYMBOL_UNSELACTDIR_WCP = 52,           /* UNSELACTDIR_WCP  */
  YYSYMBOL_SELACTDIR_WCP = 53,             /* SELACTDIR_WCP  */
  YYSYMBOL_UNSELACTFILE_WCP = 54,          /* UNSELACTFILE_WCP  */
  YYSYMBOL_SELACTFILE_WCP = 55,            /* SELACTFILE_WCP  */
  YYSYMBOL_LVBG_WCP = 56,                  /* LVBG_WCP  */
  YYSYMBOL_STARTUP_WCP = 57,               /* STARTUP_WCP  */
  YYSYMBOL_BUTTON_WCP = 58,                /* BUTTON_WCP  */
  YYSYMBOL_BUTTONS_WCP = 59,               /* BUTTONS_WCP  */
  YYSYMBOL_POSITION_WCP = 60,              /* POSITION_WCP  */
  YYSYMBOL_TITLE_WCP = 61,                 /* TITLE_WCP  */
  YYSYMBOL_COMMANDS_WCP = 62,              /* COMMANDS_WCP  */
  YYSYMBOL_SHORTKEYS_WCP = 63,             /* SHORTKEYS_WCP  */
  YYSYMBOL_PATHS_WCP = 64,                 /* PATHS_WCP  */
  YYSYMBOL_PATH_WCP = 65,                  /* PATH_WCP  */
  YYSYMBOL_FILETYPES_WCP = 66,             /* FILETYPES_WCP  */
  YYSYMBOL_FILETYPE_WCP = 67,              /* FILETYPE_WCP  */
  YYSYMBOL_NORMAL_WCP = 68,                /* NORMAL_WCP  */
  YYSYMBOL_NOTYETCHECKED_WCP = 69,         /* NOTYETCHECKED_WCP  */
  YYSYMBOL_UNKNOWN_WCP = 70,               /* UNKNOWN_WCP  */
  YYSYMBOL_NOSELECT_WCP = 71,              /* NOSELECT_WCP  */
  YYSYMBOL_DIR_WCP = 72,                   /* DIR_WCP  */
  YYSYMBOL_USEPATTERN_WCP = 73,            /* USEPATTERN_WCP  */
  YYSYMBOL_USECONTENT_WCP = 74,            /* USECONTENT_WCP  */
  YYSYMBOL_PATTERN_WCP = 75,               /* PATTERN_WCP  */
  YYSYMBOL_CONTENT_WCP = 76,               /* CONTENT_WCP  */
  YYSYMBOL_FTCOMMANDS_WCP = 77,            /* FTCOMMANDS_WCP  */
  YYSYMBOL_DND_WCP = 78,                   /* DND_WCP  */
  YYSYMBOL_DC_WCP = 79,                    /* DC_WCP  */
  YYSYMBOL_SHOW_WCP = 80,                  /* SHOW_WCP  */
  YYSYMBOL_RAWSHOW_WCP = 81,               /* RAWSHOW_WCP  */
  YYSYMBOL_USER_WCP = 82,                  /* USER_WCP  */
  YYSYMBOL_FLAGS_WCP = 83,                 /* FLAGS_WCP  */
  YYSYMBOL_IGNOREDIRS_WCP = 84,            /* IGNOREDIRS_WCP  */
  YYSYMBOL_HOTKEYS_WCP = 85,               /* HOTKEYS_WCP  */
  YYSYMBOL_HOTKEY_WCP = 86,                /* HOTKEY_WCP  */
  YYSYMBOL_FONTS_WCP = 87,                 /* FONTS_WCP  */
  YYSYMBOL_GLOBALFONT_WCP = 88,            /* GLOBALFONT_WCP  */
  YYSYMBOL_BUTTONFONT_WCP = 89,            /* BUTTONFONT_WCP  */
  YYSYMBOL_LEFTFONT_WCP = 90,              /* LEFTFONT_WCP  */
  YYSYMBOL_RIGHTFONT_WCP = 91,             /* RIGHTFONT_WCP  */
  YYSYMBOL_TEXTVIEWFONT_WCP = 92,          /* TEXTVIEWFONT_WCP  */
  YYSYMBOL_STATEBARFONT_WCP = 93,          /* STATEBARFONT_WCP  */
  YYSYMBOL_TEXTVIEWALTFONT_WCP = 94,       /* TEXTVIEWALTFONT_WCP  */
  YYSYMBOL_CLOCKBARSETS_WCP = 95,          /* CLOCKBARSETS_WCP  */
  YYSYMBOL_MODUS_WCP = 96,                 /* MODUS_WCP  */
  YYSYMBOL_TIMESPACE_WCP = 97,             /* TIMESPACE_WCP  */
  YYSYMBOL_VERSION_WCP = 98,               /* VERSION_WCP  */
  YYSYMBOL_EXTERN_WCP = 99,                /* EXTERN_WCP  */
  YYSYMBOL_UPDATETIME_WCP = 100,           /* UPDATETIME_WCP  */
  YYSYMBOL_PROGRAM_WCP = 101,              /* PROGRAM_WCP  */
  YYSYMBOL_SHOWHINTS_WCP = 102,            /* SHOWHINTS_WCP  */
  YYSYMBOL_KEY_WCP = 103,                  /* KEY_WCP  */
  YYSYMBOL_DOUBLE_WCP = 104,               /* DOUBLE_WCP  */
  YYSYMBOL_MOD_WCP = 105,                  /* MOD_WCP  */
  YYSYMBOL_CONTROL_WCP = 106,              /* CONTROL_WCP  */
  YYSYMBOL_SHIFT_WCP = 107,                /* SHIFT_WCP  */
  YYSYMBOL_LOCK_WCP = 108,                 /* LOCK_WCP  */
  YYSYMBOL_MOD1_WCP = 109,                 /* MOD1_WCP  */
  YYSYMBOL_MOD2_WCP = 110,                 /* MOD2_WCP  */
  YYSYMBOL_MOD3_WCP = 111,                 /* MOD3_WCP  */
  YYSYMBOL_MOD4_WCP = 112,                 /* MOD4_WCP  */
  YYSYMBOL_MOD5_WCP = 113,                 /* MOD5_WCP  */
  YYSYMBOL_DNDACTION_WCP = 114,            /* DNDACTION_WCP  */
  YYSYMBOL_DCACTION_WCP = 115,             /* DCACTION_WCP  */
  YYSYMBOL_SHOWACTION_WCP = 116,           /* SHOWACTION_WCP  */
  YYSYMBOL_RSHOWACTION_WCP = 117,          /* RSHOWACTION_WCP  */
  YYSYMBOL_USERACTION_WCP = 118,           /* USERACTION_WCP  */
  YYSYMBOL_ROWUP_WCP = 119,                /* ROWUP_WCP  */
  YYSYMBOL_ROWDOWN_WCP = 120,              /* ROWDOWN_WCP  */
  YYSYMBOL_CHANGEHIDDENFLAG_WCP = 121,     /* CHANGEHIDDENFLAG_WCP  */
  YYSYMBOL_COPYOP_WCP = 122,               /* COPYOP_WCP  */
  YYSYMBOL_FIRSTROW_WCP = 123,             /* FIRSTROW_WCP  */
  YYSYMBOL_LASTROW_WCP = 124,              /* LASTROW_WCP  */
  YYSYMBOL_PAGEUP_WCP = 125,               /* PAGEUP_WCP  */
  YYSYMBOL_PAGEDOWN_WCP = 126,             /* PAGEDOWN_WCP  */
  YYSYMBOL_SELECTOP_WCP = 127,             /* SELECTOP_WCP  */
  YYSYMBOL_SELECTALLOP_WCP = 128,          /* SELECTALLOP_WCP  */
  YYSYMBOL_SELECTNONEOP_WCP = 129,         /* SELECTNONEOP_WCP  */
  YYSYMBOL_INVERTALLOP_WCP = 130,          /* INVERTALLOP_WCP  */
  YYSYMBOL_PARENTDIROP_WCP = 131,          /* PARENTDIROP_WCP  */
  YYSYMBOL_ENTERDIROP_WCP = 132,           /* ENTERDIROP_WCP  */
  YYSYMBOL_CHANGELISTERSETOP_WCP = 133,    /* CHANGELISTERSETOP_WCP  */
  YYSYMBOL_SWITCHLISTEROP_WCP = 134,       /* SWITCHLISTEROP_WCP  */
  YYSYMBOL_FILTERSELECTOP_WCP = 135,       /* FILTERSELECTOP_WCP  */
  YYSYMBOL_FILTERUNSELECTOP_WCP = 136,     /* FILTERUNSELECTOP_WCP  */
  YYSYMBOL_PATHTOOTHERSIDEOP_WCP = 137,    /* PATHTOOTHERSIDEOP_WCP  */
  YYSYMBOL_QUITOP_WCP = 138,               /* QUITOP_WCP  */
  YYSYMBOL_DELETEOP_WCP = 139,             /* DELETEOP_WCP  */
  YYSYMBOL_RELOADOP_WCP = 140,             /* RELOADOP_WCP  */
  YYSYMBOL_MAKEDIROP_WCP = 141,            /* MAKEDIROP_WCP  */
  YYSYMBOL_RENAMEOP_WCP = 142,             /* RENAMEOP_WCP  */
  YYSYMBOL_DIRSIZEOP_WCP = 143,            /* DIRSIZEOP_WCP  */
  YYSYMBOL_SIMDDOP_WCP = 144,              /* SIMDDOP_WCP  */
  YYSYMBOL_STARTPROGOP_WCP = 145,          /* STARTPROGOP_WCP  */
  YYSYMBOL_SEARCHENTRYOP_WCP = 146,        /* SEARCHENTRYOP_WCP  */
  YYSYMBOL_ENTERPATHOP_WCP = 147,          /* ENTERPATHOP_WCP  */
  YYSYMBOL_SCROLLLISTEROP_WCP = 148,       /* SCROLLLISTEROP_WCP  */
  YYSYMBOL_CREATESYMLINKOP_WCP = 149,      /* CREATESYMLINKOP_WCP  */
  YYSYMBOL_CHANGESYMLINKOP_WCP = 150,      /* CHANGESYMLINKOP_WCP  */
  YYSYMBOL_CHMODOP_WCP = 151,              /* CHMODOP_WCP  */
  YYSYMBOL_TOGGLELISTERMODEOP_WCP = 152,   /* TOGGLELISTERMODEOP_WCP  */
  YYSYMBOL_SETSORTMODEOP_WCP = 153,        /* SETSORTMODEOP_WCP  */
  YYSYMBOL_SETFILTEROP_WCP = 154,          /* SETFILTEROP_WCP  */
  YYSYMBOL_SHORTKEYFROMLISTOP_WCP = 155,   /* SHORTKEYFROMLISTOP_WCP  */
  YYSYMBOL_CHOWNOP_WCP = 156,              /* CHOWNOP_WCP  */
  YYSYMBOL_WORKERCONFIG_WCP = 157,         /* WORKERCONFIG_WCP  */
  YYSYMBOL_USERSTYLE_WCP = 158,            /* USERSTYLE_WCP  */
  YYSYMBOL_DATESTRING_WCP = 159,           /* DATESTRING_WCP  */
  YYSYMBOL_TIMESTRING_WCP = 160,           /* TIMESTRING_WCP  */
  YYSYMBOL_COLOR_WCP = 161,                /* COLOR_WCP  */
  YYSYMBOL_PATTERNIGNORECASE_WCP = 162,    /* PATTERNIGNORECASE_WCP  */
  YYSYMBOL_PATTERNUSEREGEXP_WCP = 163,     /* PATTERNUSEREGEXP_WCP  */
  YYSYMBOL_PATTERNUSEFULLNAME_WCP = 164,   /* PATTERNUSEFULLNAME_WCP  */
  YYSYMBOL_COM_WCP = 165,                  /* COM_WCP  */
  YYSYMBOL_SEPARATEEACHENTRY_WCP = 166,    /* SEPARATEEACHENTRY_WCP  */
  YYSYMBOL_RECURSIVE_WCP = 167,            /* RECURSIVE_WCP  */
  YYSYMBOL_START_WCP = 168,                /* START_WCP  */
  YYSYMBOL_TERMINALWAIT_WCP = 169,         /* TERMINALWAIT_WCP  */
  YYSYMBOL_SHOWOUTPUT_WCP = 170,           /* SHOWOUTPUT_WCP  */
  YYSYMBOL_VIEWSTR_WCP = 171,              /* VIEWSTR_WCP  */
  YYSYMBOL_SHOWOUTPUTINT_WCP = 172,        /* SHOWOUTPUTINT_WCP  */
  YYSYMBOL_SHOWOUTPUTOTHERSIDE_WCP = 173,  /* SHOWOUTPUTOTHERSIDE_WCP  */
  YYSYMBOL_SHOWOUTPUTCUSTOMATTRIBUTE_WCP = 174, /* SHOWOUTPUTCUSTOMATTRIBUTE_WCP  */
  YYSYMBOL_INBACKGROUND_WCP = 175,         /* INBACKGROUND_WCP  */
  YYSYMBOL_TAKEDIRS_WCP = 176,             /* TAKEDIRS_WCP  */
  YYSYMBOL_ACTIONNUMBER_WCP = 177,         /* ACTIONNUMBER_WCP  */
  YYSYMBOL_HIDDENFILES_WCP = 178,          /* HIDDENFILES_WCP  */
  YYSYMBOL_HIDE_WCP = 179,                 /* HIDE_WCP  */
  YYSYMBOL_TOGGLE_WCP = 180,               /* TOGGLE_WCP  */
  YYSYMBOL_FOLLOWSYMLINKS_WCP = 181,       /* FOLLOWSYMLINKS_WCP  */
  YYSYMBOL_MOVE_WCP = 182,                 /* MOVE_WCP  */
  YYSYMBOL_RENAME_WCP = 183,               /* RENAME_WCP  */
  YYSYMBOL_SAMEDIR_WCP = 184,              /* SAMEDIR_WCP  */
  YYSYMBOL_REQUESTDEST_WCP = 185,          /* REQUESTDEST_WCP  */
  YYSYMBOL_REQUESTFLAGS_WCP = 186,         /* REQUESTFLAGS_WCP  */
  YYSYMBOL_OVERWRITE_WCP = 187,            /* OVERWRITE_WCP  */
  YYSYMBOL_ALWAYS_WCP = 188,               /* ALWAYS_WCP  */
  YYSYMBOL_NEVER_WCP = 189,                /* NEVER_WCP  */
  YYSYMBOL_COPYMODE_WCP = 190,             /* COPYMODE_WCP  */
  YYSYMBOL_FAST_WCP = 191,                 /* FAST_WCP  */
  YYSYMBOL_PRESERVEATTR_WCP = 192,         /* PRESERVEATTR_WCP  */
  YYSYMBOL_MODE_WCP = 193,                 /* MODE_WCP  */
  YYSYMBOL_ACTIVE_WCP = 194,               /* ACTIVE_WCP  */
  YYSYMBOL_ACTIVE2OTHER_WCP = 195,         /* ACTIVE2OTHER_WCP  */
  YYSYMBOL_SPECIAL_WCP = 196,              /* SPECIAL_WCP  */
  YYSYMBOL_REQUEST_WCP = 197,              /* REQUEST_WCP  */
  YYSYMBOL_CURRENT_WCP = 198,              /* CURRENT_WCP  */
  YYSYMBOL_OTHER_WCP = 199,                /* OTHER_WCP  */
  YYSYMBOL_FILTER_WCP = 200,               /* FILTER_WCP  */
  YYSYMBOL_QUICK_WCP = 201,                /* QUICK_WCP  */
  YYSYMBOL_ALSOACTIVE_WCP = 202,           /* ALSOACTIVE_WCP  */
  YYSYMBOL_RESETDIRSIZES_WCP = 203,        /* RESETDIRSIZES_WCP  */
  YYSYMBOL_KEEPFILETYPES_WCP = 204,        /* KEEPFILETYPES_WCP  */
  YYSYMBOL_RELATIVE_WCP = 205,             /* RELATIVE_WCP  */
  YYSYMBOL_ONFILES_WCP = 206,              /* ONFILES_WCP  */
  YYSYMBOL_ONDIRS_WCP = 207,               /* ONDIRS_WCP  */
  YYSYMBOL_SORTBY_WCP = 208,               /* SORTBY_WCP  */
  YYSYMBOL_SORTFLAG_WCP = 209,             /* SORTFLAG_WCP  */
  YYSYMBOL_REVERSE_WCP = 210,              /* REVERSE_WCP  */
  YYSYMBOL_DIRLAST_WCP = 211,              /* DIRLAST_WCP  */
  YYSYMBOL_DIRMIXED_WCP = 212,             /* DIRMIXED_WCP  */
  YYSYMBOL_EXCLUDE_WCP = 213,              /* EXCLUDE_WCP  */
  YYSYMBOL_INCLUDE_WCP = 214,              /* INCLUDE_WCP  */
  YYSYMBOL_UNSET_WCP = 215,                /* UNSET_WCP  */
  YYSYMBOL_UNSETALL_WCP = 216,             /* UNSETALL_WCP  */
  YYSYMBOL_SCRIPTOP_WCP = 217,             /* SCRIPTOP_WCP  */
  YYSYMBOL_NOP_WCP = 218,                  /* NOP_WCP  */
  YYSYMBOL_PUSH_WCP = 219,                 /* PUSH_WCP  */
  YYSYMBOL_LABEL_WCP = 220,                /* LABEL_WCP  */
  YYSYMBOL_IF_WCP = 221,                   /* IF_WCP  */
  YYSYMBOL_END_WCP = 222,                  /* END_WCP  */
  YYSYMBOL_POP_WCP = 223,                  /* POP_WCP  */
  YYSYMBOL_SETTINGS_WCP = 224,             /* SETTINGS_WCP  */
  YYSYMBOL_WINDOW_WCP = 225,               /* WINDOW_WCP  */
  YYSYMBOL_GOTO_WCP = 226,                 /* GOTO_WCP  */
  YYSYMBOL_PUSHUSEOUTPUT_WCP = 227,        /* PUSHUSEOUTPUT_WCP  */
  YYSYMBOL_DODEBUG_WCP = 228,              /* DODEBUG_WCP  */
  YYSYMBOL_WPURECURSIVE_WCP = 229,         /* WPURECURSIVE_WCP  */
  YYSYMBOL_WPUTAKEDIRS_WCP = 230,          /* WPUTAKEDIRS_WCP  */
  YYSYMBOL_STACKNR_WCP = 231,              /* STACKNR_WCP  */
  YYSYMBOL_PUSHSTRING_WCP = 232,           /* PUSHSTRING_WCP  */
  YYSYMBOL_PUSHOUTPUTRETURNCODE_WCP = 233, /* PUSHOUTPUTRETURNCODE_WCP  */
  YYSYMBOL_IFTEST_WCP = 234,               /* IFTEST_WCP  */
  YYSYMBOL_IFLABEL_WCP = 235,              /* IFLABEL_WCP  */
  YYSYMBOL_WINTYPE_WCP = 236,              /* WINTYPE_WCP  */
  YYSYMBOL_OPEN_WCP = 237,                 /* OPEN_WCP  */
  YYSYMBOL_CLOSE_WCP = 238,                /* CLOSE_WCP  */
  YYSYMBOL_LEAVE_WCP = 239,                /* LEAVE_WCP  */
  YYSYMBOL_CHANGEPROGRESS_WCP = 240,       /* CHANGEPROGRESS_WCP  */
  YYSYMBOL_CHANGETEXT_WCP = 241,           /* CHANGETEXT_WCP  */
  YYSYMBOL_PROGRESSUSEOUTPUT_WCP = 242,    /* PROGRESSUSEOUTPUT_WCP  */
  YYSYMBOL_WINTEXTUSEOUTPUT_WCP = 243,     /* WINTEXTUSEOUTPUT_WCP  */
  YYSYMBOL_PROGRESS_WCP = 244,             /* PROGRESS_WCP  */
  YYSYMBOL_WINTEXT_WCP = 245,              /* WINTEXT_WCP  */
  YYSYMBOL_SHOWDIRCACHEOP_WCP = 246,       /* SHOWDIRCACHEOP_WCP  */
  YYSYMBOL_INODE_WCP = 247,                /* INODE_WCP  */
  YYSYMBOL_NLINK_WCP = 248,                /* NLINK_WCP  */
  YYSYMBOL_BLOCKS_WCP = 249,               /* BLOCKS_WCP  */
  YYSYMBOL_SHOWHEADER_WCP = 250,           /* SHOWHEADER_WCP  */
  YYSYMBOL_LVHEADER_WCP = 251,             /* LVHEADER_WCP  */
  YYSYMBOL_IGNORECASE_WCP = 252,           /* IGNORECASE_WCP  */
  YYSYMBOL_LISTVIEWS_WCP = 253,            /* LISTVIEWS_WCP  */
  YYSYMBOL_BLL_WCP = 254,                  /* BLL_WCP  */
  YYSYMBOL_LBL_WCP = 255,                  /* LBL_WCP  */
  YYSYMBOL_LLB_WCP = 256,                  /* LLB_WCP  */
  YYSYMBOL_BL_WCP = 257,                   /* BL_WCP  */
  YYSYMBOL_LB_WCP = 258,                   /* LB_WCP  */
  YYSYMBOL_LAYOUT_WCP = 259,               /* LAYOUT_WCP  */
  YYSYMBOL_BUTTONSVERT_WCP = 260,          /* BUTTONSVERT_WCP  */
  YYSYMBOL_LISTVIEWSVERT_WCP = 261,        /* LISTVIEWSVERT_WCP  */
  YYSYMBOL_LISTVIEWWEIGHT_WCP = 262,       /* LISTVIEWWEIGHT_WCP  */
  YYSYMBOL_WEIGHTTOACTIVE_WCP = 263,       /* WEIGHTTOACTIVE_WCP  */
  YYSYMBOL_EXTCOND_WCP = 264,              /* EXTCOND_WCP  */
  YYSYMBOL_USEEXTCOND_WCP = 265,           /* USEEXTCOND_WCP  */
  YYSYMBOL_SUBTYPE_WCP = 266,              /* SUBTYPE_WCP  */
  YYSYMBOL_PARENTACTIONOP_WCP = 267,       /* PARENTACTIONOP_WCP  */
  YYSYMBOL_NOOPERATIONOP_WCP = 268,        /* NOOPERATIONOP_WCP  */
  YYSYMBOL_COLORMODE_WCP = 269,            /* COLORMODE_WCP  */
  YYSYMBOL_DEFAULT_WCP = 270,              /* DEFAULT_WCP  */
  YYSYMBOL_CUSTOM_WCP = 271,               /* CUSTOM_WCP  */
  YYSYMBOL_UNSELECTCOLOR_WCP = 272,        /* UNSELECTCOLOR_WCP  */
  YYSYMBOL_SELECTCOLOR_WCP = 273,          /* SELECTCOLOR_WCP  */
  YYSYMBOL_UNSELECTACTIVECOLOR_WCP = 274,  /* UNSELECTACTIVECOLOR_WCP  */
  YYSYMBOL_SELECTACTIVECOLOR_WCP = 275,    /* SELECTACTIVECOLOR_WCP  */
  YYSYMBOL_COLOREXTERNPROG_WCP = 276,      /* COLOREXTERNPROG_WCP  */
  YYSYMBOL_PARENT_WCP = 277,               /* PARENT_WCP  */
  YYSYMBOL_DONTCD_WCP = 278,               /* DONTCD_WCP  */
  YYSYMBOL_DONTCHECKVIRTUAL_WCP = 279,     /* DONTCHECKVIRTUAL_WCP  */
  YYSYMBOL_GOFTPOP_WCP = 280,              /* GOFTPOP_WCP  */
  YYSYMBOL_HOSTNAME_WCP = 281,             /* HOSTNAME_WCP  */
  YYSYMBOL_USERNAME_WCP = 282,             /* USERNAME_WCP  */
  YYSYMBOL_PASSWORD_WCP = 283,             /* PASSWORD_WCP  */
  YYSYMBOL_DONTENTERFTP_WCP = 284,         /* DONTENTERFTP_WCP  */
  YYSYMBOL_ALWAYSSTOREPW_WCP = 285,        /* ALWAYSSTOREPW_WCP  */
  YYSYMBOL_INTERNALVIEWOP_WCP = 286,       /* INTERNALVIEWOP_WCP  */
  YYSYMBOL_CUSTOMFILES_WCP = 287,          /* CUSTOMFILES_WCP  */
  YYSYMBOL_SHOWMODE_WCP = 288,             /* SHOWMODE_WCP  */
  YYSYMBOL_SELECTED_WCP = 289,             /* SELECTED_WCP  */
  YYSYMBOL_REVERSESEARCH_WCP = 290,        /* REVERSESEARCH_WCP  */
  YYSYMBOL_MOUSECONF_WCP = 291,            /* MOUSECONF_WCP  */
  YYSYMBOL_SELECTBUTTON_WCP = 292,         /* SELECTBUTTON_WCP  */
  YYSYMBOL_ACTIVATEBUTTON_WCP = 293,       /* ACTIVATEBUTTON_WCP  */
  YYSYMBOL_SCROLLBUTTON_WCP = 294,         /* SCROLLBUTTON_WCP  */
  YYSYMBOL_SELECTMETHOD_WCP = 295,         /* SELECTMETHOD_WCP  */
  YYSYMBOL_ALTERNATIVE_WCP = 296,          /* ALTERNATIVE_WCP  */
  YYSYMBOL_SEARCHOP_WCP = 297,             /* SEARCHOP_WCP  */
  YYSYMBOL_EDITCOMMAND_WCP = 298,          /* EDITCOMMAND_WCP  */
  YYSYMBOL_SHOWPREVRESULTS_WCP = 299,      /* SHOWPREVRESULTS_WCP  */
  YYSYMBOL_TEXTVIEW_WCP = 300,             /* TEXTVIEW_WCP  */
  YYSYMBOL_TEXTVIEWHIGHLIGHTED_WCP = 301,  /* TEXTVIEWHIGHLIGHTED_WCP  */
  YYSYMBOL_TEXTVIEWSELECTION_WCP = 302,    /* TEXTVIEWSELECTION_WCP  */
  YYSYMBOL_DIRBOOKMARKOP_WCP = 303,        /* DIRBOOKMARKOP_WCP  */
  YYSYMBOL_OPENCONTEXTMENUOP_WCP = 304,    /* OPENCONTEXTMENUOP_WCP  */
  YYSYMBOL_RUNCUSTOMACTION_WCP = 305,      /* RUNCUSTOMACTION_WCP  */
  YYSYMBOL_CUSTOMNAME_WCP = 306,           /* CUSTOMNAME_WCP  */
  YYSYMBOL_CONTEXTBUTTON_WCP = 307,        /* CONTEXTBUTTON_WCP  */
  YYSYMBOL_ACTIVATEMOD_WCP = 308,          /* ACTIVATEMOD_WCP  */
  YYSYMBOL_SCROLLMOD_WCP = 309,            /* SCROLLMOD_WCP  */
  YYSYMBOL_CONTEXTMOD_WCP = 310,           /* CONTEXTMOD_WCP  */
  YYSYMBOL_NONE_WCP = 311,                 /* NONE_WCP  */
  YYSYMBOL_CUSTOMACTION_WCP = 312,         /* CUSTOMACTION_WCP  */
  YYSYMBOL_SAVE_WORKER_STATE_ON_EXIT_WCP = 313, /* SAVE_WORKER_STATE_ON_EXIT_WCP  */
  YYSYMBOL_OPENWORKERMENUOP_WCP = 314,     /* OPENWORKERMENUOP_WCP  */
  YYSYMBOL_CHANGELABELOP_WCP = 315,        /* CHANGELABELOP_WCP  */
  YYSYMBOL_ASKFORLABEL_WCP = 316,          /* ASKFORLABEL_WCP  */
  YYSYMBOL_LABELCOLORS_WCP = 317,          /* LABELCOLORS_WCP  */
  YYSYMBOL_BOOKMARKLABEL_WCP = 318,        /* BOOKMARKLABEL_WCP  */
  YYSYMBOL_BOOKMARKFILTER_WCP = 319,       /* BOOKMARKFILTER_WCP  */
  YYSYMBOL_SHOWONLYBOOKMARKS_WCP = 320,    /* SHOWONLYBOOKMARKS_WCP  */
  YYSYMBOL_SHOWONLYLABEL_WCP = 321,        /* SHOWONLYLABEL_WCP  */
  YYSYMBOL_SHOWALL_WCP = 322,              /* SHOWALL_WCP  */
  YYSYMBOL_OPTIONMODE_WCP = 323,           /* OPTIONMODE_WCP  */
  YYSYMBOL_INVERT_WCP = 324,               /* INVERT_WCP  */
  YYSYMBOL_SET_WCP = 325,                  /* SET_WCP  */
  YYSYMBOL_CHANGEFILTERS_WCP = 326,        /* CHANGEFILTERS_WCP  */
  YYSYMBOL_CHANGEBOOKMARKS_WCP = 327,      /* CHANGEBOOKMARKS_WCP  */
  YYSYMBOL_QUERYLABEL_WCP = 328,           /* QUERYLABEL_WCP  */
  YYSYMBOL_MODIFYTABSOP_WCP = 329,         /* MODIFYTABSOP_WCP  */
  YYSYMBOL_TABACTION_WCP = 330,            /* TABACTION_WCP  */
  YYSYMBOL_NEWTAB_WCP = 331,               /* NEWTAB_WCP  */
  YYSYMBOL_CLOSECURRENTTAB_WCP = 332,      /* CLOSECURRENTTAB_WCP  */
  YYSYMBOL_NEXTTAB_WCP = 333,              /* NEXTTAB_WCP  */
  YYSYMBOL_PREVTAB_WCP = 334,              /* PREVTAB_WCP  */
  YYSYMBOL_TOGGLELOCKSTATE_WCP = 335,      /* TOGGLELOCKSTATE_WCP  */
  YYSYMBOL_MOVETABLEFT_WCP = 336,          /* MOVETABLEFT_WCP  */
  YYSYMBOL_MOVETABRIGHT_WCP = 337,         /* MOVETABRIGHT_WCP  */
  YYSYMBOL_CHANGELAYOUTOP_WCP = 338,       /* CHANGELAYOUTOP_WCP  */
  YYSYMBOL_INFIXSEARCH_WCP = 339,          /* INFIXSEARCH_WCP  */
  YYSYMBOL_VOLUMEMANAGEROP_WCP = 340,      /* VOLUMEMANAGEROP_WCP  */
  YYSYMBOL_ERROR = 341,                    /* ERROR  */
  YYSYMBOL_ACTIVESIDE_WCP = 342,           /* ACTIVESIDE_WCP  */
  YYSYMBOL_SHOWFREESPACE_WCP = 343,        /* SHOWFREESPACE_WCP  */
  YYSYMBOL_ACTIVEMODE_WCP = 344,           /* ACTIVEMODE_WCP  */
  YYSYMBOL_ASK_WCP = 345,                  /* ASK_WCP  */
  YYSYMBOL_SSHALLOW_WCP = 346,             /* SSHALLOW_WCP  */
  YYSYMBOL_FIELD_WIDTH_WCP = 347,          /* FIELD_WIDTH_WCP  */
  YYSYMBOL_QUICKSEARCHENABLED_WCP = 348,   /* QUICKSEARCHENABLED_WCP  */
  YYSYMBOL_FILTEREDSEARCHENABLED_WCP = 349, /* FILTEREDSEARCHENABLED_WCP  */
  YYSYMBOL_XFTFONTS_WCP = 350,             /* XFTFONTS_WCP  */
  YYSYMBOL_USEMAGIC_WCP = 351,             /* USEMAGIC_WCP  */
  YYSYMBOL_MAGICPATTERN_WCP = 352,         /* MAGICPATTERN_WCP  */
  YYSYMBOL_MAGICCOMPRESSED_WCP = 353,      /* MAGICCOMPRESSED_WCP  */
  YYSYMBOL_MAGICMIME_WCP = 354,            /* MAGICMIME_WCP  */
  YYSYMBOL_VOLUMEMANAGER_WCP = 355,        /* VOLUMEMANAGER_WCP  */
  YYSYMBOL_MOUNTCOMMAND_WCP = 356,         /* MOUNTCOMMAND_WCP  */
  YYSYMBOL_UNMOUNTCOMMAND_WCP = 357,       /* UNMOUNTCOMMAND_WCP  */
  YYSYMBOL_FSTABFILE_WCP = 358,            /* FSTABFILE_WCP  */
  YYSYMBOL_MTABFILE_WCP = 359,             /* MTABFILE_WCP  */
  YYSYMBOL_PARTFILE_WCP = 360,             /* PARTFILE_WCP  */
  YYSYMBOL_REQUESTACTION_WCP = 361,        /* REQUESTACTION_WCP  */
  YYSYMBOL_SIZEH_WCP = 362,                /* SIZEH_WCP  */
  YYSYMBOL_NEXTAGING_WCP = 363,            /* NEXTAGING_WCP  */
  YYSYMBOL_ENTRY_WCP = 364,                /* ENTRY_WCP  */
  YYSYMBOL_PREFIX_WCP = 365,               /* PREFIX_WCP  */
  YYSYMBOL_VALUE_WCP = 366,                /* VALUE_WCP  */
  YYSYMBOL_EXTENSION_WCP = 367,            /* EXTENSION_WCP  */
  YYSYMBOL_EJECTCOMMAND_WCP = 368,         /* EJECTCOMMAND_WCP  */
  YYSYMBOL_CLOSETRAYCOMMAND_WCP = 369,     /* CLOSETRAYCOMMAND_WCP  */
  YYSYMBOL_AVFSMODULE_WCP = 370,           /* AVFSMODULE_WCP  */
  YYSYMBOL_FLEXIBLEMATCH_WCP = 371,        /* FLEXIBLEMATCH_WCP  */
  YYSYMBOL_USE_VERSION_STRING_COMPARE_WCP = 372, /* USE_VERSION_STRING_COMPARE_WCP  */
  YYSYMBOL_SWITCHBUTTONBANKOP_WCP = 373,   /* SWITCHBUTTONBANKOP_WCP  */
  YYSYMBOL_SWITCHTONEXTBANK_WCP = 374,     /* SWITCHTONEXTBANK_WCP  */
  YYSYMBOL_SWITCHTOPREVBANK_WCP = 375,     /* SWITCHTOPREVBANK_WCP  */
  YYSYMBOL_SWITCHTOBANKNR_WCP = 376,       /* SWITCHTOBANKNR_WCP  */
  YYSYMBOL_BANKNR_WCP = 377,               /* BANKNR_WCP  */
  YYSYMBOL_USE_VIRTUAL_TEMP_COPIES_WCP = 378, /* USE_VIRTUAL_TEMP_COPIES_WCP  */
  YYSYMBOL_PATHJUMPOP_WCP = 379,           /* PATHJUMPOP_WCP  */
  YYSYMBOL_PATHJUMPALLOWDIRS_WCP = 380,    /* PATHJUMPALLOWDIRS_WCP  */
  YYSYMBOL_CLIPBOARDOP_WCP = 381,          /* CLIPBOARDOP_WCP  */
  YYSYMBOL_CLIPBOARDSTRING_WCP = 382,      /* CLIPBOARDSTRING_WCP  */
  YYSYMBOL_COMMANDSTRING_WCP = 383,        /* COMMANDSTRING_WCP  */
  YYSYMBOL_EVALCOMMAND_WCP = 384,          /* EVALCOMMAND_WCP  */
  YYSYMBOL_WATCHMODE_WCP = 385,            /* WATCHMODE_WCP  */
  YYSYMBOL_APPLY_WINDOW_DIALOG_TYPE_WCP = 386, /* APPLY_WINDOW_DIALOG_TYPE_WCP  */
  YYSYMBOL_FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP = 387, /* FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP  */
  YYSYMBOL_COMMANDMENUOP_WCP = 388,        /* COMMANDMENUOP_WCP  */
  YYSYMBOL_SEARCHMODEONKEYPRESS_WCP = 389, /* SEARCHMODEONKEYPRESS_WCP  */
  YYSYMBOL_PATHENTRYONTOP_WCP = 390,       /* PATHENTRYONTOP_WCP  */
  YYSYMBOL_STORE_FILES_ALWAYS_WCP = 391,   /* STORE_FILES_ALWAYS_WCP  */
  YYSYMBOL_PATHJUMPSETS_WCP = 392,         /* PATHJUMPSETS_WCP  */
  YYSYMBOL_SHOWDOTDOT_WCP = 393,           /* SHOWDOTDOT_WCP  */
  YYSYMBOL_SHOWBREADCRUMB_WCP = 394,       /* SHOWBREADCRUMB_WCP  */
  YYSYMBOL_FACES_WCP = 395,                /* FACES_WCP  */
  YYSYMBOL_FACE_WCP = 396,                 /* FACE_WCP  */
  YYSYMBOL_ENABLE_INFO_LINE_WCP = 397,     /* ENABLE_INFO_LINE_WCP  */
  YYSYMBOL_INFO_LINE_LUA_MODE_WCP = 398,   /* INFO_LINE_LUA_MODE_WCP  */
  YYSYMBOL_INFO_LINE_CONTENT_WCP = 399,    /* INFO_LINE_CONTENT_WCP  */
  YYSYMBOL_RESTORE_TABS_MODE_WCP = 400,    /* RESTORE_TABS_MODE_WCP  */
  YYSYMBOL_STORE_TABS_MODE_WCP = 401,      /* STORE_TABS_MODE_WCP  */
  YYSYMBOL_AS_EXIT_STATE_WCP = 402,        /* AS_EXIT_STATE_WCP  */
  YYSYMBOL_USE_EXTENDED_REGEX_WCP = 403,   /* USE_EXTENDED_REGEX_WCP  */
  YYSYMBOL_HIGHLIGHT_USER_ACTION_WCP = 404, /* HIGHLIGHT_USER_ACTION_WCP  */
  YYSYMBOL_CHTIMEOP_WCP = 405,             /* CHTIMEOP_WCP  */
  YYSYMBOL_ADJUSTRELATIVESYMLINKS_WCP = 406, /* ADJUSTRELATIVESYMLINKS_WCP  */
  YYSYMBOL_OUTSIDE_WCP = 407,              /* OUTSIDE_WCP  */
  YYSYMBOL_EDIT_WCP = 408,                 /* EDIT_WCP  */
  YYSYMBOL_MAKEABSOLUTE_WCP = 409,         /* MAKEABSOLUTE_WCP  */
  YYSYMBOL_MAKERELATIVE_WCP = 410,         /* MAKERELATIVE_WCP  */
  YYSYMBOL_MAGICIGNORECASE_WCP = 411,      /* MAGICIGNORECASE_WCP  */
  YYSYMBOL_ENSURE_FILE_PERMISSIONS_WCP = 412, /* ENSURE_FILE_PERMISSIONS_WCP  */
  YYSYMBOL_USER_RW_WCP = 413,              /* USER_RW_WCP  */
  YYSYMBOL_USER_RW_GROUP_R_WCP = 414,      /* USER_RW_GROUP_R_WCP  */
  YYSYMBOL_USER_RW_ALL_R_WCP = 415,        /* USER_RW_ALL_R_WCP  */
  YYSYMBOL_LEAVE_UNMODIFIED_WCP = 416,     /* LEAVE_UNMODIFIED_WCP  */
  YYSYMBOL_PREFERUDISKSVERSION_WCP = 417,  /* PREFERUDISKSVERSION_WCP  */
  YYSYMBOL_CHANGECOLUMNSOP_WCP = 418,      /* CHANGECOLUMNSOP_WCP  */
  YYSYMBOL_PATTERNISCOMMASEPARATED_WCP = 419, /* PATTERNISCOMMASEPARATED_WCP  */
  YYSYMBOL_STRCASECMP_WCP = 420,           /* STRCASECMP_WCP  */
  YYSYMBOL_USE_STRING_COMPARE_MODE_WCP = 421, /* USE_STRING_COMPARE_MODE_WCP  */
  YYSYMBOL_VIEWNEWESTFILESOP_WCP = 422,    /* VIEWNEWESTFILESOP_WCP  */
  YYSYMBOL_SHOWRECENT_WCP = 423,           /* SHOWRECENT_WCP  */
  YYSYMBOL_DIRCOMPAREOP_WCP = 424,         /* DIRCOMPAREOP_WCP  */
  YYSYMBOL_TABPROFILESOP_WCP = 425,        /* TABPROFILESOP_WCP  */
  YYSYMBOL_EXTERNALVDIROP_WCP = 426,       /* EXTERNALVDIROP_WCP  */
  YYSYMBOL_VDIR_PRESERVE_DIR_STRUCTURE_WCP = 427, /* VDIR_PRESERVE_DIR_STRUCTURE_WCP  */
  YYSYMBOL_OPENTABMENUOP_WCP = 428,        /* OPENTABMENUOP_WCP  */
  YYSYMBOL_INITIALTAB_WCP = 429,           /* INITIALTAB_WCP  */
  YYSYMBOL_SHOWBYTIME_WCP = 430,           /* SHOWBYTIME_WCP  */
  YYSYMBOL_SHOWBYFILTER_WCP = 431,         /* SHOWBYFILTER_WCP  */
  YYSYMBOL_SHOWBYPROGRAM_WCP = 432,        /* SHOWBYPROGRAM_WCP  */
  YYSYMBOL_TERMINAL_RETURNS_EARLY_WCP = 433, /* TERMINAL_RETURNS_EARLY_WCP  */
  YYSYMBOL_DIRECTORYPRESETS_WCP = 434,     /* DIRECTORYPRESETS_WCP  */
  YYSYMBOL_DIRECTORYPRESET_WCP = 435,      /* DIRECTORYPRESET_WCP  */
  YYSYMBOL_HELPOP_WCP = 436,               /* HELPOP_WCP  */
  YYSYMBOL_PRIORITY_WCP = 437,             /* PRIORITY_WCP  */
  YYSYMBOL_AUTOFILTER_WCP = 438,           /* AUTOFILTER_WCP  */
  YYSYMBOL_ENABLE_CUSTOM_LVB_LINE_WCP = 439, /* ENABLE_CUSTOM_LVB_LINE_WCP  */
  YYSYMBOL_CUSTOM_LVB_LINE_WCP = 440,      /* CUSTOM_LVB_LINE_WCP  */
  YYSYMBOL_CUSTOM_DIRECTORY_INFO_COMMAND_WCP = 441, /* CUSTOM_DIRECTORY_INFO_COMMAND_WCP  */
  YYSYMBOL_IMMEDIATE_FILTER_APPLY_WCP = 442, /* IMMEDIATE_FILTER_APPLY_WCP  */
  YYSYMBOL_DISABLEBGCHECKPREFIX_WCP = 443, /* DISABLEBGCHECKPREFIX_WCP  */
  YYSYMBOL_DISPLAYMODE_WCP = 444,          /* DISPLAYMODE_WCP  */
  YYSYMBOL_SHOWSUBDIRS_WCP = 445,          /* SHOWSUBDIRS_WCP  */
  YYSYMBOL_SHOWDIRECTSUBDIRS_WCP = 446,    /* SHOWDIRECTSUBDIRS_WCP  */
  YYSYMBOL_INCLUDEALL_WCP = 447,           /* INCLUDEALL_WCP  */
  YYSYMBOL_APPLYMODE_WCP = 448,            /* APPLYMODE_WCP  */
  YYSYMBOL_ADD_WCP = 449,                  /* ADD_WCP  */
  YYSYMBOL_REMOVE_WCP = 450,               /* REMOVE_WCP  */
  YYSYMBOL_HIDENONEXISTING_WCP = 451,      /* HIDENONEXISTING_WCP  */
  YYSYMBOL_VIEWCOMMANDLOGOP_WCP = 452,     /* VIEWCOMMANDLOGOP_WCP  */
  YYSYMBOL_LOCKONCURRENTDIR_WCP = 453,     /* LOCKONCURRENTDIR_WCP  */
  YYSYMBOL_STARTNODE_WCP = 454,            /* STARTNODE_WCP  */
  YYSYMBOL_MENUS_WCP = 455,                /* MENUS_WCP  */
  YYSYMBOL_LVMODES_WCP = 456,              /* LVMODES_WCP  */
  YYSYMBOL_LVCOMMANDS_WCP = 457,           /* LVCOMMANDS_WCP  */
  YYSYMBOL_TOPLEVEL_WCP = 458,             /* TOPLEVEL_WCP  */
  YYSYMBOL_FOLLOWACTIVE_WCP = 459,         /* FOLLOWACTIVE_WCP  */
  YYSYMBOL_WATCHFILE_WCP = 460,            /* WATCHFILE_WCP  */
  YYSYMBOL_ACTIVATETEXTVIEWMODEOP_WCP = 461, /* ACTIVATETEXTVIEWMODEOP_WCP  */
  YYSYMBOL_SOURCEMODE_WCP = 462,           /* SOURCEMODE_WCP  */
  YYSYMBOL_ACTIVEFILE_WCP = 463,           /* ACTIVEFILE_WCP  */
  YYSYMBOL_FILETYPEACTION_WCP = 464,       /* FILETYPEACTION_WCP  */
  YYSYMBOL_IGNOREDIRS2_WCP = 465,          /* IGNOREDIRS2_WCP  */
  YYSYMBOL_IGNOREDIR_WCP = 466,            /* IGNOREDIR_WCP  */
  YYSYMBOL_CUSTOMATTRIBUTE_WCP = 467,      /* CUSTOMATTRIBUTE_WCP  */
  YYSYMBOL_STRING_WCP = 468,               /* STRING_WCP  */
  YYSYMBOL_NUM_WCP = 469,                  /* NUM_WCP  */
  YYSYMBOL_470_ = 470,                     /* '.'  */
  YYSYMBOL_471_ = 471,                     /* ';'  */
  YYSYMBOL_472_ = 472,                     /* '='  */
  YYSYMBOL_473_ = 473,                     /* ','  */
  YYSYMBOL_YYACCEPT = 474,                 /* $accept  */
  YYSYMBOL_S = 475,                        /* S  */
  YYSYMBOL_476_1 = 476,                    /* $@1  */
  YYSYMBOL_477_2 = 477,                    /* $@2  */
  YYSYMBOL_start = 478,                    /* start  */
  YYSYMBOL_479_3 = 479,                    /* $@3  */
  YYSYMBOL_480_4 = 480,                    /* $@4  */
  YYSYMBOL_481_5 = 481,                    /* $@5  */
  YYSYMBOL_482_6 = 482,                    /* $@6  */
  YYSYMBOL_483_7 = 483,                    /* $@7  */
  YYSYMBOL_484_8 = 484,                    /* $@8  */
  YYSYMBOL_485_9 = 485,                    /* $@9  */
  YYSYMBOL_486_10 = 486,                   /* $@10  */
  YYSYMBOL_487_11 = 487,                   /* $@11  */
  YYSYMBOL_488_12 = 488,                   /* $@12  */
  YYSYMBOL_489_13 = 489,                   /* $@13  */
  YYSYMBOL_490_14 = 490,                   /* $@14  */
  YYSYMBOL_491_15 = 491,                   /* $@15  */
  YYSYMBOL_492_16 = 492,                   /* $@16  */
  YYSYMBOL_pjallowdirs = 493,              /* pjallowdirs  */
  YYSYMBOL_494_17 = 494,                   /* $@17  */
  YYSYMBOL_pathjumpsets = 495,             /* pathjumpsets  */
  YYSYMBOL_496_18 = 496,                   /* $@18  */
  YYSYMBOL_glb = 497,                      /* glb  */
  YYSYMBOL_498_19 = 498,                   /* $@19  */
  YYSYMBOL_499_20 = 499,                   /* $@20  */
  YYSYMBOL_500_21 = 500,                   /* $@21  */
  YYSYMBOL_501_22 = 501,                   /* $@22  */
  YYSYMBOL_502_23 = 502,                   /* $@23  */
  YYSYMBOL_503_24 = 503,                   /* $@24  */
  YYSYMBOL_504_25 = 504,                   /* $@25  */
  YYSYMBOL_505_26 = 505,                   /* $@26  */
  YYSYMBOL_506_27 = 506,                   /* $@27  */
  YYSYMBOL_507_28 = 507,                   /* $@28  */
  YYSYMBOL_508_29 = 508,                   /* $@29  */
  YYSYMBOL_509_30 = 509,                   /* $@30  */
  YYSYMBOL_510_31 = 510,                   /* $@31  */
  YYSYMBOL_511_32 = 511,                   /* $@32  */
  YYSYMBOL_512_33 = 512,                   /* $@33  */
  YYSYMBOL_513_34 = 513,                   /* $@34  */
  YYSYMBOL_514_35 = 514,                   /* $@35  */
  YYSYMBOL_layout = 515,                   /* layout  */
  YYSYMBOL_516_36 = 516,                   /* $@36  */
  YYSYMBOL_517_37 = 517,                   /* $@37  */
  YYSYMBOL_518_38 = 518,                   /* $@38  */
  YYSYMBOL_519_39 = 519,                   /* $@39  */
  YYSYMBOL_520_40 = 520,                   /* $@40  */
  YYSYMBOL_521_41 = 521,                   /* $@41  */
  YYSYMBOL_522_42 = 522,                   /* $@42  */
  YYSYMBOL_523_43 = 523,                   /* $@43  */
  YYSYMBOL_524_44 = 524,                   /* $@44  */
  YYSYMBOL_525_45 = 525,                   /* $@45  */
  YYSYMBOL_526_46 = 526,                   /* $@46  */
  YYSYMBOL_527_47 = 527,                   /* $@47  */
  YYSYMBOL_528_48 = 528,                   /* $@48  */
  YYSYMBOL_pal = 529,                      /* pal  */
  YYSYMBOL_530_49 = 530,                   /* $@49  */
  YYSYMBOL_listersets = 531,               /* listersets  */
  YYSYMBOL_532_50 = 532,                   /* $@50  */
  YYSYMBOL_533_51 = 533,                   /* $@51  */
  YYSYMBOL_listersets2 = 534,              /* listersets2  */
  YYSYMBOL_535_52 = 535,                   /* $@52  */
  YYSYMBOL_536_53 = 536,                   /* $@53  */
  YYSYMBOL_537_54 = 537,                   /* $@54  */
  YYSYMBOL_538_55 = 538,                   /* $@55  */
  YYSYMBOL_539_56 = 539,                   /* $@56  */
  YYSYMBOL_540_57 = 540,                   /* $@57  */
  YYSYMBOL_541_58 = 541,                   /* $@58  */
  YYSYMBOL_displaysets = 542,              /* displaysets  */
  YYSYMBOL_543_59 = 543,                   /* $@59  */
  YYSYMBOL_544_60 = 544,                   /* $@60  */
  YYSYMBOL_545_61 = 545,                   /* $@61  */
  YYSYMBOL_546_62 = 546,                   /* $@62  */
  YYSYMBOL_547_63 = 547,                   /* $@63  */
  YYSYMBOL_548_64 = 548,                   /* $@64  */
  YYSYMBOL_549_65 = 549,                   /* $@65  */
  YYSYMBOL_550_66 = 550,                   /* $@66  */
  YYSYMBOL_551_67 = 551,                   /* $@67  */
  YYSYMBOL_552_68 = 552,                   /* $@68  */
  YYSYMBOL_553_69 = 553,                   /* $@69  */
  YYSYMBOL_554_70 = 554,                   /* $@70  */
  YYSYMBOL_555_71 = 555,                   /* $@71  */
  YYSYMBOL_556_72 = 556,                   /* $@72  */
  YYSYMBOL_557_73 = 557,                   /* $@73  */
  YYSYMBOL_timesets = 558,                 /* timesets  */
  YYSYMBOL_559_74 = 559,                   /* $@74  */
  YYSYMBOL_560_75 = 560,                   /* $@75  */
  YYSYMBOL_561_76 = 561,                   /* $@76  */
  YYSYMBOL_562_77 = 562,                   /* $@77  */
  YYSYMBOL_563_78 = 563,                   /* $@78  */
  YYSYMBOL_564_79 = 564,                   /* $@79  */
  YYSYMBOL_565_80 = 565,                   /* $@80  */
  YYSYMBOL_566_81 = 566,                   /* $@81  */
  YYSYMBOL_567_82 = 567,                   /* $@82  */
  YYSYMBOL_568_83 = 568,                   /* $@83  */
  YYSYMBOL_569_84 = 569,                   /* $@84  */
  YYSYMBOL_mouseconf = 570,                /* mouseconf  */
  YYSYMBOL_571_85 = 571,                   /* $@85  */
  YYSYMBOL_572_86 = 572,                   /* $@86  */
  YYSYMBOL_573_87 = 573,                   /* $@87  */
  YYSYMBOL_574_88 = 574,                   /* $@88  */
  YYSYMBOL_575_89 = 575,                   /* $@89  */
  YYSYMBOL_576_90 = 576,                   /* $@90  */
  YYSYMBOL_577_91 = 577,                   /* $@91  */
  YYSYMBOL_578_92 = 578,                   /* $@92  */
  YYSYMBOL_579_93 = 579,                   /* $@93  */
  YYSYMBOL_580_94 = 580,                   /* $@94  */
  YYSYMBOL_581_95 = 581,                   /* $@95  */
  YYSYMBOL_582_96 = 582,                   /* $@96  */
  YYSYMBOL_cols = 583,                     /* cols  */
  YYSYMBOL_584_97 = 584,                   /* $@97  */
  YYSYMBOL_585_98 = 585,                   /* $@98  */
  YYSYMBOL_586_99 = 586,                   /* $@99  */
  YYSYMBOL_587_100 = 587,                  /* $@100  */
  YYSYMBOL_588_101 = 588,                  /* $@101  */
  YYSYMBOL_589_102 = 589,                  /* $@102  */
  YYSYMBOL_590_103 = 590,                  /* $@103  */
  YYSYMBOL_591_104 = 591,                  /* $@104  */
  YYSYMBOL_592_105 = 592,                  /* $@105  */
  YYSYMBOL_593_106 = 593,                  /* $@106  */
  YYSYMBOL_594_107 = 594,                  /* $@107  */
  YYSYMBOL_595_108 = 595,                  /* $@108  */
  YYSYMBOL_596_109 = 596,                  /* $@109  */
  YYSYMBOL_597_110 = 597,                  /* $@110  */
  YYSYMBOL_598_111 = 598,                  /* $@111  */
  YYSYMBOL_599_112 = 599,                  /* $@112  */
  YYSYMBOL_600_113 = 600,                  /* $@113  */
  YYSYMBOL_601_114 = 601,                  /* $@114  */
  YYSYMBOL_602_115 = 602,                  /* $@115  */
  YYSYMBOL_603_116 = 603,                  /* $@116  */
  YYSYMBOL_labelcolors = 604,              /* labelcolors  */
  YYSYMBOL_605_117 = 605,                  /* $@117  */
  YYSYMBOL_606_118 = 606,                  /* $@118  */
  YYSYMBOL_labelcolor = 607,               /* labelcolor  */
  YYSYMBOL_608_119 = 608,                  /* $@119  */
  YYSYMBOL_609_120 = 609,                  /* $@120  */
  YYSYMBOL_610_121 = 610,                  /* $@121  */
  YYSYMBOL_faces = 611,                    /* faces  */
  YYSYMBOL_612_122 = 612,                  /* $@122  */
  YYSYMBOL_613_123 = 613,                  /* $@123  */
  YYSYMBOL_face = 614,                     /* face  */
  YYSYMBOL_615_124 = 615,                  /* $@124  */
  YYSYMBOL_616_125 = 616,                  /* $@125  */
  YYSYMBOL_startup = 617,                  /* startup  */
  YYSYMBOL_618_126 = 618,                  /* $@126  */
  YYSYMBOL_619_127 = 619,                  /* $@127  */
  YYSYMBOL_620_128 = 620,                  /* $@128  */
  YYSYMBOL_621_129 = 621,                  /* $@129  */
  YYSYMBOL_622_130 = 622,                  /* $@130  */
  YYSYMBOL_623_131 = 623,                  /* $@131  */
  YYSYMBOL_624_132 = 624,                  /* $@132  */
  YYSYMBOL_625_133 = 625,                  /* $@133  */
  YYSYMBOL_626_134 = 626,                  /* $@134  */
  YYSYMBOL_paths = 627,                    /* paths  */
  YYSYMBOL_628_135 = 628,                  /* $@135  */
  YYSYMBOL_629_136 = 629,                  /* $@136  */
  YYSYMBOL_path = 630,                     /* path  */
  YYSYMBOL_631_137 = 631,                  /* $@137  */
  YYSYMBOL_632_138 = 632,                  /* $@138  */
  YYSYMBOL_633_139 = 633,                  /* $@139  */
  YYSYMBOL_634_140 = 634,                  /* $@140  */
  YYSYMBOL_635_141 = 635,                  /* $@141  */
  YYSYMBOL_636_142 = 636,                  /* $@142  */
  YYSYMBOL_keylist = 637,                  /* keylist  */
  YYSYMBOL_638_143 = 638,                  /* $@143  */
  YYSYMBOL_639_144 = 639,                  /* $@144  */
  YYSYMBOL_640_145 = 640,                  /* $@145  */
  YYSYMBOL_641_146 = 641,                  /* $@146  */
  YYSYMBOL_642_147 = 642,                  /* $@147  */
  YYSYMBOL_643_148 = 643,                  /* $@148  */
  YYSYMBOL_644_149 = 644,                  /* $@149  */
  YYSYMBOL_645_150 = 645,                  /* $@150  */
  YYSYMBOL_646_151 = 646,                  /* $@151  */
  YYSYMBOL_647_152 = 647,                  /* $@152  */
  YYSYMBOL_mods = 648,                     /* mods  */
  YYSYMBOL_649_153 = 649,                  /* $@153  */
  YYSYMBOL_650_154 = 650,                  /* $@154  */
  YYSYMBOL_651_155 = 651,                  /* $@155  */
  YYSYMBOL_652_156 = 652,                  /* $@156  */
  YYSYMBOL_653_157 = 653,                  /* $@157  */
  YYSYMBOL_654_158 = 654,                  /* $@158  */
  YYSYMBOL_655_159 = 655,                  /* $@159  */
  YYSYMBOL_656_160 = 656,                  /* $@160  */
  YYSYMBOL_filetypes = 657,                /* filetypes  */
  YYSYMBOL_658_161 = 658,                  /* $@161  */
  YYSYMBOL_659_162 = 659,                  /* $@162  */
  YYSYMBOL_660_163 = 660,                  /* $@163  */
  YYSYMBOL_661_164 = 661,                  /* $@164  */
  YYSYMBOL_662_165 = 662,                  /* $@165  */
  YYSYMBOL_663_166 = 663,                  /* $@166  */
  YYSYMBOL_664_167 = 664,                  /* $@167  */
  YYSYMBOL_ignoredirs = 665,               /* ignoredirs  */
  YYSYMBOL_666_168 = 666,                  /* $@168  */
  YYSYMBOL_ignoredirs2 = 667,              /* ignoredirs2  */
  YYSYMBOL_668_169 = 668,                  /* $@169  */
  YYSYMBOL_669_170 = 669,                  /* $@170  */
  YYSYMBOL_ignoredir = 670,                /* ignoredir  */
  YYSYMBOL_671_171 = 671,                  /* $@171  */
  YYSYMBOL_subfiletypes = 672,             /* subfiletypes  */
  YYSYMBOL_673_172 = 673,                  /* $@172  */
  YYSYMBOL_674_173 = 674,                  /* $@173  */
  YYSYMBOL_filetype = 675,                 /* filetype  */
  YYSYMBOL_676_174 = 676,                  /* $@174  */
  YYSYMBOL_677_175 = 677,                  /* $@175  */
  YYSYMBOL_678_176 = 678,                  /* $@176  */
  YYSYMBOL_679_177 = 679,                  /* $@177  */
  YYSYMBOL_680_178 = 680,                  /* $@178  */
  YYSYMBOL_681_179 = 681,                  /* $@179  */
  YYSYMBOL_682_180 = 682,                  /* $@180  */
  YYSYMBOL_683_181 = 683,                  /* $@181  */
  YYSYMBOL_684_182 = 684,                  /* $@182  */
  YYSYMBOL_685_183 = 685,                  /* $@183  */
  YYSYMBOL_686_184 = 686,                  /* $@184  */
  YYSYMBOL_687_185 = 687,                  /* $@185  */
  YYSYMBOL_688_186 = 688,                  /* $@186  */
  YYSYMBOL_689_187 = 689,                  /* $@187  */
  YYSYMBOL_690_188 = 690,                  /* $@188  */
  YYSYMBOL_691_189 = 691,                  /* $@189  */
  YYSYMBOL_692_190 = 692,                  /* $@190  */
  YYSYMBOL_693_191 = 693,                  /* $@191  */
  YYSYMBOL_694_192 = 694,                  /* $@192  */
  YYSYMBOL_695_193 = 695,                  /* $@193  */
  YYSYMBOL_696_194 = 696,                  /* $@194  */
  YYSYMBOL_697_195 = 697,                  /* $@195  */
  YYSYMBOL_698_196 = 698,                  /* $@196  */
  YYSYMBOL_699_197 = 699,                  /* $@197  */
  YYSYMBOL_700_198 = 700,                  /* $@198  */
  YYSYMBOL_701_199 = 701,                  /* $@199  */
  YYSYMBOL_702_200 = 702,                  /* $@200  */
  YYSYMBOL_filecontent = 703,              /* filecontent  */
  YYSYMBOL_704_201 = 704,                  /* $@201  */
  YYSYMBOL_ft_type = 705,                  /* ft_type  */
  YYSYMBOL_ftcommands = 706,               /* ftcommands  */
  YYSYMBOL_707_202 = 707,                  /* $@202  */
  YYSYMBOL_708_203 = 708,                  /* $@203  */
  YYSYMBOL_709_204 = 709,                  /* $@204  */
  YYSYMBOL_710_205 = 710,                  /* $@205  */
  YYSYMBOL_711_206 = 711,                  /* $@206  */
  YYSYMBOL_712_207 = 712,                  /* $@207  */
  YYSYMBOL_713_208 = 713,                  /* $@208  */
  YYSYMBOL_714_209 = 714,                  /* $@209  */
  YYSYMBOL_715_210 = 715,                  /* $@210  */
  YYSYMBOL_716_211 = 716,                  /* $@211  */
  YYSYMBOL_717_212 = 717,                  /* $@212  */
  YYSYMBOL_718_213 = 718,                  /* $@213  */
  YYSYMBOL_commands = 719,                 /* commands  */
  YYSYMBOL_flags = 720,                    /* flags  */
  YYSYMBOL_hotkeys = 721,                  /* hotkeys  */
  YYSYMBOL_722_214 = 722,                  /* $@214  */
  YYSYMBOL_723_215 = 723,                  /* $@215  */
  YYSYMBOL_hotkey = 724,                   /* hotkey  */
  YYSYMBOL_725_216 = 725,                  /* $@216  */
  YYSYMBOL_726_217 = 726,                  /* $@217  */
  YYSYMBOL_727_218 = 727,                  /* $@218  */
  YYSYMBOL_728_219 = 728,                  /* $@219  */
  YYSYMBOL_729_220 = 729,                  /* $@220  */
  YYSYMBOL_buttons = 730,                  /* buttons  */
  YYSYMBOL_731_221 = 731,                  /* $@221  */
  YYSYMBOL_732_222 = 732,                  /* $@222  */
  YYSYMBOL_button = 733,                   /* button  */
  YYSYMBOL_734_223 = 734,                  /* $@223  */
  YYSYMBOL_735_224 = 735,                  /* $@224  */
  YYSYMBOL_736_225 = 736,                  /* $@225  */
  YYSYMBOL_737_226 = 737,                  /* $@226  */
  YYSYMBOL_738_227 = 738,                  /* $@227  */
  YYSYMBOL_739_228 = 739,                  /* $@228  */
  YYSYMBOL_740_229 = 740,                  /* $@229  */
  YYSYMBOL_fonts = 741,                    /* fonts  */
  YYSYMBOL_742_230 = 742,                  /* $@230  */
  YYSYMBOL_743_231 = 743,                  /* $@231  */
  YYSYMBOL_744_232 = 744,                  /* $@232  */
  YYSYMBOL_745_233 = 745,                  /* $@233  */
  YYSYMBOL_746_234 = 746,                  /* $@234  */
  YYSYMBOL_747_235 = 747,                  /* $@235  */
  YYSYMBOL_748_236 = 748,                  /* $@236  */
  YYSYMBOL_xftfonts = 749,                 /* xftfonts  */
  YYSYMBOL_750_237 = 750,                  /* $@237  */
  YYSYMBOL_751_238 = 751,                  /* $@238  */
  YYSYMBOL_752_239 = 752,                  /* $@239  */
  YYSYMBOL_753_240 = 753,                  /* $@240  */
  YYSYMBOL_754_241 = 754,                  /* $@241  */
  YYSYMBOL_755_242 = 755,                  /* $@242  */
  YYSYMBOL_756_243 = 756,                  /* $@243  */
  YYSYMBOL_volumemanager = 757,            /* volumemanager  */
  YYSYMBOL_758_244 = 758,                  /* $@244  */
  YYSYMBOL_759_245 = 759,                  /* $@245  */
  YYSYMBOL_760_246 = 760,                  /* $@246  */
  YYSYMBOL_761_247 = 761,                  /* $@247  */
  YYSYMBOL_762_248 = 762,                  /* $@248  */
  YYSYMBOL_763_249 = 763,                  /* $@249  */
  YYSYMBOL_764_250 = 764,                  /* $@250  */
  YYSYMBOL_765_251 = 765,                  /* $@251  */
  YYSYMBOL_766_252 = 766,                  /* $@252  */
  YYSYMBOL_clockbarsets = 767,             /* clockbarsets  */
  YYSYMBOL_768_253 = 768,                  /* $@253  */
  YYSYMBOL_769_254 = 769,                  /* $@254  */
  YYSYMBOL_770_255 = 770,                  /* $@255  */
  YYSYMBOL_771_256 = 771,                  /* $@256  */
  YYSYMBOL_772_257 = 772,                  /* $@257  */
  YYSYMBOL_773_258 = 773,                  /* $@258  */
  YYSYMBOL_774_259 = 774,                  /* $@259  */
  YYSYMBOL_bool = 775,                     /* bool  */
  YYSYMBOL_command = 776,                  /* command  */
  YYSYMBOL_ownop = 777,                    /* ownop  */
  YYSYMBOL_778_260 = 778,                  /* $@260  */
  YYSYMBOL_ownopbody = 779,                /* ownopbody  */
  YYSYMBOL_780_261 = 780,                  /* $@261  */
  YYSYMBOL_781_262 = 781,                  /* $@262  */
  YYSYMBOL_782_263 = 782,                  /* $@263  */
  YYSYMBOL_783_264 = 783,                  /* $@264  */
  YYSYMBOL_784_265 = 784,                  /* $@265  */
  YYSYMBOL_785_266 = 785,                  /* $@266  */
  YYSYMBOL_786_267 = 786,                  /* $@267  */
  YYSYMBOL_787_268 = 787,                  /* $@268  */
  YYSYMBOL_788_269 = 788,                  /* $@269  */
  YYSYMBOL_789_270 = 789,                  /* $@270  */
  YYSYMBOL_790_271 = 790,                  /* $@271  */
  YYSYMBOL_791_272 = 791,                  /* $@272  */
  YYSYMBOL_792_273 = 792,                  /* $@273  */
  YYSYMBOL_793_274 = 793,                  /* $@274  */
  YYSYMBOL_794_275 = 794,                  /* $@275  */
  YYSYMBOL_795_276 = 795,                  /* $@276  */
  YYSYMBOL_796_277 = 796,                  /* $@277  */
  YYSYMBOL_797_278 = 797,                  /* $@278  */
  YYSYMBOL_dndaction = 798,                /* dndaction  */
  YYSYMBOL_799_279 = 799,                  /* $@279  */
  YYSYMBOL_dcaction = 800,                 /* dcaction  */
  YYSYMBOL_801_280 = 801,                  /* $@280  */
  YYSYMBOL_showaction = 802,               /* showaction  */
  YYSYMBOL_803_281 = 803,                  /* $@281  */
  YYSYMBOL_rshowaction = 804,              /* rshowaction  */
  YYSYMBOL_805_282 = 805,                  /* $@282  */
  YYSYMBOL_useraction = 806,               /* useraction  */
  YYSYMBOL_807_283 = 807,                  /* $@283  */
  YYSYMBOL_useractionbody = 808,           /* useractionbody  */
  YYSYMBOL_809_284 = 809,                  /* $@284  */
  YYSYMBOL_rowup = 810,                    /* rowup  */
  YYSYMBOL_811_285 = 811,                  /* $@285  */
  YYSYMBOL_rowdown = 812,                  /* rowdown  */
  YYSYMBOL_813_286 = 813,                  /* $@286  */
  YYSYMBOL_changehiddenflag = 814,         /* changehiddenflag  */
  YYSYMBOL_815_287 = 815,                  /* $@287  */
  YYSYMBOL_changehiddenflagbody = 816,     /* changehiddenflagbody  */
  YYSYMBOL_817_288 = 817,                  /* $@288  */
  YYSYMBOL_818_289 = 818,                  /* $@289  */
  YYSYMBOL_819_290 = 819,                  /* $@290  */
  YYSYMBOL_copyop = 820,                   /* copyop  */
  YYSYMBOL_821_291 = 821,                  /* $@291  */
  YYSYMBOL_copyopbody = 822,               /* copyopbody  */
  YYSYMBOL_823_292 = 823,                  /* $@292  */
  YYSYMBOL_824_293 = 824,                  /* $@293  */
  YYSYMBOL_825_294 = 825,                  /* $@294  */
  YYSYMBOL_826_295 = 826,                  /* $@295  */
  YYSYMBOL_827_296 = 827,                  /* $@296  */
  YYSYMBOL_828_297 = 828,                  /* $@297  */
  YYSYMBOL_829_298 = 829,                  /* $@298  */
  YYSYMBOL_830_299 = 830,                  /* $@299  */
  YYSYMBOL_831_300 = 831,                  /* $@300  */
  YYSYMBOL_832_301 = 832,                  /* $@301  */
  YYSYMBOL_833_302 = 833,                  /* $@302  */
  YYSYMBOL_834_303 = 834,                  /* $@303  */
  YYSYMBOL_835_304 = 835,                  /* $@304  */
  YYSYMBOL_836_305 = 836,                  /* $@305  */
  YYSYMBOL_837_306 = 837,                  /* $@306  */
  YYSYMBOL_838_307 = 838,                  /* $@307  */
  YYSYMBOL_839_308 = 839,                  /* $@308  */
  YYSYMBOL_840_309 = 840,                  /* $@309  */
  YYSYMBOL_841_310 = 841,                  /* $@310  */
  YYSYMBOL_842_311 = 842,                  /* $@311  */
  YYSYMBOL_firstrow = 843,                 /* firstrow  */
  YYSYMBOL_844_312 = 844,                  /* $@312  */
  YYSYMBOL_lastrow = 845,                  /* lastrow  */
  YYSYMBOL_846_313 = 846,                  /* $@313  */
  YYSYMBOL_pageup = 847,                   /* pageup  */
  YYSYMBOL_848_314 = 848,                  /* $@314  */
  YYSYMBOL_pagedown = 849,                 /* pagedown  */
  YYSYMBOL_850_315 = 850,                  /* $@315  */
  YYSYMBOL_selectop = 851,                 /* selectop  */
  YYSYMBOL_852_316 = 852,                  /* $@316  */
  YYSYMBOL_selectallop = 853,              /* selectallop  */
  YYSYMBOL_854_317 = 854,                  /* $@317  */
  YYSYMBOL_selectnoneop = 855,             /* selectnoneop  */
  YYSYMBOL_856_318 = 856,                  /* $@318  */
  YYSYMBOL_invertallop = 857,              /* invertallop  */
  YYSYMBOL_858_319 = 858,                  /* $@319  */
  YYSYMBOL_parentdirop = 859,              /* parentdirop  */
  YYSYMBOL_860_320 = 860,                  /* $@320  */
  YYSYMBOL_enterdirop = 861,               /* enterdirop  */
  YYSYMBOL_862_321 = 862,                  /* $@321  */
  YYSYMBOL_enterdiropbody = 863,           /* enterdiropbody  */
  YYSYMBOL_864_322 = 864,                  /* $@322  */
  YYSYMBOL_865_323 = 865,                  /* $@323  */
  YYSYMBOL_866_324 = 866,                  /* $@324  */
  YYSYMBOL_867_325 = 867,                  /* $@325  */
  YYSYMBOL_868_326 = 868,                  /* $@326  */
  YYSYMBOL_changelistersetop = 869,        /* changelistersetop  */
  YYSYMBOL_870_327 = 870,                  /* $@327  */
  YYSYMBOL_changelistersetopbody = 871,    /* changelistersetopbody  */
  YYSYMBOL_872_328 = 872,                  /* $@328  */
  YYSYMBOL_873_329 = 873,                  /* $@329  */
  YYSYMBOL_874_330 = 874,                  /* $@330  */
  YYSYMBOL_875_331 = 875,                  /* $@331  */
  YYSYMBOL_switchlisterop = 876,           /* switchlisterop  */
  YYSYMBOL_877_332 = 877,                  /* $@332  */
  YYSYMBOL_filterselectop = 878,           /* filterselectop  */
  YYSYMBOL_879_333 = 879,                  /* $@333  */
  YYSYMBOL_filterselectopbody = 880,       /* filterselectopbody  */
  YYSYMBOL_881_334 = 881,                  /* $@334  */
  YYSYMBOL_882_335 = 882,                  /* $@335  */
  YYSYMBOL_883_336 = 883,                  /* $@336  */
  YYSYMBOL_filterunselectop = 884,         /* filterunselectop  */
  YYSYMBOL_885_337 = 885,                  /* $@337  */
  YYSYMBOL_filterunselectopbody = 886,     /* filterunselectopbody  */
  YYSYMBOL_887_338 = 887,                  /* $@338  */
  YYSYMBOL_888_339 = 888,                  /* $@339  */
  YYSYMBOL_889_340 = 889,                  /* $@340  */
  YYSYMBOL_pathtoothersideop = 890,        /* pathtoothersideop  */
  YYSYMBOL_891_341 = 891,                  /* $@341  */
  YYSYMBOL_quitop = 892,                   /* quitop  */
  YYSYMBOL_893_342 = 893,                  /* $@342  */
  YYSYMBOL_quitopbody = 894,               /* quitopbody  */
  YYSYMBOL_895_343 = 895,                  /* $@343  */
  YYSYMBOL_896_344 = 896,                  /* $@344  */
  YYSYMBOL_deleteop = 897,                 /* deleteop  */
  YYSYMBOL_898_345 = 898,                  /* $@345  */
  YYSYMBOL_deleteopbody = 899,             /* deleteopbody  */
  YYSYMBOL_900_346 = 900,                  /* $@346  */
  YYSYMBOL_reloadop = 901,                 /* reloadop  */
  YYSYMBOL_902_347 = 902,                  /* $@347  */
  YYSYMBOL_reloadopbody = 903,             /* reloadopbody  */
  YYSYMBOL_904_348 = 904,                  /* $@348  */
  YYSYMBOL_905_349 = 905,                  /* $@349  */
  YYSYMBOL_906_350 = 906,                  /* $@350  */
  YYSYMBOL_907_351 = 907,                  /* $@351  */
  YYSYMBOL_908_352 = 908,                  /* $@352  */
  YYSYMBOL_909_353 = 909,                  /* $@353  */
  YYSYMBOL_makedirop = 910,                /* makedirop  */
  YYSYMBOL_911_354 = 911,                  /* $@354  */
  YYSYMBOL_renameop = 912,                 /* renameop  */
  YYSYMBOL_913_355 = 913,                  /* $@355  */
  YYSYMBOL_dirsizeop = 914,                /* dirsizeop  */
  YYSYMBOL_915_356 = 915,                  /* $@356  */
  YYSYMBOL_simddop = 916,                  /* simddop  */
  YYSYMBOL_917_357 = 917,                  /* $@357  */
  YYSYMBOL_startprogop = 918,              /* startprogop  */
  YYSYMBOL_919_358 = 919,                  /* $@358  */
  YYSYMBOL_startprogopbody = 920,          /* startprogopbody  */
  YYSYMBOL_921_359 = 921,                  /* $@359  */
  YYSYMBOL_922_360 = 922,                  /* $@360  */
  YYSYMBOL_923_361 = 923,                  /* $@361  */
  YYSYMBOL_924_362 = 924,                  /* $@362  */
  YYSYMBOL_925_363 = 925,                  /* $@363  */
  YYSYMBOL_926_364 = 926,                  /* $@364  */
  YYSYMBOL_927_365 = 927,                  /* $@365  */
  YYSYMBOL_928_366 = 928,                  /* $@366  */
  YYSYMBOL_929_367 = 929,                  /* $@367  */
  YYSYMBOL_930_368 = 930,                  /* $@368  */
  YYSYMBOL_931_369 = 931,                  /* $@369  */
  YYSYMBOL_932_370 = 932,                  /* $@370  */
  YYSYMBOL_933_371 = 933,                  /* $@371  */
  YYSYMBOL_934_372 = 934,                  /* $@372  */
  YYSYMBOL_935_373 = 935,                  /* $@373  */
  YYSYMBOL_searchentryop = 936,            /* searchentryop  */
  YYSYMBOL_937_374 = 937,                  /* $@374  */
  YYSYMBOL_searchentryopbody = 938,        /* searchentryopbody  */
  YYSYMBOL_939_375 = 939,                  /* $@375  */
  YYSYMBOL_940_376 = 940,                  /* $@376  */
  YYSYMBOL_941_377 = 941,                  /* $@377  */
  YYSYMBOL_942_378 = 942,                  /* $@378  */
  YYSYMBOL_enterpathop = 943,              /* enterpathop  */
  YYSYMBOL_944_379 = 944,                  /* $@379  */
  YYSYMBOL_enterpathopbody = 945,          /* enterpathopbody  */
  YYSYMBOL_946_380 = 946,                  /* $@380  */
  YYSYMBOL_947_381 = 947,                  /* $@381  */
  YYSYMBOL_948_382 = 948,                  /* $@382  */
  YYSYMBOL_949_383 = 949,                  /* $@383  */
  YYSYMBOL_scrolllisterop = 950,           /* scrolllisterop  */
  YYSYMBOL_951_384 = 951,                  /* $@384  */
  YYSYMBOL_scrolllisteropbody = 952,       /* scrolllisteropbody  */
  YYSYMBOL_953_385 = 953,                  /* $@385  */
  YYSYMBOL_954_386 = 954,                  /* $@386  */
  YYSYMBOL_createsymlinkop = 955,          /* createsymlinkop  */
  YYSYMBOL_956_387 = 956,                  /* $@387  */
  YYSYMBOL_createsymlinkopbody = 957,      /* createsymlinkopbody  */
  YYSYMBOL_958_388 = 958,                  /* $@388  */
  YYSYMBOL_959_389 = 959,                  /* $@389  */
  YYSYMBOL_960_390 = 960,                  /* $@390  */
  YYSYMBOL_changesymlinkop = 961,          /* changesymlinkop  */
  YYSYMBOL_962_391 = 962,                  /* $@391  */
  YYSYMBOL_changesymlinkopbody = 963,      /* changesymlinkopbody  */
  YYSYMBOL_964_392 = 964,                  /* $@392  */
  YYSYMBOL_965_393 = 965,                  /* $@393  */
  YYSYMBOL_966_394 = 966,                  /* $@394  */
  YYSYMBOL_chmodop = 967,                  /* chmodop  */
  YYSYMBOL_968_395 = 968,                  /* $@395  */
  YYSYMBOL_chmodopbody = 969,              /* chmodopbody  */
  YYSYMBOL_970_396 = 970,                  /* $@396  */
  YYSYMBOL_971_397 = 971,                  /* $@397  */
  YYSYMBOL_972_398 = 972,                  /* $@398  */
  YYSYMBOL_973_399 = 973,                  /* $@399  */
  YYSYMBOL_974_400 = 974,                  /* $@400  */
  YYSYMBOL_975_401 = 975,                  /* $@401  */
  YYSYMBOL_976_402 = 976,                  /* $@402  */
  YYSYMBOL_977_403 = 977,                  /* $@403  */
  YYSYMBOL_978_404 = 978,                  /* $@404  */
  YYSYMBOL_chtimeop = 979,                 /* chtimeop  */
  YYSYMBOL_980_405 = 980,                  /* $@405  */
  YYSYMBOL_chtimeopbody = 981,             /* chtimeopbody  */
  YYSYMBOL_982_406 = 982,                  /* $@406  */
  YYSYMBOL_983_407 = 983,                  /* $@407  */
  YYSYMBOL_984_408 = 984,                  /* $@408  */
  YYSYMBOL_985_409 = 985,                  /* $@409  */
  YYSYMBOL_togglelistermodeop = 986,       /* togglelistermodeop  */
  YYSYMBOL_987_410 = 987,                  /* $@410  */
  YYSYMBOL_togglelistermodeopbody = 988,   /* togglelistermodeopbody  */
  YYSYMBOL_989_411 = 989,                  /* $@411  */
  YYSYMBOL_setsortmodeop = 990,            /* setsortmodeop  */
  YYSYMBOL_991_412 = 991,                  /* $@412  */
  YYSYMBOL_setsortmodeopbody = 992,        /* setsortmodeopbody  */
  YYSYMBOL_993_413 = 993,                  /* $@413  */
  YYSYMBOL_994_414 = 994,                  /* $@414  */
  YYSYMBOL_995_415 = 995,                  /* $@415  */
  YYSYMBOL_996_416 = 996,                  /* $@416  */
  YYSYMBOL_997_417 = 997,                  /* $@417  */
  YYSYMBOL_998_418 = 998,                  /* $@418  */
  YYSYMBOL_999_419 = 999,                  /* $@419  */
  YYSYMBOL_1000_420 = 1000,                /* $@420  */
  YYSYMBOL_1001_421 = 1001,                /* $@421  */
  YYSYMBOL_1002_422 = 1002,                /* $@422  */
  YYSYMBOL_1003_423 = 1003,                /* $@423  */
  YYSYMBOL_1004_424 = 1004,                /* $@424  */
  YYSYMBOL_1005_425 = 1005,                /* $@425  */
  YYSYMBOL_1006_426 = 1006,                /* $@426  */
  YYSYMBOL_1007_427 = 1007,                /* $@427  */
  YYSYMBOL_setfilterop = 1008,             /* setfilterop  */
  YYSYMBOL_1009_428 = 1009,                /* $@428  */
  YYSYMBOL_setfilteropbody = 1010,         /* setfilteropbody  */
  YYSYMBOL_1011_429 = 1011,                /* $@429  */
  YYSYMBOL_1012_430 = 1012,                /* $@430  */
  YYSYMBOL_1013_431 = 1013,                /* $@431  */
  YYSYMBOL_1014_432 = 1014,                /* $@432  */
  YYSYMBOL_1015_433 = 1015,                /* $@433  */
  YYSYMBOL_1016_434 = 1016,                /* $@434  */
  YYSYMBOL_1017_435 = 1017,                /* $@435  */
  YYSYMBOL_1018_436 = 1018,                /* $@436  */
  YYSYMBOL_1019_437 = 1019,                /* $@437  */
  YYSYMBOL_1020_438 = 1020,                /* $@438  */
  YYSYMBOL_1021_439 = 1021,                /* $@439  */
  YYSYMBOL_1022_440 = 1022,                /* $@440  */
  YYSYMBOL_1023_441 = 1023,                /* $@441  */
  YYSYMBOL_1024_442 = 1024,                /* $@442  */
  YYSYMBOL_1025_443 = 1025,                /* $@443  */
  YYSYMBOL_shortkeyfromlistop = 1026,      /* shortkeyfromlistop  */
  YYSYMBOL_1027_444 = 1027,                /* $@444  */
  YYSYMBOL_chownop = 1028,                 /* chownop  */
  YYSYMBOL_1029_445 = 1029,                /* $@445  */
  YYSYMBOL_chownopbody = 1030,             /* chownopbody  */
  YYSYMBOL_1031_446 = 1031,                /* $@446  */
  YYSYMBOL_1032_447 = 1032,                /* $@447  */
  YYSYMBOL_1033_448 = 1033,                /* $@448  */
  YYSYMBOL_1034_449 = 1034,                /* $@449  */
  YYSYMBOL_scriptop = 1035,                /* scriptop  */
  YYSYMBOL_1036_450 = 1036,                /* $@450  */
  YYSYMBOL_scriptopbody = 1037,            /* scriptopbody  */
  YYSYMBOL_1038_451 = 1038,                /* $@451  */
  YYSYMBOL_1039_452 = 1039,                /* $@452  */
  YYSYMBOL_1040_453 = 1040,                /* $@453  */
  YYSYMBOL_1041_454 = 1041,                /* $@454  */
  YYSYMBOL_1042_455 = 1042,                /* $@455  */
  YYSYMBOL_1043_456 = 1043,                /* $@456  */
  YYSYMBOL_1044_457 = 1044,                /* $@457  */
  YYSYMBOL_1045_458 = 1045,                /* $@458  */
  YYSYMBOL_1046_459 = 1046,                /* $@459  */
  YYSYMBOL_1047_460 = 1047,                /* $@460  */
  YYSYMBOL_1048_461 = 1048,                /* $@461  */
  YYSYMBOL_1049_462 = 1049,                /* $@462  */
  YYSYMBOL_1050_463 = 1050,                /* $@463  */
  YYSYMBOL_1051_464 = 1051,                /* $@464  */
  YYSYMBOL_1052_465 = 1052,                /* $@465  */
  YYSYMBOL_1053_466 = 1053,                /* $@466  */
  YYSYMBOL_1054_467 = 1054,                /* $@467  */
  YYSYMBOL_1055_468 = 1055,                /* $@468  */
  YYSYMBOL_1056_469 = 1056,                /* $@469  */
  YYSYMBOL_1057_470 = 1057,                /* $@470  */
  YYSYMBOL_1058_471 = 1058,                /* $@471  */
  YYSYMBOL_1059_472 = 1059,                /* $@472  */
  YYSYMBOL_1060_473 = 1060,                /* $@473  */
  YYSYMBOL_1061_474 = 1061,                /* $@474  */
  YYSYMBOL_1062_475 = 1062,                /* $@475  */
  YYSYMBOL_1063_476 = 1063,                /* $@476  */
  YYSYMBOL_1064_477 = 1064,                /* $@477  */
  YYSYMBOL_1065_478 = 1065,                /* $@478  */
  YYSYMBOL_1066_479 = 1066,                /* $@479  */
  YYSYMBOL_1067_480 = 1067,                /* $@480  */
  YYSYMBOL_showdircacheop = 1068,          /* showdircacheop  */
  YYSYMBOL_1069_481 = 1069,                /* $@481  */
  YYSYMBOL_parentactionop = 1070,          /* parentactionop  */
  YYSYMBOL_1071_482 = 1071,                /* $@482  */
  YYSYMBOL_nooperationop = 1072,           /* nooperationop  */
  YYSYMBOL_1073_483 = 1073,                /* $@483  */
  YYSYMBOL_goftpop = 1074,                 /* goftpop  */
  YYSYMBOL_1075_484 = 1075,                /* $@484  */
  YYSYMBOL_goftpopbody = 1076,             /* goftpopbody  */
  YYSYMBOL_1077_485 = 1077,                /* $@485  */
  YYSYMBOL_1078_486 = 1078,                /* $@486  */
  YYSYMBOL_1079_487 = 1079,                /* $@487  */
  YYSYMBOL_1080_488 = 1080,                /* $@488  */
  YYSYMBOL_1081_489 = 1081,                /* $@489  */
  YYSYMBOL_1082_490 = 1082,                /* $@490  */
  YYSYMBOL_1083_491 = 1083,                /* $@491  */
  YYSYMBOL_internalviewop = 1084,          /* internalviewop  */
  YYSYMBOL_1085_492 = 1085,                /* $@492  */
  YYSYMBOL_internalviewopbody = 1086,      /* internalviewopbody  */
  YYSYMBOL_1087_493 = 1087,                /* $@493  */
  YYSYMBOL_1088_494 = 1088,                /* $@494  */
  YYSYMBOL_1089_495 = 1089,                /* $@495  */
  YYSYMBOL_1090_496 = 1090,                /* $@496  */
  YYSYMBOL_clipboardop = 1091,             /* clipboardop  */
  YYSYMBOL_1092_497 = 1092,                /* $@497  */
  YYSYMBOL_clipboardopbody = 1093,         /* clipboardopbody  */
  YYSYMBOL_1094_498 = 1094,                /* $@498  */
  YYSYMBOL_searchop = 1095,                /* searchop  */
  YYSYMBOL_1096_499 = 1096,                /* $@499  */
  YYSYMBOL_searchopbody = 1097,            /* searchopbody  */
  YYSYMBOL_1098_500 = 1098,                /* $@500  */
  YYSYMBOL_1099_501 = 1099,                /* $@501  */
  YYSYMBOL_dirbookmarkop = 1100,           /* dirbookmarkop  */
  YYSYMBOL_1101_502 = 1101,                /* $@502  */
  YYSYMBOL_opencontextmenuop = 1102,       /* opencontextmenuop  */
  YYSYMBOL_1103_503 = 1103,                /* $@503  */
  YYSYMBOL_opencontextmenuopbody = 1104,   /* opencontextmenuopbody  */
  YYSYMBOL_1105_504 = 1105,                /* $@504  */
  YYSYMBOL_runcustomaction = 1106,         /* runcustomaction  */
  YYSYMBOL_1107_505 = 1107,                /* $@505  */
  YYSYMBOL_runcustomactionbody = 1108,     /* runcustomactionbody  */
  YYSYMBOL_1109_506 = 1109,                /* $@506  */
  YYSYMBOL_openworkermenuop = 1110,        /* openworkermenuop  */
  YYSYMBOL_1111_507 = 1111,                /* $@507  */
  YYSYMBOL_changelabelop = 1112,           /* changelabelop  */
  YYSYMBOL_1113_508 = 1113,                /* $@508  */
  YYSYMBOL_changelabelopbody = 1114,       /* changelabelopbody  */
  YYSYMBOL_1115_509 = 1115,                /* $@509  */
  YYSYMBOL_1116_510 = 1116,                /* $@510  */
  YYSYMBOL_modifytabsop = 1117,            /* modifytabsop  */
  YYSYMBOL_1118_511 = 1118,                /* $@511  */
  YYSYMBOL_modifytabsopbody = 1119,        /* modifytabsopbody  */
  YYSYMBOL_1120_512 = 1120,                /* $@512  */
  YYSYMBOL_1121_513 = 1121,                /* $@513  */
  YYSYMBOL_1122_514 = 1122,                /* $@514  */
  YYSYMBOL_1123_515 = 1123,                /* $@515  */
  YYSYMBOL_1124_516 = 1124,                /* $@516  */
  YYSYMBOL_1125_517 = 1125,                /* $@517  */
  YYSYMBOL_1126_518 = 1126,                /* $@518  */
  YYSYMBOL_changelayoutop = 1127,          /* changelayoutop  */
  YYSYMBOL_1128_519 = 1128,                /* $@519  */
  YYSYMBOL_changelayoutopbody = 1129,      /* changelayoutopbody  */
  YYSYMBOL_1130_520 = 1130,                /* $@520  */
  YYSYMBOL_1131_521 = 1131,                /* $@521  */
  YYSYMBOL_1132_522 = 1132,                /* $@522  */
  YYSYMBOL_1133_523 = 1133,                /* $@523  */
  YYSYMBOL_1134_524 = 1134,                /* $@524  */
  YYSYMBOL_1135_525 = 1135,                /* $@525  */
  YYSYMBOL_1136_526 = 1136,                /* $@526  */
  YYSYMBOL_1137_527 = 1137,                /* $@527  */
  YYSYMBOL_1138_528 = 1138,                /* $@528  */
  YYSYMBOL_1139_529 = 1139,                /* $@529  */
  YYSYMBOL_1140_530 = 1140,                /* $@530  */
  YYSYMBOL_1141_531 = 1141,                /* $@531  */
  YYSYMBOL_1142_532 = 1142,                /* $@532  */
  YYSYMBOL_changecolumnsop = 1143,         /* changecolumnsop  */
  YYSYMBOL_1144_533 = 1144,                /* $@533  */
  YYSYMBOL_changecolumnsopbody = 1145,     /* changecolumnsopbody  */
  YYSYMBOL_1146_534 = 1146,                /* $@534  */
  YYSYMBOL_1147_535 = 1147,                /* $@535  */
  YYSYMBOL_1148_536 = 1148,                /* $@536  */
  YYSYMBOL_1149_537 = 1149,                /* $@537  */
  YYSYMBOL_1150_538 = 1150,                /* $@538  */
  YYSYMBOL_1151_539 = 1151,                /* $@539  */
  YYSYMBOL_1152_540 = 1152,                /* $@540  */
  YYSYMBOL_1153_541 = 1153,                /* $@541  */
  YYSYMBOL_1154_542 = 1154,                /* $@542  */
  YYSYMBOL_1155_543 = 1155,                /* $@543  */
  YYSYMBOL_1156_544 = 1156,                /* $@544  */
  YYSYMBOL_1157_545 = 1157,                /* $@545  */
  YYSYMBOL_1158_546 = 1158,                /* $@546  */
  YYSYMBOL_1159_547 = 1159,                /* $@547  */
  YYSYMBOL_1160_548 = 1160,                /* $@548  */
  YYSYMBOL_volumemanagerop = 1161,         /* volumemanagerop  */
  YYSYMBOL_1162_549 = 1162,                /* $@549  */
  YYSYMBOL_switchbuttonbankop = 1163,      /* switchbuttonbankop  */
  YYSYMBOL_1164_550 = 1164,                /* $@550  */
  YYSYMBOL_switchbuttonbankopbody = 1165,  /* switchbuttonbankopbody  */
  YYSYMBOL_1166_551 = 1166,                /* $@551  */
  YYSYMBOL_1167_552 = 1167,                /* $@552  */
  YYSYMBOL_1168_553 = 1168,                /* $@553  */
  YYSYMBOL_1169_554 = 1169,                /* $@554  */
  YYSYMBOL_pathjumpop = 1170,              /* pathjumpop  */
  YYSYMBOL_1171_555 = 1171,                /* $@555  */
  YYSYMBOL_pathjumpopbody = 1172,          /* pathjumpopbody  */
  YYSYMBOL_1173_556 = 1173,                /* $@556  */
  YYSYMBOL_1174_557 = 1174,                /* $@557  */
  YYSYMBOL_1175_558 = 1175,                /* $@558  */
  YYSYMBOL_1176_559 = 1176,                /* $@559  */
  YYSYMBOL_1177_560 = 1177,                /* $@560  */
  YYSYMBOL_1178_561 = 1178,                /* $@561  */
  YYSYMBOL_1179_562 = 1179,                /* $@562  */
  YYSYMBOL_1180_563 = 1180,                /* $@563  */
  YYSYMBOL_1181_564 = 1181,                /* $@564  */
  YYSYMBOL_commandmenuop = 1182,           /* commandmenuop  */
  YYSYMBOL_1183_565 = 1183,                /* $@565  */
  YYSYMBOL_commandmenuopbody = 1184,       /* commandmenuopbody  */
  YYSYMBOL_1185_566 = 1185,                /* $@566  */
  YYSYMBOL_1186_567 = 1186,                /* $@567  */
  YYSYMBOL_1187_568 = 1187,                /* $@568  */
  YYSYMBOL_1188_569 = 1188,                /* $@569  */
  YYSYMBOL_1189_570 = 1189,                /* $@570  */
  YYSYMBOL_1190_571 = 1190,                /* $@571  */
  YYSYMBOL_1191_572 = 1191,                /* $@572  */
  YYSYMBOL_1192_573 = 1192,                /* $@573  */
  YYSYMBOL_1193_574 = 1193,                /* $@574  */
  YYSYMBOL_viewnewestfilesop = 1194,       /* viewnewestfilesop  */
  YYSYMBOL_1195_575 = 1195,                /* $@575  */
  YYSYMBOL_dircompareop = 1196,            /* dircompareop  */
  YYSYMBOL_1197_576 = 1197,                /* $@576  */
  YYSYMBOL_tabprofilesop = 1198,           /* tabprofilesop  */
  YYSYMBOL_1199_577 = 1199,                /* $@577  */
  YYSYMBOL_externalvdirop = 1200,          /* externalvdirop  */
  YYSYMBOL_1201_578 = 1201,                /* $@578  */
  YYSYMBOL_externalvdiropbody = 1202,      /* externalvdiropbody  */
  YYSYMBOL_1203_579 = 1203,                /* $@579  */
  YYSYMBOL_opentabmenuop = 1204,           /* opentabmenuop  */
  YYSYMBOL_1205_580 = 1205,                /* $@580  */
  YYSYMBOL_directorypresets = 1206,        /* directorypresets  */
  YYSYMBOL_1207_581 = 1207,                /* $@581  */
  YYSYMBOL_1208_582 = 1208,                /* $@582  */
  YYSYMBOL_directorypreset = 1209,         /* directorypreset  */
  YYSYMBOL_1210_583 = 1210,                /* $@583  */
  YYSYMBOL_1211_584 = 1211,                /* $@584  */
  YYSYMBOL_1212_585 = 1212,                /* $@585  */
  YYSYMBOL_1213_586 = 1213,                /* $@586  */
  YYSYMBOL_1214_587 = 1214,                /* $@587  */
  YYSYMBOL_1215_588 = 1215,                /* $@588  */
  YYSYMBOL_1216_589 = 1216,                /* $@589  */
  YYSYMBOL_1217_590 = 1217,                /* $@590  */
  YYSYMBOL_1218_591 = 1218,                /* $@591  */
  YYSYMBOL_1219_592 = 1219,                /* $@592  */
  YYSYMBOL_1220_593 = 1220,                /* $@593  */
  YYSYMBOL_1221_594 = 1221,                /* $@594  */
  YYSYMBOL_1222_595 = 1222,                /* $@595  */
  YYSYMBOL_1223_596 = 1223,                /* $@596  */
  YYSYMBOL_1224_597 = 1224,                /* $@597  */
  YYSYMBOL_1225_598 = 1225,                /* $@598  */
  YYSYMBOL_1226_599 = 1226,                /* $@599  */
  YYSYMBOL_1227_600 = 1227,                /* $@600  */
  YYSYMBOL_1228_601 = 1228,                /* $@601  */
  YYSYMBOL_1229_602 = 1229,                /* $@602  */
  YYSYMBOL_directorypreset_filter = 1230,  /* directorypreset_filter  */
  YYSYMBOL_1231_603 = 1231,                /* $@603  */
  YYSYMBOL_1232_604 = 1232,                /* $@604  */
  YYSYMBOL_1233_605 = 1233,                /* $@605  */
  YYSYMBOL_helpop = 1234,                  /* helpop  */
  YYSYMBOL_1235_606 = 1235,                /* $@606  */
  YYSYMBOL_viewcommandlogop = 1236,        /* viewcommandlogop  */
  YYSYMBOL_1237_607 = 1237,                /* $@607  */
  YYSYMBOL_activatetextviewmodeop = 1238,  /* activatetextviewmodeop  */
  YYSYMBOL_1239_608 = 1239,                /* $@608  */
  YYSYMBOL_activatetextviewmodeopbody = 1240, /* activatetextviewmodeopbody  */
  YYSYMBOL_1241_609 = 1241,                /* $@609  */
  YYSYMBOL_1242_610 = 1242,                /* $@610  */
  YYSYMBOL_1243_611 = 1243,                /* $@611  */
  YYSYMBOL_1244_612 = 1244,                /* $@612  */
  YYSYMBOL_1245_613 = 1245,                /* $@613  */
  YYSYMBOL_1246_614 = 1246,                /* $@614  */
  YYSYMBOL_1247_615 = 1247,                /* $@615  */
  YYSYMBOL_1248_616 = 1248,                /* $@616  */
  YYSYMBOL_1249_617 = 1249                 /* $@617  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2766

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  474
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  776
/* YYNRULES -- Number of rules.  */
#define YYNRULES  1368
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  3247

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   724


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   473,     2,   470,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   471,
       2,   472,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   329,   329,   329,   329,   332,   333,   334,   333,   337,
     338,   339,   338,   350,   351,   350,   362,   363,   362,   374,
     375,   374,   386,   387,   388,   389,   390,   391,   390,   394,
     395,   394,   397,   399,   399,   400,   402,   402,   403,   405,
     405,   406,   406,   407,   407,   408,   408,   409,   410,   410,
     411,   411,   412,   412,   413,   413,   414,   414,   416,   417,
     417,   418,   418,   419,   420,   420,   421,   421,   422,   422,
     423,   423,   424,   424,   425,   426,   426,   427,   429,   429,
     430,   430,   431,   431,   432,   432,   433,   433,   434,   434,
     435,   435,   436,   436,   437,   437,   438,   438,   439,   439,
     440,   440,   441,   441,   442,   444,   444,   445,   447,   447,
     448,   448,   449,   451,   451,   452,   452,   453,   453,   454,
     454,   455,   455,   456,   456,   457,   457,   458,   460,   460,
     461,   461,   462,   462,   463,   463,   464,   464,   465,   465,
     466,   466,   467,   467,   468,   468,   469,   469,   470,   470,
     471,   471,   472,   472,   473,   473,   474,   474,   475,   477,
     477,   478,   478,   479,   479,   480,   480,   481,   481,   482,
     482,   483,   483,   484,   484,   485,   485,   486,   486,   487,
     487,   488,   490,   490,   491,   491,   492,   492,   493,   493,
     494,   494,   495,   495,   496,   497,   496,   498,   499,   498,
     500,   501,   500,   502,   504,   504,   508,   508,   512,   512,
     516,   516,   520,   520,   524,   524,   528,   528,   532,   532,
     536,   536,   540,   540,   544,   544,   548,   548,   552,   552,
     556,   556,   560,   560,   563,   563,   567,   567,   571,   571,
     575,   576,   575,   581,   583,   585,   592,   585,   595,   597,
     597,   601,   601,   605,   605,   606,   608,   612,   608,   615,
     617,   617,   620,   620,   621,   623,   623,   624,   624,   625,
     625,   626,   626,   627,   627,   628,   628,   629,   629,   630,
     630,   631,   631,   632,   634,   635,   634,   641,   643,   643,
     647,   647,   648,   648,   651,   651,   652,   653,   652,   665,
     667,   670,   675,   676,   667,   678,   681,   686,   687,   692,
     693,   678,   695,   697,   697,   698,   698,   699,   699,   700,
     700,   701,   701,   702,   702,   703,   703,   704,   704,   705,
     707,   710,   707,   713,   714,   713,   716,   717,   716,   719,
     719,   720,   722,   722,   723,   725,   726,   725,   727,   729,
     729,   730,   732,   735,   732,   744,   746,   746,   747,   747,
     748,   748,   749,   749,   750,   750,   752,   752,   753,   753,
     754,   754,   755,   755,   756,   757,   757,   758,   758,   759,
     760,   760,   763,   763,   766,   766,   769,   769,   772,   772,
     773,   773,   774,   774,   775,   775,   776,   776,   777,   777,
     778,   778,   779,   779,   780,   780,   781,   781,   782,   782,
     783,   783,   784,   786,   786,   787,   789,   790,   791,   792,
     793,   796,   797,   796,   800,   801,   800,   804,   805,   804,
     808,   809,   808,   812,   813,   812,   816,   819,   816,   822,
     824,   825,   826,   828,   830,   831,   830,   834,   836,   836,
     837,   838,   837,   841,   842,   841,   854,   856,   857,   856,
     863,   865,   865,   869,   869,   870,   870,   873,   874,   873,
     877,   878,   877,   890,   893,   892,   900,   899,   907,   906,
     914,   913,   921,   920,   928,   927,   935,   934,   941,   944,
     943,   951,   950,   958,   957,   965,   964,   972,   971,   979,
     978,   986,   985,   992,   995,   994,   999,   998,  1003,  1002,
    1007,  1006,  1011,  1010,  1015,  1014,  1019,  1018,  1023,  1022,
    1027,  1026,  1030,  1032,  1032,  1033,  1033,  1034,  1034,  1035,
    1035,  1036,  1036,  1037,  1037,  1038,  1038,  1039,  1041,  1042,
    1044,  1045,  1046,  1047,  1048,  1049,  1050,  1051,  1052,  1053,
    1054,  1055,  1056,  1057,  1058,  1059,  1060,  1061,  1062,  1063,
    1064,  1065,  1066,  1067,  1068,  1069,  1070,  1071,  1072,  1073,
    1074,  1075,  1076,  1077,  1078,  1079,  1080,  1081,  1082,  1083,
    1084,  1085,  1086,  1087,  1088,  1089,  1090,  1091,  1092,  1093,
    1094,  1095,  1096,  1097,  1098,  1099,  1100,  1101,  1102,  1103,
    1104,  1105,  1106,  1107,  1108,  1109,  1110,  1111,  1112,  1113,
    1114,  1115,  1116,  1118,  1118,  1121,  1121,  1122,  1122,  1123,
    1123,  1124,  1124,  1125,  1125,  1126,  1126,  1127,  1127,  1128,
    1128,  1129,  1129,  1130,  1130,  1131,  1131,  1132,  1132,  1133,
    1133,  1134,  1134,  1135,  1135,  1136,  1136,  1137,  1137,  1138,
    1138,  1139,  1141,  1141,  1144,  1144,  1147,  1147,  1150,  1150,
    1153,  1153,  1156,  1156,  1157,  1159,  1159,  1166,  1166,  1173,
    1173,  1176,  1176,  1177,  1177,  1178,  1178,  1179,  1181,  1181,
    1184,  1184,  1185,  1185,  1186,  1186,  1187,  1187,  1188,  1188,
    1189,  1189,  1190,  1190,  1191,  1191,  1192,  1192,  1193,  1193,
    1194,  1194,  1195,  1195,  1196,  1196,  1197,  1197,  1198,  1198,
    1199,  1199,  1200,  1200,  1201,  1201,  1202,  1202,  1203,  1203,
    1204,  1206,  1206,  1213,  1213,  1220,  1220,  1227,  1227,  1234,
    1234,  1241,  1241,  1248,  1248,  1255,  1255,  1262,  1262,  1269,
    1269,  1272,  1272,  1273,  1273,  1274,  1274,  1275,  1275,  1276,
    1276,  1277,  1279,  1279,  1282,  1282,  1283,  1283,  1284,  1284,
    1285,  1285,  1286,  1288,  1288,  1291,  1291,  1294,  1294,  1295,
    1295,  1296,  1296,  1297,  1299,  1299,  1302,  1302,  1303,  1303,
    1304,  1304,  1305,  1307,  1307,  1310,  1310,  1313,  1313,  1314,
    1314,  1315,  1317,  1317,  1320,  1320,  1321,  1323,  1323,  1326,
    1326,  1327,  1327,  1328,  1328,  1329,  1329,  1330,  1330,  1331,
    1331,  1332,  1334,  1334,  1337,  1337,  1340,  1340,  1343,  1343,
    1350,  1350,  1353,  1353,  1354,  1354,  1355,  1355,  1356,  1356,
    1357,  1357,  1358,  1358,  1359,  1359,  1360,  1360,  1361,  1361,
    1362,  1362,  1363,  1363,  1364,  1364,  1365,  1365,  1366,  1366,
    1367,  1367,  1368,  1370,  1370,  1373,  1373,  1374,  1374,  1375,
    1375,  1376,  1376,  1377,  1379,  1379,  1382,  1382,  1383,  1383,
    1384,  1384,  1385,  1385,  1386,  1388,  1388,  1391,  1391,  1392,
    1392,  1393,  1395,  1395,  1398,  1398,  1399,  1399,  1400,  1400,
    1401,  1403,  1403,  1406,  1406,  1407,  1407,  1408,  1408,  1409,
    1411,  1411,  1414,  1414,  1415,  1415,  1416,  1416,  1417,  1417,
    1418,  1418,  1419,  1419,  1420,  1420,  1421,  1421,  1422,  1422,
    1423,  1425,  1425,  1428,  1428,  1429,  1429,  1430,  1430,  1431,
    1431,  1432,  1434,  1434,  1437,  1437,  1438,  1440,  1440,  1445,
    1445,  1446,  1446,  1447,  1447,  1448,  1448,  1449,  1449,  1450,
    1450,  1451,  1451,  1452,  1452,  1453,  1453,  1454,  1454,  1455,
    1455,  1456,  1456,  1457,  1457,  1458,  1458,  1459,  1459,  1460,
    1462,  1462,  1465,  1465,  1466,  1466,  1467,  1467,  1468,  1468,
    1469,  1469,  1470,  1470,  1471,  1471,  1472,  1472,  1473,  1473,
    1474,  1474,  1475,  1475,  1476,  1476,  1477,  1477,  1478,  1478,
    1479,  1479,  1480,  1482,  1482,  1485,  1485,  1488,  1488,  1489,
    1489,  1490,  1490,  1491,  1491,  1492,  1494,  1494,  1497,  1497,
    1498,  1498,  1499,  1499,  1500,  1500,  1501,  1501,  1502,  1502,
    1503,  1503,  1504,  1504,  1505,  1505,  1506,  1506,  1507,  1507,
    1508,  1508,  1509,  1509,  1510,  1510,  1511,  1511,  1512,  1512,
    1513,  1513,  1514,  1514,  1515,  1515,  1516,  1516,  1517,  1517,
    1518,  1518,  1519,  1519,  1520,  1520,  1521,  1521,  1522,  1522,
    1523,  1523,  1524,  1524,  1525,  1525,  1526,  1526,  1527,  1529,
    1529,  1532,  1532,  1535,  1535,  1538,  1538,  1541,  1541,  1542,
    1542,  1543,  1543,  1544,  1544,  1545,  1545,  1546,  1546,  1547,
    1547,  1548,  1550,  1550,  1553,  1553,  1554,  1554,  1555,  1555,
    1556,  1556,  1558,  1560,  1560,  1563,  1563,  1564,  1566,  1566,
    1569,  1569,  1570,  1570,  1571,  1573,  1573,  1576,  1576,  1579,
    1579,  1580,  1582,  1582,  1585,  1585,  1586,  1588,  1588,  1591,
    1591,  1594,  1594,  1595,  1595,  1596,  1598,  1598,  1601,  1601,
    1602,  1602,  1603,  1603,  1604,  1604,  1605,  1605,  1606,  1606,
    1607,  1607,  1608,  1610,  1610,  1618,  1618,  1619,  1619,  1620,
    1620,  1621,  1621,  1622,  1622,  1623,  1623,  1624,  1624,  1625,
    1625,  1626,  1626,  1627,  1627,  1628,  1628,  1629,  1629,  1630,
    1630,  1631,  1633,  1633,  1641,  1641,  1642,  1642,  1643,  1643,
    1644,  1644,  1645,  1645,  1646,  1646,  1647,  1647,  1648,  1648,
    1649,  1649,  1650,  1650,  1651,  1651,  1652,  1652,  1653,  1653,
    1654,  1654,  1655,  1655,  1656,  1658,  1658,  1661,  1661,  1664,
    1664,  1665,  1665,  1666,  1666,  1667,  1667,  1668,  1670,  1670,
    1673,  1673,  1674,  1674,  1675,  1675,  1676,  1676,  1677,  1677,
    1678,  1678,  1679,  1679,  1680,  1680,  1681,  1681,  1682,  1684,
    1684,  1687,  1687,  1688,  1688,  1689,  1689,  1690,  1690,  1691,
    1691,  1692,  1692,  1693,  1693,  1694,  1694,  1695,  1695,  1696,
    1698,  1698,  1701,  1701,  1704,  1704,  1707,  1707,  1710,  1710,
    1711,  1713,  1713,  1720,  1721,  1720,  1723,  1725,  1725,  1726,
    1726,  1727,  1727,  1728,  1728,  1729,  1729,  1730,  1730,  1731,
    1731,  1732,  1732,  1733,  1733,  1734,  1734,  1735,  1735,  1736,
    1736,  1737,  1737,  1738,  1738,  1739,  1739,  1740,  1740,  1741,
    1741,  1742,  1742,  1743,  1744,  1743,  1745,  1747,  1747,  1748,
    1748,  1749,  1749,  1750,  1752,  1752,  1757,  1757,  1762,  1762,
    1765,  1765,  1766,  1766,  1767,  1767,  1768,  1768,  1769,  1769,
    1770,  1770,  1771,  1771,  1772,  1772,  1773,  1773,  1774
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "LEFTBRACE_WCP",
  "RIGHTBRACE_WCP", "GLOBAL_WCP", "COLORS_WCP", "LANG_WCP", "PALETTE_WCP",
  "OWNOP_WCP", "LISTERSETS_WCP", "YES_WCP", "NO_WCP", "LEFT_WCP",
  "RIGHT_WCP", "HBARTOP_WCP", "HBARHEIGHT_WCP", "VBARLEFT_WCP",
  "VBARWIDTH_WCP", "DISPLAYSETS_WCP", "NAME_WCP", "SIZE_WCP", "TYPE_WCP",
  "PERMISSION_WCP", "OWNER_WCP", "DESTINATION_WCP", "MODTIME_WCP",
  "ACCTIME_WCP", "CHGTIME_WCP", "ROWS_WCP", "COLUMNS_WCP", "CACHESIZE_WCP",
  "OWNERSTYLE_WCP", "TERMINAL_WCP", "USESTRINGFORDIRSIZE_WCP",
  "TIMESETS_WCP", "STYLE1_WCP", "STYLE2_WCP", "STYLE3_WCP", "DATE_WCP",
  "TIME_WCP", "DATESUBSTITUTION_WCP", "DATEBEFORETIME_WCP", "STATEBAR_WCP",
  "SELLVBAR_WCP", "UNSELLVBAR_WCP", "CLOCKBAR_WCP", "REQUESTER_WCP",
  "UNSELDIR_WCP", "SELDIR_WCP", "UNSELFILE_WCP", "SELFILE_WCP",
  "UNSELACTDIR_WCP", "SELACTDIR_WCP", "UNSELACTFILE_WCP", "SELACTFILE_WCP",
  "LVBG_WCP", "STARTUP_WCP", "BUTTON_WCP", "BUTTONS_WCP", "POSITION_WCP",
  "TITLE_WCP", "COMMANDS_WCP", "SHORTKEYS_WCP", "PATHS_WCP", "PATH_WCP",
  "FILETYPES_WCP", "FILETYPE_WCP", "NORMAL_WCP", "NOTYETCHECKED_WCP",
  "UNKNOWN_WCP", "NOSELECT_WCP", "DIR_WCP", "USEPATTERN_WCP",
  "USECONTENT_WCP", "PATTERN_WCP", "CONTENT_WCP", "FTCOMMANDS_WCP",
  "DND_WCP", "DC_WCP", "SHOW_WCP", "RAWSHOW_WCP", "USER_WCP", "FLAGS_WCP",
  "IGNOREDIRS_WCP", "HOTKEYS_WCP", "HOTKEY_WCP", "FONTS_WCP",
  "GLOBALFONT_WCP", "BUTTONFONT_WCP", "LEFTFONT_WCP", "RIGHTFONT_WCP",
  "TEXTVIEWFONT_WCP", "STATEBARFONT_WCP", "TEXTVIEWALTFONT_WCP",
  "CLOCKBARSETS_WCP", "MODUS_WCP", "TIMESPACE_WCP", "VERSION_WCP",
  "EXTERN_WCP", "UPDATETIME_WCP", "PROGRAM_WCP", "SHOWHINTS_WCP",
  "KEY_WCP", "DOUBLE_WCP", "MOD_WCP", "CONTROL_WCP", "SHIFT_WCP",
  "LOCK_WCP", "MOD1_WCP", "MOD2_WCP", "MOD3_WCP", "MOD4_WCP", "MOD5_WCP",
  "DNDACTION_WCP", "DCACTION_WCP", "SHOWACTION_WCP", "RSHOWACTION_WCP",
  "USERACTION_WCP", "ROWUP_WCP", "ROWDOWN_WCP", "CHANGEHIDDENFLAG_WCP",
  "COPYOP_WCP", "FIRSTROW_WCP", "LASTROW_WCP", "PAGEUP_WCP",
  "PAGEDOWN_WCP", "SELECTOP_WCP", "SELECTALLOP_WCP", "SELECTNONEOP_WCP",
  "INVERTALLOP_WCP", "PARENTDIROP_WCP", "ENTERDIROP_WCP",
  "CHANGELISTERSETOP_WCP", "SWITCHLISTEROP_WCP", "FILTERSELECTOP_WCP",
  "FILTERUNSELECTOP_WCP", "PATHTOOTHERSIDEOP_WCP", "QUITOP_WCP",
  "DELETEOP_WCP", "RELOADOP_WCP", "MAKEDIROP_WCP", "RENAMEOP_WCP",
  "DIRSIZEOP_WCP", "SIMDDOP_WCP", "STARTPROGOP_WCP", "SEARCHENTRYOP_WCP",
  "ENTERPATHOP_WCP", "SCROLLLISTEROP_WCP", "CREATESYMLINKOP_WCP",
  "CHANGESYMLINKOP_WCP", "CHMODOP_WCP", "TOGGLELISTERMODEOP_WCP",
  "SETSORTMODEOP_WCP", "SETFILTEROP_WCP", "SHORTKEYFROMLISTOP_WCP",
  "CHOWNOP_WCP", "WORKERCONFIG_WCP", "USERSTYLE_WCP", "DATESTRING_WCP",
  "TIMESTRING_WCP", "COLOR_WCP", "PATTERNIGNORECASE_WCP",
  "PATTERNUSEREGEXP_WCP", "PATTERNUSEFULLNAME_WCP", "COM_WCP",
  "SEPARATEEACHENTRY_WCP", "RECURSIVE_WCP", "START_WCP",
  "TERMINALWAIT_WCP", "SHOWOUTPUT_WCP", "VIEWSTR_WCP", "SHOWOUTPUTINT_WCP",
  "SHOWOUTPUTOTHERSIDE_WCP", "SHOWOUTPUTCUSTOMATTRIBUTE_WCP",
  "INBACKGROUND_WCP", "TAKEDIRS_WCP", "ACTIONNUMBER_WCP",
  "HIDDENFILES_WCP", "HIDE_WCP", "TOGGLE_WCP", "FOLLOWSYMLINKS_WCP",
  "MOVE_WCP", "RENAME_WCP", "SAMEDIR_WCP", "REQUESTDEST_WCP",
  "REQUESTFLAGS_WCP", "OVERWRITE_WCP", "ALWAYS_WCP", "NEVER_WCP",
  "COPYMODE_WCP", "FAST_WCP", "PRESERVEATTR_WCP", "MODE_WCP", "ACTIVE_WCP",
  "ACTIVE2OTHER_WCP", "SPECIAL_WCP", "REQUEST_WCP", "CURRENT_WCP",
  "OTHER_WCP", "FILTER_WCP", "QUICK_WCP", "ALSOACTIVE_WCP",
  "RESETDIRSIZES_WCP", "KEEPFILETYPES_WCP", "RELATIVE_WCP", "ONFILES_WCP",
  "ONDIRS_WCP", "SORTBY_WCP", "SORTFLAG_WCP", "REVERSE_WCP", "DIRLAST_WCP",
  "DIRMIXED_WCP", "EXCLUDE_WCP", "INCLUDE_WCP", "UNSET_WCP",
  "UNSETALL_WCP", "SCRIPTOP_WCP", "NOP_WCP", "PUSH_WCP", "LABEL_WCP",
  "IF_WCP", "END_WCP", "POP_WCP", "SETTINGS_WCP", "WINDOW_WCP", "GOTO_WCP",
  "PUSHUSEOUTPUT_WCP", "DODEBUG_WCP", "WPURECURSIVE_WCP",
  "WPUTAKEDIRS_WCP", "STACKNR_WCP", "PUSHSTRING_WCP",
  "PUSHOUTPUTRETURNCODE_WCP", "IFTEST_WCP", "IFLABEL_WCP", "WINTYPE_WCP",
  "OPEN_WCP", "CLOSE_WCP", "LEAVE_WCP", "CHANGEPROGRESS_WCP",
  "CHANGETEXT_WCP", "PROGRESSUSEOUTPUT_WCP", "WINTEXTUSEOUTPUT_WCP",
  "PROGRESS_WCP", "WINTEXT_WCP", "SHOWDIRCACHEOP_WCP", "INODE_WCP",
  "NLINK_WCP", "BLOCKS_WCP", "SHOWHEADER_WCP", "LVHEADER_WCP",
  "IGNORECASE_WCP", "LISTVIEWS_WCP", "BLL_WCP", "LBL_WCP", "LLB_WCP",
  "BL_WCP", "LB_WCP", "LAYOUT_WCP", "BUTTONSVERT_WCP", "LISTVIEWSVERT_WCP",
  "LISTVIEWWEIGHT_WCP", "WEIGHTTOACTIVE_WCP", "EXTCOND_WCP",
  "USEEXTCOND_WCP", "SUBTYPE_WCP", "PARENTACTIONOP_WCP",
  "NOOPERATIONOP_WCP", "COLORMODE_WCP", "DEFAULT_WCP", "CUSTOM_WCP",
  "UNSELECTCOLOR_WCP", "SELECTCOLOR_WCP", "UNSELECTACTIVECOLOR_WCP",
  "SELECTACTIVECOLOR_WCP", "COLOREXTERNPROG_WCP", "PARENT_WCP",
  "DONTCD_WCP", "DONTCHECKVIRTUAL_WCP", "GOFTPOP_WCP", "HOSTNAME_WCP",
  "USERNAME_WCP", "PASSWORD_WCP", "DONTENTERFTP_WCP", "ALWAYSSTOREPW_WCP",
  "INTERNALVIEWOP_WCP", "CUSTOMFILES_WCP", "SHOWMODE_WCP", "SELECTED_WCP",
  "REVERSESEARCH_WCP", "MOUSECONF_WCP", "SELECTBUTTON_WCP",
  "ACTIVATEBUTTON_WCP", "SCROLLBUTTON_WCP", "SELECTMETHOD_WCP",
  "ALTERNATIVE_WCP", "SEARCHOP_WCP", "EDITCOMMAND_WCP",
  "SHOWPREVRESULTS_WCP", "TEXTVIEW_WCP", "TEXTVIEWHIGHLIGHTED_WCP",
  "TEXTVIEWSELECTION_WCP", "DIRBOOKMARKOP_WCP", "OPENCONTEXTMENUOP_WCP",
  "RUNCUSTOMACTION_WCP", "CUSTOMNAME_WCP", "CONTEXTBUTTON_WCP",
  "ACTIVATEMOD_WCP", "SCROLLMOD_WCP", "CONTEXTMOD_WCP", "NONE_WCP",
  "CUSTOMACTION_WCP", "SAVE_WORKER_STATE_ON_EXIT_WCP",
  "OPENWORKERMENUOP_WCP", "CHANGELABELOP_WCP", "ASKFORLABEL_WCP",
  "LABELCOLORS_WCP", "BOOKMARKLABEL_WCP", "BOOKMARKFILTER_WCP",
  "SHOWONLYBOOKMARKS_WCP", "SHOWONLYLABEL_WCP", "SHOWALL_WCP",
  "OPTIONMODE_WCP", "INVERT_WCP", "SET_WCP", "CHANGEFILTERS_WCP",
  "CHANGEBOOKMARKS_WCP", "QUERYLABEL_WCP", "MODIFYTABSOP_WCP",
  "TABACTION_WCP", "NEWTAB_WCP", "CLOSECURRENTTAB_WCP", "NEXTTAB_WCP",
  "PREVTAB_WCP", "TOGGLELOCKSTATE_WCP", "MOVETABLEFT_WCP",
  "MOVETABRIGHT_WCP", "CHANGELAYOUTOP_WCP", "INFIXSEARCH_WCP",
  "VOLUMEMANAGEROP_WCP", "ERROR", "ACTIVESIDE_WCP", "SHOWFREESPACE_WCP",
  "ACTIVEMODE_WCP", "ASK_WCP", "SSHALLOW_WCP", "FIELD_WIDTH_WCP",
  "QUICKSEARCHENABLED_WCP", "FILTEREDSEARCHENABLED_WCP", "XFTFONTS_WCP",
  "USEMAGIC_WCP", "MAGICPATTERN_WCP", "MAGICCOMPRESSED_WCP",
  "MAGICMIME_WCP", "VOLUMEMANAGER_WCP", "MOUNTCOMMAND_WCP",
  "UNMOUNTCOMMAND_WCP", "FSTABFILE_WCP", "MTABFILE_WCP", "PARTFILE_WCP",
  "REQUESTACTION_WCP", "SIZEH_WCP", "NEXTAGING_WCP", "ENTRY_WCP",
  "PREFIX_WCP", "VALUE_WCP", "EXTENSION_WCP", "EJECTCOMMAND_WCP",
  "CLOSETRAYCOMMAND_WCP", "AVFSMODULE_WCP", "FLEXIBLEMATCH_WCP",
  "USE_VERSION_STRING_COMPARE_WCP", "SWITCHBUTTONBANKOP_WCP",
  "SWITCHTONEXTBANK_WCP", "SWITCHTOPREVBANK_WCP", "SWITCHTOBANKNR_WCP",
  "BANKNR_WCP", "USE_VIRTUAL_TEMP_COPIES_WCP", "PATHJUMPOP_WCP",
  "PATHJUMPALLOWDIRS_WCP", "CLIPBOARDOP_WCP", "CLIPBOARDSTRING_WCP",
  "COMMANDSTRING_WCP", "EVALCOMMAND_WCP", "WATCHMODE_WCP",
  "APPLY_WINDOW_DIALOG_TYPE_WCP", "FLAGS_ARE_RELATIVE_TO_BASEDIR_WCP",
  "COMMANDMENUOP_WCP", "SEARCHMODEONKEYPRESS_WCP", "PATHENTRYONTOP_WCP",
  "STORE_FILES_ALWAYS_WCP", "PATHJUMPSETS_WCP", "SHOWDOTDOT_WCP",
  "SHOWBREADCRUMB_WCP", "FACES_WCP", "FACE_WCP", "ENABLE_INFO_LINE_WCP",
  "INFO_LINE_LUA_MODE_WCP", "INFO_LINE_CONTENT_WCP",
  "RESTORE_TABS_MODE_WCP", "STORE_TABS_MODE_WCP", "AS_EXIT_STATE_WCP",
  "USE_EXTENDED_REGEX_WCP", "HIGHLIGHT_USER_ACTION_WCP", "CHTIMEOP_WCP",
  "ADJUSTRELATIVESYMLINKS_WCP", "OUTSIDE_WCP", "EDIT_WCP",
  "MAKEABSOLUTE_WCP", "MAKERELATIVE_WCP", "MAGICIGNORECASE_WCP",
  "ENSURE_FILE_PERMISSIONS_WCP", "USER_RW_WCP", "USER_RW_GROUP_R_WCP",
  "USER_RW_ALL_R_WCP", "LEAVE_UNMODIFIED_WCP", "PREFERUDISKSVERSION_WCP",
  "CHANGECOLUMNSOP_WCP", "PATTERNISCOMMASEPARATED_WCP", "STRCASECMP_WCP",
  "USE_STRING_COMPARE_MODE_WCP", "VIEWNEWESTFILESOP_WCP", "SHOWRECENT_WCP",
  "DIRCOMPAREOP_WCP", "TABPROFILESOP_WCP", "EXTERNALVDIROP_WCP",
  "VDIR_PRESERVE_DIR_STRUCTURE_WCP", "OPENTABMENUOP_WCP", "INITIALTAB_WCP",
  "SHOWBYTIME_WCP", "SHOWBYFILTER_WCP", "SHOWBYPROGRAM_WCP",
  "TERMINAL_RETURNS_EARLY_WCP", "DIRECTORYPRESETS_WCP",
  "DIRECTORYPRESET_WCP", "HELPOP_WCP", "PRIORITY_WCP", "AUTOFILTER_WCP",
  "ENABLE_CUSTOM_LVB_LINE_WCP", "CUSTOM_LVB_LINE_WCP",
  "CUSTOM_DIRECTORY_INFO_COMMAND_WCP", "IMMEDIATE_FILTER_APPLY_WCP",
  "DISABLEBGCHECKPREFIX_WCP", "DISPLAYMODE_WCP", "SHOWSUBDIRS_WCP",
  "SHOWDIRECTSUBDIRS_WCP", "INCLUDEALL_WCP", "APPLYMODE_WCP", "ADD_WCP",
  "REMOVE_WCP", "HIDENONEXISTING_WCP", "VIEWCOMMANDLOGOP_WCP",
  "LOCKONCURRENTDIR_WCP", "STARTNODE_WCP", "MENUS_WCP", "LVMODES_WCP",
  "LVCOMMANDS_WCP", "TOPLEVEL_WCP", "FOLLOWACTIVE_WCP", "WATCHFILE_WCP",
  "ACTIVATETEXTVIEWMODEOP_WCP", "SOURCEMODE_WCP", "ACTIVEFILE_WCP",
  "FILETYPEACTION_WCP", "IGNOREDIRS2_WCP", "IGNOREDIR_WCP",
  "CUSTOMATTRIBUTE_WCP", "STRING_WCP", "NUM_WCP", "'.'", "';'", "'='",
  "','", "$accept", "S", "$@1", "$@2", "start", "$@3", "$@4", "$@5", "$@6",
  "$@7", "$@8", "$@9", "$@10", "$@11", "$@12", "$@13", "$@14", "$@15",
  "$@16", "pjallowdirs", "$@17", "pathjumpsets", "$@18", "glb", "$@19",
  "$@20", "$@21", "$@22", "$@23", "$@24", "$@25", "$@26", "$@27", "$@28",
  "$@29", "$@30", "$@31", "$@32", "$@33", "$@34", "$@35", "layout", "$@36",
  "$@37", "$@38", "$@39", "$@40", "$@41", "$@42", "$@43", "$@44", "$@45",
  "$@46", "$@47", "$@48", "pal", "$@49", "listersets", "$@50", "$@51",
  "listersets2", "$@52", "$@53", "$@54", "$@55", "$@56", "$@57", "$@58",
  "displaysets", "$@59", "$@60", "$@61", "$@62", "$@63", "$@64", "$@65",
  "$@66", "$@67", "$@68", "$@69", "$@70", "$@71", "$@72", "$@73",
  "timesets", "$@74", "$@75", "$@76", "$@77", "$@78", "$@79", "$@80",
  "$@81", "$@82", "$@83", "$@84", "mouseconf", "$@85", "$@86", "$@87",
  "$@88", "$@89", "$@90", "$@91", "$@92", "$@93", "$@94", "$@95", "$@96",
  "cols", "$@97", "$@98", "$@99", "$@100", "$@101", "$@102", "$@103",
  "$@104", "$@105", "$@106", "$@107", "$@108", "$@109", "$@110", "$@111",
  "$@112", "$@113", "$@114", "$@115", "$@116", "labelcolors", "$@117",
  "$@118", "labelcolor", "$@119", "$@120", "$@121", "faces", "$@122",
  "$@123", "face", "$@124", "$@125", "startup", "$@126", "$@127", "$@128",
  "$@129", "$@130", "$@131", "$@132", "$@133", "$@134", "paths", "$@135",
  "$@136", "path", "$@137", "$@138", "$@139", "$@140", "$@141", "$@142",
  "keylist", "$@143", "$@144", "$@145", "$@146", "$@147", "$@148", "$@149",
  "$@150", "$@151", "$@152", "mods", "$@153", "$@154", "$@155", "$@156",
  "$@157", "$@158", "$@159", "$@160", "filetypes", "$@161", "$@162",
  "$@163", "$@164", "$@165", "$@166", "$@167", "ignoredirs", "$@168",
  "ignoredirs2", "$@169", "$@170", "ignoredir", "$@171", "subfiletypes",
  "$@172", "$@173", "filetype", "$@174", "$@175", "$@176", "$@177",
  "$@178", "$@179", "$@180", "$@181", "$@182", "$@183", "$@184", "$@185",
  "$@186", "$@187", "$@188", "$@189", "$@190", "$@191", "$@192", "$@193",
  "$@194", "$@195", "$@196", "$@197", "$@198", "$@199", "$@200",
  "filecontent", "$@201", "ft_type", "ftcommands", "$@202", "$@203",
  "$@204", "$@205", "$@206", "$@207", "$@208", "$@209", "$@210", "$@211",
  "$@212", "$@213", "commands", "flags", "hotkeys", "$@214", "$@215",
  "hotkey", "$@216", "$@217", "$@218", "$@219", "$@220", "buttons",
  "$@221", "$@222", "button", "$@223", "$@224", "$@225", "$@226", "$@227",
  "$@228", "$@229", "fonts", "$@230", "$@231", "$@232", "$@233", "$@234",
  "$@235", "$@236", "xftfonts", "$@237", "$@238", "$@239", "$@240",
  "$@241", "$@242", "$@243", "volumemanager", "$@244", "$@245", "$@246",
  "$@247", "$@248", "$@249", "$@250", "$@251", "$@252", "clockbarsets",
  "$@253", "$@254", "$@255", "$@256", "$@257", "$@258", "$@259", "bool",
  "command", "ownop", "$@260", "ownopbody", "$@261", "$@262", "$@263",
  "$@264", "$@265", "$@266", "$@267", "$@268", "$@269", "$@270", "$@271",
  "$@272", "$@273", "$@274", "$@275", "$@276", "$@277", "$@278",
  "dndaction", "$@279", "dcaction", "$@280", "showaction", "$@281",
  "rshowaction", "$@282", "useraction", "$@283", "useractionbody", "$@284",
  "rowup", "$@285", "rowdown", "$@286", "changehiddenflag", "$@287",
  "changehiddenflagbody", "$@288", "$@289", "$@290", "copyop", "$@291",
  "copyopbody", "$@292", "$@293", "$@294", "$@295", "$@296", "$@297",
  "$@298", "$@299", "$@300", "$@301", "$@302", "$@303", "$@304", "$@305",
  "$@306", "$@307", "$@308", "$@309", "$@310", "$@311", "firstrow",
  "$@312", "lastrow", "$@313", "pageup", "$@314", "pagedown", "$@315",
  "selectop", "$@316", "selectallop", "$@317", "selectnoneop", "$@318",
  "invertallop", "$@319", "parentdirop", "$@320", "enterdirop", "$@321",
  "enterdiropbody", "$@322", "$@323", "$@324", "$@325", "$@326",
  "changelistersetop", "$@327", "changelistersetopbody", "$@328", "$@329",
  "$@330", "$@331", "switchlisterop", "$@332", "filterselectop", "$@333",
  "filterselectopbody", "$@334", "$@335", "$@336", "filterunselectop",
  "$@337", "filterunselectopbody", "$@338", "$@339", "$@340",
  "pathtoothersideop", "$@341", "quitop", "$@342", "quitopbody", "$@343",
  "$@344", "deleteop", "$@345", "deleteopbody", "$@346", "reloadop",
  "$@347", "reloadopbody", "$@348", "$@349", "$@350", "$@351", "$@352",
  "$@353", "makedirop", "$@354", "renameop", "$@355", "dirsizeop", "$@356",
  "simddop", "$@357", "startprogop", "$@358", "startprogopbody", "$@359",
  "$@360", "$@361", "$@362", "$@363", "$@364", "$@365", "$@366", "$@367",
  "$@368", "$@369", "$@370", "$@371", "$@372", "$@373", "searchentryop",
  "$@374", "searchentryopbody", "$@375", "$@376", "$@377", "$@378",
  "enterpathop", "$@379", "enterpathopbody", "$@380", "$@381", "$@382",
  "$@383", "scrolllisterop", "$@384", "scrolllisteropbody", "$@385",
  "$@386", "createsymlinkop", "$@387", "createsymlinkopbody", "$@388",
  "$@389", "$@390", "changesymlinkop", "$@391", "changesymlinkopbody",
  "$@392", "$@393", "$@394", "chmodop", "$@395", "chmodopbody", "$@396",
  "$@397", "$@398", "$@399", "$@400", "$@401", "$@402", "$@403", "$@404",
  "chtimeop", "$@405", "chtimeopbody", "$@406", "$@407", "$@408", "$@409",
  "togglelistermodeop", "$@410", "togglelistermodeopbody", "$@411",
  "setsortmodeop", "$@412", "setsortmodeopbody", "$@413", "$@414", "$@415",
  "$@416", "$@417", "$@418", "$@419", "$@420", "$@421", "$@422", "$@423",
  "$@424", "$@425", "$@426", "$@427", "setfilterop", "$@428",
  "setfilteropbody", "$@429", "$@430", "$@431", "$@432", "$@433", "$@434",
  "$@435", "$@436", "$@437", "$@438", "$@439", "$@440", "$@441", "$@442",
  "$@443", "shortkeyfromlistop", "$@444", "chownop", "$@445",
  "chownopbody", "$@446", "$@447", "$@448", "$@449", "scriptop", "$@450",
  "scriptopbody", "$@451", "$@452", "$@453", "$@454", "$@455", "$@456",
  "$@457", "$@458", "$@459", "$@460", "$@461", "$@462", "$@463", "$@464",
  "$@465", "$@466", "$@467", "$@468", "$@469", "$@470", "$@471", "$@472",
  "$@473", "$@474", "$@475", "$@476", "$@477", "$@478", "$@479", "$@480",
  "showdircacheop", "$@481", "parentactionop", "$@482", "nooperationop",
  "$@483", "goftpop", "$@484", "goftpopbody", "$@485", "$@486", "$@487",
  "$@488", "$@489", "$@490", "$@491", "internalviewop", "$@492",
  "internalviewopbody", "$@493", "$@494", "$@495", "$@496", "clipboardop",
  "$@497", "clipboardopbody", "$@498", "searchop", "$@499", "searchopbody",
  "$@500", "$@501", "dirbookmarkop", "$@502", "opencontextmenuop", "$@503",
  "opencontextmenuopbody", "$@504", "runcustomaction", "$@505",
  "runcustomactionbody", "$@506", "openworkermenuop", "$@507",
  "changelabelop", "$@508", "changelabelopbody", "$@509", "$@510",
  "modifytabsop", "$@511", "modifytabsopbody", "$@512", "$@513", "$@514",
  "$@515", "$@516", "$@517", "$@518", "changelayoutop", "$@519",
  "changelayoutopbody", "$@520", "$@521", "$@522", "$@523", "$@524",
  "$@525", "$@526", "$@527", "$@528", "$@529", "$@530", "$@531", "$@532",
  "changecolumnsop", "$@533", "changecolumnsopbody", "$@534", "$@535",
  "$@536", "$@537", "$@538", "$@539", "$@540", "$@541", "$@542", "$@543",
  "$@544", "$@545", "$@546", "$@547", "$@548", "volumemanagerop", "$@549",
  "switchbuttonbankop", "$@550", "switchbuttonbankopbody", "$@551",
  "$@552", "$@553", "$@554", "pathjumpop", "$@555", "pathjumpopbody",
  "$@556", "$@557", "$@558", "$@559", "$@560", "$@561", "$@562", "$@563",
  "$@564", "commandmenuop", "$@565", "commandmenuopbody", "$@566", "$@567",
  "$@568", "$@569", "$@570", "$@571", "$@572", "$@573", "$@574",
  "viewnewestfilesop", "$@575", "dircompareop", "$@576", "tabprofilesop",
  "$@577", "externalvdirop", "$@578", "externalvdiropbody", "$@579",
  "opentabmenuop", "$@580", "directorypresets", "$@581", "$@582",
  "directorypreset", "$@583", "$@584", "$@585", "$@586", "$@587", "$@588",
  "$@589", "$@590", "$@591", "$@592", "$@593", "$@594", "$@595", "$@596",
  "$@597", "$@598", "$@599", "$@600", "$@601", "$@602",
  "directorypreset_filter", "$@603", "$@604", "$@605", "helpop", "$@606",
  "viewcommandlogop", "$@607", "activatetextviewmodeop", "$@608",
  "activatetextviewmodeopbody", "$@609", "$@610", "$@611", "$@612",
  "$@613", "$@614", "$@615", "$@616", "$@617", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-2382)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
   -2382,   126,    20, -2382,  -315,  -312,  -309,  -282,  -276,  -247,
   -2382,   102,   207,   225,   227,   241,   269,   288,   292,   299,
     304,   320,   324,   327,   337, -2382,   280, -2382,    63, -2382,
   -2382, -2382, -2382,   769,   282,   844, -2382,  -117, -2382,  -153,
     340,   358,  -103,   -97,   -92,   -87,   -74,   -17,   464,   470,
     480,    13,   495,    31,    35,    91,   100,   107,   120,   537,
     688,   135,   157,   167,   173,   643,   591,   586,    -3,   568,
     184,   187,   193,   208,   213,   228,   239,   710,   245,   249,
     273,   301,   756,   312,   313,   325,   328,   339,   359,   360,
     795,   341,   393,   841,   432,   401, -2382,   150,   403,   409,
     410,   317,   402,   526,   316, -2382,   369,   526,   478,   526,
     526,   526,   477,   526,   430,   102,   448,   493,   497,   523,
     524,   573,   583,   584,   587,   672,   759,   766,   767,   768,
     770,   771,   772,   773,   915,   964, -2382,   590,   614,   -85,
    -144,   102,  1238,  1242,  1244,  1245,  1247,  1248,   776,  1249,
    1250,  1252,  1253,   785,   788,   790,   791,   792,   793,   794,
     102,   446,   796,   798,   526,   102,   799,   800,   801,   802,
     803,   804,   805,   102,   806,  1259,   526,   102,  1261,  1270,
     807,   810, -2382, -2382,  1271,   809,   811,   812,   813,   814,
     815, -2382, -2382,   816,   817,   818,   819,   820,   821,   822,
    1272,   110,   823,   824,   825,   826,   827,  1278,  1285,  1297,
    1298,   830,   831,   832,   833,   834,   835,   836,   837,   838,
     839,  1308,   842,   845,   846,   847,   848,   849, -2382,   852,
     853,   854,   855,   856,   857,   858,   859,   860,   861,   862,
     863,   864,   865,   866,   867,   868,   869, -2382,   918,  1311,
     870,   871,   872,   873,   874,   875,   876,   877,   878, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382,   526, -2382, -2382, -2382,
   -2382,   879,   880,   881,   882,   883,   884,   885, -2382,   886,
     887,   888,   889,   890,   891,   892, -2382,   893,   894,   895,
     896,   897,   898,   899, -2382, -2382, -2382,   900, -2382, -2382,
   -2382, -2382,   901,  1335,  1337,  1369,   280, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,    42,    69,   526,   526,   906,   907,
     280,   905,   908,   909,   910,   911,   912,   913,   914,   916,
     917,   919,   920,   921,  1373,   925,   926,   927,   -25,   928,
   -2382, -2382, -2382,   280, -2382,   922,   930,   931,   932,   933,
     526,   934,   935,   936,   280, -2382, -2382, -2382, -2382, -2382,
   -2382,   937,   938,   939,   940,   941,   942,   943,   944,   945,
     946,   947,   948,   949,   952,   951,   953,   954,   955,  1217,
    1383,  1384,   102, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382,    37,   102,   489,   102,   529,   957,   958,   965,
     102,   284,   102, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,   341,   102, -2382,   142,   102,   280,
     961,   280,    75,    75, -2382,   280,   280,   280,   280,   280,
     280,   280,   962,   963,   966,   967,   968,   969,   970,   971,
     972,   973,   974, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,   526,   526,   977,   526,   280,   976,   978,
     979,   980,   981,   982,  1299,  1299,  1299, -2382,   280,   983,
     984,   985,   986,   987,   988,   989,   990,   991, -2382,   280,
     280,   280,   280,   280,   280,   994,   995,   996,   997,   998,
     999,  1000,  1001,  1002,  1003,  1004,  1005,  1006, -2382,  1007,
    1008,  1009,  1010,  1403,  1404, -2382,   688, -2382,    63,    63,
      63,    63,    63,    63,    63,    63,    63,   960,  1011,  1406,
    1432,  1012,  1444, -2382,  1013,  1014,  1433,  1015,  1016,  1476,
   -2382,  1017,  1018,  1019,  1020,  1021,  1478,  1479,  1022,  1023,
    1024,  1025,  1026,  1496,  1028,  1029,  1030,  1031,  1032,  1033,
    1034,  1035,  1036,  1037,  1038,  1039,  1040,  1509,  1043,  1511,
   -2382,  1513,  1514, -2382,  1045,  1516,  1517,  1518, -2382,   769,
     769,   769,   769,   769,   769,   769,   282,   282,   282,   282,
     282,   282,   282,   844,   844,   844,   844,   844,   844,   844,
   -2382, -2382,  -117,  1049,  1051,  1521,  1053,  1054,  1523, -2382,
   -2382,  1055, -2382,  1057,  1058,  1059,  1060,  1530,  1062,  1063,
    1532,  1533, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
     110,   110,   110,   110,   110,   110,   110,   110,   110,  1067,
    1068,  1069,  1070, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
    1071,  1538,  1540,  1541, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
    1075,  1076,  1077,  1078,  1079,  1080,  1081,  1082,  1083,  1084,
    1085,  1086,  1087,   688,  1088,  1089,  1090,  1091, -2382, -2382,
      29, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382,  1094,  1096, -2382, -2382,  1097, -2382,  1098,  1100, -2382,
    1101,  1102, -2382,   463,  1104,   526,   526,  1105, -2382,     4,
     526,   526,   526,  1106,   526,  1340,    58,  1107,  1108,  1109,
    1110,  1112,   526,  1113,   526,   526,   526,   526,  1114, -2382,
   -2382, -2382,    -3, -2382, -2382,  1116, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382,  1117,    60, -2382,    30,   195, -2382,  1118,   526,  1119,
     526,  1120, -2382,   526,   526,   150,   150,   316,   316,   316,
     316,   316,   316,   316,   316,   316,   316,   316, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382,   369,   369,   369,   369,   369,   369,   843, -2382, -2382,
   -2382,   478,   478,   478,   478,   478,   478,   478,   478,   478,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,   111,   688,
    1093,  1103,  1566,  1111,  1115,   294,    51,  1121,   591,  1122,
    1124,    51,  1125,  1126,   586, -2382, -2382, -2382, -2382, -2382,
    1127,  1129,  1130,  1131,  1132,  1123,  1587,  1588,  1594,  1601,
    1136,  1603,  1604,  1138,  1139,  1140,  1141,  1142,  1611,  1612,
    1144,  1146,  1147,  1148,  1134,  1149,  1150,  1151,  1154,  1155,
    1156,  1157,  1158,  1159,  1160,  1161,    -3,   957,    -3, -2382,
    1545,    -3,  1162,   294,    51,   568,  1163,  1164,  1165,    12,
    1166,  1167,  1168,  1169,  1170,  1171,  1172,  1173,  1174,  1175,
    1176,  1177,  1178,  1179,  1180,   432,  1181,  1182,  1184,  1185,
    1186,    14,  1187,  1188, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382,   110,   110,   110,
     110, -2382, -2382, -2382, -2382, -2382, -2382,  1189,  1190,  1191,
    1192,  1193,  1194,  1195,  1196,   369,   369,   369, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382,   688,   688,   688,
     688,   688,   688,   688,   688,   688,   688,   688,   688,   688,
     688,   688,   688,   688,  1197,  1198,  1199,  1617, -2382,  1200,
    1183, -2382, -2382, -2382,  1669,  1670,  1671,  1672,  1673,  1674,
    1675,  1676,  1677,  1678,  1679,  1680,  1681,  1682,  1683,  1684,
    1685,  1686,  1687,  1688,  1689,  1690,  1691,  1692,  1693,  1694,
    1695,  1696,  1697,  1698,  1699,  1700,  1701,  1702,  1703,  1704,
    1705,  1706,  1707,  1708,  1709,  1710,  1711,  1712,  1713,  1714,
    1715,  1716,  1717,  1718,  1719,  1720,  1721,  1722,  1723,  1724,
    1725,  1726,  1727,  1728,  1729,  1730,  1731,  1732,  1733,  1734,
    1735,  1736,  1737,  1738,  1739,  1740,  1741,  1742,  1743,   294,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,  1745,  1746,  1747,  1277, -2382, -2382, -2382,
    1748, -2382,  1281, -2382, -2382, -2382, -2382, -2382, -2382,  1282,
    1749, -2382, -2382, -2382, -2382,  1752, -2382,   529, -2382, -2382,
   -2382, -2382, -2382, -2382,   529, -2382, -2382, -2382, -2382,  1287,
    1288,  1289,  1290, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  1291,  1756, -2382, -2382,  1757,  1758,
   -2382, -2382, -2382, -2382,  1292,  1293,  1762, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,  1300, -2382, -2382, -2382, -2382,  1296,  1301,
    1302,  1303,  1304,  1305,  1306,  1307,  1309,  1310,  1312,  1313,
    1314,  1315,  1316,  1764, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,  1320,  1321,
    1322, -2382,  1318,  1323,   918,    37,    37, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382,  1324,   489,   489, -2382,
     489,  1325,   529,   529,   529,   529,   529,  1328,   529,   294,
     294,   294,   294, -2382,  1750, -2382,   529,   529,   529,   529,
     529,   529, -2382,   529,   529,   529,   529,  1327,  1329,  1330,
    1331,   529,   529,   529,   529,   529,   529,   529,   529,  1336,
   -2382,   284, -2382, -2382,   142,   142,   142,  1338,   287, -2382,
     142,   142,   142,   142,   142,   142,   142,   142,   142,   142,
     142,   142,   142,   142,   142,  1332,    75,    75,    75,    75,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382,    75,    75,    75,  1299,  1299,
    1299,  1299,  1299,  1299,  1299,  1299,  1334,  1319,  1326,  1217,
   -2382, -2382, -2382, -2382, -2382,   118,  1767,  1775,  1778,  1789,
    1803,  1631,  1805,  1806,  1633,    83,  1808,  1809,  1810,  1811,
    1812,  1813,  1814,  1815,  1816,    -1,  1628,  1818,  -141,  -134,
    1819,  1632,  1622,   -52,  1822,  1823,  1824,  1825,    28,   214,
    1637,  1638,   -19,  1639,     9,  1640,   350,   460,  1830,   451,
     159,  1831,  1832,  1833,  -111,   166,   271,  1834,  1435,  1534,
    1837,   -90,  1512,   514,  1839,  -130,    55,  1462,  -334,   488,
      89,  1841,  1842,  1843,  1465,  1845,  1846,  1847,  -138,    37,
    1751,  1753,    37, -2382, -2382, -2382,   489, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  1381, -2382,  1849,  1851,  1853,  1854,
     294,  1387, -2382, -2382, -2382, -2382, -2382,  1856, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  1390,   965, -2382,   284,   284, -2382,
   -2382, -2382,  1391,  1392,  1393,   142, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382,    14,    14,    14,    14,
      14,    14,    14,    14,    14,    14,    14,    14,    14,    14,
      14, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,  1396,  1397, -2382,    29,    29,  1395,  1398,
    1399,  1400,  1401,  1405,  1413,  1414,  1415,  1416,  1417,  1418,
    1864,   294, -2382, -2382, -2382, -2382,  1425,  1865, -2382, -2382,
    1426,  1872,  1428,  1429,  1431,  1434,  1436,  1437,  1440,  1441,
    1445,  1446,  1448,  1449,  1900, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  1452,  1455,  1901,  1456,  1903, -2382,
    1467,  1468,  1469,  1912,  1470,  1471,  1475,  1930, -2382,  1477,
    1944,  1483,  1946,  1484,  1485,  1486,  1955, -2382, -2382, -2382,
   -2382,  1488,  1489,  1491,  1492,  1493,  1494,  1495,  1499,  1504,
    1973,  1506,  1515,  1522,  1524,  1979,  1526,  1980,  1527,  1982,
    1528,  1531,  1535,  1984,  1536,  1989,  1537,  1539,  1542,  1543,
    1544,  1546,  1991,  1547,  2000,  1548,  1549,  2002,  1550,  1551,
    1552,  1553,  1554,  1555,  1556,  1557,  1560,  2009, -2382,  1562,
    1563,  1564,  1565,  2034,  1567,  1571,  1572,  1573,  1575,  1576,
    1581,  1582,  1583,  1584,  1585,  1586,  1589,  1590,  1591,  1592,
    1593,  1595,  1596,  2055, -2382, -2382, -2382,  1598,  1599,  1602,
    1605,  1606,  1607,  1608,  2056,  1609,  1610,  1613,  2062,  1614,
    1615,  2079, -2382,  1616,  2080,  1618,  2085, -2382,  1619,  1620,
    2089,  1623,  2090,  1625,  1626,  1627,  1629,  1630,  1634,  1635,
    1636,  1641,  1642,  1643,  1644,  1645,  2095, -2382,  1646,  1647,
    2098,  1648,  1649,  1650,  1651,  1652,  2099,  1653,  2100,  1654,
    1655,  2104,  1656,  1657,  1658,  1659,  2105,  1661,  1662,  1663,
    1664,  1665,  1666,  1667,  1668,  1744,  1754,  1755,  1759,  1760,
    1761,  1763,  2106, -2382, -2382, -2382,  1765,  2107, -2382, -2382,
   -2382,  1766,  1768,  1769,  1770,  1771,  1772,  1773,  2109, -2382,
    1774,  1776, -2382,    37, -2382,   489, -2382, -2382, -2382, -2382,
   -2382,  2136,  1779, -2382,   529,   529,   529,   529, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382,   810, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382,   111,  1780,  1781, -2382, -2382,  1782,   526,   526,
     441,  1785,   526,   526,   526,   526,   526,   526,   526, -2382,
   -2382,  1786, -2382,    38, -2382,   526,   526,   526,   526,   526,
     526,   143,     0,   526,  -115,    64,   526, -2382,  1788,   387,
   -2382,    33, -2382,  1790,   526,   526, -2382,  1791,   526,   526,
   -2382,     1, -2382,   526, -2382,    82,   526,   526, -2382,   526,
     526,   682,  1792,   526,   526,   526,   526,   526, -2382,   526,
     526,   526,   526, -2382,   119, -2382,   562, -2382,   526,   526,
     526, -2382,   138, -2382,  1793,   526,   526,   526,   526,  -116,
   -2382,  1794, -2382,   101,   377, -2382,   526,   538,  1795,  1796,
     274,   276,   526,   526,   526, -2382,   526,   526,   526,   526,
   -2382,   484,  1797,   526,   526,   526,   526,  1798,  1800,   526,
    1801,  1802,   459,   526,   526,   526,   526,  1804,  1807,  1817,
   -2382,   526,  1820,  1821,  1826,   526,   526,  1827, -2382,   526,
    1828,   -93, -2382,  1829,   526, -2382,   526, -2382,  1835, -2382,
    1836,   526, -2382,   610, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,   526,   526,  1838,   526, -2382,   373,
    1840, -2382,   418,  -261,   526,   526,   526, -2382,  1844, -2382,
     526,     3, -2382,   526,   526,   526,   526, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  1848, -2382,   526,   526,  1850,   526,
     526,  -281,  1852, -2382,  1855,  1857, -2382, -2382,  1123,     4,
       4,     4,     4, -2382,  1783,  1340, -2382, -2382, -2382, -2382,
    1545,    12,    12,    12, -2382, -2382, -2382, -2382,  1858,  1859,
    1860,  1861,  1862,  1863,  1866,  1867,  1868,  1869,  1870,  1871,
    1873,  1874,  1875,  1876,  1877,  1878,  1879,  1880,  1881,  1882,
    1883,  1884,  1885,  1886,  1887,  1888,  1889,  1890,  1891,  1892,
    1893,  1895,  1896,  1897,  1898,  1899,  1902,  1904,  1906,  1907,
    1908,  1909,  1910,  1911,  1913,  1914,  1915,  1916,  1917,  1918,
    1919,  1920,  1921,  1922,  1923,  1924,  1925,  1926,  1927,  1928,
    1929,  1931,  1932,  1933,  1934,  1935,  1936,  1937,  1938,  1939,
    1940,  1941,  1942,  1943,  1945,  1947,  1948,  1949,  1950,  1951,
    1952,  1953,  1954,  1956,  1957,  1958,  1959,  1960,  1961,  1962,
    1963,  1964,  1965,  1966,  1967,  1968,  1969,  1970,  1971,  1972,
    1974,  1975,  1976,  1977,  1978,  1981,  1983,  1985,  1986,  1987,
    1988,  1990,  1992,  1993,  1994,  1995,  1996,  1997,  1998,  1999,
    2001,  2003,  2004,  2005,  2006,  2007,  2008,  2010,  2011,  2012,
    2013,  2014,  2015,  2016,  2017,  2018,  2019,  2020,  2021,  2022,
    2023,  2024,  2025,  2026,  2027,  2028,  2029,  2030,  2031,  2032,
    2033,  2035,  2036,  2037,  2038,  2039,  2042,  2043,  2044,  2045,
    2046,  2047,  2048,  2049,  2050,  2051,  2052,  2053,  2054,  2057,
    2058,  2059,  2060,  2061,  2063,  2064,  2065,  2066,  2067,  2068,
    2069,  2070,  2071,  2072,  2073,  2074,  2075,  2076,  2077,  2078,
    2081,  2082,  2083,   514,   514,   514,   514,   514,   514,   514,
     514,   514,  2084,  2086,  2087,  2088,  2091,  2092,  2093,  2094,
    2096,  2097,  2101,  2102,  2103,  2108,  2110,  2111,  2112,  2113,
    2114,  2115,  2116,  2117,  2118,  2119,  2120,  2121,  2122,  2123,
    2124,  2125,  2126,    89,    89,    89,    89,    89,    89,    89,
      89,    89,    89,    89,    89,    89,    89,    89,  2127,  2128,
    2129,  2130,  2131,  2132,  2133,  2134,  2135,  2137,  2138,  2139,
   -2382, -2382, -2382, -2382, -2382,     4,  2140, -2382, -2382, -2382,
   -2382, -2382,   111,   111, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382,  2141, -2382, -2382,
     118,   118,   118,   118,   118,   118,   118,   118,   118,   118,
     118,   118,   118,   118,   118,   118,   118,   118,  1631,  1633,
    1633,  1633,    83,    83,    83,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    83,    83,    83,    83,    83,
      83,    83,    -1,    -1,    -1,    -1,    -1,  1628,  1628,  1628,
    1628,  -141,  -141,  -141,  -134,  -134,  -134,  1632,  1632,  1622,
     -52,   -52,   -52,   -52,   -52,   -52,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,   214,   214,   214,   214,  1637,  1637,  1637,  1637,  1638,
    1638,   -19,   -19,   -19,  1639,  1639,  1639,     9,     9,     9,
       9,     9,     9,     9,     9,     9,  1640,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   350,   460,   460,   460,   460,   460,   460,   460,   460,
     460,   460,   460,   460,   460,   460,   460,   451,   451,   451,
     451,   159,   159,   159,   159,   159,   159,   159,   159,   159,
     159,   159,   159,   159,   159,   159,   159,   159,   159,   159,
     159,   159,   159,   159,   159,   159,   159,   159,   159,   159,
     159,  -111,  -111,  -111,  -111,  -111,  -111,  -111,   166,   166,
     166,   166,   271,   271,  1435,  1534,   -90,   -90,  1512,  1512,
    1512,  1512,  1512,  1512,  1512,   514,   514,   514,   514,  -130,
    -130,  -130,  -130,    55,    55,    55,    55,    55,    55,    55,
      55,    55,  1462,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,   488,   488,   488,   488,  1465,  -138,  -138,  -138,
    -138,  -138,  -138,  -138,  -138,  -138,  1299,  1299,   294, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382,  2142,  2143,  2040,
    2144, -2382,  1777, -2382,    51,  2145,     4, -2382,  2146, -2382,
   -2382,  1299, -2382,  2147, -2382,    51, -2382
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       2,     0,     0,     1,     0,     0,     0,     0,     0,     0,
       3,    32,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     4,    77,     6,   283,    19,
      10,    13,    16,   488,   537,   503,    26,    38,    29,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     244,     0,     0,     0,     0,     0,   460,   287,   341,   447,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    35,     0,     0,  1296,     0,    59,   112,     0,     0,
       0,     0,     0,     0,   181,    61,   203,     0,   522,     0,
       0,     0,     0,     0,     0,    32,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     7,     0,     0,     0,
       0,    32,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      32,     0,     0,     0,     0,    32,     0,     0,     0,     0,
       0,     0,     0,    32,     0,     0,     0,    32,     0,     0,
       0,   107,   108,   110,     0,     0,     0,     0,     0,     0,
       0,   538,   539,     0,     0,     0,     0,     0,     0,     0,
       0,   104,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     5,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   240,   259,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     9,
     457,    20,   284,    11,   330,   333,     0,   336,    14,   444,
      17,     0,     0,     0,     0,     0,     0,     0,    22,     0,
       0,     0,     0,     0,     0,     0,    24,     0,     0,     0,
       0,     0,     0,     0,    23,    33,    27,     0,    25,  1293,
      30,    39,     0,     0,     0,     0,    77,    41,    43,    45,
      48,    50,    52,    56,     0,     0,     0,     0,     0,     0,
      77,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     194,   197,   200,    77,    64,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    77,    66,    70,    72,    68,    54,
      75,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   248,
       0,     0,    32,   265,   267,   271,   269,   273,   277,   275,
     281,   279,   473,    32,   299,    32,   412,   344,     0,   348,
      32,   456,    32,   474,   476,   478,   480,   482,   484,   486,
     525,   523,   527,   529,   531,   533,   535,   489,   491,   493,
     495,   497,   499,   501,    35,    32,    36,  1336,    32,    77,
       0,    77,   127,   127,    47,    77,    77,    77,    77,    77,
      77,    77,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    58,    78,    80,    82,    84,    86,    88,
      90,    92,    94,     0,     0,     0,     0,    77,     0,     0,
       0,     0,     0,     0,   329,   329,   329,    63,    77,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    74,    77,
      77,    77,    77,    77,    77,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   232,     0,
       0,     0,     0,     0,     0,   256,   244,     8,   283,   283,
     283,   283,   283,   283,   283,   283,   283,     0,     0,     0,
       0,     0,     0,    21,     0,     0,     0,     0,     0,     0,
      12,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     339,     0,     0,    15,     0,     0,     0,     0,    18,   488,
     488,   488,   488,   488,   488,   488,   537,   537,   537,   537,
     537,   537,   537,   503,   503,   503,   503,   503,   503,   503,
      34,    28,    38,     0,     0,     0,     0,     0,     0,    31,
      40,     0,    60,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    42,    44,    46,    49,    51,    53,    57,   159,
     161,   163,   165,   171,   173,   175,   169,   179,   167,   177,
     104,   104,   104,   104,   104,   104,   104,   104,   104,     0,
       0,     0,     0,    62,   182,   184,   186,   188,   190,   192,
       0,     0,     0,     0,    65,   504,   506,   512,   514,   516,
     518,   508,   510,   520,    67,    71,    73,    69,    55,    76,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   244,     0,     0,     0,     0,   245,   241,
     264,   243,   266,   268,   272,   270,   274,   278,   276,   282,
     280,     0,     0,   467,   470,     0,   458,     0,     0,   296,
       0,     0,   285,     0,     0,     0,     0,     0,   364,   439,
       0,     0,     0,     0,     0,   355,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   331,
     342,   334,   341,   345,   337,     0,   450,   453,   445,   475,
     477,   479,   481,   483,   485,   487,   526,   524,   528,   530,
     532,   534,   536,   490,   492,   494,   496,   498,   500,   502,
      37,     0,     0,  1333,     0,     0,  1294,     0,     0,     0,
       0,     0,   121,     0,     0,   112,   112,   181,   181,   181,
     181,   181,   181,   181,   181,   181,   181,   181,    79,    81,
      83,    85,    87,    89,    91,    93,    95,    96,    98,   100,
     102,   203,   203,   203,   203,   203,   203,     0,   195,   198,
     201,   522,   522,   522,   522,   522,   522,   522,   522,   522,
     204,   206,   208,   218,   220,   212,   210,   216,   214,   224,
     222,   228,   226,   233,   230,   234,   236,   238,   255,   244,
       0,     0,     0,     0,     0,   442,   312,     0,   460,     0,
       0,   312,     0,     0,   287,   416,   417,   418,   419,   420,
       0,     0,     0,     0,     0,   415,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   341,   344,   341,   340,
     351,   341,     0,   442,   312,   447,     0,     0,     0,  1343,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  1296,     0,     0,     0,     0,
       0,   158,     0,     0,   109,   111,   160,   162,   164,   166,
     172,   174,   176,   170,   180,   168,   178,   104,   104,   104,
     104,   183,   185,   187,   189,   191,   193,     0,     0,     0,
       0,     0,     0,     0,     0,   203,   203,   203,   505,   507,
     513,   515,   517,   519,   509,   511,   521,   244,   244,   244,
     244,   244,   244,   244,   244,   244,   244,   244,   244,   244,
     244,   244,   244,   244,     0,     0,     0,     0,   242,     0,
       0,   257,   461,   463,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   442,
     540,   541,   542,   543,   544,   545,   546,   547,   548,   549,
     550,   551,   552,   553,   554,   555,   556,   557,   558,   559,
     560,   561,   562,   563,   564,   565,   566,   567,   568,   569,
     570,   571,   572,   573,   574,   575,   576,   577,   578,   603,
     579,   580,   581,   582,   583,   584,   585,   586,   587,   588,
     589,   601,   590,   591,   592,   593,   594,   595,   596,   597,
     604,   598,   599,   600,   602,   605,   606,   607,   608,   609,
     610,   611,   612,     0,     0,     0,     0,   459,   288,   290,
       0,   294,     0,   286,   356,   358,   362,   366,   360,     0,
       0,   421,   424,   427,   430,     0,   436,   412,   368,   370,
     372,   375,   377,   352,   412,   394,   390,   392,   396,     0,
       0,     0,     0,   388,   400,   398,   402,   404,   406,   408,
     410,   332,   343,   335,     0,     0,   338,   448,     0,     0,
     446,  1297,  1329,  1331,     0,     0,     0,  1299,  1301,  1309,
    1317,  1311,  1305,  1303,  1307,  1313,  1315,  1325,  1327,  1319,
    1321,  1323,  1295,     0,   113,   115,   117,   119,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   123,   125,    97,    99,   101,   103,
     313,   315,   317,   319,   321,   323,   325,   327,   196,   199,
     202,   205,   207,   209,   219,   221,   213,   211,   217,   215,
     225,   223,   229,   227,   231,   235,   237,   239,     0,     0,
       0,   246,     0,     0,   259,   473,   473,   613,   443,   652,
     654,   656,   658,   660,   665,   667,   669,   678,   721,   723,
     725,   727,   729,   731,   733,   735,   737,   739,   752,   763,
     765,   774,   783,   785,   792,   797,   812,   814,   816,   818,
     820,   853,   864,   875,   882,   891,   900,   932,   937,   970,
    1003,  1005,  1016,  1079,  1081,  1083,  1085,  1102,  1118,  1125,
    1127,  1132,  1137,  1139,  1146,  1163,  1225,  1227,  1238,  1113,
    1259,   921,  1192,  1280,  1282,  1284,  1286,  1291,  1344,  1346,
    1348,   468,   440,   300,   305,   471,     0,   299,   299,   297,
     299,     0,   412,   412,   412,   412,   412,     0,   412,   442,
     442,   442,   442,   433,     0,   374,   412,   412,   412,   412,
     412,   412,   379,   412,   412,   412,   412,     0,     0,     0,
       0,   412,   412,   412,   412,   412,   412,   412,   412,     0,
     346,   456,   451,   454,  1336,  1336,  1336,     0,     0,  1334,
    1336,  1336,  1336,  1336,  1336,  1336,  1336,  1336,  1336,  1336,
    1336,  1336,  1336,  1336,  1336,     0,   127,   127,   127,   127,
     128,   130,   132,   134,   136,   138,   140,   142,   144,   146,
     148,   150,   152,   154,   156,   127,   127,   127,   329,   329,
     329,   329,   329,   329,   329,   329,     0,     0,     0,   248,
     262,   260,   258,   462,   464,   651,     0,     0,     0,     0,
       0,   664,     0,     0,   677,   720,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   751,   762,     0,   773,   782,
       0,   791,   796,   811,     0,     0,     0,     0,   852,   863,
     874,   881,   890,   899,   920,   936,   969,  1002,     0,  1015,
    1078,     0,     0,     0,  1101,  1112,  1124,     0,  1131,  1136,
       0,  1145,  1162,  1191,     0,  1237,  1258,  1117,  1279,   931,
    1224,     0,     0,     0,  1290,     0,     0,     0,  1368,   473,
       0,     0,   473,   465,   289,   291,   299,   295,   292,   357,
     359,   363,   367,   361,     0,   365,     0,     0,     0,     0,
     442,     0,   369,   371,   373,   376,   378,     0,   395,   391,
     393,   397,   380,   382,   384,   386,   389,   401,   399,   403,
     405,   407,   409,   411,     0,   348,   449,   456,   456,  1298,
    1330,  1332,     0,     0,     0,  1336,  1300,  1302,  1310,  1318,
    1312,  1306,  1304,  1308,  1314,  1316,  1326,  1328,  1320,  1322,
    1324,   105,   114,   116,   118,   120,   158,   158,   158,   158,
     158,   158,   158,   158,   158,   158,   158,   158,   158,   158,
     158,   122,   124,   126,   314,   316,   318,   320,   322,   324,
     326,   328,   253,     0,     0,   247,   264,   264,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   442,   653,   655,   657,   659,     0,     0,   666,   668,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   722,   724,   726,   728,   730,
     732,   734,   736,   738,     0,     0,     0,     0,     0,   764,
       0,     0,     0,     0,     0,     0,     0,     0,   784,     0,
       0,     0,     0,     0,     0,     0,     0,   813,   815,   817,
     819,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  1004,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,  1080,  1082,  1084,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1126,     0,     0,     0,     0,  1138,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1226,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1281,  1283,  1285,     0,     0,  1292,  1345,
    1347,     0,     0,     0,     0,     0,     0,     0,     0,   469,
       0,     0,   472,   473,   298,   299,   413,   422,   425,   428,
     431,     0,     0,   353,   412,   412,   412,   412,   349,   347,
     452,   455,  1337,  1339,  1341,  1335,   107,   129,   131,   133,
     135,   137,   139,   141,   143,   145,   147,   149,   151,   153,
     155,   157,   255,     0,     0,   263,   261,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   614,
     441,     0,   661,     0,   670,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   679,     0,     0,
     740,     0,   753,     0,     0,     0,   766,     0,     0,     0,
     775,     0,   786,     0,   793,     0,     0,     0,   798,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   821,     0,
       0,     0,     0,   854,     0,   865,     0,   876,     0,     0,
       0,   883,     0,   892,     0,     0,     0,     0,     0,     0,
     901,     0,   933,     0,     0,   938,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   971,     0,     0,     0,     0,
    1006,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1017,     0,     0,     0,     0,     0,     0,     0,  1086,     0,
       0,     0,  1103,     0,     0,  1119,     0,  1128,     0,  1133,
       0,     0,  1140,     0,  1147,  1165,  1167,  1169,  1171,  1173,
    1175,  1177,  1179,  1181,     0,     0,     0,     0,  1164,     0,
       0,  1228,     0,     0,     0,     0,     0,  1239,     0,  1114,
       0,     0,  1260,     0,     0,     0,     0,   922,  1194,  1196,
    1198,  1200,  1202,  1204,  1206,  1208,  1210,  1212,  1214,  1216,
    1218,  1220,  1222,  1193,     0,  1287,     0,     0,     0,     0,
       0,     0,     0,  1349,     0,     0,   466,   293,   415,   439,
     439,   439,   439,   434,     0,   355,   381,   383,   385,   387,
     351,  1343,  1343,  1343,   106,   254,   249,   251,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1191,  1191,  1191,  1191,  1191,  1191,  1191,
    1191,  1191,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1224,  1224,  1224,  1224,  1224,  1224,  1224,
    1224,  1224,  1224,  1224,  1224,  1224,  1224,  1224,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     414,   423,   426,   429,   432,   439,     0,   354,   350,  1338,
    1340,  1342,   255,   255,   615,   617,   619,   621,   633,   623,
     625,   627,   629,   631,   635,   637,   639,   641,   643,   645,
     647,   649,   662,   671,   673,   675,   680,   682,   684,   686,
     688,   690,   696,   692,   694,   698,   700,   702,   708,   704,
     706,   712,   714,   716,   710,   718,   741,   743,   745,   747,
     749,   754,   756,   758,   760,   767,   769,   771,   776,   778,
     780,   787,   789,   794,   799,   801,   803,   805,   807,   809,
     838,   850,   822,   834,   824,   826,   828,   830,   832,   836,
     842,   840,   844,   846,   848,   855,   857,   859,   861,   866,
     868,   870,   872,   877,   879,   884,   888,   886,   893,   895,
     897,   918,   906,   908,   902,   904,   912,   910,   914,   916,
     934,   939,   941,   949,   957,   951,   945,   943,   947,   953,
     955,   965,   967,   959,   961,   963,   972,   974,   976,   978,
     980,   982,   984,   986,   988,   990,   994,   992,   996,   998,
    1000,  1011,  1013,  1007,  1009,  1018,  1020,  1022,  1024,  1026,
    1028,  1030,  1032,  1034,  1036,  1050,  1038,  1042,  1044,  1046,
    1048,  1052,  1040,  1054,  1056,  1058,  1060,  1062,  1064,  1066,
    1068,  1070,  1072,  1074,  1076,  1087,  1089,  1091,  1093,  1095,
    1097,  1099,  1104,  1106,  1108,  1110,  1120,  1122,  1129,  1134,
    1143,  1141,  1148,  1150,  1152,  1154,  1156,  1158,  1160,  1166,
    1168,  1170,  1172,  1174,  1176,  1178,  1180,  1182,  1183,  1185,
    1187,  1189,  1229,  1231,  1233,  1235,  1240,  1242,  1244,  1246,
    1248,  1250,  1252,  1254,  1256,  1115,  1261,  1265,  1275,  1269,
    1267,  1263,  1271,  1273,  1277,   927,   929,   923,   925,  1195,
    1197,  1199,  1201,  1203,  1205,  1207,  1209,  1211,  1213,  1215,
    1217,  1219,  1221,  1223,  1288,  1366,  1350,  1358,  1362,  1364,
    1354,  1352,  1356,  1360,   301,   306,   435,     0,   250,   252,
     651,   651,   651,   651,   651,   651,   651,   651,   651,   651,
     651,   651,   651,   651,   651,   651,   651,   651,   664,   677,
     677,   677,   720,   720,   720,   720,   720,   720,   720,   720,
     720,   720,   720,   720,   720,   720,   720,   720,   720,   720,
     720,   720,   751,   751,   751,   751,   751,   762,   762,   762,
     762,   773,   773,   773,   782,   782,   782,   791,   791,   796,
     811,   811,   811,   811,   811,   811,   852,   852,   852,   852,
     852,   852,   852,   852,   852,   852,   852,   852,   852,   852,
     852,   863,   863,   863,   863,   874,   874,   874,   874,   881,
     881,   890,   890,   890,   899,   899,   899,   920,   920,   920,
     920,   920,   920,   920,   920,   920,   936,   969,   969,   969,
     969,   969,   969,   969,   969,   969,   969,   969,   969,   969,
     969,   969,  1002,  1002,  1002,  1002,  1002,  1002,  1002,  1002,
    1002,  1002,  1002,  1002,  1002,  1002,  1002,  1015,  1015,  1015,
    1015,  1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,
    1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,
    1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,  1078,
    1078,  1101,  1101,  1101,  1101,  1101,  1101,  1101,  1112,  1112,
    1112,  1112,  1124,  1124,  1131,  1136,  1145,  1145,  1162,  1162,
    1162,  1162,  1162,  1162,  1162,  1191,  1191,  1191,  1191,  1237,
    1237,  1237,  1237,  1258,  1258,  1258,  1258,  1258,  1258,  1258,
    1258,  1258,  1117,  1279,  1279,  1279,  1279,  1279,  1279,  1279,
    1279,  1279,   931,   931,   931,   931,  1290,  1368,  1368,  1368,
    1368,  1368,  1368,  1368,  1368,  1368,   329,   329,   442,   616,
     618,   620,   622,   634,   624,   626,   628,   630,   632,   636,
     638,   640,   642,   644,   646,   648,   650,   663,   672,   674,
     676,   681,   683,   685,   687,   689,   691,   697,   693,   695,
     699,   701,   703,   709,   705,   707,   713,   715,   717,   711,
     719,   742,   744,   746,   748,   750,   755,   757,   759,   761,
     768,   770,   772,   777,   779,   781,   788,   790,   795,   800,
     802,   804,   806,   808,   810,   839,   851,   823,   835,   825,
     827,   829,   831,   833,   837,   843,   841,   845,   847,   849,
     856,   858,   860,   862,   867,   869,   871,   873,   878,   880,
     885,   889,   887,   894,   896,   898,   919,   907,   909,   903,
     905,   913,   911,   915,   917,   935,   940,   942,   950,   958,
     952,   946,   944,   948,   954,   956,   966,   968,   960,   962,
     964,   973,   975,   977,   979,   981,   983,   985,   987,   989,
     991,   995,   993,   997,   999,  1001,  1012,  1014,  1008,  1010,
    1019,  1021,  1023,  1025,  1027,  1029,  1031,  1033,  1035,  1037,
    1051,  1039,  1043,  1045,  1047,  1049,  1053,  1041,  1055,  1057,
    1059,  1061,  1063,  1065,  1067,  1069,  1071,  1073,  1075,  1077,
    1088,  1090,  1092,  1094,  1096,  1098,  1100,  1105,  1107,  1109,
    1111,  1121,  1123,  1130,  1135,  1144,  1142,  1149,  1151,  1153,
    1155,  1157,  1159,  1161,  1184,  1186,  1188,  1190,  1230,  1232,
    1234,  1236,  1241,  1243,  1245,  1247,  1249,  1251,  1253,  1255,
    1257,  1116,  1262,  1266,  1276,  1270,  1268,  1264,  1272,  1274,
    1278,   928,   930,   924,   926,  1289,  1367,  1351,  1359,  1363,
    1365,  1355,  1353,  1357,  1361,   302,   307,     0,     0,     0,
       0,   303,     0,   437,   312,     0,   439,   304,     0,   438,
     308,   329,   309,     0,   310,   312,   311
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -2382, -2382, -2382, -2382,   471, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,  1784,
   -2382,  1558, -2382,   490, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382,  -498, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382,   185, -2382,  -179, -2382, -2382,
    -425, -2382, -2382, -2382, -2382, -2382, -2382, -2382,  -593, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,    87, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382,  -687, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,  -485,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
     646, -2382, -2382, -1953, -2382, -2382, -2382,   828, -2382, -2382,
   -1067, -2382, -2382,   452, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,  1275, -2382, -2382, -1380, -2382, -2382, -2382,
   -2382, -2382, -2382,  -869, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  -475, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,  -577, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382,  1233, -2382,   527, -2382, -2382,   -47, -2382,   -41, -2382,
   -2382, -1183, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,   -33,
   -2382, -2382, -2179, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382,  -919, -2382,  1231, -2382, -2382,
   -1371, -2382, -2382, -2382, -2382, -2382,  1294, -2382, -2382, -1300,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382,   379, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382,   405, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,   215, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382,   420, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382,  -104, -2382, -2382, -2382, -1850, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382,  -611, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -1847, -2382, -2382, -2382, -2382, -2382, -1751, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2124,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2041, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -1816, -2382, -2382, -2382,
   -2382, -2382, -1579, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2183, -2382, -2382, -2382, -2382,  -651, -2382, -2382, -2382, -2086,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -1702, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2042, -2382, -2382, -2382, -2382, -2382,
   -2382, -2008, -2382, -2382, -2382, -2382, -2382, -2382, -2152, -2382,
   -2382, -2382, -2382, -1613, -2382, -2382, -2382, -2382, -2382, -1612,
   -2382, -2382, -2382, -2382, -2382, -1672, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2132, -2382, -2382,
   -2382, -2382, -2382, -2382,  -697, -2382, -2382, -2382, -1728, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -1727, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2013, -2382, -2382, -2382,
   -2382, -2382, -2382, -1807, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -1710, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -1937, -2382, -2382, -2382, -2382, -2382, -2382,  -801,
   -2382, -2382, -2382, -2167, -2382, -2382, -2382, -2382, -2382, -2382,
    -771, -2382, -2382, -2382,  -770, -2382, -2382, -2382, -2382, -2382,
   -2156, -2382, -2382, -2382, -2382, -1720, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2381, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -1283, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -1871, -2382, -2382, -2382, -2382, -2382, -2382, -1769, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -1770, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,  -812, -2382,
   -2382, -2382,  1221, -2382, -2382,  -638, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382,  -966, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382, -1775, -2382, -2382, -2382,
   -2382, -2382, -2382, -2382, -2382, -2382
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,     1,     2,    11,    25,    60,   249,    67,   395,    68,
     400,    69,   402,    66,   393,    91,   425,    94,   428,   175,
     424,    93,   602,    59,   429,   435,   436,   437,   438,   439,
     440,   493,   441,   181,   201,   478,   489,   492,   490,   491,
     494,   334,   640,   641,   642,   643,   644,   645,   646,   647,
     648,   967,   968,   969,   970,   303,  1956,   184,   304,   305,
     620,  1466,  1467,  1468,  1469,   951,  1486,  1487,  1273,  1656,
    1657,  1658,  1659,  1660,  1661,  1662,  1663,  1664,  1665,  1666,
    1667,  1668,  1669,  1670,   200,   797,   798,   799,   800,   806,
     804,   801,   802,   803,   807,   805,   210,   821,   822,   823,
     824,   825,   826,   474,   985,   475,   986,   476,   987,   136,
     997,   998,   999,  1003,  1002,  1005,  1004,  1000,  1001,  1007,
    1006,  1009,  1008,  1010,   693,  1011,  1012,  1013,   379,   859,
     514,   858,  1499,  1017,  2482,  2483,  1972,   381,   700,  1314,
     862,  1687,  1686,    65,   518,   519,   521,   520,   522,   524,
     523,   526,   525,   145,   394,   874,   539,  1397,  1398,  1935,
    1400,   871,  1586,  1175,  1580,  2986,  3228,  3234,  1581,  2987,
    3229,  3241,  3243,  3245,   661,  1488,  1489,  1490,  1491,  1492,
    1493,  1494,  1495,   150,   396,   916,   397,   918,   399,   921,
     752,   569,   917,   572,   920,  1625,  1225,  2200,   899,  1421,
    2195,   567,  1402,  1403,  1406,  1404,   885,  1405,  1416,  1417,
    1418,  1419,  1420,  1944,  1945,  1946,  1947,  1431,  1424,  1425,
    1423,  1426,  1433,  1432,  1434,  1435,  1436,  1437,  1438,  1190,
    2188,   880,   892,  1409,  2189,  1410,  2190,  1411,  2191,  1412,
    2192,  1600,  2475,  1414,  3236,  1098,  1506,   152,   401,   925,
     577,  1441,   923,  1627,   924,  1628,   143,   392,   868,   532,
    1315,  1316,  1933,   865,  1579,   866,  1582,    77,   579,   580,
     581,   582,   583,   584,   585,    90,   593,   594,   595,   596,
     597,   598,   599,   221,   831,   832,   837,   838,   833,   834,
     835,   836,   839,    82,   587,   586,   588,   589,   590,   591,
     592,   193,  1099,  1100,  1505,  1700,  2750,  2751,  2752,  2753,
    2755,  2756,  2757,  2758,  2759,  2754,  2760,  2761,  2762,  2763,
    2764,  2765,  2766,  2767,  1101,  1507,  1102,  1508,  1103,  1509,
    1104,  1510,  1105,  1511,  1707,  2768,  1106,  1512,  1107,  1513,
    1108,  1514,  1711,  2769,  2770,  2771,  1109,  1515,  1724,  2772,
    2773,  2774,  2775,  2776,  2777,  2779,  2780,  2778,  2781,  2782,
    2783,  2785,  2786,  2784,  2790,  2787,  2788,  2789,  2791,  1110,
    1516,  1111,  1517,  1112,  1518,  1113,  1519,  1114,  1520,  1115,
    1521,  1116,  1522,  1117,  1523,  1118,  1524,  1119,  1525,  1736,
    2792,  2793,  2794,  2795,  2796,  1120,  1526,  1738,  2797,  2798,
    2799,  2800,  1121,  1527,  1122,  1528,  1743,  2801,  2802,  2803,
    1123,  1529,  1747,  2804,  2805,  2806,  1124,  1530,  1125,  1531,
    1750,  2807,  2808,  1126,  1532,  1752,  2809,  1127,  1533,  1756,
    2810,  2811,  2812,  2813,  2814,  2815,  1128,  1534,  1129,  1535,
    1130,  1536,  1131,  1537,  1132,  1538,  1770,  2818,  2820,  2821,
    2822,  2823,  2824,  2819,  2825,  2816,  2827,  2826,  2828,  2829,
    2830,  2817,  1133,  1539,  1775,  2831,  2832,  2833,  2834,  1134,
    1540,  1777,  2835,  2836,  2837,  2838,  1135,  1541,  1779,  2839,
    2840,  1136,  1542,  1783,  2841,  2843,  2842,  1137,  1543,  1785,
    2844,  2845,  2846,  1138,  1544,  1792,  2850,  2851,  2848,  2849,
    2853,  2852,  2854,  2855,  2847,  1139,  1569,  1896,  2974,  2975,
    2972,  2973,  1140,  1545,  1794,  2856,  1141,  1546,  1797,  2857,
    2858,  2863,  2862,  2864,  2859,  2861,  2865,  2866,  2860,  2869,
    2870,  2871,  2867,  2868,  1142,  1547,  1807,  2872,  2873,  2874,
    2875,  2876,  2877,  2878,  2879,  2880,  2881,  2883,  2882,  2884,
    2885,  2886,  1143,  1548,  1144,  1549,  1813,  2889,  2890,  2887,
    2888,  1145,  1550,  1833,  2891,  2892,  2893,  2894,  2895,  2896,
    2897,  2898,  2899,  2900,  2902,  2908,  2903,  2904,  2905,  2906,
    2901,  2907,  2909,  2910,  2911,  2912,  2913,  2914,  2915,  2916,
    2917,  2918,  2919,  2920,  1146,  1551,  1147,  1552,  1148,  1553,
    1149,  1554,  1844,  2921,  2922,  2923,  2924,  2925,  2926,  2927,
    1150,  1555,  1848,  2928,  2929,  2930,  2931,  1151,  1567,  1888,
    2962,  1152,  1556,  1851,  2932,  2933,  1153,  1557,  1154,  1558,
    1854,  2934,  1155,  1559,  1856,  2935,  1156,  1560,  1157,  1561,
    1860,  2937,  2936,  1158,  1562,  1862,  2938,  2939,  2940,  2941,
    2942,  2943,  2944,  1159,  1563,  1876,  2403,  2404,  2405,  2406,
    2407,  2408,  2409,  2410,  2411,  2945,  2946,  2947,  2948,  1160,
    1570,  1912,  2443,  2444,  2445,  2446,  2447,  2448,  2449,  2450,
    2451,  2452,  2453,  2454,  2455,  2456,  2457,  1161,  1564,  1162,
    1565,  1880,  2949,  2950,  2951,  2952,  1163,  1566,  1886,  2953,
    2954,  2955,  2956,  2957,  2958,  2959,  2960,  2961,  1164,  1568,
    1891,  2963,  2968,  2964,  2967,  2966,  2969,  2970,  2965,  2971,
    1165,  1571,  1166,  1572,  1167,  1573,  1168,  1574,  1917,  2976,
    1169,  1575,   179,   427,   945,   608,  1444,  1450,  1451,  1456,
    1455,  1457,  1452,  1454,  1458,  1459,  1453,  1462,  1463,  1464,
    1460,  1461,  1445,  1446,   929,  1635,  1236,  2201,  2202,  2203,
    1170,  1576,  1171,  1577,  1172,  1578,  1928,  2978,  2983,  2982,
    2984,  2979,  2985,  2980,  2981,  2977
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     662,   663,  1180,   211,  1228,   222,   223,   224,   621,   226,
    2471,  2472,  2473,  2474,  1415,  1503,  1504,  1584,  1585,  2205,
    1587,  1422,  2679,  2680,  2681,  2682,  2683,  2684,  2685,  2686,
    2687,   701,  1786,  1761,  1258,  1259,  1260,  1261,  1262,  1263,
    1264,  1265,  1266,   471,   255,   256,  2255,  2256,  1921,   860,
     930,   931,   932,   933,   934,  1229,   935,   936,   937,  1740,
     285,  2423,  2431,  1878,   146,  2432,  1744,  2433,  2239,  2265,
    1626,  1734,   297,  2242,  2243,  1837,    61,    62,   442,   443,
     444,   147,   886,   887,   888,   889,   890,  1234,  2434,  1889,
     613,   614,   615,   616,   617,  2268,  2269,   527,   528,   529,
     530,  2388,  2464,   252,   253,   446,   447,    12,    13,  1897,
    1898,  1899,  1900,  1901,  1902,  1903,  1904,  1905,  2227,  1173,
    1890,  2315,  2316,  2317,  2318,  2319,     3,  2320,  2321,  2322,
    1858,  1014,  2293,  2294,   971,   972,   973,   974,   975,   976,
     927,  1753,   808,   809,   810,   811,   812,   813,   814,   815,
     816,  1754,  1755,   321,     5,  1174,   322,   900,     6,    14,
       7,    15,   398,   182,   183,  1780,    16,  1781,    17,   323,
    1838,  1839,  1840,  1841,  1842,   919,  1787,     4,  2389,  1015,
    1392,  1814,  2465,  2466,  2424,  2425,  1782,    18,     8,    19,
     861,  2240,  1735,     9,  1762,  1788,  1763,    20,   531,  1764,
     445,   257,  2266,  1765,  1922,  1235,  1934,   603,   853,  2310,
      26,  2236,   449,   450,  1766,  1789,  1790,  2228,  2229,  1589,
    1590,  1591,  1592,  1593,    10,  1595,  1859,   448,    27,  2311,
      28,  2257,  2258,  1602,  1603,  1604,  1605,  1606,  1607,   928,
    1608,  1609,  1610,  1611,    29,  1923,   484,  1879,  1616,  1617,
    1618,  1619,  1620,  1621,  1622,  1623,  1950,  1951,   258,  1843,
     254,  1267,  1268,  1269,  1712,  1713,  1714,  1715,  1716,  1717,
    1718,   472,    30,  1719,    92,  1720,   148,   938,   939,  1929,
    2270,  2271,  1932,  1688,  1689,  1690,  1691,    39,    40,  1692,
      41,    31,  2244,  1693,  1694,    32,  2746,  1741,  1288,  1289,
    1290,  1742,    33,  1024,  1745,  1016,  1767,    34,  1746,    42,
      43,    44,    45,    46,    47,    48,   891,  2295,  2296,    95,
     604,  1924,  1925,    35,  1926,   618,  1927,    36,   901,   902,
      37,  2237,  2238,  2312,  2313,   903,  1906,  1907,  1908,  1221,
      38,  1223,   605,    96,  1226,   574,   575,   576,  2323,  2324,
     606,   607,  1845,   188,   189,   194,   195,   196,   197,   649,
     650,    97,   652,   324,   325,   326,   327,   328,   329,    98,
     330,   331,   332,   333,  1018,    99,  1270,  1025,    78,  1815,
     100,  1271,    79,    80,    81,   101,  1816,  1817,  1818,  1819,
    1820,  1821,  1822,  1823,  1824,  1825,  1695,   940,   102,  1826,
    1827,  1828,  1829,  1830,  1831,   942,   943,   944,  1026,  1027,
    1028,  1029,  1030,  1031,  1032,  1033,  1034,  1035,  1036,  1037,
    1038,  1039,  1040,  1041,  1042,  1043,  1044,  1045,  1046,  1047,
    1048,  1049,  1050,  1051,  1052,  1053,  1054,  1055,  1056,  1057,
    1058,  1059,  1060,  1061,  1062,  1063,  1064,  1065,  1066,  1067,
    1068,  1909,    21,  1846,  1847,   103,  1910,  1791,  2435,  2436,
    2437,  2438,   149,    63,    64,   619,  1771,   104,  2325,  1276,
    1277,  1278,  1279,   105,  2211,   198,   199,  2245,  2246,  2247,
    2248,  1272,    22,   106,  1881,   107,   279,  1768,  1769,  1721,
    1596,  1597,  1598,  1599,    23,  1722,  1696,   941,   108,  1882,
    1633,  1634,  1883,   109,  1772,  1697,  1884,   110,  1885,  2212,
    1723,  1069,  1291,  1292,  1293,  1294,  1295,  1296,  1297,  1298,
    1299,  1300,  1301,  1302,  1303,  1304,  1305,  1306,  1307,  2748,
    2749,   875,   876,   877,   878,   879,    24,   191,   192,    49,
    1070,   115,  1832,   280,   281,   282,  2302,  2303,  2304,   534,
     535,   541,   536,  1773,   537,  2187,  1911,  1863,  1795,  1796,
    1864,  1071,  1072,   111,  3184,  3185,  3186,  3187,  2326,  1849,
    1850,    50,   112,  1865,  1073,  2297,  2298,  1698,  1699,   113,
    1074,  2251,  2252,  2253,  2254,  1774,   228,  2327,  2328,  2329,
     542,  1075,   114,    51,  2337,  2338,  2339,  1076,  1077,  1078,
    2340,  2341,   543,   544,   545,   546,   547,   137,  1079,  1080,
    2213,  2214,   259,  2215,  2216,  2217,   954,   955,  1809,  1975,
    1976,   882,   883,  1081,  3046,  3047,   893,   894,   895,   138,
     897,   278,  1082,  2186,  1083,    52,   286,  1810,   909,   139,
     911,   912,   913,   914,   294,   140,  1798,   141,   298,   142,
     538,   144,    53,  1799,   151,  1892,   153,  1811,  1812,   154,
    1800,   202,   203,   204,   205,   155,    54,  1084,  3031,  3032,
    3033,  3034,  3035,  1085,  1893,  1086,   206,   207,   208,   209,
     156,  1941,  1087,    55,   947,   157,   949,  3078,  3079,   952,
     953,   548,   549,   550,  1894,  1895,  2369,  2370,  2371,  1088,
     158,    56,  2349,  2350,  2351,  2352,  2353,  2354,  2355,  2356,
    2357,   159,  1089,    57,   160,  2276,  1090,   161,  1091,  1092,
    1093,   162,  1094,    58,  3049,  3050,  3051,  3052,  3053,  3054,
    1095,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   163,  1096,  2416,  2417,  2418,
    2277,  2331,  2332,  2333,  2334,  1097,  3036,  3037,  3038,  3039,
     165,  2196,  2197,  2198,  2199,  3171,  3172,  1866,  1867,  1868,
    1869,  1870,  1871,   164,  1872,  1873,  1874,  1875,  1801,  1802,
    3175,  3176,  1990,  1803,   166,   167,  1804,  1805,  1806,  3070,
    3071,  3072,  3073,   551,   552,   553,   434,   168,   554,   173,
     169,   555,   556,   557,   558,   559,  1629,  1630,  1631,   174,
     453,   170,  1636,  1637,  1638,  1639,  1640,  1641,  1642,  1643,
    1644,  1645,  1646,  1647,  1648,  1649,  1650,  3074,  3075,  3076,
    3077,   171,   172,   477,   212,   213,   214,   215,   216,   217,
    3211,  3212,  3213,  3214,   488,   177,   218,   219,  2420,  2421,
    2422,  2278,  2279,   517,  2280,  2281,  2282,    70,    71,    72,
      73,    74,    75,    76,   533,   176,   540,   178,  2358,   180,
     190,   573,   185,   578,  3126,  3127,  3128,  3129,   186,   187,
     560,   561,   562,   563,   956,   957,   958,   959,   960,   961,
     962,   963,   964,   965,   966,   220,   601,   225,   227,   609,
    2989,  2990,  2991,  2992,  2993,  2994,  2995,  2996,  2997,  2998,
    2999,  3000,  3001,  3002,  3003,  3004,  3005,  3006,   247,   610,
     229,   612,  3008,  3009,  3010,   622,   623,   624,   625,   626,
     627,   628,    83,    84,    85,    86,    87,    88,    89,   130,
     564,  2396,  2397,  2398,  2399,  2400,  2401,  2402,   565,   977,
     978,   979,   980,   981,   982,   983,   984,   653,   759,   760,
     761,   762,   763,   764,   765,   230,   566,   248,   664,   231,
     702,   703,   704,   705,   706,   707,   708,   709,   710,   674,
     675,   676,   677,   678,   679,  3040,  3041,  3042,   131,   132,
     133,  3167,  3168,  3169,  3170,   232,   233,  1955,   773,   774,
     775,   776,   777,   778,   779,   134,   766,   767,   768,   769,
     770,   771,   772,  1674,  1675,  1676,  1677,  1678,  1679,  1680,
    1681,  3011,  3012,  3013,  3014,  3015,  3016,  3017,  3018,  3019,
    3020,  3021,  3022,  3023,  3024,  3025,  3026,  3027,  3028,  3029,
    3030,  1652,  1653,  1654,  1655,   234,   988,   989,   990,   991,
     992,   993,   994,   995,   996,   235,   236,  3239,   250,   237,
    1671,  1672,  1673,  1957,  1958,  1959,  1960,  1961,  1962,  1963,
    1964,  1965,  1966,  1967,  1968,  1969,  1970,  1971,  3188,  3189,
    3190,  3191,   251,   135,  3130,  3131,  3132,  3133,  3134,  3135,
    3136,  3137,  3138,  3139,  3140,  3141,  3142,  3143,  3144,  3145,
    3146,  3147,  3148,  3149,  3150,  3151,  3152,  3153,  3154,  3155,
    3156,  3157,  3158,  3159,  3055,  3056,  3057,  3058,  3059,  3060,
    3061,  3062,  3063,  3064,  3065,  3066,  3067,  3068,  3069,  3096,
    3097,  3098,  3099,  3100,  3101,  3102,  3103,  3104,  3105,  3106,
    3107,  3108,  3109,  3110,   238,  3111,  3112,  3113,  3114,  3115,
    3116,  3117,  3118,  3119,  3120,  3121,  3122,  3123,  3124,  3125,
    2719,  2720,  2721,  2722,  2723,  2724,  2725,  2726,  2727,  2728,
    2729,  2730,  2731,  2732,  2733,  3086,  3087,  3088,  3089,  3090,
    3091,  3092,  3093,  3094,  3192,  3193,  3194,  3195,  3196,  3197,
    3198,  3199,  3200,  3202,  3203,  3204,  3205,  3206,  3207,  3208,
    3209,  3210,  3216,  3217,  3218,  3219,  3220,  3221,  3222,  3223,
    3224,  3160,  3161,  3162,  3163,  3164,  3165,  3166,  3177,  3178,
    3179,  3180,  3181,  3182,  3183,  3043,  3044,  3045,  3080,  3081,
    3082,   239,  3083,  3084,  3085,  2479,  2480,  2481,   240,   241,
     242,   260,   243,   244,   245,   246,   261,   262,   266,   263,
     264,   265,   267,   271,   268,   269,   272,   270,   273,   274,
     275,   276,   277,   296,   299,   283,   284,   287,   288,   289,
     290,   291,   292,   293,   300,   306,   320,   295,   301,   302,
     307,   340,   308,   309,   310,   311,   312,   313,   341,   314,
     315,   316,   317,   318,   319,   335,   336,   337,   338,   339,
     342,   344,   343,   345,   346,   347,   348,   349,   350,   351,
     352,   353,   354,   355,   380,   382,   356,   357,   358,   359,
     360,   361,   362,   363,   364,   365,   366,   367,   368,   369,
     370,   371,   372,   373,   374,   375,   376,   377,   378,   431,
     432,   383,   384,   385,   386,   387,   388,   389,   390,   391,
     403,   404,   405,   406,   407,   408,   409,   410,   411,   412,
     413,   414,   415,   416,   417,   418,   419,   420,   421,   422,
     423,   426,   433,   430,   451,   452,   454,   467,   513,   455,
     456,   457,   458,   459,   460,   461,   515,   462,   516,   463,
     479,   464,   465,   466,   468,   469,   470,   473,   480,   481,
     482,   483,   485,   486,   660,   487,   698,   898,   699,   713,
     495,   496,   497,   498,   499,   500,   501,   502,   503,   504,
     505,   506,   507,   508,   509,   568,   510,   511,   512,   570,
     611,   571,   711,   629,   630,   714,   719,   631,   632,   633,
     634,   635,   636,   637,   638,   639,   651,   654,   716,   655,
     656,   657,   658,   659,   665,   666,   667,   668,   669,   670,
     671,   672,   673,   680,   681,   682,   683,   684,   685,   686,
     687,   688,   689,   690,   691,   692,   694,   695,   696,   697,
     722,   728,   729,   712,   715,   717,   718,   720,   721,   723,
     724,   725,   726,   727,   730,   731,   732,   733,   734,   735,
     736,   737,   738,   739,   740,   741,   742,   743,   744,   745,
     746,   747,   748,   749,   750,   751,   753,   755,   754,   756,
     757,   781,   758,   782,   783,   784,   785,   786,   787,   788,
     789,   790,   791,   792,   793,   794,   795,   796,   817,   818,
     819,   820,   828,   827,   829,   830,   840,   841,   842,   843,
     844,   845,   846,   847,   848,   849,   850,   851,   852,   854,
     855,   856,   857,   863,   864,  1019,   867,   869,   870,   872,
    1021,   873,   881,   884,   896,  1020,   904,   905,   906,   907,
     908,   910,  1022,   915,   922,   926,  1023,   946,   948,   950,
    1191,  1192,  1189,  1178,  1176,  1179,  1181,  1193,  1184,  1182,
    1185,  1186,  1187,  1188,  1194,  1195,  1196,  1209,  1197,  1198,
    1199,  1200,  1201,  1202,  1203,  1205,  1204,  1206,  1207,  1208,
    1224,  1311,  1210,  1211,  1212,  1213,  1214,  1215,  1216,  1217,
    1218,  1219,  1220,  1227,  1231,  1232,  1233,  1237,  1238,  1239,
    1240,  1241,  1242,  1243,  1244,  1245,  1246,  1247,  1248,  1249,
    1250,  1251,  1313,  1254,  1253,  1255,  1256,  1257,  1274,  1275,
    1280,  1281,  1282,  1283,  1284,  1285,  1286,  1287,  1312,  1308,
    1309,  1310,  1317,  1318,  1319,  1320,  1321,  1322,  1323,  1324,
    1325,  1326,  1327,  1328,  1329,  1330,  1331,  1332,  1333,  1334,
    1335,  1336,  1337,  1338,  1339,  1340,  1341,  1342,  1343,  1344,
    1345,  1346,  1347,  1348,  1349,  1350,  1351,  1352,  1353,  1354,
    1355,  1356,  1357,  1358,  1359,  1360,  1361,  1362,  1363,  1364,
    1365,  1366,  1367,  1368,  1369,  1370,  1371,  1372,  1373,  1374,
    1375,  1376,  1377,  1378,  1379,  1380,  1381,  1382,  1383,  1384,
    1385,  1386,  1387,  1388,  1389,  1390,  1396,  1391,  1393,  1394,
    1401,  1395,  1399,  1408,  1407,  1413,  1427,  1428,  1429,  1430,
    1440,  1442,  1443,  1439,  1447,  1448,  1449,  1470,  1485,  1465,
    1601,  1701,  1471,  1472,  1473,  1474,  1475,  1476,  1477,  1702,
    1478,  1479,  1703,  1480,  1481,  1482,  1483,  1484,  1496,  1500,
    1497,  1498,  1683,  1704,  1501,  1583,  1588,  1594,  1612,  1684,
    1613,  1614,  1615,  1651,  1624,  1682,  1632,  1705,  1706,  1708,
    1709,  1710,  1725,  1726,  1727,  1728,  1729,  1730,  1731,  1732,
    1733,  1737,  1739,  1748,  1751,  1749,  1757,  1758,  1759,  1760,
    1776,  1778,  1784,  1793,  1808,  1834,  1835,  1836,  1852,  1853,
    1855,  1857,  1861,  1877,  1887,  1913,  1914,  1915,  1916,  1918,
    1919,  1920,  1936,  1937,  1930,  1938,  1931,  1939,  1940,  1942,
    1943,  1948,  1952,  1953,  1954,  1973,  1974,  1977,  1989,  1992,
    1978,  1979,  1980,  1981,  2209,  2210,  1994,  1982,  2219,  2220,
    2221,  2222,  2223,  2224,  2225,  1983,  1984,  1985,  1986,  1987,
    1988,  2230,  2231,  2232,  2233,  2234,  2235,  1991,  1993,  2241,
    1995,  1996,  2249,  1997,  2007,  2010,  1998,  2012,  1999,  2000,
    2260,  2261,  2001,  2002,  2263,  2264,  2016,  2003,  2004,  2267,
    2005,  2006,  2272,  2273,  2008,  2274,  2275,  2009,  2011,  2284,
    2285,  2286,  2287,  2288,  2020,  2289,  2290,  2291,  2292,  2013,
    2014,  2015,  2017,  2018,  2299,  2300,  2301,  2019,  2022,  2021,
    2024,  2306,  2307,  2308,  2309,  2023,  2025,  2026,  2027,  2028,
    2029,  2030,  2330,  2031,  2032,  2033,  2034,  2035,  2342,  2343,
    2344,  2036,  2345,  2346,  2347,  2348,  2037,  2038,  2039,  2360,
    2361,  2362,  2363,  2043,  2045,  2366,  2047,  2040,  2051,  2372,
    2373,  2374,  2375,  2053,  2041,  2060,  2042,  2379,  2044,  2046,
    2048,  2383,  2384,  2049,  2062,  2386,  2065,  2050,  2052,  2054,
    2391,  2055,  2392,  2075,  2056,  2057,  2058,  2395,  2059,  2061,
    2063,  2064,  2066,  2067,  2068,  2069,  2070,  2071,  2072,  2073,
    2412,  2413,  2074,  2415,  2076,  2077,  2078,  2079,  2080,  2081,
    2426,  2427,  2428,  2082,  2083,  2084,  2430,  2085,  2086,  2439,
    2440,  2441,  2442,  2087,  2088,  2089,  2090,  2091,  2092,  2100,
    2108,  2093,  2094,  2095,  2096,  2097,  2112,  2098,  2099,  3227,
    2101,  2102,  2459,  2460,  2103,  2462,  2463,  2104,  2105,  2106,
    2107,  2109,  2110,  2115,  2117,  2111,  2113,  2114,  2116,  2119,
    2118,  2120,  2121,  2122,  2124,  2123,  2125,  2126,  2127,  2138,
    2128,  2129,  2141,  2147,  2149,  2130,  2131,  2132,  2152,  2157,
    2173,  2175,  2133,  2183,  2134,  2135,  2136,  2137,  2139,  2140,
    2142,  2143,  2144,  2145,  2146,  2148,  2150,  2151,  2153,  2154,
    2155,  2156,  2158,  2159,  2160,  2161,  2162,  2163,  2164,  2165,
    2193,  2204,  1502,  3232,  2988,  1685,  3230,  3231,  3233,  1183,
    1222,  3244,  1949,  2478,  2477,  2470,  1230,  3007,  3048,  3095,
     780,  3201,  1177,  3173,  3215,  3174,  1252,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  2747,     0,     0,     0,     0,     0,   600,     0,
       0,     0,     0,     0,     0,  2166,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  2167,  2168,     0,     0,     0,
    2169,  2170,  2171,     0,  2172,     0,     0,  2174,  2176,     0,
    2177,  2178,  2179,  2180,  2181,  2182,  2184,  2194,  2185,  3235,
    2208,  2206,  2207,  2218,  2476,  2226,  2250,     0,  2259,  2262,
    2283,  2305,  2314,  2335,  2336,  2359,     0,  2364,  2365,  2367,
    2368,     0,  2376,     0,     0,  2377,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  2378,     0,     0,  2380,  2381,
       0,     0,     0,     0,  2382,  2385,  2387,  2390,     0,     0,
       0,     0,     0,  2393,  2394,     0,     0,  2414,     0,  2419,
       0,     0,  2429,     0,     0,     0,  2458,     0,  2461,     0,
    2467,     0,     0,  2468,     0,  2469,     0,     0,     0,  2484,
    2485,  2486,  2487,  2488,  2489,     0,     0,  2490,  2491,  2492,
    2493,  2494,  2495,     0,  2496,  2497,  2498,  2499,  2500,  2501,
    2502,  2503,  2504,  2505,  2506,  2507,  2508,  2509,  2510,  2511,
    2512,  2513,  2514,  2515,  2516,  3237,  2517,  2518,  2519,  2520,
    2521,     0,     0,  2522,     0,  2523,  3246,  2524,  2525,  2526,
    2527,  2528,  2529,     0,  2530,  2531,  2532,  2533,  2534,  2535,
    2536,  2537,  2538,  2539,  2540,  2541,  2542,  2543,  2544,  2545,
    2546,     0,  2547,  2548,  2549,  2550,  2551,  2552,  2553,  2554,
    2555,  2556,  2557,  2558,  2559,     0,  2560,     0,  2561,  2562,
    2563,  2564,  2565,  2566,  2567,  2568,     0,  2569,  2570,  2571,
    2572,  2573,  2574,  2575,  2576,  2577,  2578,  2579,  2580,  2581,
    2582,  2583,  2584,  2585,     0,  2586,  2587,  2588,  2589,  2590,
       0,     0,  2591,     0,  2592,     0,  2593,  2594,  2595,  2596,
       0,  2597,     0,  2598,  2599,  2600,  2601,  2602,  2603,  2604,
    2605,     0,  2606,     0,  2607,  2608,  2609,  2610,  2611,  2612,
       0,  2613,  2614,  2615,  2616,  2617,  2618,  2619,  2620,  2621,
    2622,  2623,  2624,  2625,  2626,  2627,  2628,  2629,  2630,  2631,
    2632,  2633,  2634,  2635,  2636,     0,  2637,  2638,  2639,  2640,
    2641,  3225,  3226,  2642,  2643,  2644,  2645,  2646,  2647,  2648,
    2649,  2650,  2651,  2652,  2653,  2654,     0,     0,  2655,  2656,
    2657,  2658,  2659,     0,  2660,  2661,  2662,  2663,  2664,  2665,
    2666,  2667,  2668,  2669,  2670,  2671,  2672,  2673,  2674,  2675,
       0,     0,  2676,  2677,  2678,  2688,     0,  2689,  2690,  2691,
       0,     0,  2692,  2693,  2694,  2695,     0,  2696,  2697,     0,
       0,     0,  2698,  2699,  2700,     0,     0,     0,     0,  2701,
       0,  2702,  2703,  2704,  2705,  2706,  2707,  2708,  2709,  2710,
    2711,  2712,  2713,  2714,  2715,  2716,  2717,  2718,  2734,  2735,
    2736,  2737,  2738,  2739,  2740,  2741,  2742,     0,  2743,  2744,
    2745,     0,     0,  3238,     0,     0,     0,  3240,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  3242
};

static const yytype_int16 yycheck[] =
{
     475,   476,   871,   107,   923,   109,   110,   111,   433,   113,
    2189,  2190,  2191,  2192,  1197,  1315,  1316,  1397,  1398,  1972,
    1400,  1204,  2403,  2404,  2405,  2406,  2407,  2408,  2409,  2410,
    2411,   516,    23,     5,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    68,   188,   189,    13,    14,   186,    20,
      20,    21,    22,    23,    24,   924,    26,    27,    28,   200,
     164,   322,    59,   193,    67,    62,   200,    64,    68,    68,
    1441,    72,   176,   188,   189,   186,    13,    14,    36,    37,
      38,    84,    78,    79,    80,    81,    82,    75,    85,   423,
      15,    16,    17,    18,    19,    13,    14,    60,    61,    62,
      63,   194,   383,   188,   189,    36,    37,     5,     6,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    80,    68,
     454,    20,    21,    22,    23,    24,     0,    26,    27,    28,
     220,    20,    13,    14,   821,   822,   823,   824,   825,   826,
      80,   193,   640,   641,   642,   643,   644,   645,   646,   647,
     648,   203,   204,    43,   469,   104,    46,    99,   470,    57,
     469,    59,   266,    13,    14,   184,    64,   186,    66,    59,
     281,   282,   283,   284,   285,   752,   167,   157,   271,    68,
    1099,    22,   463,   464,   445,   446,   205,    85,   470,    87,
     161,   191,   193,   469,   166,   186,   168,    95,   161,   171,
     158,   345,   201,   175,   342,   193,  1586,    65,   693,   325,
       3,    68,   316,   317,   186,   206,   207,   179,   180,  1402,
    1403,  1404,  1405,  1406,   471,  1408,   316,   158,     3,   345,
       3,   198,   199,  1416,  1417,  1418,  1419,  1420,  1421,   179,
    1423,  1424,  1425,  1426,     3,   383,   350,   377,  1431,  1432,
    1433,  1434,  1435,  1436,  1437,  1438,  1627,  1628,   402,   370,
     345,   247,   248,   249,   181,   182,   183,   184,   185,   186,
     187,   296,     3,   190,   391,   192,   279,   247,   248,  1579,
     198,   199,  1582,   165,   166,   167,   168,     7,     8,   171,
      10,     3,   407,   175,   176,     3,  2475,   438,   985,   986,
     987,   442,     3,     9,   438,   194,   278,     3,   442,    29,
      30,    31,    32,    33,    34,    35,   312,   198,   199,   472,
     178,   459,   460,     3,   462,   250,   464,     3,   270,   271,
       3,   188,   189,   449,   450,   277,   247,   248,   249,   916,
       3,   918,   200,     3,   921,    61,    62,    63,   247,   248,
     208,   209,   186,    36,    37,    39,    40,    41,    42,   463,
     464,     3,   466,   253,   254,   255,   256,   257,   258,   472,
     260,   261,   262,   263,   859,   472,   362,    83,    96,   220,
     472,   367,   100,   101,   102,   472,   227,   228,   229,   230,
     231,   232,   233,   234,   235,   236,   278,   367,   472,   240,
     241,   242,   243,   244,   245,   210,   211,   212,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   362,   350,   287,   288,   472,   367,   448,   455,   456,
     457,   458,   465,   400,   401,   390,   252,     3,   367,   967,
     968,   969,   970,     3,    33,   159,   160,   413,   414,   415,
     416,   467,   380,     3,   429,   472,    40,   459,   460,   406,
    1409,  1410,  1411,  1412,   392,   412,   378,   467,     3,   444,
     213,   214,   447,   472,   290,   387,   451,   472,   453,    68,
     427,   217,   997,   998,   999,  1000,  1001,  1002,  1003,  1004,
    1005,  1006,  1007,  1008,  1009,  1010,  1011,  1012,  1013,  2482,
    2483,    68,    69,    70,    71,    72,   434,    11,    12,   259,
     246,     4,   383,    97,    98,    99,   408,   409,   410,    60,
      61,    22,    63,   339,    65,  1935,   467,    43,   208,   209,
      46,   267,   268,   472,  2945,  2946,  2947,  2948,   467,   298,
     299,   291,   472,    59,   280,    13,    14,   459,   460,   472,
     286,   194,   195,   196,   197,   371,   115,   210,   211,   212,
      61,   297,   472,   313,   320,   321,   322,   303,   304,   305,
     324,   325,    73,    74,    75,    76,    77,   472,   314,   315,
     169,   170,   141,   172,   173,   174,   795,   796,   167,  1686,
    1687,   725,   726,   329,  2807,  2808,   730,   731,   732,   472,
     734,   160,   338,  1933,   340,   355,   165,   186,   742,   472,
     744,   745,   746,   747,   173,   472,   186,     4,   177,    58,
     161,    65,   372,   193,    86,   167,   472,   206,   207,   472,
     200,   292,   293,   294,   295,   472,   386,   373,  2792,  2793,
    2794,  2795,  2796,   379,   186,   381,   307,   308,   309,   310,
     472,  1600,   388,   403,   788,   472,   790,  2839,  2840,   793,
     794,   162,   163,   164,   206,   207,   237,   238,   239,   405,
     472,   421,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   472,   418,   433,     4,    33,   422,   472,   424,   425,
     426,   472,   428,   443,  2810,  2811,  2812,  2813,  2814,  2815,
     436,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,   472,   452,   374,   375,   376,
      68,   213,   214,   215,   216,   461,  2797,  2798,  2799,  2800,
       4,  1944,  1945,  1946,  1947,  2932,  2933,   253,   254,   255,
     256,   257,   258,   472,   260,   261,   262,   263,   318,   319,
    2936,  2937,  1701,   323,   472,   472,   326,   327,   328,  2831,
    2832,  2833,  2834,   264,   265,   266,   306,   472,   269,     4,
     472,   272,   273,   274,   275,   276,  1444,  1445,  1446,   468,
     320,   472,  1450,  1451,  1452,  1453,  1454,  1455,  1456,  1457,
    1458,  1459,  1460,  1461,  1462,  1463,  1464,  2835,  2836,  2837,
    2838,   472,   472,   343,   356,   357,   358,   359,   360,   361,
    2972,  2973,  2974,  2975,   354,     4,   368,   369,   430,   431,
     432,   169,   170,   382,   172,   173,   174,    88,    89,    90,
      91,    92,    93,    94,   393,   472,   395,   435,   384,   468,
     468,   400,   469,   402,  2887,  2888,  2889,  2890,   469,   469,
     351,   352,   353,   354,   797,   798,   799,   800,   801,   802,
     803,   804,   805,   806,   807,   417,   425,   420,   468,   428,
    2750,  2751,  2752,  2753,  2754,  2755,  2756,  2757,  2758,  2759,
    2760,  2761,  2762,  2763,  2764,  2765,  2766,  2767,     3,   429,
     472,   431,  2769,  2770,  2771,   435,   436,   437,   438,   439,
     440,   441,    88,    89,    90,    91,    92,    93,    94,   251,
     411,   331,   332,   333,   334,   335,   336,   337,   419,   106,
     107,   108,   109,   110,   111,   112,   113,   467,   579,   580,
     581,   582,   583,   584,   585,   472,   437,     3,   478,   472,
     518,   519,   520,   521,   522,   523,   524,   525,   526,   489,
     490,   491,   492,   493,   494,  2801,  2802,  2803,   300,   301,
     302,  2928,  2929,  2930,  2931,   472,   472,  1635,   593,   594,
     595,   596,   597,   598,   599,   317,   586,   587,   588,   589,
     590,   591,   592,  1488,  1489,  1490,  1491,  1492,  1493,  1494,
    1495,  2772,  2773,  2774,  2775,  2776,  2777,  2778,  2779,  2780,
    2781,  2782,  2783,  2784,  2785,  2786,  2787,  2788,  2789,  2790,
    2791,  1466,  1467,  1468,  1469,   472,   831,   832,   833,   834,
     835,   836,   837,   838,   839,   472,   472,  3236,   468,   472,
    1485,  1486,  1487,  1656,  1657,  1658,  1659,  1660,  1661,  1662,
    1663,  1664,  1665,  1666,  1667,  1668,  1669,  1670,  2949,  2950,
    2951,  2952,   468,   395,  2891,  2892,  2893,  2894,  2895,  2896,
    2897,  2898,  2899,  2900,  2901,  2902,  2903,  2904,  2905,  2906,
    2907,  2908,  2909,  2910,  2911,  2912,  2913,  2914,  2915,  2916,
    2917,  2918,  2919,  2920,  2816,  2817,  2818,  2819,  2820,  2821,
    2822,  2823,  2824,  2825,  2826,  2827,  2828,  2829,  2830,  2857,
    2858,  2859,  2860,  2861,  2862,  2863,  2864,  2865,  2866,  2867,
    2868,  2869,  2870,  2871,   472,  2872,  2873,  2874,  2875,  2876,
    2877,  2878,  2879,  2880,  2881,  2882,  2883,  2884,  2885,  2886,
    2443,  2444,  2445,  2446,  2447,  2448,  2449,  2450,  2451,  2452,
    2453,  2454,  2455,  2456,  2457,  2847,  2848,  2849,  2850,  2851,
    2852,  2853,  2854,  2855,  2953,  2954,  2955,  2956,  2957,  2958,
    2959,  2960,  2961,  2963,  2964,  2965,  2966,  2967,  2968,  2969,
    2970,  2971,  2977,  2978,  2979,  2980,  2981,  2982,  2983,  2984,
    2985,  2921,  2922,  2923,  2924,  2925,  2926,  2927,  2938,  2939,
    2940,  2941,  2942,  2943,  2944,  2804,  2805,  2806,  2841,  2842,
    2843,   472,  2844,  2845,  2846,  2201,  2202,  2203,   472,   472,
     472,     3,   472,   472,   472,   472,     4,     3,   472,     4,
       3,     3,     3,   468,     4,     3,   468,     4,   468,   468,
     468,   468,   468,     4,     3,   469,   468,   468,   468,   468,
     468,   468,   468,   468,     4,     4,     4,   471,   471,   469,
     471,     3,   471,   471,   471,   471,   471,   471,     3,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,   472,
       3,   471,     4,   472,   472,   472,   472,   472,   472,   472,
     472,   472,     4,   471,   396,     4,   471,   471,   471,   471,
     471,   469,   469,   469,   469,   469,   469,   469,   469,   469,
     469,   469,   469,   469,   469,   469,   469,   469,   469,     4,
       3,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,     3,   472,   468,   468,   471,     4,   161,   471,
     471,   471,   471,   471,   471,   471,     3,   471,     4,   472,
     468,   472,   472,   472,   469,   469,   469,   469,   468,   468,
     468,   468,   468,   468,   105,   469,     3,    67,     4,     3,
     473,   473,   473,   473,   473,   473,   473,   473,   473,   473,
     473,   473,   473,   471,   473,   468,   473,   473,   473,   471,
     469,   466,   472,   471,   471,     3,     3,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   469,   471,     4,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   469,   469,   469,   469,   469,   469,   469,
     469,   469,   469,   469,   469,   469,   469,   469,   469,   469,
       4,     3,     3,   472,   472,   472,   472,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,     3,
     472,   472,   472,   472,   472,   472,   472,   472,   472,   472,
     472,   472,   472,     4,   471,     4,     3,   472,     4,     3,
       3,   472,     4,   472,     3,   472,   472,     4,   473,   472,
     472,   472,   472,     3,   472,   472,     4,     4,   471,   471,
     471,   471,     4,   472,     4,     4,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   469,   468,   472,   469,   469,   468,   468,
       4,   469,   468,   468,   468,   472,   469,   469,   469,   469,
     468,   468,   471,   469,   468,   468,   471,   469,   469,   469,
       3,     3,   469,   471,   473,   471,   471,     3,   471,   473,
     471,   471,   471,   471,     3,   469,     3,   473,     4,   471,
     471,   471,   471,   471,     3,   471,     4,   471,   471,   471,
      75,     4,   473,   473,   473,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   469,   471,   473,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   468,   472,
     472,   472,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,   469,     4,     3,     3,
     469,     4,     4,     4,   472,     3,   469,   469,   469,   469,
       4,     4,     4,   472,   472,   472,     4,   471,     4,   469,
      20,     4,   471,   471,   471,   471,   471,   471,   471,     4,
     471,   471,     4,   471,   471,   471,   471,   471,   468,   471,
     469,   469,   473,     4,   471,   471,   471,   469,   471,   473,
     471,   471,   471,   471,   468,   471,   468,     4,   177,     4,
       4,   178,     4,     4,     4,     4,     4,     4,     4,     4,
       4,   193,     4,     4,   202,   193,     4,     4,     4,     4,
     193,   193,   193,   193,     4,     4,     4,     4,     4,   404,
     306,     4,   330,     4,   382,     4,     4,     4,   383,     4,
       4,     4,   471,     4,   103,     4,   103,     4,     4,   472,
       4,   471,   471,   471,   471,   469,   469,   472,     4,     4,
     472,   472,   472,   472,  1978,  1979,     4,   472,  1982,  1983,
    1984,  1985,  1986,  1987,  1988,   472,   472,   472,   472,   472,
     472,  1995,  1996,  1997,  1998,  1999,  2000,   472,   472,  2003,
     472,   472,  2006,   472,     4,     4,   472,     4,   472,   472,
    2014,  2015,   472,   472,  2018,  2019,     4,   472,   472,  2023,
     472,   472,  2026,  2027,   472,  2029,  2030,   472,   472,  2033,
    2034,  2035,  2036,  2037,     4,  2039,  2040,  2041,  2042,   472,
     472,   472,   472,   472,  2048,  2049,  2050,   472,     4,   472,
       4,  2055,  2056,  2057,  2058,   472,   472,   472,   472,     4,
     472,   472,  2066,   472,   472,   472,   472,   472,  2072,  2073,
    2074,   472,  2076,  2077,  2078,  2079,   472,     4,   472,  2083,
    2084,  2085,  2086,     4,     4,  2089,     4,   472,     4,  2093,
    2094,  2095,  2096,     4,   472,     4,   472,  2101,   472,   472,
     472,  2105,  2106,   472,     4,  2109,     4,   472,   472,   472,
    2114,   472,  2116,     4,   472,   472,   472,  2121,   472,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,   472,
    2134,  2135,   472,  2137,   472,   472,   472,   472,     4,   472,
    2144,  2145,  2146,   472,   472,   472,  2150,   472,   472,  2153,
    2154,  2155,  2156,   472,   472,   472,   472,   472,   472,     4,
       4,   472,   472,   472,   472,   472,     4,   472,   472,  2988,
     472,   472,  2176,  2177,   472,  2179,  2180,   472,   472,   472,
     472,   472,   472,     4,     4,   472,   472,   472,   472,     4,
     472,   472,   472,     4,     4,   472,   471,   471,   471,     4,
     471,   471,     4,     4,     4,   471,   471,   471,     4,     4,
       4,     4,   471,     4,   472,   472,   472,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,   472,
     472,   472,   471,   471,   471,   471,   471,   471,   471,   471,
       4,  1956,  1314,   103,     3,  1499,     4,     4,     4,   874,
     917,     4,  1625,  2200,  2195,  2188,   925,  2768,  2809,  2856,
     602,  2962,   868,  2934,  2976,  2935,   945,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,   424,    -1,
      -1,    -1,    -1,    -1,    -1,   471,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   471,   471,    -1,    -1,    -1,
     471,   471,   471,    -1,   471,    -1,    -1,   472,   472,    -1,
     472,   472,   472,   472,   472,   472,   472,   468,   472,   472,
     468,   471,   471,   468,   471,   469,   468,    -1,   468,   468,
     468,   468,   468,   468,   468,   468,    -1,   469,   468,   468,
     468,    -1,   468,    -1,    -1,   468,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   468,    -1,    -1,   468,   468,
      -1,    -1,    -1,    -1,   468,   468,   468,   468,    -1,    -1,
      -1,    -1,    -1,   468,   468,    -1,    -1,   469,    -1,   469,
      -1,    -1,   468,    -1,    -1,    -1,   468,    -1,   468,    -1,
     468,    -1,    -1,   468,    -1,   468,    -1,    -1,    -1,   471,
     471,   471,   471,   471,   471,    -1,    -1,   471,   471,   471,
     471,   471,   471,    -1,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,  3234,   471,   471,   471,   471,
     471,    -1,    -1,   471,    -1,   471,  3245,   471,   471,   471,
     471,   471,   471,    -1,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,    -1,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,    -1,   471,    -1,   471,   471,
     471,   471,   471,   471,   471,   471,    -1,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,    -1,   471,   471,   471,   471,   471,
      -1,    -1,   471,    -1,   471,    -1,   471,   471,   471,   471,
      -1,   471,    -1,   471,   471,   471,   471,   471,   471,   471,
     471,    -1,   471,    -1,   471,   471,   471,   471,   471,   471,
      -1,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,    -1,   471,   471,   471,   471,
     471,  2986,  2987,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,    -1,    -1,   471,   471,
     471,   471,   471,    -1,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
      -1,    -1,   471,   471,   471,   471,    -1,   471,   471,   471,
      -1,    -1,   471,   471,   471,   471,    -1,   471,   471,    -1,
      -1,    -1,   471,   471,   471,    -1,    -1,    -1,    -1,   471,
      -1,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,    -1,   471,   471,
     471,    -1,    -1,   468,    -1,    -1,    -1,   471,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  3241
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int16 yystos[] =
{
       0,   475,   476,     0,   157,   469,   470,   469,   470,   469,
     471,   477,     5,     6,    57,    59,    64,    66,    85,    87,
      95,   350,   380,   392,   434,   478,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     7,
       8,    10,    29,    30,    31,    32,    33,    34,    35,   259,
     291,   313,   355,   372,   386,   403,   421,   433,   443,   497,
     479,    13,    14,   400,   401,   617,   487,   481,   483,   485,
      88,    89,    90,    91,    92,    93,    94,   741,    96,   100,
     101,   102,   767,    88,    89,    90,    91,    92,    93,    94,
     749,   489,   391,   495,   491,   472,     3,     3,   472,   472,
     472,   472,   472,   472,     3,     3,     3,   472,     3,   472,
     472,   472,   472,   472,   472,     4,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
     251,   300,   301,   302,   317,   395,   583,   472,   472,   472,
     472,     4,    58,   730,    65,   627,    67,    84,   279,   465,
     657,    86,   721,   472,   472,   472,   472,   472,   472,   472,
       4,   472,   472,   472,   472,     4,   472,   472,   472,   472,
     472,   472,   472,     4,   468,   493,   472,     4,   435,  1206,
     468,   507,    13,    14,   531,   469,   469,   469,    36,    37,
     468,    11,    12,   775,    39,    40,    41,    42,   159,   160,
     558,   508,   292,   293,   294,   295,   307,   308,   309,   310,
     570,   775,   356,   357,   358,   359,   360,   361,   368,   369,
     417,   757,   775,   775,   775,   420,   775,   468,   478,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,     3,     3,   480,
     468,   468,   188,   189,   345,   188,   189,   345,   402,   478,
       3,     4,     3,     4,     3,     3,   472,     3,     4,     3,
       4,   468,   468,   468,   468,   468,   468,   468,   478,    40,
      97,    98,    99,   469,   468,   775,   478,   468,   468,   468,
     468,   468,   468,   468,   478,   471,     4,   775,   478,     3,
       4,   471,   469,   529,   532,   533,     4,   471,   471,   471,
     471,   471,   471,   471,   472,   472,   472,   472,   472,   472,
       4,    43,    46,    59,   253,   254,   255,   256,   257,   258,
     260,   261,   262,   263,   515,   472,   472,   472,   472,   472,
       3,     3,     3,     4,   471,   472,   472,   472,   472,   472,
     472,   472,   472,   472,     4,   471,   471,   471,   471,   471,
     471,   469,   469,   469,   469,   469,   469,   469,   469,   469,
     469,   469,   469,   469,   469,   469,   469,   469,   469,   602,
     396,   611,     4,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   731,   488,   628,   482,   658,   660,   775,   662,
     484,   722,   486,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   494,   490,   471,  1207,   492,   498,
     472,     4,     3,     3,   497,   499,   500,   501,   502,   503,
     504,   506,    36,    37,    38,   158,    36,    37,   158,   775,
     775,   468,   468,   497,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   472,   472,   472,   472,     4,   469,   469,
     469,    68,   296,   469,   577,   579,   581,   497,   509,   468,
     468,   468,   468,   468,   775,   468,   468,   469,   497,   510,
     512,   513,   511,   505,   514,   473,   473,   473,   473,   473,
     473,   473,   473,   473,   473,   473,   473,   473,   471,   473,
     473,   473,   473,   161,   604,     3,     4,   478,   618,   619,
     621,   620,   622,   624,   623,   626,   625,    60,    61,    62,
      63,   161,   733,   478,    60,    61,    63,    65,   161,   630,
     478,    22,    61,    73,    74,    75,    76,    77,   162,   163,
     164,   264,   265,   266,   269,   272,   273,   274,   275,   276,
     351,   352,   353,   354,   411,   419,   437,   675,   468,   665,
     471,   466,   667,   478,    61,    62,    63,   724,   478,   742,
     743,   744,   745,   746,   747,   748,   769,   768,   770,   771,
     772,   773,   774,   750,   751,   752,   753,   754,   755,   756,
     493,   478,   496,    65,   178,   200,   208,   209,  1209,   478,
     497,   469,   497,    15,    16,    17,    18,    19,   250,   390,
     534,   534,   497,   497,   497,   497,   497,   497,   497,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     516,   517,   518,   519,   520,   521,   522,   523,   524,   775,
     775,   469,   775,   497,   471,   471,   471,   471,   471,   471,
     105,   648,   648,   648,   497,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   497,   497,   497,   497,   497,   497,
     469,   469,   469,   469,   469,   469,   469,   469,   469,   469,
     469,   469,   469,   598,   469,   469,   469,   469,     3,     4,
     612,   583,   617,   617,   617,   617,   617,   617,   617,   617,
     617,   472,   472,     3,     3,   472,     4,   472,   472,     3,
     472,   472,     4,   472,   472,   472,   472,   472,     3,     3,
     472,   472,   472,   472,   472,     3,   472,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,     4,
     471,     4,   664,     3,     4,   472,     3,     3,     4,   741,
     741,   741,   741,   741,   741,   741,   767,   767,   767,   767,
     767,   767,   767,   749,   749,   749,   749,   749,   749,   749,
     495,   472,   472,     3,   472,   472,     4,   473,   472,   472,
     472,   472,     3,   472,   472,     4,     4,   559,   560,   561,
     562,   565,   566,   567,   564,   569,   563,   568,   515,   515,
     515,   515,   515,   515,   515,   515,   515,   471,   471,   471,
     471,   571,   572,   573,   574,   575,   576,   472,     4,     4,
       4,   758,   759,   762,   763,   764,   765,   760,   761,   766,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   583,   471,   471,   471,   471,   605,   603,
      20,   161,   614,   469,   468,   737,   739,   469,   732,   469,
     468,   635,   468,   469,   629,    68,    69,    70,    71,    72,
     705,   468,   775,   775,   468,   680,    78,    79,    80,    81,
      82,   312,   706,   775,   775,   775,   468,   775,    67,   672,
      99,   270,   271,   277,   469,   469,   469,   469,   468,   775,
     468,   775,   775,   775,   775,   469,   659,   666,   661,   657,
     668,   663,   468,   726,   728,   723,   468,    80,   179,  1228,
      20,    21,    22,    23,    24,    26,    27,    28,   247,   248,
     367,   467,   210,   211,   212,  1208,   469,   775,   469,   775,
     469,   539,   775,   775,   531,   531,   558,   558,   558,   558,
     558,   558,   558,   558,   558,   558,   558,   525,   526,   527,
     528,   570,   570,   570,   570,   570,   570,   106,   107,   108,
     109,   110,   111,   112,   113,   578,   580,   582,   757,   757,
     757,   757,   757,   757,   757,   757,   757,   584,   585,   586,
     591,   592,   588,   587,   590,   589,   594,   593,   596,   595,
     597,   599,   600,   601,    20,    68,   194,   607,   583,   472,
     472,     4,   471,   471,     9,    83,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   217,
     246,   267,   268,   280,   286,   297,   303,   304,   305,   314,
     315,   329,   338,   340,   373,   379,   381,   388,   405,   418,
     422,   424,   425,   426,   428,   436,   452,   461,   719,   776,
     777,   798,   800,   802,   804,   806,   810,   812,   814,   820,
     843,   845,   847,   849,   851,   853,   855,   857,   859,   861,
     869,   876,   878,   884,   890,   892,   897,   901,   910,   912,
     914,   916,   918,   936,   943,   950,   955,   961,   967,   979,
     986,   990,  1008,  1026,  1028,  1035,  1068,  1070,  1072,  1074,
    1084,  1091,  1095,  1100,  1102,  1106,  1110,  1112,  1117,  1127,
    1143,  1161,  1163,  1170,  1182,  1194,  1196,  1198,  1200,  1204,
    1234,  1236,  1238,    68,   104,   637,   473,   730,   471,   471,
     637,   471,   473,   627,   471,   471,   471,   471,   471,   469,
     703,     3,     3,     3,     3,   469,     3,     4,   471,   471,
     471,   471,   471,     3,     4,   471,   471,   471,   471,   473,
     473,   473,   473,   471,   471,   471,   471,   471,   471,   471,
     471,   657,   665,   657,    75,   670,   657,   471,   719,   637,
     721,   471,   471,   471,    75,   193,  1230,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,  1206,   473,   471,   471,   471,   471,    20,    21,
      22,    23,    24,    25,    26,    27,    28,   247,   248,   249,
     362,   367,   467,   542,   471,   471,   515,   515,   515,   515,
     471,   471,   471,   471,   471,   471,   471,   471,   570,   570,
     570,   583,   583,   583,   583,   583,   583,   583,   583,   583,
     583,   583,   583,   583,   583,   583,   583,   583,   472,   472,
     472,     4,   468,   469,   613,   734,   735,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     4,   719,     3,     3,     4,   469,   631,   632,     4,
     634,   469,   676,   677,   679,   681,   678,   472,     4,   707,
     709,   711,   713,     3,   717,   675,   682,   683,   684,   685,
     686,   673,   675,   694,   692,   693,   695,   469,   469,   469,
     469,   691,   697,   696,   698,   699,   700,   701,   702,   472,
       4,   725,     4,     4,  1210,  1226,  1227,   472,   472,     4,
    1211,  1212,  1216,  1220,  1217,  1214,  1213,  1215,  1218,  1219,
    1224,  1225,  1221,  1222,  1223,   469,   535,   536,   537,   538,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,     4,   540,   541,   649,   650,
     651,   652,   653,   654,   655,   656,   468,   469,   469,   606,
     471,   471,   611,   733,   733,   778,   720,   799,   801,   803,
     805,   807,   811,   813,   815,   821,   844,   846,   848,   850,
     852,   854,   856,   858,   860,   862,   870,   877,   879,   885,
     891,   893,   898,   902,   911,   913,   915,   917,   919,   937,
     944,   951,   956,   962,   968,   987,   991,  1009,  1027,  1029,
    1036,  1069,  1071,  1073,  1075,  1085,  1096,  1101,  1103,  1107,
    1111,  1113,  1118,  1128,  1162,  1164,  1171,  1092,  1183,   980,
    1144,  1195,  1197,  1199,  1201,  1205,  1235,  1237,  1239,   738,
     638,   642,   740,   471,   630,   630,   636,   630,   471,   675,
     675,   675,   675,   675,   469,   675,   719,   719,   719,   719,
     715,    20,   675,   675,   675,   675,   675,   675,   675,   675,
     675,   675,   471,   471,   471,   471,   675,   675,   675,   675,
     675,   675,   675,   675,   468,   669,   724,   727,   729,  1209,
    1209,  1209,   468,   213,   214,  1229,  1209,  1209,  1209,  1209,
    1209,  1209,  1209,  1209,  1209,  1209,  1209,  1209,  1209,  1209,
    1209,   471,   534,   534,   534,   534,   543,   544,   545,   546,
     547,   548,   549,   550,   551,   552,   553,   554,   555,   556,
     557,   534,   534,   534,   648,   648,   648,   648,   648,   648,
     648,   648,   471,   473,   473,   604,   616,   615,   165,   166,
     167,   168,   171,   175,   176,   278,   378,   387,   459,   460,
     779,     4,     4,     4,     4,     4,   177,   808,     4,     4,
     178,   816,   181,   182,   183,   184,   185,   186,   187,   190,
     192,   406,   412,   427,   822,     4,     4,     4,     4,     4,
       4,     4,     4,     4,    72,   193,   863,   193,   871,     4,
     200,   438,   442,   880,   200,   438,   442,   886,     4,   193,
     894,   202,   899,   193,   203,   204,   903,     4,     4,     4,
       4,     5,   166,   168,   171,   175,   186,   278,   459,   460,
     920,   252,   290,   339,   371,   938,   193,   945,   193,   952,
     184,   186,   205,   957,   193,   963,    23,   167,   186,   206,
     207,   448,   969,   193,   988,   208,   209,   992,   186,   193,
     200,   318,   319,   323,   326,   327,   328,  1010,     4,   167,
     186,   206,   207,  1030,    22,   220,   227,   228,   229,   230,
     231,   232,   233,   234,   235,   236,   240,   241,   242,   243,
     244,   245,   383,  1037,     4,     4,     4,   186,   281,   282,
     283,   284,   285,   370,  1076,   186,   287,   288,  1086,   298,
     299,  1097,     4,   404,  1104,   306,  1108,     4,   220,   316,
    1114,   330,  1119,    43,    46,    59,   253,   254,   255,   256,
     257,   258,   260,   261,   262,   263,  1129,     4,   193,   377,
    1165,   429,   444,   447,   451,   453,  1172,   382,  1093,   423,
     454,  1184,   167,   186,   206,   207,   981,    20,    21,    22,
      23,    24,    25,    26,    27,    28,   247,   248,   249,   362,
     367,   467,  1145,     4,     4,     4,   383,  1202,     4,     4,
       4,   186,   342,   383,   459,   460,   462,   464,  1240,   733,
     103,   103,   733,   736,   630,   633,   471,     4,     4,     4,
       4,   719,   472,     4,   687,   688,   689,   690,   471,   667,
     724,   724,   471,   471,   471,  1209,   530,   542,   542,   542,
     542,   542,   542,   542,   542,   542,   542,   542,   542,   542,
     542,   542,   610,   469,   469,   614,   614,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,     4,
     719,   472,     4,   472,     4,   472,   472,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,     4,   472,   472,
       4,   472,     4,   472,   472,   472,     4,   472,   472,   472,
       4,   472,     4,   472,     4,   472,   472,   472,     4,   472,
     472,   472,   472,   472,   472,   472,   472,   472,     4,   472,
     472,   472,   472,     4,   472,     4,   472,     4,   472,   472,
     472,     4,   472,     4,   472,   472,   472,   472,   472,   472,
       4,   472,     4,   472,   472,     4,   472,   472,   472,   472,
     472,   472,   472,   472,   472,     4,   472,   472,   472,   472,
       4,   472,   472,   472,   472,   472,   472,   472,   472,   472,
     472,   472,   472,   472,   472,   472,   472,   472,   472,   472,
       4,   472,   472,   472,   472,   472,   472,   472,     4,   472,
     472,   472,     4,   472,   472,     4,   472,     4,   472,     4,
     472,   472,     4,   472,     4,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   472,   472,   472,   472,     4,   472,
     472,     4,   472,   472,   472,   472,   472,     4,   472,     4,
     472,   472,     4,   472,   472,   472,   472,     4,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,     4,   472,     4,   472,   472,   472,   472,
     472,   472,   472,     4,   472,   472,   733,   630,   704,   708,
     710,   712,   714,     4,   468,   674,   675,   675,   675,   675,
     671,  1231,  1232,  1233,   529,   607,   471,   471,   468,   775,
     775,    33,    68,   169,   170,   172,   173,   174,   468,   775,
     775,   775,   775,   775,   775,   775,   469,    80,   179,   180,
     775,   775,   775,   775,   775,   775,    68,   188,   189,    68,
     191,   775,   188,   189,   407,   413,   414,   415,   416,   775,
     468,   194,   195,   196,   197,    13,    14,   198,   199,   468,
     775,   775,   468,   775,   775,    68,   201,   775,    13,    14,
     198,   199,   775,   775,   775,   775,    33,    68,   169,   170,
     172,   173,   174,   468,   775,   775,   775,   775,   775,   775,
     775,   775,   775,    13,    14,   198,   199,    13,    14,   775,
     775,   775,   408,   409,   410,   468,   775,   775,   775,   775,
     325,   345,   449,   450,   468,    20,    21,    22,    23,    24,
      26,    27,    28,   247,   248,   367,   467,   210,   211,   212,
     775,   213,   214,   215,   216,   468,   468,   320,   321,   322,
     324,   325,   775,   775,   775,   775,   775,   775,   775,   218,
     219,   220,   221,   222,   223,   224,   225,   226,   384,   468,
     775,   775,   775,   775,   469,   468,   775,   468,   468,   237,
     238,   239,   775,   775,   775,   775,   468,   468,   468,   775,
     468,   468,   468,   775,   775,   468,   775,   468,   194,   271,
     468,   775,   775,   468,   468,   775,   331,   332,   333,   334,
     335,   336,   337,  1130,  1131,  1132,  1133,  1134,  1135,  1136,
    1137,  1138,   775,   775,   469,   775,   374,   375,   376,   469,
     430,   431,   432,   322,   445,   446,   775,   775,   775,   468,
     775,    59,    62,    64,    85,   455,   456,   457,   458,   775,
     775,   775,   775,  1146,  1147,  1148,  1149,  1150,  1151,  1152,
    1153,  1154,  1155,  1156,  1157,  1158,  1159,  1160,   468,   775,
     775,   468,   775,   775,   383,   463,   464,   468,   468,   468,
     703,   706,   706,   706,   706,   716,   471,   672,   670,  1230,
    1230,  1230,   608,   609,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,  1129,
    1129,  1129,  1129,  1129,  1129,  1129,  1129,  1129,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   471,   471,   471,  1145,
    1145,  1145,  1145,  1145,  1145,  1145,  1145,  1145,  1145,  1145,
    1145,  1145,  1145,  1145,   471,   471,   471,   471,   471,   471,
     471,   471,   471,   471,   471,   471,   706,    62,   607,   607,
     780,   781,   782,   783,   789,   784,   785,   786,   787,   788,
     790,   791,   792,   793,   794,   795,   796,   797,   809,   817,
     818,   819,   823,   824,   825,   826,   827,   828,   831,   829,
     830,   832,   833,   834,   837,   835,   836,   839,   840,   841,
     838,   842,   864,   865,   866,   867,   868,   872,   873,   874,
     875,   881,   882,   883,   887,   888,   889,   895,   896,   900,
     904,   905,   906,   907,   908,   909,   929,   935,   921,   927,
     922,   923,   924,   925,   926,   928,   931,   930,   932,   933,
     934,   939,   940,   941,   942,   946,   947,   948,   949,   953,
     954,   958,   960,   959,   964,   965,   966,   978,   972,   973,
     970,   971,   975,   974,   976,   977,   989,   993,   994,   998,
    1002,   999,   996,   995,   997,  1000,  1001,  1006,  1007,  1003,
    1004,  1005,  1011,  1012,  1013,  1014,  1015,  1016,  1017,  1018,
    1019,  1020,  1022,  1021,  1023,  1024,  1025,  1033,  1034,  1031,
    1032,  1038,  1039,  1040,  1041,  1042,  1043,  1044,  1045,  1046,
    1047,  1054,  1048,  1050,  1051,  1052,  1053,  1055,  1049,  1056,
    1057,  1058,  1059,  1060,  1061,  1062,  1063,  1064,  1065,  1066,
    1067,  1077,  1078,  1079,  1080,  1081,  1082,  1083,  1087,  1088,
    1089,  1090,  1098,  1099,  1105,  1109,  1116,  1115,  1120,  1121,
    1122,  1123,  1124,  1125,  1126,  1139,  1140,  1141,  1142,  1166,
    1167,  1168,  1169,  1173,  1174,  1175,  1176,  1177,  1178,  1179,
    1180,  1181,  1094,  1185,  1187,  1192,  1189,  1188,  1186,  1190,
    1191,  1193,   984,   985,   982,   983,  1203,  1249,  1241,  1245,
    1247,  1248,  1243,  1242,  1244,  1246,   639,   643,     3,   779,
     779,   779,   779,   779,   779,   779,   779,   779,   779,   779,
     779,   779,   779,   779,   779,   779,   779,   808,   816,   816,
     816,   822,   822,   822,   822,   822,   822,   822,   822,   822,
     822,   822,   822,   822,   822,   822,   822,   822,   822,   822,
     822,   863,   863,   863,   863,   863,   871,   871,   871,   871,
     880,   880,   880,   886,   886,   886,   894,   894,   899,   903,
     903,   903,   903,   903,   903,   920,   920,   920,   920,   920,
     920,   920,   920,   920,   920,   920,   920,   920,   920,   920,
     938,   938,   938,   938,   945,   945,   945,   945,   952,   952,
     957,   957,   957,   963,   963,   963,   969,   969,   969,   969,
     969,   969,   969,   969,   969,   988,   992,   992,   992,   992,
     992,   992,   992,   992,   992,   992,   992,   992,   992,   992,
     992,  1010,  1010,  1010,  1010,  1010,  1010,  1010,  1010,  1010,
    1010,  1010,  1010,  1010,  1010,  1010,  1030,  1030,  1030,  1030,
    1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,
    1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,
    1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,  1037,
    1076,  1076,  1076,  1076,  1076,  1076,  1076,  1086,  1086,  1086,
    1086,  1097,  1097,  1104,  1108,  1114,  1114,  1119,  1119,  1119,
    1119,  1119,  1119,  1119,  1129,  1129,  1129,  1129,  1165,  1165,
    1165,  1165,  1172,  1172,  1172,  1172,  1172,  1172,  1172,  1172,
    1172,  1093,  1184,  1184,  1184,  1184,  1184,  1184,  1184,  1184,
    1184,   981,   981,   981,   981,  1202,  1240,  1240,  1240,  1240,
    1240,  1240,  1240,  1240,  1240,   648,   648,   719,   640,   644,
       4,     4,   103,     4,   641,   472,   718,   637,   468,   706,
     471,   645,   648,   646,     4,   647,   637
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int16 yyr1[] =
{
       0,   474,   476,   477,   475,   478,   479,   480,   478,   478,
     481,   482,   478,   483,   484,   478,   485,   486,   478,   487,
     488,   478,   478,   478,   478,   478,   489,   490,   478,   491,
     492,   478,   478,   494,   493,   493,   496,   495,   495,   498,
     497,   499,   497,   500,   497,   501,   497,   497,   502,   497,
     503,   497,   504,   497,   505,   497,   506,   497,   497,   507,
     497,   508,   497,   497,   509,   497,   510,   497,   511,   497,
     512,   497,   513,   497,   497,   514,   497,   497,   516,   515,
     517,   515,   518,   515,   519,   515,   520,   515,   521,   515,
     522,   515,   523,   515,   524,   515,   525,   515,   526,   515,
     527,   515,   528,   515,   515,   530,   529,   529,   532,   531,
     533,   531,   531,   535,   534,   536,   534,   537,   534,   538,
     534,   539,   534,   540,   534,   541,   534,   534,   543,   542,
     544,   542,   545,   542,   546,   542,   547,   542,   548,   542,
     549,   542,   550,   542,   551,   542,   552,   542,   553,   542,
     554,   542,   555,   542,   556,   542,   557,   542,   542,   559,
     558,   560,   558,   561,   558,   562,   558,   563,   558,   564,
     558,   565,   558,   566,   558,   567,   558,   568,   558,   569,
     558,   558,   571,   570,   572,   570,   573,   570,   574,   570,
     575,   570,   576,   570,   577,   578,   570,   579,   580,   570,
     581,   582,   570,   570,   584,   583,   585,   583,   586,   583,
     587,   583,   588,   583,   589,   583,   590,   583,   591,   583,
     592,   583,   593,   583,   594,   583,   595,   583,   596,   583,
     597,   583,   598,   583,   599,   583,   600,   583,   601,   583,
     602,   603,   583,   583,   583,   605,   606,   604,   604,   608,
     607,   609,   607,   610,   607,   607,   612,   613,   611,   611,
     615,   614,   616,   614,   614,   618,   617,   619,   617,   620,
     617,   621,   617,   622,   617,   623,   617,   624,   617,   625,
     617,   626,   617,   617,   628,   629,   627,   627,   631,   630,
     632,   630,   633,   630,   634,   630,   635,   636,   630,   630,
     638,   639,   640,   641,   637,   642,   643,   644,   645,   646,
     647,   637,   637,   649,   648,   650,   648,   651,   648,   652,
     648,   653,   648,   654,   648,   655,   648,   656,   648,   648,
     658,   659,   657,   660,   661,   657,   662,   663,   657,   664,
     657,   657,   666,   665,   665,   668,   669,   667,   667,   671,
     670,   670,   673,   674,   672,   672,   676,   675,   677,   675,
     678,   675,   679,   675,   680,   675,   681,   675,   682,   675,
     683,   675,   684,   675,   675,   685,   675,   686,   675,   675,
     687,   675,   688,   675,   689,   675,   690,   675,   691,   675,
     692,   675,   693,   675,   694,   675,   695,   675,   696,   675,
     697,   675,   698,   675,   699,   675,   700,   675,   701,   675,
     702,   675,   675,   704,   703,   703,   705,   705,   705,   705,
     705,   707,   708,   706,   709,   710,   706,   711,   712,   706,
     713,   714,   706,   715,   716,   706,   717,   718,   706,   706,
     719,   719,   719,   720,   722,   723,   721,   721,   725,   724,
     726,   727,   724,   728,   729,   724,   724,   731,   732,   730,
     730,   734,   733,   735,   733,   736,   733,   737,   738,   733,
     739,   740,   733,   733,   742,   741,   743,   741,   744,   741,
     745,   741,   746,   741,   747,   741,   748,   741,   741,   750,
     749,   751,   749,   752,   749,   753,   749,   754,   749,   755,
     749,   756,   749,   749,   758,   757,   759,   757,   760,   757,
     761,   757,   762,   757,   763,   757,   764,   757,   765,   757,
     766,   757,   757,   768,   767,   769,   767,   770,   767,   771,
     767,   772,   767,   773,   767,   774,   767,   767,   775,   775,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   776,   776,   776,   776,   776,   776,   776,
     776,   776,   776,   778,   777,   780,   779,   781,   779,   782,
     779,   783,   779,   784,   779,   785,   779,   786,   779,   787,
     779,   788,   779,   789,   779,   790,   779,   791,   779,   792,
     779,   793,   779,   794,   779,   795,   779,   796,   779,   797,
     779,   779,   799,   798,   801,   800,   803,   802,   805,   804,
     807,   806,   809,   808,   808,   811,   810,   813,   812,   815,
     814,   817,   816,   818,   816,   819,   816,   816,   821,   820,
     823,   822,   824,   822,   825,   822,   826,   822,   827,   822,
     828,   822,   829,   822,   830,   822,   831,   822,   832,   822,
     833,   822,   834,   822,   835,   822,   836,   822,   837,   822,
     838,   822,   839,   822,   840,   822,   841,   822,   842,   822,
     822,   844,   843,   846,   845,   848,   847,   850,   849,   852,
     851,   854,   853,   856,   855,   858,   857,   860,   859,   862,
     861,   864,   863,   865,   863,   866,   863,   867,   863,   868,
     863,   863,   870,   869,   872,   871,   873,   871,   874,   871,
     875,   871,   871,   877,   876,   879,   878,   881,   880,   882,
     880,   883,   880,   880,   885,   884,   887,   886,   888,   886,
     889,   886,   886,   891,   890,   893,   892,   895,   894,   896,
     894,   894,   898,   897,   900,   899,   899,   902,   901,   904,
     903,   905,   903,   906,   903,   907,   903,   908,   903,   909,
     903,   903,   911,   910,   913,   912,   915,   914,   917,   916,
     919,   918,   921,   920,   922,   920,   923,   920,   924,   920,
     925,   920,   926,   920,   927,   920,   928,   920,   929,   920,
     930,   920,   931,   920,   932,   920,   933,   920,   934,   920,
     935,   920,   920,   937,   936,   939,   938,   940,   938,   941,
     938,   942,   938,   938,   944,   943,   946,   945,   947,   945,
     948,   945,   949,   945,   945,   951,   950,   953,   952,   954,
     952,   952,   956,   955,   958,   957,   959,   957,   960,   957,
     957,   962,   961,   964,   963,   965,   963,   966,   963,   963,
     968,   967,   970,   969,   971,   969,   972,   969,   973,   969,
     974,   969,   975,   969,   976,   969,   977,   969,   978,   969,
     969,   980,   979,   982,   981,   983,   981,   984,   981,   985,
     981,   981,   987,   986,   989,   988,   988,   991,   990,   993,
     992,   994,   992,   995,   992,   996,   992,   997,   992,   998,
     992,   999,   992,  1000,   992,  1001,   992,  1002,   992,  1003,
     992,  1004,   992,  1005,   992,  1006,   992,  1007,   992,   992,
    1009,  1008,  1011,  1010,  1012,  1010,  1013,  1010,  1014,  1010,
    1015,  1010,  1016,  1010,  1017,  1010,  1018,  1010,  1019,  1010,
    1020,  1010,  1021,  1010,  1022,  1010,  1023,  1010,  1024,  1010,
    1025,  1010,  1010,  1027,  1026,  1029,  1028,  1031,  1030,  1032,
    1030,  1033,  1030,  1034,  1030,  1030,  1036,  1035,  1038,  1037,
    1039,  1037,  1040,  1037,  1041,  1037,  1042,  1037,  1043,  1037,
    1044,  1037,  1045,  1037,  1046,  1037,  1047,  1037,  1048,  1037,
    1049,  1037,  1050,  1037,  1051,  1037,  1052,  1037,  1053,  1037,
    1054,  1037,  1055,  1037,  1056,  1037,  1057,  1037,  1058,  1037,
    1059,  1037,  1060,  1037,  1061,  1037,  1062,  1037,  1063,  1037,
    1064,  1037,  1065,  1037,  1066,  1037,  1067,  1037,  1037,  1069,
    1068,  1071,  1070,  1073,  1072,  1075,  1074,  1077,  1076,  1078,
    1076,  1079,  1076,  1080,  1076,  1081,  1076,  1082,  1076,  1083,
    1076,  1076,  1085,  1084,  1087,  1086,  1088,  1086,  1089,  1086,
    1090,  1086,  1086,  1092,  1091,  1094,  1093,  1093,  1096,  1095,
    1098,  1097,  1099,  1097,  1097,  1101,  1100,  1103,  1102,  1105,
    1104,  1104,  1107,  1106,  1109,  1108,  1108,  1111,  1110,  1113,
    1112,  1115,  1114,  1116,  1114,  1114,  1118,  1117,  1120,  1119,
    1121,  1119,  1122,  1119,  1123,  1119,  1124,  1119,  1125,  1119,
    1126,  1119,  1119,  1128,  1127,  1130,  1129,  1131,  1129,  1132,
    1129,  1133,  1129,  1134,  1129,  1135,  1129,  1136,  1129,  1137,
    1129,  1138,  1129,  1139,  1129,  1140,  1129,  1141,  1129,  1142,
    1129,  1129,  1144,  1143,  1146,  1145,  1147,  1145,  1148,  1145,
    1149,  1145,  1150,  1145,  1151,  1145,  1152,  1145,  1153,  1145,
    1154,  1145,  1155,  1145,  1156,  1145,  1157,  1145,  1158,  1145,
    1159,  1145,  1160,  1145,  1145,  1162,  1161,  1164,  1163,  1166,
    1165,  1167,  1165,  1168,  1165,  1169,  1165,  1165,  1171,  1170,
    1173,  1172,  1174,  1172,  1175,  1172,  1176,  1172,  1177,  1172,
    1178,  1172,  1179,  1172,  1180,  1172,  1181,  1172,  1172,  1183,
    1182,  1185,  1184,  1186,  1184,  1187,  1184,  1188,  1184,  1189,
    1184,  1190,  1184,  1191,  1184,  1192,  1184,  1193,  1184,  1184,
    1195,  1194,  1197,  1196,  1199,  1198,  1201,  1200,  1203,  1202,
    1202,  1205,  1204,  1207,  1208,  1206,  1206,  1210,  1209,  1211,
    1209,  1212,  1209,  1213,  1209,  1214,  1209,  1215,  1209,  1216,
    1209,  1217,  1209,  1218,  1209,  1219,  1209,  1220,  1209,  1221,
    1209,  1222,  1209,  1223,  1209,  1224,  1209,  1225,  1209,  1226,
    1209,  1227,  1209,  1228,  1229,  1209,  1209,  1231,  1230,  1232,
    1230,  1233,  1230,  1230,  1235,  1234,  1237,  1236,  1239,  1238,
    1241,  1240,  1242,  1240,  1243,  1240,  1244,  1240,  1245,  1240,
    1246,  1240,  1247,  1240,  1248,  1240,  1249,  1240,  1240
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     0,    10,     5,     0,     0,     7,     5,
       0,     0,     7,     0,     0,     7,     0,     0,     7,     0,
       0,     7,     5,     5,     5,     5,     0,     0,     7,     0,
       0,     7,     0,     0,     4,     0,     0,     6,     0,     0,
       6,     0,     6,     0,     6,     0,     6,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     5,     0,
       6,     0,     6,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     5,     0,     6,     0,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,    10,     0,     0,     6,
       0,     6,     0,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,     7,     0,     0,     7,
       0,     0,     7,     0,     0,     8,     0,     8,     0,     8,
       0,     8,     0,     8,     0,     8,     0,     8,     0,     8,
       0,     8,     0,     8,     0,     8,     0,     8,     0,     8,
       0,     8,     0,     6,     0,     8,     0,     8,     0,     8,
       0,     0,     7,     5,     0,     0,     0,     7,     0,     0,
       8,     0,     8,     0,     6,     0,     0,     0,     7,     0,
       0,     6,     0,     6,     0,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     0,     7,     0,     0,     6,
       0,     6,     0,     8,     0,     6,     0,     0,     7,     0,
       0,     0,     0,     0,    13,     0,     0,     0,     0,     0,
       0,    20,     0,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       0,     0,     7,     0,     0,     7,     0,     0,     7,     0,
       6,     0,     0,     4,     0,     0,     0,     7,     0,     0,
       6,     0,     0,     0,     7,     0,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     5,     0,     6,     0,     6,     5,
       0,     8,     0,     8,     0,     8,     0,     8,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     6,     0,     1,     1,     1,     1,
       1,     0,     0,     7,     0,     0,     7,     0,     0,     7,
       0,     0,     7,     0,     0,     8,     0,     0,    14,     0,
       2,     5,     0,     0,     0,     0,     7,     0,     0,     6,
       0,     0,     7,     0,     0,     7,     0,     0,     0,     7,
       0,     0,     6,     0,     6,     0,     8,     0,     0,     7,
       0,     0,     7,     0,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     5,     0,     6,     0,     0,     4,     0,     4,     0,
       5,     0,     6,     0,     6,     0,     6,     0,     0,     5,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       5,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     4,     0,     5,     0,     6,     0,
       6,     0,     6,     0,     0,     5,     0,     6,     0,     6,
       0,     6,     0,     0,     4,     0,     5,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     0,     5,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     5,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     5,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     5,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     0,     5,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     0,     5,     0,     6,     0,     6,     0,     6,     0,
       0,     5,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     0,     5,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     0,     5,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       0,     5,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     4,     0,     5,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     0,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     0,
       4,     0,     4,     0,     4,     0,     5,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     5,     0,     6,     0,     0,     5,
       0,     6,     0,     6,     0,     0,     4,     0,     5,     0,
       6,     0,     0,     5,     0,     6,     0,     0,     4,     0,
       5,     0,     6,     0,     6,     0,     0,     5,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     0,     5,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     4,     0,     4,     0,     4,     0,
       4,     0,     4,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     0,     5,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     4,     0,     4,     0,     4,
       0,     4,     0,     4,     0,     0,     4,     0,     5,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     0,     5,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     0,
       5,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       0,     4,     0,     4,     0,     4,     0,     5,     0,     6,
       0,     0,     4,     0,     0,     7,     0,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     6,     0,     6,     0,     6,     0,
       6,     0,     6,     0,     0,     7,     0,     0,     6,     0,
       6,     0,     6,     0,     0,     4,     0,     4,     0,     5,
       0,     6,     0,     6,     0,     6,     0,     6,     0,     6,
       0,     6,     0,     6,     0,     6,     0,     6,     0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* $@1: %empty  */
#line 329 "configparser.yy"
  { lconfig_parseinit();}
#line 5862 "configparser.cc"
    break;

  case 3: /* $@2: %empty  */
#line 329 "configparser.yy"
                                                                        { lconfig->setLoadedVersion( (yyvsp[-5].num), (yyvsp[-3].num), (yyvsp[-1].num) ); }
#line 5868 "configparser.cc"
    break;

  case 6: /* $@3: %empty  */
#line 333 "configparser.yy"
                               { lconfig_faces_list.clear(); }
#line 5874 "configparser.cc"
    break;

  case 7: /* $@4: %empty  */
#line 334 "configparser.yy"
      {
        lconfig->applyNewFacesFromList( lconfig_faces_list );
      }
#line 5882 "configparser.cc"
    break;

  case 10: /* $@5: %empty  */
#line 338 "configparser.yy"
                              { lconfig_listp = new List(); }
#line 5888 "configparser.cc"
    break;

  case 11: /* $@6: %empty  */
#line 339 "configparser.yy"
                           { lconfig->setPaths( lconfig_listp );
                             lconfig_id = lconfig_listp->initEnum();
                             lconfig_p1 = (WCPath*)lconfig_listp->getFirstElement( lconfig_id );
                             while ( lconfig_p1 != NULL ) {
                               delete lconfig_p1;
                               lconfig_p1 = (WCPath*)lconfig_listp->getNextElement( lconfig_id );
                             }
                             lconfig_listp->closeEnum( lconfig_id );
                             delete lconfig_listp;
                             lconfig_listp = NULL;
                           }
#line 5904 "configparser.cc"
    break;

  case 13: /* $@7: %empty  */
#line 350 "configparser.yy"
                                  { lconfig_listft = new List(); }
#line 5910 "configparser.cc"
    break;

  case 14: /* $@8: %empty  */
#line 351 "configparser.yy"
                               { lconfig->setFiletypes( lconfig_listft );
                                 lconfig_id = lconfig_listft->initEnum();
                                 lconfig_ft1 = (WCFiletype*)lconfig_listft->getFirstElement( lconfig_id );
                                 while ( lconfig_ft1 != NULL ) {
                                   delete lconfig_ft1;
                                   lconfig_ft1 = (WCFiletype*)lconfig_listft->getNextElement( lconfig_id );
                                 }
                                 lconfig_listft->closeEnum( lconfig_id );
                                 delete lconfig_listft;
                                 lconfig_listft = NULL;
                               }
#line 5926 "configparser.cc"
    break;

  case 16: /* $@9: %empty  */
#line 362 "configparser.yy"
                                { lconfig_listh = new List(); }
#line 5932 "configparser.cc"
    break;

  case 17: /* $@10: %empty  */
#line 363 "configparser.yy"
                             { lconfig->setHotkeys( lconfig_listh );
                               lconfig_id = lconfig_listh->initEnum();
                               lconfig_hk1 = (WCHotkey*)lconfig_listh->getFirstElement( lconfig_id );
                               while ( lconfig_hk1 != NULL ) {
                                 delete lconfig_hk1;
                                 lconfig_hk1 = (WCHotkey*)lconfig_listh->getNextElement( lconfig_id );
                               }
                               lconfig_listh->closeEnum( lconfig_id );
                               delete lconfig_listh;
                               lconfig_listh = NULL;
                             }
#line 5948 "configparser.cc"
    break;

  case 19: /* $@11: %empty  */
#line 374 "configparser.yy"
                                { lconfig_listb = new List(); }
#line 5954 "configparser.cc"
    break;

  case 20: /* $@12: %empty  */
#line 375 "configparser.yy"
                             { lconfig->setButtons( lconfig_listb );
                               lconfig_id = lconfig_listb->initEnum();
                               lconfig_bt1 = (WCButton*)lconfig_listb->getFirstElement( lconfig_id );
                               while ( lconfig_bt1 != NULL ) {
                                 delete lconfig_bt1;
                                 lconfig_bt1 = (WCButton*)lconfig_listb->getNextElement( lconfig_id );
                               }
                               lconfig_listb->closeEnum( lconfig_id );
                               delete lconfig_listb;
                               lconfig_listb = NULL;
                             }
#line 5970 "configparser.cc"
    break;

  case 26: /* $@13: %empty  */
#line 390 "configparser.yy"
                                          { lconfig_pathjump_allow.clear(); }
#line 5976 "configparser.cc"
    break;

  case 27: /* $@14: %empty  */
#line 391 "configparser.yy"
                                 { lconfig->setPathJumpAllowDirs( lconfig_pathjump_allow );
                                   lconfig_pathjump_allow.clear();
                                 }
#line 5984 "configparser.cc"
    break;

  case 29: /* $@15: %empty  */
#line 394 "configparser.yy"
                                         { dir_presets.clear(); }
#line 5990 "configparser.cc"
    break;

  case 30: /* $@16: %empty  */
#line 395 "configparser.yy"
                                      { lconfig->setDirectoryPresets( dir_presets );
                                 }
#line 5997 "configparser.cc"
    break;

  case 33: /* $@17: %empty  */
#line 399 "configparser.yy"
                          { lconfig_pathjump_allow.push_back( (yyvsp[-1].strptr) ); }
#line 6003 "configparser.cc"
    break;

  case 36: /* $@18: %empty  */
#line 402 "configparser.yy"
                                             { lconfig->setPathJumpStoreFilesAlways( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6009 "configparser.cc"
    break;

  case 39: /* $@19: %empty  */
#line 405 "configparser.yy"
                             { lconfig->setLang( (yyvsp[-1].strptr) ); }
#line 6015 "configparser.cc"
    break;

  case 41: /* $@20: %empty  */
#line 406 "configparser.yy"
                         { lconfig->setRows( (yyvsp[-1].num) ); }
#line 6021 "configparser.cc"
    break;

  case 43: /* $@21: %empty  */
#line 407 "configparser.yy"
                            { lconfig->setColumns( (yyvsp[-1].num) ); }
#line 6027 "configparser.cc"
    break;

  case 45: /* $@22: %empty  */
#line 408 "configparser.yy"
                              { lconfig->setCacheSize( (yyvsp[-1].num) ); }
#line 6033 "configparser.cc"
    break;

  case 48: /* $@23: %empty  */
#line 410 "configparser.yy"
                                   { lconfig->setOwnerstringtype( 0 ); }
#line 6039 "configparser.cc"
    break;

  case 50: /* $@24: %empty  */
#line 411 "configparser.yy"
                                  { lconfig->setOwnerstringtype( 1 ); }
#line 6045 "configparser.cc"
    break;

  case 52: /* $@25: %empty  */
#line 412 "configparser.yy"
                                { lconfig->setTerminalBin( (yyvsp[-1].strptr) ); }
#line 6051 "configparser.cc"
    break;

  case 54: /* $@26: %empty  */
#line 413 "configparser.yy"
                                         { lconfig->setTerminalReturnsEarly( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6057 "configparser.cc"
    break;

  case 56: /* $@27: %empty  */
#line 414 "configparser.yy"
                                     { lconfig->setShowStringForDirSize( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6063 "configparser.cc"
    break;

  case 59: /* $@28: %empty  */
#line 417 "configparser.yy"
                               { lconfig->resetColors(); }
#line 6069 "configparser.cc"
    break;

  case 61: /* $@29: %empty  */
#line 418 "configparser.yy"
                              { lconfig->clearLayoutOrders(); }
#line 6075 "configparser.cc"
    break;

  case 64: /* $@30: %empty  */
#line 420 "configparser.yy"
                                            { lconfig->setSaveWorkerStateOnExit( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6081 "configparser.cc"
    break;

  case 66: /* $@31: %empty  */
#line 421 "configparser.yy"
                                             { lconfig->setUseStringCompareMode( ( ( (yyvsp[-1].num) ) == 1 ) ? StringComparator::STRING_COMPARE_VERSION : StringComparator::STRING_COMPARE_REGULAR ); }
#line 6087 "configparser.cc"
    break;

  case 68: /* $@32: %empty  */
#line 422 "configparser.yy"
                                                    { lconfig->setUseStringCompareMode( StringComparator::STRING_COMPARE_NOCASE ); }
#line 6093 "configparser.cc"
    break;

  case 70: /* $@33: %empty  */
#line 423 "configparser.yy"
                                           { lconfig->setApplyWindowDialogType( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6099 "configparser.cc"
    break;

  case 72: /* $@34: %empty  */
#line 424 "configparser.yy"
                                     { lconfig->setUseExtendedRegEx( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6105 "configparser.cc"
    break;

  case 75: /* $@35: %empty  */
#line 426 "configparser.yy"
                                             { lconfig->setDisableBGCheckPrefix( (yyvsp[-1].strptr) ); }
#line 6111 "configparser.cc"
    break;

  case 78: /* $@36: %empty  */
#line 429 "configparser.yy"
                       { lconfig->layoutAddEntry( LayoutSettings::LO_STATEBAR ); }
#line 6117 "configparser.cc"
    break;

  case 80: /* $@37: %empty  */
#line 430 "configparser.yy"
                       { lconfig->layoutAddEntry( LayoutSettings::LO_CLOCKBAR ); }
#line 6123 "configparser.cc"
    break;

  case 82: /* $@38: %empty  */
#line 431 "configparser.yy"
                      { lconfig->layoutAddEntry( LayoutSettings::LO_BUTTONS ); }
#line 6129 "configparser.cc"
    break;

  case 84: /* $@39: %empty  */
#line 432 "configparser.yy"
                        { lconfig->layoutAddEntry( LayoutSettings::LO_LISTVIEWS ); }
#line 6135 "configparser.cc"
    break;

  case 86: /* $@40: %empty  */
#line 433 "configparser.yy"
                  { lconfig->layoutAddEntry( LayoutSettings::LO_BLL ); }
#line 6141 "configparser.cc"
    break;

  case 88: /* $@41: %empty  */
#line 434 "configparser.yy"
                  { lconfig->layoutAddEntry( LayoutSettings::LO_LBL ); }
#line 6147 "configparser.cc"
    break;

  case 90: /* $@42: %empty  */
#line 435 "configparser.yy"
                  { lconfig->layoutAddEntry( LayoutSettings::LO_LLB ); }
#line 6153 "configparser.cc"
    break;

  case 92: /* $@43: %empty  */
#line 436 "configparser.yy"
                 { lconfig->layoutAddEntry( LayoutSettings::LO_BL ); }
#line 6159 "configparser.cc"
    break;

  case 94: /* $@44: %empty  */
#line 437 "configparser.yy"
                 { lconfig->layoutAddEntry( LayoutSettings::LO_LB ); }
#line 6165 "configparser.cc"
    break;

  case 96: /* $@45: %empty  */
#line 438 "configparser.yy"
                                { lconfig->setLayoutButtonVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6171 "configparser.cc"
    break;

  case 98: /* $@46: %empty  */
#line 439 "configparser.yy"
                                  { lconfig->setLayoutListviewVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6177 "configparser.cc"
    break;

  case 100: /* $@47: %empty  */
#line 440 "configparser.yy"
                                      { lconfig->setLayoutListViewWeight( (yyvsp[-1].num) ); }
#line 6183 "configparser.cc"
    break;

  case 102: /* $@48: %empty  */
#line 441 "configparser.yy"
                                   { lconfig->setLayoutWeightRelToActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6189 "configparser.cc"
    break;

  case 105: /* $@49: %empty  */
#line 444 "configparser.yy"
                                            { lconfig->setColor( (yyvsp[-7].num), (yyvsp[-5].num), (yyvsp[-3].num), (yyvsp[-1].num) ); }
#line 6195 "configparser.cc"
    break;

  case 108: /* $@50: %empty  */
#line 447 "configparser.yy"
                   { lconfig_side = 0; }
#line 6201 "configparser.cc"
    break;

  case 110: /* $@51: %empty  */
#line 448 "configparser.yy"
                    { lconfig_side = 1; }
#line 6207 "configparser.cc"
    break;

  case 113: /* $@52: %empty  */
#line 451 "configparser.yy"
                                 { lconfig->setHBarTop( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6213 "configparser.cc"
    break;

  case 115: /* $@53: %empty  */
#line 452 "configparser.yy"
                                       { lconfig->setHBarHeight( lconfig_side, (yyvsp[-1].num) ); }
#line 6219 "configparser.cc"
    break;

  case 117: /* $@54: %empty  */
#line 453 "configparser.yy"
                                  { lconfig->setVBarLeft( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6225 "configparser.cc"
    break;

  case 119: /* $@55: %empty  */
#line 454 "configparser.yy"
                                      { lconfig->setVBarWidth( lconfig_side, (yyvsp[-1].num) ); }
#line 6231 "configparser.cc"
    break;

  case 121: /* $@56: %empty  */
#line 455 "configparser.yy"
                                          { lconfig->clearVisCols( lconfig_side ); }
#line 6237 "configparser.cc"
    break;

  case 123: /* $@57: %empty  */
#line 456 "configparser.yy"
                                    { lconfig->setShowHeader( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6243 "configparser.cc"
    break;

  case 125: /* $@58: %empty  */
#line 457 "configparser.yy"
                                        { lconfig->setPathEntryOnTop( lconfig_side, ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6249 "configparser.cc"
    break;

  case 128: /* $@59: %empty  */
#line 460 "configparser.yy"
                       { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_NAME ); }
#line 6255 "configparser.cc"
    break;

  case 130: /* $@60: %empty  */
#line 461 "configparser.yy"
                       { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_SIZE ); }
#line 6261 "configparser.cc"
    break;

  case 132: /* $@61: %empty  */
#line 462 "configparser.yy"
                       { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_TYPE ); }
#line 6267 "configparser.cc"
    break;

  case 134: /* $@62: %empty  */
#line 463 "configparser.yy"
                             { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_PERM ); }
#line 6273 "configparser.cc"
    break;

  case 136: /* $@63: %empty  */
#line 464 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_OWNER ); }
#line 6279 "configparser.cc"
    break;

  case 138: /* $@64: %empty  */
#line 465 "configparser.yy"
                              { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_DEST ); }
#line 6285 "configparser.cc"
    break;

  case 140: /* $@65: %empty  */
#line 466 "configparser.yy"
                          { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_MOD ); }
#line 6291 "configparser.cc"
    break;

  case 142: /* $@66: %empty  */
#line 467 "configparser.yy"
                          { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_ACC ); }
#line 6297 "configparser.cc"
    break;

  case 144: /* $@67: %empty  */
#line 468 "configparser.yy"
                          { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_CHANGE ); }
#line 6303 "configparser.cc"
    break;

  case 146: /* $@68: %empty  */
#line 469 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_INODE ); }
#line 6309 "configparser.cc"
    break;

  case 148: /* $@69: %empty  */
#line 470 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_NLINK ); }
#line 6315 "configparser.cc"
    break;

  case 150: /* $@70: %empty  */
#line 471 "configparser.yy"
                         { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_BLOCKS ); }
#line 6321 "configparser.cc"
    break;

  case 152: /* $@71: %empty  */
#line 472 "configparser.yy"
                        { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_SIZEH ); }
#line 6327 "configparser.cc"
    break;

  case 154: /* $@72: %empty  */
#line 473 "configparser.yy"
                            { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_EXTENSION ); }
#line 6333 "configparser.cc"
    break;

  case 156: /* $@73: %empty  */
#line 474 "configparser.yy"
                                  { lconfig->addListCol( lconfig_side, WorkerTypes::LISTCOL_CUSTOM_ATTR ); }
#line 6339 "configparser.cc"
    break;

  case 159: /* $@74: %empty  */
#line 477 "configparser.yy"
                                  { lconfig->setDateFormat( 0 ); }
#line 6345 "configparser.cc"
    break;

  case 161: /* $@75: %empty  */
#line 478 "configparser.yy"
                                  { lconfig->setDateFormat( 1 ); }
#line 6351 "configparser.cc"
    break;

  case 163: /* $@76: %empty  */
#line 479 "configparser.yy"
                                  { lconfig->setDateFormat( 2 ); }
#line 6357 "configparser.cc"
    break;

  case 165: /* $@77: %empty  */
#line 480 "configparser.yy"
                                     { lconfig->setDateFormat( -1 ); }
#line 6363 "configparser.cc"
    break;

  case 167: /* $@78: %empty  */
#line 481 "configparser.yy"
                                        { lconfig->setDateFormatString( (yyvsp[-1].strptr) ); }
#line 6369 "configparser.cc"
    break;

  case 169: /* $@79: %empty  */
#line 482 "configparser.yy"
                                        { lconfig->setDateSubst( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6375 "configparser.cc"
    break;

  case 171: /* $@80: %empty  */
#line 483 "configparser.yy"
                                  { lconfig->setTimeFormat( 0 ); }
#line 6381 "configparser.cc"
    break;

  case 173: /* $@81: %empty  */
#line 484 "configparser.yy"
                                  { lconfig->setTimeFormat( 1 ); }
#line 6387 "configparser.cc"
    break;

  case 175: /* $@82: %empty  */
#line 485 "configparser.yy"
                                     { lconfig->setTimeFormat( -1 ); }
#line 6393 "configparser.cc"
    break;

  case 177: /* $@83: %empty  */
#line 486 "configparser.yy"
                                        { lconfig->setTimeFormatString( (yyvsp[-1].strptr) ); }
#line 6399 "configparser.cc"
    break;

  case 179: /* $@84: %empty  */
#line 487 "configparser.yy"
                                      { lconfig->setDateBeforeTime( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 6405 "configparser.cc"
    break;

  case 182: /* $@85: %empty  */
#line 490 "configparser.yy"
                                        { lconfig->setMouseSelectButton( (yyvsp[-1].num) ); }
#line 6411 "configparser.cc"
    break;

  case 184: /* $@86: %empty  */
#line 491 "configparser.yy"
                                          { lconfig->setMouseActivateButton( (yyvsp[-1].num) ); }
#line 6417 "configparser.cc"
    break;

  case 186: /* $@87: %empty  */
#line 492 "configparser.yy"
                                        { lconfig->setMouseScrollButton( (yyvsp[-1].num) ); }
#line 6423 "configparser.cc"
    break;

  case 188: /* $@88: %empty  */
#line 493 "configparser.yy"
                                           { lconfig->setMouseSelectMethod( WConfig::MOUSECONF_NORMAL_MODE ); }
#line 6429 "configparser.cc"
    break;

  case 190: /* $@89: %empty  */
#line 494 "configparser.yy"
                                                { lconfig->setMouseSelectMethod( WConfig::MOUSECONF_ALT_MODE ); }
#line 6435 "configparser.cc"
    break;

  case 192: /* $@90: %empty  */
#line 495 "configparser.yy"
                                         { lconfig->setMouseContextButton( (yyvsp[-1].num) ); }
#line 6441 "configparser.cc"
    break;

  case 194: /* $@91: %empty  */
#line 496 "configparser.yy"
                                        { lconfig_keymod = 0; }
#line 6447 "configparser.cc"
    break;

  case 195: /* $@92: %empty  */
#line 497 "configparser.yy"
                              { lconfig->setMouseActivateMod( lconfig_keymod ); }
#line 6453 "configparser.cc"
    break;

  case 197: /* $@93: %empty  */
#line 498 "configparser.yy"
                                      { lconfig_keymod = 0; }
#line 6459 "configparser.cc"
    break;

  case 198: /* $@94: %empty  */
#line 499 "configparser.yy"
                              { lconfig->setMouseScrollMod( lconfig_keymod ); }
#line 6465 "configparser.cc"
    break;

  case 200: /* $@95: %empty  */
#line 500 "configparser.yy"
                                       { lconfig_keymod = 0; }
#line 6471 "configparser.cc"
    break;

  case 201: /* $@96: %empty  */
#line 501 "configparser.yy"
                              { lconfig->setMouseContextMod( lconfig_keymod ); }
#line 6477 "configparser.cc"
    break;

  case 204: /* $@97: %empty  */
#line 504 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "statusbar-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "statusbar-bg", (yyvsp[-1].num) ) );
        }
#line 6486 "configparser.cc"
    break;

  case 206: /* $@98: %empty  */
#line 508 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "lvb-active-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "lvb-active-bg", (yyvsp[-1].num) ) );
        }
#line 6495 "configparser.cc"
    break;

  case 208: /* $@99: %empty  */
#line 512 "configparser.yy"
                                           {
        lconfig_faces_list.push_back( std::make_pair( "lvb-inactive-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "lvb-inactive-bg", (yyvsp[-1].num) ) );
        }
#line 6504 "configparser.cc"
    break;

  case 210: /* $@100: %empty  */
#line 516 "configparser.yy"
                                       {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-select-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-select-bg", (yyvsp[-1].num) ) );
        }
#line 6513 "configparser.cc"
    break;

  case 212: /* $@101: %empty  */
#line 520 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-normal-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-normal-bg", (yyvsp[-1].num) ) );
        }
#line 6522 "configparser.cc"
    break;

  case 214: /* $@102: %empty  */
#line 524 "configparser.yy"
                                        {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-select-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-select-bg", (yyvsp[-1].num) ) );
        }
#line 6531 "configparser.cc"
    break;

  case 216: /* $@103: %empty  */
#line 528 "configparser.yy"
                                          {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-normal-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-normal-bg", (yyvsp[-1].num) ) );
        }
#line 6540 "configparser.cc"
    break;

  case 218: /* $@104: %empty  */
#line 532 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "clockbar-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "clockbar-bg", (yyvsp[-1].num) ) );
        }
#line 6549 "configparser.cc"
    break;

  case 220: /* $@105: %empty  */
#line 536 "configparser.yy"
                                          {
        lconfig_faces_list.push_back( std::make_pair( "request-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "request-bg", (yyvsp[-1].num) ) );
        }
#line 6558 "configparser.cc"
    break;

  case 222: /* $@106: %empty  */
#line 540 "configparser.yy"
                                          {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-selact-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-selact-bg", (yyvsp[-1].num) ) );
        }
#line 6567 "configparser.cc"
    break;

  case 224: /* $@107: %empty  */
#line 544 "configparser.yy"
                                            {
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-active-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-dir-active-bg", (yyvsp[-1].num) ) );
        }
#line 6576 "configparser.cc"
    break;

  case 226: /* $@108: %empty  */
#line 548 "configparser.yy"
                                           {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-selact-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-selact-bg", (yyvsp[-1].num) ) );
        }
#line 6585 "configparser.cc"
    break;

  case 228: /* $@109: %empty  */
#line 552 "configparser.yy"
                                             {
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-active-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-file-active-bg", (yyvsp[-1].num) ) );
        }
#line 6594 "configparser.cc"
    break;

  case 230: /* $@110: %empty  */
#line 556 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "dirview-header-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "dirview-header-bg", (yyvsp[-1].num) ) );
        }
#line 6603 "configparser.cc"
    break;

  case 232: /* $@111: %empty  */
#line 560 "configparser.yy"
                           {
        lconfig_faces_list.push_back( std::make_pair( "dirview-bg", (yyvsp[-1].num) ) );
        }
#line 6611 "configparser.cc"
    break;

  case 234: /* $@112: %empty  */
#line 563 "configparser.yy"
                                         {
        lconfig_faces_list.push_back( std::make_pair( "textview-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-bg", (yyvsp[-1].num) ) );
        }
#line 6620 "configparser.cc"
    break;

  case 236: /* $@113: %empty  */
#line 567 "configparser.yy"
                                                    {
        lconfig_faces_list.push_back( std::make_pair( "textview-highlight-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-highlight-bg", (yyvsp[-1].num) ) );
        }
#line 6629 "configparser.cc"
    break;

  case 238: /* $@114: %empty  */
#line 571 "configparser.yy"
                                                  {
        lconfig_faces_list.push_back( std::make_pair( "textview-selection-fg", (yyvsp[-3].num) ) );
        lconfig_faces_list.push_back( std::make_pair( "textview-selection-bg", (yyvsp[-1].num) ) );
        }
#line 6638 "configparser.cc"
    break;

  case 240: /* $@115: %empty  */
#line 575 "configparser.yy"
                                   { lconfig_labelcolors.clear(); }
#line 6644 "configparser.cc"
    break;

  case 241: /* $@116: %empty  */
#line 576 "configparser.yy"
                                   {
          WConfig::ColorDef c = lconfig->getColorDefs();
          c.setLabelColors( lconfig_labelcolors );
          lconfig->setColorDefs( c );
        }
#line 6654 "configparser.cc"
    break;

  case 245: /* $@117: %empty  */
#line 585 "configparser.yy"
                                    {
              lconfig_labelcolor.normal_fg = -1;
              lconfig_labelcolor.normal_bg = -1;
              lconfig_labelcolor.active_fg = -1;
              lconfig_labelcolor.active_bg = -1;
              lconfig_labelcolor_name = "";
            }
#line 6666 "configparser.cc"
    break;

  case 246: /* $@118: %empty  */
#line 592 "configparser.yy"
                                      {
              lconfig_labelcolors[lconfig_labelcolor_name] = lconfig_labelcolor;
            }
#line 6674 "configparser.cc"
    break;

  case 249: /* $@119: %empty  */
#line 597 "configparser.yy"
                                             {
             lconfig_labelcolor.normal_fg = (yyvsp[-3].num);
             lconfig_labelcolor.normal_bg = (yyvsp[-1].num);
            }
#line 6683 "configparser.cc"
    break;

  case 251: /* $@120: %empty  */
#line 601 "configparser.yy"
                                             {
             lconfig_labelcolor.active_fg = (yyvsp[-3].num);
             lconfig_labelcolor.active_bg = (yyvsp[-1].num);
            }
#line 6692 "configparser.cc"
    break;

  case 253: /* $@121: %empty  */
#line 605 "configparser.yy"
                                      { lconfig_labelcolor_name = (yyvsp[-1].strptr); }
#line 6698 "configparser.cc"
    break;

  case 256: /* $@122: %empty  */
#line 608 "configparser.yy"
                             {
              lconfig_face_name.clear();
              lconfig_face_color = -1;
            }
#line 6707 "configparser.cc"
    break;

  case 257: /* $@123: %empty  */
#line 612 "configparser.yy"
                                {
              lconfig_faces_list.push_back( std::make_pair( lconfig_face_name, lconfig_face_color ) );
            }
#line 6715 "configparser.cc"
    break;

  case 260: /* $@124: %empty  */
#line 617 "configparser.yy"
                            {
             lconfig_face_color = (yyvsp[-1].num);
            }
#line 6723 "configparser.cc"
    break;

  case 262: /* $@125: %empty  */
#line 620 "configparser.yy"
                                      { lconfig_face_name = (yyvsp[-1].strptr); }
#line 6729 "configparser.cc"
    break;

  case 265: /* $@126: %empty  */
#line 623 "configparser.yy"
                                 { lconfig->setStartDir( 0, (yyvsp[-1].strptr) ); }
#line 6735 "configparser.cc"
    break;

  case 267: /* $@127: %empty  */
#line 624 "configparser.yy"
                                  { lconfig->setStartDir( 1, (yyvsp[-1].strptr) ); }
#line 6741 "configparser.cc"
    break;

  case 269: /* $@128: %empty  */
#line 625 "configparser.yy"
                                             { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_NEVER ); }
#line 6747 "configparser.cc"
    break;

  case 271: /* $@129: %empty  */
#line 626 "configparser.yy"
                                              { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_ALWAYS ); }
#line 6753 "configparser.cc"
    break;

  case 273: /* $@130: %empty  */
#line 627 "configparser.yy"
                                           { lconfig->setRestoreTabsMode( WConfig::RESTORE_TABS_ASK ); }
#line 6759 "configparser.cc"
    break;

  case 275: /* $@131: %empty  */
#line 628 "configparser.yy"
                                           { lconfig->setStoreTabsMode( WConfig::STORE_TABS_NEVER ); }
#line 6765 "configparser.cc"
    break;

  case 277: /* $@132: %empty  */
#line 629 "configparser.yy"
                                            { lconfig->setStoreTabsMode( WConfig::STORE_TABS_ALWAYS ); }
#line 6771 "configparser.cc"
    break;

  case 279: /* $@133: %empty  */
#line 630 "configparser.yy"
                                                   { lconfig->setStoreTabsMode( WConfig::STORE_TABS_AS_EXIT_STATE ); }
#line 6777 "configparser.cc"
    break;

  case 281: /* $@134: %empty  */
#line 631 "configparser.yy"
                                         { lconfig->setStoreTabsMode( WConfig::STORE_TABS_ASK ); }
#line 6783 "configparser.cc"
    break;

  case 284: /* $@135: %empty  */
#line 634 "configparser.yy"
                             { lconfig_p1 = new WCPath(); }
#line 6789 "configparser.cc"
    break;

  case 285: /* $@136: %empty  */
#line 635 "configparser.yy"
                          { lconfig_p1->setCheck( true );
                        while ( lconfig_listp->size() < ( lconfig_pos + 1 ) ) lconfig_listp->addElement( new WCPath() );
                        lconfig_p1 = (WCPath*)lconfig_listp->exchangeElement( lconfig_pos, lconfig_p1 );
                        delete lconfig_p1;
                        lconfig_p1 = NULL;
                      }
#line 6800 "configparser.cc"
    break;

  case 288: /* $@137: %empty  */
#line 643 "configparser.yy"
                               { lconfig_pos = (yyvsp[-1].num);
                         if ( lconfig_pos < 0 ) lconfig_pos = 0;
                         if ( lconfig_pos > 10240 ) lconfig_pos = 10240;
                       }
#line 6809 "configparser.cc"
    break;

  case 290: /* $@138: %empty  */
#line 647 "configparser.yy"
                               { lconfig_p1->setName( (yyvsp[-1].strptr) ); }
#line 6815 "configparser.cc"
    break;

  case 292: /* $@139: %empty  */
#line 648 "configparser.yy"
                                      { lconfig_p1->setFG( (yyvsp[-3].num) );
                            lconfig_p1->setBG( (yyvsp[-1].num) );
                          }
#line 6823 "configparser.cc"
    break;

  case 294: /* $@140: %empty  */
#line 651 "configparser.yy"
                              { lconfig_p1->setPath( (yyvsp[-1].strptr) ); }
#line 6829 "configparser.cc"
    break;

  case 296: /* $@141: %empty  */
#line 652 "configparser.yy"
                                 { lconfig_listsk = new List(); }
#line 6835 "configparser.cc"
    break;

  case 297: /* $@142: %empty  */
#line 653 "configparser.yy"
                            { lconfig_p1->setDoubleKeys( lconfig_listsk );
                              lconfig_id = lconfig_listsk->initEnum();
                              lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                              while ( lconfig_dk != NULL ) {
                                delete lconfig_dk;
                                lconfig_listsk->removeFirstElement();
                                lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                              }
                              lconfig_listsk->closeEnum( lconfig_id );
                              delete lconfig_listsk;
                              lconfig_listsk = NULL;
                            }
#line 6852 "configparser.cc"
    break;

  case 300: /* $@143: %empty  */
#line 667 "configparser.yy"
                                 { lconfig_dk = new WCDoubleShortkey();
                                   lconfig_dk->setType( WCDoubleShortkey::WCDS_NORMAL );
                                 }
#line 6860 "configparser.cc"
    break;

  case 301: /* $@144: %empty  */
#line 670 "configparser.yy"
                                { lconfig_nkeysym = AGUIX::getKeySymForString( (yyvsp[-1].strptr) );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 0 );
                          else lconfig_dk->setKeySym( 0, 0 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
#line 6870 "configparser.cc"
    break;

  case 302: /* $@145: %empty  */
#line 675 "configparser.yy"
             { lconfig_dk->setMod( lconfig_keymod, 0 ); }
#line 6876 "configparser.cc"
    break;

  case 303: /* $@146: %empty  */
#line 676 "configparser.yy"
                       { lconfig_listsk->addElement( lconfig_dk ); }
#line 6882 "configparser.cc"
    break;

  case 305: /* $@147: %empty  */
#line 678 "configparser.yy"
                                 { lconfig_dk = new WCDoubleShortkey();
                                   lconfig_dk->setType( WCDoubleShortkey::WCDS_DOUBLE );
                                  }
#line 6890 "configparser.cc"
    break;

  case 306: /* $@148: %empty  */
#line 681 "configparser.yy"
                                { lconfig_nkeysym = AGUIX::getKeySymForString( (yyvsp[-1].strptr) );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 0 );
                          else lconfig_dk->setKeySym( 0, 0 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
#line 6900 "configparser.cc"
    break;

  case 307: /* $@149: %empty  */
#line 686 "configparser.yy"
             { lconfig_dk->setMod( lconfig_keymod, 0 ); /*TODO:finzt das so mit dem lconfig_keymod?*/}
#line 6906 "configparser.cc"
    break;

  case 308: /* $@150: %empty  */
#line 687 "configparser.yy"
                                { lconfig_nkeysym = AGUIX::getKeySymForString( (yyvsp[-1].strptr) );
                          if ( lconfig_nkeysym != NoSymbol ) lconfig_dk->setKeySym( lconfig_nkeysym, 1 );
                          else lconfig_dk->setKeySym( 0, 1 );  /*TODO:Fehler printen?*/
                          lconfig_keymod = 0;
                        }
#line 6916 "configparser.cc"
    break;

  case 309: /* $@151: %empty  */
#line 692 "configparser.yy"
             { lconfig_dk->setMod( lconfig_keymod, 1 ); }
#line 6922 "configparser.cc"
    break;

  case 310: /* $@152: %empty  */
#line 693 "configparser.yy"
                       { lconfig_listsk->addElement( lconfig_dk ); }
#line 6928 "configparser.cc"
    break;

  case 313: /* $@153: %empty  */
#line 697 "configparser.yy"
                              { lconfig_keymod |= ControlMask; }
#line 6934 "configparser.cc"
    break;

  case 315: /* $@154: %empty  */
#line 698 "configparser.yy"
                            { lconfig_keymod |= ShiftMask; }
#line 6940 "configparser.cc"
    break;

  case 317: /* $@155: %empty  */
#line 699 "configparser.yy"
                           { lconfig_keymod |= LockMask; }
#line 6946 "configparser.cc"
    break;

  case 319: /* $@156: %empty  */
#line 700 "configparser.yy"
                           { lconfig_keymod |= Mod1Mask; }
#line 6952 "configparser.cc"
    break;

  case 321: /* $@157: %empty  */
#line 701 "configparser.yy"
                           { lconfig_keymod |= Mod2Mask; }
#line 6958 "configparser.cc"
    break;

  case 323: /* $@158: %empty  */
#line 702 "configparser.yy"
                           { lconfig_keymod |= Mod3Mask; }
#line 6964 "configparser.cc"
    break;

  case 325: /* $@159: %empty  */
#line 703 "configparser.yy"
                           { lconfig_keymod |= Mod4Mask; }
#line 6970 "configparser.cc"
    break;

  case 327: /* $@160: %empty  */
#line 704 "configparser.yy"
                           { lconfig_keymod |= Mod5Mask; }
#line 6976 "configparser.cc"
    break;

  case 330: /* $@161: %empty  */
#line 707 "configparser.yy"
                                     { lconfig_ft1 = new WCFiletype();
                                       lconfig_ignorelist.clear();
                                     }
#line 6984 "configparser.cc"
    break;

  case 331: /* $@162: %empty  */
#line 710 "configparser.yy"
                                  { lconfig_listft->addElement( lconfig_ft1 );
                                lconfig_ft1 = NULL;
                              }
#line 6992 "configparser.cc"
    break;

  case 333: /* $@163: %empty  */
#line 713 "configparser.yy"
                                       { lconfig_ignorelist.remove_if( []( const auto &d ){ return ! d.is_pattern; } ); }
#line 6998 "configparser.cc"
    break;

  case 334: /* $@164: %empty  */
#line 714 "configparser.yy"
                                    { lconfig->setDontCheckDirs( lconfig_ignorelist );
                                    }
#line 7005 "configparser.cc"
    break;

  case 336: /* $@165: %empty  */
#line 716 "configparser.yy"
                                        { lconfig_ignorelist.remove_if( []( const auto &d ){ return d.is_pattern; } ); }
#line 7011 "configparser.cc"
    break;

  case 337: /* $@166: %empty  */
#line 717 "configparser.yy"
                                     { lconfig->setDontCheckDirs( lconfig_ignorelist );
                                     }
#line 7018 "configparser.cc"
    break;

  case 339: /* $@167: %empty  */
#line 719 "configparser.yy"
                                             { lconfig->setDontCheckVirtual( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7024 "configparser.cc"
    break;

  case 342: /* $@168: %empty  */
#line 722 "configparser.yy"
                         { lconfig_ignorelist.push_back( { .is_pattern = false, .dir = (yyvsp[-1].strptr) } ); }
#line 7030 "configparser.cc"
    break;

  case 345: /* $@169: %empty  */
#line 725 "configparser.yy"
                                        { lconfig_ignore_dir = {}; }
#line 7036 "configparser.cc"
    break;

  case 346: /* $@170: %empty  */
#line 726 "configparser.yy"
                                     { lconfig_ignorelist.push_back( lconfig_ignore_dir ); }
#line 7042 "configparser.cc"
    break;

  case 349: /* $@171: %empty  */
#line 729 "configparser.yy"
                                      { lconfig_ignore_dir = { .is_pattern = true, .dir = (yyvsp[-1].strptr) }; }
#line 7048 "configparser.cc"
    break;

  case 352: /* $@172: %empty  */
#line 732 "configparser.yy"
                                        { filetypehier.push_back( lconfig_ft1 );
                                          lconfig_ft1 = new WCFiletype();
                                        }
#line 7056 "configparser.cc"
    break;

  case 353: /* $@173: %empty  */
#line 735 "configparser.yy"
                                     { old_lconfig_ft1 = filetypehier.back();
	                               filetypehier.pop_back();
				       if ( old_lconfig_ft1 != NULL ) {
					 old_lconfig_ft1->addSubType( lconfig_ft1 );
				       } else {
					 delete lconfig_ft1;
				       }
				       lconfig_ft1 = old_lconfig_ft1;
	                             }
#line 7070 "configparser.cc"
    break;

  case 356: /* $@174: %empty  */
#line 746 "configparser.yy"
                               { lconfig_ft1->setinternID( (yyvsp[-1].num) ); }
#line 7076 "configparser.cc"
    break;

  case 358: /* $@175: %empty  */
#line 747 "configparser.yy"
                                   { lconfig_ft1->setName( (yyvsp[-1].strptr) ); }
#line 7082 "configparser.cc"
    break;

  case 360: /* $@176: %empty  */
#line 748 "configparser.yy"
                                     { lconfig_ft1->setPattern( (yyvsp[-1].strptr) ); }
#line 7088 "configparser.cc"
    break;

  case 362: /* $@177: %empty  */
#line 749 "configparser.yy"
                                  { lconfig_ft1->setUsePattern( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7094 "configparser.cc"
    break;

  case 364: /* $@178: %empty  */
#line 750 "configparser.yy"
                                   { lconfig_ft1->clearFiledesc(); }
#line 7100 "configparser.cc"
    break;

  case 366: /* $@179: %empty  */
#line 752 "configparser.yy"
                                  { lconfig_ft1->setUseFiledesc( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7106 "configparser.cc"
    break;

  case 368: /* $@180: %empty  */
#line 753 "configparser.yy"
                                         { lconfig_ft1->setPatternIgnoreCase( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7112 "configparser.cc"
    break;

  case 370: /* $@181: %empty  */
#line 754 "configparser.yy"
                                        { lconfig_ft1->setPatternUseRegExp( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7118 "configparser.cc"
    break;

  case 372: /* $@182: %empty  */
#line 755 "configparser.yy"
                                          { lconfig_ft1->setPatternUseFullname( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7124 "configparser.cc"
    break;

  case 375: /* $@183: %empty  */
#line 757 "configparser.yy"
                                     { lconfig_ft1->setExtCond( (yyvsp[-1].strptr) ); }
#line 7130 "configparser.cc"
    break;

  case 377: /* $@184: %empty  */
#line 758 "configparser.yy"
                                  { lconfig_ft1->setUseExtCond( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7136 "configparser.cc"
    break;

  case 380: /* $@185: %empty  */
#line 760 "configparser.yy"
                                                  { lconfig_ft1->setCustomColor( 0, 0, (yyvsp[-3].num) );
	                                            lconfig_ft1->setCustomColor( 1, 0, (yyvsp[-1].num) );
                                                  }
#line 7144 "configparser.cc"
    break;

  case 382: /* $@186: %empty  */
#line 763 "configparser.yy"
                                                { lconfig_ft1->setCustomColor( 0, 1, (yyvsp[-3].num) );
	                                          lconfig_ft1->setCustomColor( 1, 1, (yyvsp[-1].num) );
                                                }
#line 7152 "configparser.cc"
    break;

  case 384: /* $@187: %empty  */
#line 766 "configparser.yy"
                                                        { lconfig_ft1->setCustomColor( 0, 2, (yyvsp[-3].num) );
	                                                  lconfig_ft1->setCustomColor( 1, 2, (yyvsp[-1].num) );
                                                        }
#line 7160 "configparser.cc"
    break;

  case 386: /* $@188: %empty  */
#line 769 "configparser.yy"
                                                      { lconfig_ft1->setCustomColor( 0, 3, (yyvsp[-3].num) );
	                                                lconfig_ft1->setCustomColor( 1, 3, (yyvsp[-1].num) );
                                                      }
#line 7168 "configparser.cc"
    break;

  case 388: /* $@189: %empty  */
#line 772 "configparser.yy"
                                             { lconfig_ft1->setColorExt( (yyvsp[-1].strptr) ); }
#line 7174 "configparser.cc"
    break;

  case 390: /* $@190: %empty  */
#line 773 "configparser.yy"
                                        { lconfig_ft1->setColorMode( WCFiletype::DEFAULTCOL ); }
#line 7180 "configparser.cc"
    break;

  case 392: /* $@191: %empty  */
#line 774 "configparser.yy"
                                       { lconfig_ft1->setColorMode( WCFiletype::CUSTOMCOL ); }
#line 7186 "configparser.cc"
    break;

  case 394: /* $@192: %empty  */
#line 775 "configparser.yy"
                                       { lconfig_ft1->setColorMode( WCFiletype::EXTERNALCOL ); }
#line 7192 "configparser.cc"
    break;

  case 396: /* $@193: %empty  */
#line 776 "configparser.yy"
                                       { lconfig_ft1->setColorMode( WCFiletype::PARENTCOL ); }
#line 7198 "configparser.cc"
    break;

  case 398: /* $@194: %empty  */
#line 777 "configparser.yy"
                                          { lconfig_ft1->setMagicPattern( (yyvsp[-1].strptr) ); }
#line 7204 "configparser.cc"
    break;

  case 400: /* $@195: %empty  */
#line 778 "configparser.yy"
                                { lconfig_ft1->setUseMagic( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7210 "configparser.cc"
    break;

  case 402: /* $@196: %empty  */
#line 779 "configparser.yy"
                                       { lconfig_ft1->setMagicCompressed( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7216 "configparser.cc"
    break;

  case 404: /* $@197: %empty  */
#line 780 "configparser.yy"
                                 { lconfig_ft1->setMagicMime( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7222 "configparser.cc"
    break;

  case 406: /* $@198: %empty  */
#line 781 "configparser.yy"
                                       { lconfig_ft1->setMagicIgnoreCase( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7228 "configparser.cc"
    break;

  case 408: /* $@199: %empty  */
#line 782 "configparser.yy"
                                               { lconfig_ft1->setPatternIsCommaSeparated( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7234 "configparser.cc"
    break;

  case 410: /* $@200: %empty  */
#line 783 "configparser.yy"
                                   { lconfig_ft1->setPriority( (yyvsp[-1].num) ); }
#line 7240 "configparser.cc"
    break;

  case 413: /* $@201: %empty  */
#line 786 "configparser.yy"
                                 { lconfig_ft1->setFiledesc( (yyvsp[-3].num), (yyvsp[-1].num) ); }
#line 7246 "configparser.cc"
    break;

  case 416: /* ft_type: NORMAL_WCP  */
#line 789 "configparser.yy"
                   { (yyval.num) = NORMALTYPE; }
#line 7252 "configparser.cc"
    break;

  case 417: /* ft_type: NOTYETCHECKED_WCP  */
#line 790 "configparser.yy"
                          { (yyval.num) = NOTYETTYPE; }
#line 7258 "configparser.cc"
    break;

  case 418: /* ft_type: UNKNOWN_WCP  */
#line 791 "configparser.yy"
                    { (yyval.num) = UNKNOWNTYPE; }
#line 7264 "configparser.cc"
    break;

  case 419: /* ft_type: NOSELECT_WCP  */
#line 792 "configparser.yy"
                     { (yyval.num) = VOIDTYPE; }
#line 7270 "configparser.cc"
    break;

  case 420: /* ft_type: DIR_WCP  */
#line 793 "configparser.yy"
                { (yyval.num) = DIRTYPE; }
#line 7276 "configparser.cc"
    break;

  case 421: /* $@202: %empty  */
#line 796 "configparser.yy"
                                 { lconfig_listcom.clear(); }
#line 7282 "configparser.cc"
    break;

  case 422: /* $@203: %empty  */
#line 797 "configparser.yy"
                                   { lconfig_ft1->setDNDActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7290 "configparser.cc"
    break;

  case 424: /* $@204: %empty  */
#line 800 "configparser.yy"
                                { lconfig_listcom.clear(); }
#line 7296 "configparser.cc"
    break;

  case 425: /* $@205: %empty  */
#line 801 "configparser.yy"
                                   { lconfig_ft1->setDoubleClickActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7304 "configparser.cc"
    break;

  case 427: /* $@206: %empty  */
#line 804 "configparser.yy"
                                  { lconfig_listcom.clear(); }
#line 7310 "configparser.cc"
    break;

  case 428: /* $@207: %empty  */
#line 805 "configparser.yy"
                                   { lconfig_ft1->setShowActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7318 "configparser.cc"
    break;

  case 430: /* $@208: %empty  */
#line 808 "configparser.yy"
                                     { lconfig_listcom.clear(); }
#line 7324 "configparser.cc"
    break;

  case 431: /* $@209: %empty  */
#line 809 "configparser.yy"
                                   { lconfig_ft1->setRawShowActions( lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7332 "configparser.cc"
    break;

  case 433: /* $@210: %empty  */
#line 812 "configparser.yy"
                                          { lconfig_listcom.clear(); }
#line 7338 "configparser.cc"
    break;

  case 434: /* $@211: %empty  */
#line 813 "configparser.yy"
                                   { lconfig_ft1->setUserActions( (yyvsp[-4].num), lconfig_listcom );
                                     lconfig_listcom.clear();
                               }
#line 7346 "configparser.cc"
    break;

  case 436: /* $@212: %empty  */
#line 816 "configparser.yy"
                                          { lconfig_listcom.clear(); }
#line 7352 "configparser.cc"
    break;

  case 437: /* $@213: %empty  */
#line 819 "configparser.yy"
                                                  { lconfig_ft1->setCustomActions( (yyvsp[-6].strptr), lconfig_listcom );
                                                    lconfig_listcom.clear();
                               }
#line 7360 "configparser.cc"
    break;

  case 444: /* $@214: %empty  */
#line 830 "configparser.yy"
                                 { lconfig_hk1 = new WCHotkey(); }
#line 7366 "configparser.cc"
    break;

  case 445: /* $@215: %empty  */
#line 831 "configparser.yy"
                              { lconfig_listh->addElement( lconfig_hk1 );
                            lconfig_hk1 = NULL;
                          }
#line 7374 "configparser.cc"
    break;

  case 448: /* $@216: %empty  */
#line 836 "configparser.yy"
                                 { lconfig_hk1->setName( (yyvsp[-1].strptr) ); }
#line 7380 "configparser.cc"
    break;

  case 450: /* $@217: %empty  */
#line 837 "configparser.yy"
                                  { lconfig_listcom.clear(); }
#line 7386 "configparser.cc"
    break;

  case 451: /* $@218: %empty  */
#line 838 "configparser.yy"
                               { lconfig_hk1->setComs( lconfig_listcom );
                                 lconfig_listcom.clear();
                           }
#line 7394 "configparser.cc"
    break;

  case 453: /* $@219: %empty  */
#line 841 "configparser.yy"
                                   { lconfig_listsk = new List(); }
#line 7400 "configparser.cc"
    break;

  case 454: /* $@220: %empty  */
#line 842 "configparser.yy"
                              { lconfig_hk1->setDoubleKeys( lconfig_listsk );
                            lconfig_id = lconfig_listsk->initEnum();
                            lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            while ( lconfig_dk != NULL ) {
                              delete lconfig_dk;
                              lconfig_listsk->removeFirstElement();
                              lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            }
                            lconfig_listsk->closeEnum( lconfig_id );
                            delete lconfig_listsk;
                            lconfig_listsk = NULL;
                          }
#line 7417 "configparser.cc"
    break;

  case 457: /* $@221: %empty  */
#line 856 "configparser.yy"
                                 { lconfig_bt1 = new WCButton(); }
#line 7423 "configparser.cc"
    break;

  case 458: /* $@222: %empty  */
#line 857 "configparser.yy"
                              { lconfig_bt1->setCheck( true );
                            while ( lconfig_listb->size() < ( lconfig_pos + 1 ) ) lconfig_listb->addElement( new WCButton() );
                            lconfig_bt1 = (WCButton*)lconfig_listb->exchangeElement( lconfig_pos, lconfig_bt1 );
                            delete lconfig_bt1;
                            lconfig_bt1 = NULL;
                          }
#line 7434 "configparser.cc"
    break;

  case 461: /* $@223: %empty  */
#line 865 "configparser.yy"
                                 { lconfig_pos = (yyvsp[-1].num);
                           if ( lconfig_pos < 0 ) lconfig_pos = 0;
                           if ( lconfig_pos > 100000 ) lconfig_pos = 100000;
                         }
#line 7443 "configparser.cc"
    break;

  case 463: /* $@224: %empty  */
#line 869 "configparser.yy"
                                { lconfig_bt1->setText( (yyvsp[-1].strptr) ); }
#line 7449 "configparser.cc"
    break;

  case 465: /* $@225: %empty  */
#line 870 "configparser.yy"
                                        { lconfig_bt1->setFG( (yyvsp[-3].num) );
                              lconfig_bt1->setBG( (yyvsp[-1].num) );
                            }
#line 7457 "configparser.cc"
    break;

  case 467: /* $@226: %empty  */
#line 873 "configparser.yy"
                                  { lconfig_listcom.clear(); }
#line 7463 "configparser.cc"
    break;

  case 468: /* $@227: %empty  */
#line 874 "configparser.yy"
                               { lconfig_bt1->setComs( lconfig_listcom );
                                 lconfig_listcom.clear();
                           }
#line 7471 "configparser.cc"
    break;

  case 470: /* $@228: %empty  */
#line 877 "configparser.yy"
                                   { lconfig_listsk = new List(); }
#line 7477 "configparser.cc"
    break;

  case 471: /* $@229: %empty  */
#line 878 "configparser.yy"
                              { lconfig_bt1->setDoubleKeys( lconfig_listsk );
                            lconfig_id = lconfig_listsk->initEnum();
                            lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            while ( lconfig_dk != NULL ) {
                              delete lconfig_dk;
                              lconfig_listsk->removeFirstElement();
                             lconfig_dk = (WCDoubleShortkey*)lconfig_listsk->getFirstElement( lconfig_id );
                            }
                            lconfig_listsk->closeEnum( lconfig_id );
                            delete lconfig_listsk;
                            lconfig_listsk = NULL;
                          }
#line 7494 "configparser.cc"
    break;

  case 474: /* $@230: %empty  */
#line 893 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 0, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_GLOBAL, (yyvsp[-1].strptr) );
      }
#line 7505 "configparser.cc"
    break;

  case 476: /* $@231: %empty  */
#line 900 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 1, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_BUTTONS, (yyvsp[-1].strptr) );
      }
#line 7516 "configparser.cc"
    break;

  case 478: /* $@232: %empty  */
#line 907 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 2, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_LEFT, (yyvsp[-1].strptr) );
      }
#line 7527 "configparser.cc"
    break;

  case 480: /* $@233: %empty  */
#line 914 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 3, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_RIGHT, (yyvsp[-1].strptr) );
      }
#line 7538 "configparser.cc"
    break;

  case 482: /* $@234: %empty  */
#line 921 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 4, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_TEXTVIEW, (yyvsp[-1].strptr) );
      }
#line 7549 "configparser.cc"
    break;

  case 484: /* $@235: %empty  */
#line 928 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 5, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_STATEBAR, (yyvsp[-1].strptr) );
      }
#line 7560 "configparser.cc"
    break;

  case 486: /* $@236: %empty  */
#line 935 "configparser.yy"
      {
#ifndef HAVE_XFT
        lconfig->setFont( 6, (yyvsp[-1].strptr) );
#endif
        lconfig->setFontName( WConfig::FONT_X11_TEXTVIEW_ALT, (yyvsp[-1].strptr) );
      }
#line 7571 "configparser.cc"
    break;

  case 489: /* $@237: %empty  */
#line 944 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 0, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_GLOBAL, (yyvsp[-1].strptr) );
         }
#line 7582 "configparser.cc"
    break;

  case 491: /* $@238: %empty  */
#line 951 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 1, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_BUTTONS, (yyvsp[-1].strptr) );
         }
#line 7593 "configparser.cc"
    break;

  case 493: /* $@239: %empty  */
#line 958 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 2, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_LEFT, (yyvsp[-1].strptr) );
         }
#line 7604 "configparser.cc"
    break;

  case 495: /* $@240: %empty  */
#line 965 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 3, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_RIGHT, (yyvsp[-1].strptr) );
         }
#line 7615 "configparser.cc"
    break;

  case 497: /* $@241: %empty  */
#line 972 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 4, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_TEXTVIEW, (yyvsp[-1].strptr) );
         }
#line 7626 "configparser.cc"
    break;

  case 499: /* $@242: %empty  */
#line 979 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 5, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_STATEBAR, (yyvsp[-1].strptr) );
         }
#line 7637 "configparser.cc"
    break;

  case 501: /* $@243: %empty  */
#line 986 "configparser.yy"
         {
#ifdef HAVE_XFT
           lconfig->setFont( 6, (yyvsp[-1].strptr) );
#endif
           lconfig->setFontName( WConfig::FONT_XFT_TEXTVIEW_ALT, (yyvsp[-1].strptr) );
         }
#line 7648 "configparser.cc"
    break;

  case 504: /* $@244: %empty  */
#line 995 "configparser.yy"
              {
                  lconfig->setVMMountCommand( (yyvsp[-1].strptr) );
              }
#line 7656 "configparser.cc"
    break;

  case 506: /* $@245: %empty  */
#line 999 "configparser.yy"
              {
                  lconfig->setVMUnmountCommand( (yyvsp[-1].strptr) );
              }
#line 7664 "configparser.cc"
    break;

  case 508: /* $@246: %empty  */
#line 1003 "configparser.yy"
              {
                  lconfig->setVMEjectCommand( (yyvsp[-1].strptr) );
              }
#line 7672 "configparser.cc"
    break;

  case 510: /* $@247: %empty  */
#line 1007 "configparser.yy"
              {
                  lconfig->setVMCloseTrayCommand( (yyvsp[-1].strptr) );
              }
#line 7680 "configparser.cc"
    break;

  case 512: /* $@248: %empty  */
#line 1011 "configparser.yy"
              {
                  lconfig->setVMFStabFile( (yyvsp[-1].strptr) );
              }
#line 7688 "configparser.cc"
    break;

  case 514: /* $@249: %empty  */
#line 1015 "configparser.yy"
              {
                  lconfig->setVMMtabFile( (yyvsp[-1].strptr) );
              }
#line 7696 "configparser.cc"
    break;

  case 516: /* $@250: %empty  */
#line 1019 "configparser.yy"
              {
                  lconfig->setVMPartitionFile( (yyvsp[-1].strptr) );
              }
#line 7704 "configparser.cc"
    break;

  case 518: /* $@251: %empty  */
#line 1023 "configparser.yy"
              {
                  lconfig->setVMRequestAction( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false );
              }
#line 7712 "configparser.cc"
    break;

  case 520: /* $@252: %empty  */
#line 1027 "configparser.yy"
              {
                  lconfig->setVMPreferredUdisksVersion( (yyvsp[-1].num) );
              }
#line 7720 "configparser.cc"
    break;

  case 523: /* $@253: %empty  */
#line 1032 "configparser.yy"
                                          { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_TIMERAM ); }
#line 7726 "configparser.cc"
    break;

  case 525: /* $@254: %empty  */
#line 1033 "configparser.yy"
                                     { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_TIME ); }
#line 7732 "configparser.cc"
    break;

  case 527: /* $@255: %empty  */
#line 1034 "configparser.yy"
                                        { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_VERSION ); }
#line 7738 "configparser.cc"
    break;

  case 529: /* $@256: %empty  */
#line 1035 "configparser.yy"
                                       { lconfig->setClockbarMode( WConfig::CLOCKBAR_MODE_EXTERN ); }
#line 7744 "configparser.cc"
    break;

  case 531: /* $@257: %empty  */
#line 1036 "configparser.yy"
                                         { lconfig->setClockbarUpdatetime( (yyvsp[-1].num) ); }
#line 7750 "configparser.cc"
    break;

  case 533: /* $@258: %empty  */
#line 1037 "configparser.yy"
                                         { lconfig->setClockbarCommand( (yyvsp[-1].strptr) ); }
#line 7756 "configparser.cc"
    break;

  case 535: /* $@259: %empty  */
#line 1038 "configparser.yy"
                                     { lconfig->setClockbarHints( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7762 "configparser.cc"
    break;

  case 538: /* bool: YES_WCP  */
#line 1041 "configparser.yy"
             { (yyval.num) = 1; }
#line 7768 "configparser.cc"
    break;

  case 539: /* bool: NO_WCP  */
#line 1042 "configparser.yy"
            { (yyval.num) = 0; }
#line 7774 "configparser.cc"
    break;

  case 613: /* $@260: %empty  */
#line 1118 "configparser.yy"
                              { lconfig_fp1 = std::make_shared< OwnOp >(); }
#line 7780 "configparser.cc"
    break;

  case 614: /* ownop: OWNOP_WCP LEFTBRACE_WCP $@260 ownopbody RIGHTBRACE_WCP  */
#line 1119 "configparser.yy"
                               { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7786 "configparser.cc"
    break;

  case 615: /* $@261: %empty  */
#line 1121 "configparser.yy"
                                  { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setComStr( (yyvsp[-1].strptr) ); }
#line 7792 "configparser.cc"
    break;

  case 617: /* $@262: %empty  */
#line 1122 "configparser.yy"
                                          { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setSeparateEachEntry( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7798 "configparser.cc"
    break;

  case 619: /* $@263: %empty  */
#line 1123 "configparser.yy"
                                  { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7804 "configparser.cc"
    break;

  case 621: /* $@264: %empty  */
#line 1124 "configparser.yy"
                                      { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_IN_TERMINAL ); }
#line 7810 "configparser.cc"
    break;

  case 623: /* $@265: %empty  */
#line 1125 "configparser.yy"
                                          { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_IN_TERMINAL_AND_WAIT4KEY ); }
#line 7816 "configparser.cc"
    break;

  case 625: /* $@266: %empty  */
#line 1126 "configparser.yy"
                                        { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT ); }
#line 7822 "configparser.cc"
    break;

  case 627: /* $@267: %empty  */
#line 1127 "configparser.yy"
                                           { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_INT ); }
#line 7828 "configparser.cc"
    break;

  case 629: /* $@268: %empty  */
#line 1128 "configparser.yy"
                                                 { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_OTHER_SIDE ); }
#line 7834 "configparser.cc"
    break;

  case 631: /* $@269: %empty  */
#line 1129 "configparser.yy"
                                                       { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE ); }
#line 7840 "configparser.cc"
    break;

  case 633: /* $@270: %empty  */
#line 1130 "configparser.yy"
                                    { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setOwnStart( OwnOp::OWNOP_START_NORMAL ); }
#line 7846 "configparser.cc"
    break;

  case 635: /* $@271: %empty  */
#line 1131 "configparser.yy"
                                      { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setViewStr( (yyvsp[-1].strptr) ); }
#line 7852 "configparser.cc"
    break;

  case 637: /* $@272: %empty  */
#line 1132 "configparser.yy"
                                     { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setInBackground( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7858 "configparser.cc"
    break;

  case 639: /* $@273: %empty  */
#line 1133 "configparser.yy"
                                 { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setTakeDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7864 "configparser.cc"
    break;

  case 641: /* $@274: %empty  */
#line 1134 "configparser.yy"
                               { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setDontCD( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7870 "configparser.cc"
    break;

  case 643: /* $@275: %empty  */
#line 1135 "configparser.yy"
                                                { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setUseVirtualTempCopies( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7876 "configparser.cc"
    break;

  case 645: /* $@276: %empty  */
#line 1136 "configparser.yy"
                                                      { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setFlagsAreRelativeToBaseDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7882 "configparser.cc"
    break;

  case 647: /* $@277: %empty  */
#line 1137 "configparser.yy"
                                     { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setFollowActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7888 "configparser.cc"
    break;

  case 649: /* $@278: %empty  */
#line 1138 "configparser.yy"
                                  { std::dynamic_pointer_cast<OwnOp>(lconfig_fp1)->setWatchFile( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 7894 "configparser.cc"
    break;

  case 652: /* $@279: %empty  */
#line 1141 "configparser.yy"
                                      { lconfig_fp1 = std::make_shared<DNDAction>(); }
#line 7900 "configparser.cc"
    break;

  case 653: /* dndaction: DNDACTION_WCP LEFTBRACE_WCP $@279 RIGHTBRACE_WCP  */
#line 1142 "configparser.yy"
                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7906 "configparser.cc"
    break;

  case 654: /* $@280: %empty  */
#line 1144 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<DoubleClickAction>(); }
#line 7912 "configparser.cc"
    break;

  case 655: /* dcaction: DCACTION_WCP LEFTBRACE_WCP $@280 RIGHTBRACE_WCP  */
#line 1145 "configparser.yy"
                        { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7918 "configparser.cc"
    break;

  case 656: /* $@281: %empty  */
#line 1147 "configparser.yy"
                                        { lconfig_fp1 = std::make_shared<ShowAction>(); }
#line 7924 "configparser.cc"
    break;

  case 657: /* showaction: SHOWACTION_WCP LEFTBRACE_WCP $@281 RIGHTBRACE_WCP  */
#line 1148 "configparser.yy"
                          { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7930 "configparser.cc"
    break;

  case 658: /* $@282: %empty  */
#line 1150 "configparser.yy"
                                          { lconfig_fp1 = std::make_shared<RawShowAction>(); }
#line 7936 "configparser.cc"
    break;

  case 659: /* rshowaction: RSHOWACTION_WCP LEFTBRACE_WCP $@282 RIGHTBRACE_WCP  */
#line 1151 "configparser.yy"
                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7942 "configparser.cc"
    break;

  case 660: /* $@283: %empty  */
#line 1153 "configparser.yy"
                                        { lconfig_fp1 = std::make_shared<UserAction>(); }
#line 7948 "configparser.cc"
    break;

  case 661: /* useraction: USERACTION_WCP LEFTBRACE_WCP $@283 useractionbody RIGHTBRACE_WCP  */
#line 1154 "configparser.yy"
                                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7954 "configparser.cc"
    break;

  case 662: /* $@284: %empty  */
#line 1156 "configparser.yy"
                                             { std::dynamic_pointer_cast<UserAction>(lconfig_fp1)->setNr( (yyvsp[-1].num) ); }
#line 7960 "configparser.cc"
    break;

  case 665: /* $@285: %empty  */
#line 1159 "configparser.yy"
                              { auto tfp = std::make_shared< ScriptOp >();
                                tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                tfp->setCommandStr( "up" );
                                lconfig_fp1 = tfp;
                              }
#line 7970 "configparser.cc"
    break;

  case 666: /* rowup: ROWUP_WCP LEFTBRACE_WCP $@285 RIGHTBRACE_WCP  */
#line 1164 "configparser.yy"
                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7976 "configparser.cc"
    break;

  case 667: /* $@286: %empty  */
#line 1166 "configparser.yy"
                                  { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "down" );
                                    lconfig_fp1 = tfp;
                                  }
#line 7986 "configparser.cc"
    break;

  case 668: /* rowdown: ROWDOWN_WCP LEFTBRACE_WCP $@286 RIGHTBRACE_WCP  */
#line 1171 "configparser.yy"
                       { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 7992 "configparser.cc"
    break;

  case 669: /* $@287: %empty  */
#line 1173 "configparser.yy"
                                                    { lconfig_fp1 = std::make_shared<ChangeHiddenFlag>(); }
#line 7998 "configparser.cc"
    break;

  case 670: /* changehiddenflag: CHANGEHIDDENFLAG_WCP LEFTBRACE_WCP $@287 changehiddenflagbody RIGHTBRACE_WCP  */
#line 1174 "configparser.yy"
                                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8004 "configparser.cc"
    break;

  case 671: /* $@288: %empty  */
#line 1176 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeHiddenFlag>(lconfig_fp1)->setMode( 2 ); }
#line 8010 "configparser.cc"
    break;

  case 673: /* $@289: %empty  */
#line 1177 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeHiddenFlag>(lconfig_fp1)->setMode( 1 ); }
#line 8016 "configparser.cc"
    break;

  case 675: /* $@290: %empty  */
#line 1178 "configparser.yy"
                                                     { std::dynamic_pointer_cast<ChangeHiddenFlag>(lconfig_fp1)->setMode( 0 ); }
#line 8022 "configparser.cc"
    break;

  case 678: /* $@291: %empty  */
#line 1181 "configparser.yy"
                                { lconfig_fp1 = std::make_shared<CopyOp>(); }
#line 8028 "configparser.cc"
    break;

  case 679: /* copyop: COPYOP_WCP LEFTBRACE_WCP $@291 copyopbody RIGHTBRACE_WCP  */
#line 1182 "configparser.yy"
                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8034 "configparser.cc"
    break;

  case 680: /* $@292: %empty  */
#line 1184 "configparser.yy"
                                        { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setFollowSymlinks( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8040 "configparser.cc"
    break;

  case 682: /* $@293: %empty  */
#line 1185 "configparser.yy"
                              { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setMove( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8046 "configparser.cc"
    break;

  case 684: /* $@294: %empty  */
#line 1186 "configparser.yy"
                                { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setRename( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8052 "configparser.cc"
    break;

  case 686: /* $@295: %empty  */
#line 1187 "configparser.yy"
                                 { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setSameDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8058 "configparser.cc"
    break;

  case 688: /* $@296: %empty  */
#line 1188 "configparser.yy"
                                     { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setRequestDest( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8064 "configparser.cc"
    break;

  case 690: /* $@297: %empty  */
#line 1189 "configparser.yy"
                                      { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8070 "configparser.cc"
    break;

  case 692: /* $@298: %empty  */
#line 1190 "configparser.yy"
                                         { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_ALWAYS ); }
#line 8076 "configparser.cc"
    break;

  case 694: /* $@299: %empty  */
#line 1191 "configparser.yy"
                                        { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_NEVER ); }
#line 8082 "configparser.cc"
    break;

  case 696: /* $@300: %empty  */
#line 1192 "configparser.yy"
                                         { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setOverwrite( CopyOp::COPYOP_OVERWRITE_NORMAL ); }
#line 8088 "configparser.cc"
    break;

  case 698: /* $@301: %empty  */
#line 1193 "configparser.yy"
                                        {}
#line 8094 "configparser.cc"
    break;

  case 700: /* $@302: %empty  */
#line 1194 "configparser.yy"
                                      {}
#line 8100 "configparser.cc"
    break;

  case 702: /* $@303: %empty  */
#line 1195 "configparser.yy"
                                      { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setPreserveAttr( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8106 "configparser.cc"
    break;

  case 704: /* $@304: %empty  */
#line 1196 "configparser.yy"
                                                     { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_NEVER ); }
#line 8112 "configparser.cc"
    break;

  case 706: /* $@305: %empty  */
#line 1197 "configparser.yy"
                                                       { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_OUTSIDE ); }
#line 8118 "configparser.cc"
    break;

  case 708: /* $@306: %empty  */
#line 1198 "configparser.yy"
                                                      { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setAdjustRelativeSymlinks( CopyOp::COPYOP_ADJUST_SYMLINK_ALWAYS ); }
#line 8124 "configparser.cc"
    break;

  case 710: /* $@307: %empty  */
#line 1199 "configparser.yy"
                                                                 { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setEnsureFilePermissions( LEAVE_PERMISSIONS_UNMODIFIED ); }
#line 8130 "configparser.cc"
    break;

  case 712: /* $@308: %empty  */
#line 1200 "configparser.yy"
                                                        { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_PERMISSION ); }
#line 8136 "configparser.cc"
    break;

  case 714: /* $@309: %empty  */
#line 1201 "configparser.yy"
                                                                { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_GROUP_R_PERMISSION ); }
#line 8142 "configparser.cc"
    break;

  case 716: /* $@310: %empty  */
#line 1202 "configparser.yy"
                                                              { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setEnsureFilePermissions( ENSURE_USER_RW_ALL_R_PERMISSION ); }
#line 8148 "configparser.cc"
    break;

  case 718: /* $@311: %empty  */
#line 1203 "configparser.yy"
                                                     { std::dynamic_pointer_cast<CopyOp>(lconfig_fp1)->setVdirPreserveDirStructure( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8154 "configparser.cc"
    break;

  case 721: /* $@312: %empty  */
#line 1206 "configparser.yy"
                                    { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "first" );
                                      lconfig_fp1 = tfp;
                                    }
#line 8164 "configparser.cc"
    break;

  case 722: /* firstrow: FIRSTROW_WCP LEFTBRACE_WCP $@312 RIGHTBRACE_WCP  */
#line 1211 "configparser.yy"
                        { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8170 "configparser.cc"
    break;

  case 723: /* $@313: %empty  */
#line 1213 "configparser.yy"
                                  { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "last" );
                                    lconfig_fp1 = tfp;
                                  }
#line 8180 "configparser.cc"
    break;

  case 724: /* lastrow: LASTROW_WCP LEFTBRACE_WCP $@313 RIGHTBRACE_WCP  */
#line 1218 "configparser.yy"
                       { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8186 "configparser.cc"
    break;

  case 725: /* $@314: %empty  */
#line 1220 "configparser.yy"
                                { auto tfp = std::make_shared< ScriptOp >();
                                  tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                  tfp->setCommandStr( "pageup" );
                                  lconfig_fp1 = tfp;
                                }
#line 8196 "configparser.cc"
    break;

  case 726: /* pageup: PAGEUP_WCP LEFTBRACE_WCP $@314 RIGHTBRACE_WCP  */
#line 1225 "configparser.yy"
                      { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8202 "configparser.cc"
    break;

  case 727: /* $@315: %empty  */
#line 1227 "configparser.yy"
                                    { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "pagedown" );
                                      lconfig_fp1 = tfp;
                                    }
#line 8212 "configparser.cc"
    break;

  case 728: /* pagedown: PAGEDOWN_WCP LEFTBRACE_WCP $@315 RIGHTBRACE_WCP  */
#line 1232 "configparser.yy"
                        { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8218 "configparser.cc"
    break;

  case 729: /* $@316: %empty  */
#line 1234 "configparser.yy"
                                    { auto tfp = std::make_shared< ScriptOp >();
                                      tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                      tfp->setCommandStr( "selectentry" );
                                      lconfig_fp1 = tfp;
                                    }
#line 8228 "configparser.cc"
    break;

  case 730: /* selectop: SELECTOP_WCP LEFTBRACE_WCP $@316 RIGHTBRACE_WCP  */
#line 1239 "configparser.yy"
                        { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8234 "configparser.cc"
    break;

  case 731: /* $@317: %empty  */
#line 1241 "configparser.yy"
                                          { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "selectall" );
                                            lconfig_fp1 = tfp;
                                          }
#line 8244 "configparser.cc"
    break;

  case 732: /* selectallop: SELECTALLOP_WCP LEFTBRACE_WCP $@317 RIGHTBRACE_WCP  */
#line 1246 "configparser.yy"
                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8250 "configparser.cc"
    break;

  case 733: /* $@318: %empty  */
#line 1248 "configparser.yy"
                                            { auto tfp = std::make_shared< ScriptOp >();
                                              tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                              tfp->setCommandStr( "selectnone" );
                                              lconfig_fp1 = tfp;
                                            }
#line 8260 "configparser.cc"
    break;

  case 734: /* selectnoneop: SELECTNONEOP_WCP LEFTBRACE_WCP $@318 RIGHTBRACE_WCP  */
#line 1253 "configparser.yy"
                            { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8266 "configparser.cc"
    break;

  case 735: /* $@319: %empty  */
#line 1255 "configparser.yy"
                                          { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "invertall" );
                                            lconfig_fp1 = tfp;
                                          }
#line 8276 "configparser.cc"
    break;

  case 736: /* invertallop: INVERTALLOP_WCP LEFTBRACE_WCP $@319 RIGHTBRACE_WCP  */
#line 1260 "configparser.yy"
                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8282 "configparser.cc"
    break;

  case 737: /* $@320: %empty  */
#line 1262 "configparser.yy"
                                          { auto tfp = std::make_shared< ScriptOp >();
                                            tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                            tfp->setCommandStr( "parentdir" );
                                            lconfig_fp1 = tfp;
                                          }
#line 8292 "configparser.cc"
    break;

  case 738: /* parentdirop: PARENTDIROP_WCP LEFTBRACE_WCP $@320 RIGHTBRACE_WCP  */
#line 1267 "configparser.yy"
                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8298 "configparser.cc"
    break;

  case 739: /* $@321: %empty  */
#line 1269 "configparser.yy"
                                        { lconfig_fp1 = std::make_shared<EnterDirOp>(); }
#line 8304 "configparser.cc"
    break;

  case 740: /* enterdirop: ENTERDIROP_WCP LEFTBRACE_WCP $@321 enterdiropbody RIGHTBRACE_WCP  */
#line 1270 "configparser.yy"
                                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8310 "configparser.cc"
    break;

  case 741: /* $@322: %empty  */
#line 1272 "configparser.yy"
                                       { std::dynamic_pointer_cast<EnterDirOp>(lconfig_fp1)->setDir( (yyvsp[-1].strptr) ); }
#line 8316 "configparser.cc"
    break;

  case 743: /* $@323: %empty  */
#line 1273 "configparser.yy"
                                        { std::dynamic_pointer_cast<EnterDirOp>(lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_ACTIVE ); }
#line 8322 "configparser.cc"
    break;

  case 745: /* $@324: %empty  */
#line 1274 "configparser.yy"
                                              { std::dynamic_pointer_cast<EnterDirOp>(lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_ACTIVE2OTHER ); }
#line 8328 "configparser.cc"
    break;

  case 747: /* $@325: %empty  */
#line 1275 "configparser.yy"
                                         { std::dynamic_pointer_cast<EnterDirOp>(lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_SPECIAL ); }
#line 8334 "configparser.cc"
    break;

  case 749: /* $@326: %empty  */
#line 1276 "configparser.yy"
                                         { std::dynamic_pointer_cast<EnterDirOp>(lconfig_fp1)->setMode( EnterDirOp::ENTERDIROP_REQUEST ); }
#line 8340 "configparser.cc"
    break;

  case 752: /* $@327: %empty  */
#line 1279 "configparser.yy"
                                                      { lconfig_fp1 = std::make_shared<ChangeListerSetOp>(); }
#line 8346 "configparser.cc"
    break;

  case 753: /* changelistersetop: CHANGELISTERSETOP_WCP LEFTBRACE_WCP $@327 changelistersetopbody RIGHTBRACE_WCP  */
#line 1280 "configparser.yy"
                                                       { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8352 "configparser.cc"
    break;

  case 754: /* $@328: %empty  */
#line 1282 "configparser.yy"
                                             { std::dynamic_pointer_cast<ChangeListerSetOp>(lconfig_fp1)->setMode( ChangeListerSetOp::CLS_LEFT_LISTER ); }
#line 8358 "configparser.cc"
    break;

  case 756: /* $@329: %empty  */
#line 1283 "configparser.yy"
                                              { std::dynamic_pointer_cast<ChangeListerSetOp>(lconfig_fp1)->setMode( ChangeListerSetOp::CLS_RIGHT_LISTER ); }
#line 8364 "configparser.cc"
    break;

  case 758: /* $@330: %empty  */
#line 1284 "configparser.yy"
                                                { std::dynamic_pointer_cast<ChangeListerSetOp>(lconfig_fp1)->setMode( ChangeListerSetOp::CLS_CURRENT_LISTER ); }
#line 8370 "configparser.cc"
    break;

  case 760: /* $@331: %empty  */
#line 1285 "configparser.yy"
                                              { std::dynamic_pointer_cast<ChangeListerSetOp>(lconfig_fp1)->setMode( ChangeListerSetOp::CLS_OTHER_LISTER ); }
#line 8376 "configparser.cc"
    break;

  case 763: /* $@332: %empty  */
#line 1288 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<SwitchListerOp>(); }
#line 8382 "configparser.cc"
    break;

  case 764: /* switchlisterop: SWITCHLISTEROP_WCP LEFTBRACE_WCP $@332 RIGHTBRACE_WCP  */
#line 1289 "configparser.yy"
                              { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8388 "configparser.cc"
    break;

  case 765: /* $@333: %empty  */
#line 1291 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<FilterSelectOp>(); }
#line 8394 "configparser.cc"
    break;

  case 766: /* filterselectop: FILTERSELECTOP_WCP LEFTBRACE_WCP $@333 filterselectopbody RIGHTBRACE_WCP  */
#line 1292 "configparser.yy"
                                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8400 "configparser.cc"
    break;

  case 767: /* $@334: %empty  */
#line 1294 "configparser.yy"
                                              { std::dynamic_pointer_cast<FilterSelectOp>(lconfig_fp1)->setFilter( (yyvsp[-1].strptr) ); }
#line 8406 "configparser.cc"
    break;

  case 769: /* $@335: %empty  */
#line 1295 "configparser.yy"
                                            { std::dynamic_pointer_cast<FilterSelectOp>(lconfig_fp1)->setAutoFilter( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8412 "configparser.cc"
    break;

  case 771: /* $@336: %empty  */
#line 1296 "configparser.yy"
                                                        { std::dynamic_pointer_cast<FilterSelectOp>(lconfig_fp1)->setImmediateFilterApply( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8418 "configparser.cc"
    break;

  case 774: /* $@337: %empty  */
#line 1299 "configparser.yy"
                                                    { lconfig_fp1 = std::make_shared<FilterUnSelectOp>(); }
#line 8424 "configparser.cc"
    break;

  case 775: /* filterunselectop: FILTERUNSELECTOP_WCP LEFTBRACE_WCP $@337 filterunselectopbody RIGHTBRACE_WCP  */
#line 1300 "configparser.yy"
                                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8430 "configparser.cc"
    break;

  case 776: /* $@338: %empty  */
#line 1302 "configparser.yy"
                                                { std::dynamic_pointer_cast<FilterUnSelectOp>(lconfig_fp1)->setFilter( (yyvsp[-1].strptr) ); }
#line 8436 "configparser.cc"
    break;

  case 778: /* $@339: %empty  */
#line 1303 "configparser.yy"
                                              { std::dynamic_pointer_cast<FilterUnSelectOp>(lconfig_fp1)->setAutoFilter( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8442 "configparser.cc"
    break;

  case 780: /* $@340: %empty  */
#line 1304 "configparser.yy"
                                                          { std::dynamic_pointer_cast<FilterUnSelectOp>(lconfig_fp1)->setImmediateFilterApply( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8448 "configparser.cc"
    break;

  case 783: /* $@341: %empty  */
#line 1307 "configparser.yy"
                                                      { lconfig_fp1 = std::make_shared<Path2OSideOp>(); }
#line 8454 "configparser.cc"
    break;

  case 784: /* pathtoothersideop: PATHTOOTHERSIDEOP_WCP LEFTBRACE_WCP $@341 RIGHTBRACE_WCP  */
#line 1308 "configparser.yy"
                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8460 "configparser.cc"
    break;

  case 785: /* $@342: %empty  */
#line 1310 "configparser.yy"
                                { lconfig_fp1 = std::make_shared<QuitOp>(); }
#line 8466 "configparser.cc"
    break;

  case 786: /* quitop: QUITOP_WCP LEFTBRACE_WCP $@342 quitopbody RIGHTBRACE_WCP  */
#line 1311 "configparser.yy"
                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8472 "configparser.cc"
    break;

  case 787: /* $@343: %empty  */
#line 1313 "configparser.yy"
                                    { std::dynamic_pointer_cast<QuitOp>(lconfig_fp1)->setMode( QuitOp::Q_NORMAL_QUIT ); }
#line 8478 "configparser.cc"
    break;

  case 789: /* $@344: %empty  */
#line 1314 "configparser.yy"
                                   { std::dynamic_pointer_cast<QuitOp>(lconfig_fp1)->setMode( QuitOp::Q_QUICK_QUIT ); }
#line 8484 "configparser.cc"
    break;

  case 792: /* $@345: %empty  */
#line 1317 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<DeleteOp>(); }
#line 8490 "configparser.cc"
    break;

  case 793: /* deleteop: DELETEOP_WCP LEFTBRACE_WCP $@345 deleteopbody RIGHTBRACE_WCP  */
#line 1318 "configparser.yy"
                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8496 "configparser.cc"
    break;

  case 794: /* $@346: %empty  */
#line 1320 "configparser.yy"
                                      { std::dynamic_pointer_cast<DeleteOp>(lconfig_fp1)->setAlsoActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8502 "configparser.cc"
    break;

  case 797: /* $@347: %empty  */
#line 1323 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<ReloadOp>(); }
#line 8508 "configparser.cc"
    break;

  case 798: /* reloadop: RELOADOP_WCP LEFTBRACE_WCP $@347 reloadopbody RIGHTBRACE_WCP  */
#line 1324 "configparser.yy"
                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8514 "configparser.cc"
    break;

  case 799: /* $@348: %empty  */
#line 1326 "configparser.yy"
                                    { std::dynamic_pointer_cast<ReloadOp>(lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_LEFTSIDE ); }
#line 8520 "configparser.cc"
    break;

  case 801: /* $@349: %empty  */
#line 1327 "configparser.yy"
                                     { std::dynamic_pointer_cast<ReloadOp>(lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_RIGHTSIDE ); }
#line 8526 "configparser.cc"
    break;

  case 803: /* $@350: %empty  */
#line 1328 "configparser.yy"
                                       { std::dynamic_pointer_cast<ReloadOp>(lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_THISSIDE ); }
#line 8532 "configparser.cc"
    break;

  case 805: /* $@351: %empty  */
#line 1329 "configparser.yy"
                                     { std::dynamic_pointer_cast<ReloadOp>(lconfig_fp1)->setReloadside( ReloadOp::RELOADOP_OTHERSIDE ); }
#line 8538 "configparser.cc"
    break;

  case 807: /* $@352: %empty  */
#line 1330 "configparser.yy"
                                         { std::dynamic_pointer_cast<ReloadOp>(lconfig_fp1)->setResetDirSizes( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8544 "configparser.cc"
    break;

  case 809: /* $@353: %empty  */
#line 1331 "configparser.yy"
                                         { std::dynamic_pointer_cast<ReloadOp>(lconfig_fp1)->setKeepFiletypes( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8550 "configparser.cc"
    break;

  case 812: /* $@354: %empty  */
#line 1334 "configparser.yy"
                                      { lconfig_fp1 = std::make_shared<MakeDirOp>(); }
#line 8556 "configparser.cc"
    break;

  case 813: /* makedirop: MAKEDIROP_WCP LEFTBRACE_WCP $@354 RIGHTBRACE_WCP  */
#line 1335 "configparser.yy"
                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8562 "configparser.cc"
    break;

  case 814: /* $@355: %empty  */
#line 1337 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<RenameOp>(); }
#line 8568 "configparser.cc"
    break;

  case 815: /* renameop: RENAMEOP_WCP LEFTBRACE_WCP $@355 RIGHTBRACE_WCP  */
#line 1338 "configparser.yy"
                        { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8574 "configparser.cc"
    break;

  case 816: /* $@356: %empty  */
#line 1340 "configparser.yy"
                                      { lconfig_fp1 = std::make_shared<DirSizeOp>(); }
#line 8580 "configparser.cc"
    break;

  case 817: /* dirsizeop: DIRSIZEOP_WCP LEFTBRACE_WCP $@356 RIGHTBRACE_WCP  */
#line 1341 "configparser.yy"
                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8586 "configparser.cc"
    break;

  case 818: /* $@357: %empty  */
#line 1343 "configparser.yy"
                                  { auto tfp = std::make_shared< ScriptOp >();
                                    tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                    tfp->setCommandStr( "simulate_doubleclick" );
                                    lconfig_fp1 = tfp;
                                  }
#line 8596 "configparser.cc"
    break;

  case 819: /* simddop: SIMDDOP_WCP LEFTBRACE_WCP $@357 RIGHTBRACE_WCP  */
#line 1348 "configparser.yy"
                       { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8602 "configparser.cc"
    break;

  case 820: /* $@358: %empty  */
#line 1350 "configparser.yy"
                                          { lconfig_fp1 = std::make_shared<StartProgOp>(); }
#line 8608 "configparser.cc"
    break;

  case 821: /* startprogop: STARTPROGOP_WCP LEFTBRACE_WCP $@358 startprogopbody RIGHTBRACE_WCP  */
#line 1351 "configparser.yy"
                                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8614 "configparser.cc"
    break;

  case 822: /* $@359: %empty  */
#line 1353 "configparser.yy"
                                            { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_IN_TERMINAL ); }
#line 8620 "configparser.cc"
    break;

  case 824: /* $@360: %empty  */
#line 1354 "configparser.yy"
                                                { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_IN_TERMINAL_AND_WAIT4KEY ); }
#line 8626 "configparser.cc"
    break;

  case 826: /* $@361: %empty  */
#line 1355 "configparser.yy"
                                              { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT ); }
#line 8632 "configparser.cc"
    break;

  case 828: /* $@362: %empty  */
#line 1356 "configparser.yy"
                                                 { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_INT ); }
#line 8638 "configparser.cc"
    break;

  case 830: /* $@363: %empty  */
#line 1357 "configparser.yy"
                                                       { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_OTHER_SIDE ); }
#line 8644 "configparser.cc"
    break;

  case 832: /* $@364: %empty  */
#line 1358 "configparser.yy"
                                                             { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_SHOW_OUTPUT_CUSTOM_ATTRIBUTE ); }
#line 8650 "configparser.cc"
    break;

  case 834: /* $@365: %empty  */
#line 1359 "configparser.yy"
                                          { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setStart( StartProgOp::STARTPROGOP_START_NORMAL ); }
#line 8656 "configparser.cc"
    break;

  case 836: /* $@366: %empty  */
#line 1360 "configparser.yy"
                                            { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setViewStr( (yyvsp[-1].strptr) ); }
#line 8662 "configparser.cc"
    break;

  case 838: /* $@367: %empty  */
#line 1361 "configparser.yy"
                                     { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setGlobal( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8668 "configparser.cc"
    break;

  case 840: /* $@368: %empty  */
#line 1362 "configparser.yy"
                                           { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8674 "configparser.cc"
    break;

  case 842: /* $@369: %empty  */
#line 1363 "configparser.yy"
                                           { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setInBackground( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8680 "configparser.cc"
    break;

  case 844: /* $@370: %empty  */
#line 1364 "configparser.yy"
                                     { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setDontCD( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8686 "configparser.cc"
    break;

  case 846: /* $@371: %empty  */
#line 1365 "configparser.yy"
                                           { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setFollowActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8692 "configparser.cc"
    break;

  case 848: /* $@372: %empty  */
#line 1366 "configparser.yy"
                                        { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setWatchFile( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8698 "configparser.cc"
    break;

  case 850: /* $@373: %empty  */
#line 1367 "configparser.yy"
                                                { std::dynamic_pointer_cast<StartProgOp>(lconfig_fp1)->setSeparateEachEntry( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8704 "configparser.cc"
    break;

  case 853: /* $@374: %empty  */
#line 1370 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<SearchEntryOp>(); }
#line 8710 "configparser.cc"
    break;

  case 854: /* searchentryop: SEARCHENTRYOP_WCP LEFTBRACE_WCP $@374 searchentryopbody RIGHTBRACE_WCP  */
#line 1371 "configparser.yy"
                                               { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8716 "configparser.cc"
    break;

  case 855: /* $@375: %empty  */
#line 1373 "configparser.yy"
                                           { std::dynamic_pointer_cast<SearchEntryOp>(lconfig_fp1)->setIgnoreCase( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8722 "configparser.cc"
    break;

  case 857: /* $@376: %empty  */
#line 1374 "configparser.yy"
                                              { std::dynamic_pointer_cast<SearchEntryOp>(lconfig_fp1)->setReverseSearch( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8728 "configparser.cc"
    break;

  case 859: /* $@377: %empty  */
#line 1375 "configparser.yy"
                                            { std::dynamic_pointer_cast<SearchEntryOp>(lconfig_fp1)->setInfixSearch( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8734 "configparser.cc"
    break;

  case 861: /* $@378: %empty  */
#line 1376 "configparser.yy"
                                              { std::dynamic_pointer_cast<SearchEntryOp>(lconfig_fp1)->setFlexibleMatch( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8740 "configparser.cc"
    break;

  case 864: /* $@379: %empty  */
#line 1379 "configparser.yy"
                                          { lconfig_fp1 = std::make_shared<EnterPathOp>(); }
#line 8746 "configparser.cc"
    break;

  case 865: /* enterpathop: ENTERPATHOP_WCP LEFTBRACE_WCP $@379 enterpathopbody RIGHTBRACE_WCP  */
#line 1380 "configparser.yy"
                                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8752 "configparser.cc"
    break;

  case 866: /* $@380: %empty  */
#line 1382 "configparser.yy"
                                       { std::dynamic_pointer_cast<EnterPathOp>(lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_LEFTSIDE ); }
#line 8758 "configparser.cc"
    break;

  case 868: /* $@381: %empty  */
#line 1383 "configparser.yy"
                                        { std::dynamic_pointer_cast<EnterPathOp>(lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_RIGHTSIDE ); }
#line 8764 "configparser.cc"
    break;

  case 870: /* $@382: %empty  */
#line 1384 "configparser.yy"
                                          { std::dynamic_pointer_cast<EnterPathOp>(lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_THISSIDE ); }
#line 8770 "configparser.cc"
    break;

  case 872: /* $@383: %empty  */
#line 1385 "configparser.yy"
                                        { std::dynamic_pointer_cast<EnterPathOp>(lconfig_fp1)->setSide( EnterPathOp::ENTERPATHOP_OTHERSIDE ); }
#line 8776 "configparser.cc"
    break;

  case 875: /* $@384: %empty  */
#line 1388 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<ScrollListerOp>(); }
#line 8782 "configparser.cc"
    break;

  case 876: /* scrolllisterop: SCROLLLISTEROP_WCP LEFTBRACE_WCP $@384 scrolllisteropbody RIGHTBRACE_WCP  */
#line 1389 "configparser.yy"
                                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8788 "configparser.cc"
    break;

  case 877: /* $@385: %empty  */
#line 1391 "configparser.yy"
                                          { std::dynamic_pointer_cast<ScrollListerOp>(lconfig_fp1)->setDir( ScrollListerOp::SCROLLLISTEROP_LEFT ); }
#line 8794 "configparser.cc"
    break;

  case 879: /* $@386: %empty  */
#line 1392 "configparser.yy"
                                           { std::dynamic_pointer_cast<ScrollListerOp>(lconfig_fp1)->setDir( ScrollListerOp::SCROLLLISTEROP_RIGHT ); }
#line 8800 "configparser.cc"
    break;

  case 882: /* $@387: %empty  */
#line 1395 "configparser.yy"
                                                  { lconfig_fp1 = std::make_shared<CreateSymlinkOp>(); }
#line 8806 "configparser.cc"
    break;

  case 883: /* createsymlinkop: CREATESYMLINKOP_WCP LEFTBRACE_WCP $@387 createsymlinkopbody RIGHTBRACE_WCP  */
#line 1396 "configparser.yy"
                                                   { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8812 "configparser.cc"
    break;

  case 884: /* $@388: %empty  */
#line 1398 "configparser.yy"
                                          { std::dynamic_pointer_cast<CreateSymlinkOp>(lconfig_fp1)->setSameDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8818 "configparser.cc"
    break;

  case 886: /* $@389: %empty  */
#line 1399 "configparser.yy"
                                           { std::dynamic_pointer_cast<CreateSymlinkOp>(lconfig_fp1)->setLocal( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8824 "configparser.cc"
    break;

  case 888: /* $@390: %empty  */
#line 1400 "configparser.yy"
                                               { std::dynamic_pointer_cast<CreateSymlinkOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8830 "configparser.cc"
    break;

  case 891: /* $@391: %empty  */
#line 1403 "configparser.yy"
                                                  { lconfig_fp1 = std::make_shared<ChangeSymlinkOp>(); }
#line 8836 "configparser.cc"
    break;

  case 892: /* changesymlinkop: CHANGESYMLINKOP_WCP LEFTBRACE_WCP $@391 changesymlinkopbody RIGHTBRACE_WCP  */
#line 1404 "configparser.yy"
                                                   { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8842 "configparser.cc"
    break;

  case 893: /* $@392: %empty  */
#line 1406 "configparser.yy"
                                           { std::dynamic_pointer_cast<ChangeSymlinkOp>(lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_EDIT ); }
#line 8848 "configparser.cc"
    break;

  case 895: /* $@393: %empty  */
#line 1407 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeSymlinkOp>(lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_MAKE_ABSOLUTE ); }
#line 8854 "configparser.cc"
    break;

  case 897: /* $@394: %empty  */
#line 1408 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ChangeSymlinkOp>(lconfig_fp1)->setMode( ChangeSymlinkOp::CHANGESYMLINK_MAKE_RELATIVE ); }
#line 8860 "configparser.cc"
    break;

  case 900: /* $@395: %empty  */
#line 1411 "configparser.yy"
                                  { lconfig_fp1 = std::make_shared<ChModOp>(); }
#line 8866 "configparser.cc"
    break;

  case 901: /* chmodop: CHMODOP_WCP LEFTBRACE_WCP $@395 chmodopbody RIGHTBRACE_WCP  */
#line 1412 "configparser.yy"
                                   { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8872 "configparser.cc"
    break;

  case 902: /* $@396: %empty  */
#line 1414 "configparser.yy"
                                  { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setOnFiles( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8878 "configparser.cc"
    break;

  case 904: /* $@397: %empty  */
#line 1415 "configparser.yy"
                                 { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setOnDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8884 "configparser.cc"
    break;

  case 906: /* $@398: %empty  */
#line 1416 "configparser.yy"
                                    { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8890 "configparser.cc"
    break;

  case 908: /* $@399: %empty  */
#line 1417 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8896 "configparser.cc"
    break;

  case 910: /* $@400: %empty  */
#line 1418 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setApplyMode( CHMOD_ASK_PERMISSIONS ); }
#line 8902 "configparser.cc"
    break;

  case 912: /* $@401: %empty  */
#line 1419 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setApplyMode( CHMOD_SET_PERMISSIONS ); }
#line 8908 "configparser.cc"
    break;

  case 914: /* $@402: %empty  */
#line 1420 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setApplyMode( CHMOD_ADD_PERMISSIONS ); }
#line 8914 "configparser.cc"
    break;

  case 916: /* $@403: %empty  */
#line 1421 "configparser.yy"
                                          { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setApplyMode( CHMOD_REMOVE_PERMISSIONS ); }
#line 8920 "configparser.cc"
    break;

  case 918: /* $@404: %empty  */
#line 1422 "configparser.yy"
                                           { std::dynamic_pointer_cast<ChModOp>(lconfig_fp1)->setPermissions( (yyvsp[-1].strptr) ); }
#line 8926 "configparser.cc"
    break;

  case 921: /* $@405: %empty  */
#line 1425 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<ChTimeOp>(); }
#line 8932 "configparser.cc"
    break;

  case 922: /* chtimeop: CHTIMEOP_WCP LEFTBRACE_WCP $@405 chtimeopbody RIGHTBRACE_WCP  */
#line 1426 "configparser.yy"
                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8938 "configparser.cc"
    break;

  case 923: /* $@406: %empty  */
#line 1428 "configparser.yy"
                                   { std::dynamic_pointer_cast<ChTimeOp>(lconfig_fp1)->setOnFiles( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8944 "configparser.cc"
    break;

  case 925: /* $@407: %empty  */
#line 1429 "configparser.yy"
                                  { std::dynamic_pointer_cast<ChTimeOp>(lconfig_fp1)->setOnDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8950 "configparser.cc"
    break;

  case 927: /* $@408: %empty  */
#line 1430 "configparser.yy"
                                     { std::dynamic_pointer_cast<ChTimeOp>(lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8956 "configparser.cc"
    break;

  case 929: /* $@409: %empty  */
#line 1431 "configparser.yy"
                                        { std::dynamic_pointer_cast<ChTimeOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 8962 "configparser.cc"
    break;

  case 932: /* $@410: %empty  */
#line 1434 "configparser.yy"
                                                        { lconfig_fp1 = std::make_shared<ToggleListermodeOp>(); }
#line 8968 "configparser.cc"
    break;

  case 933: /* togglelistermodeop: TOGGLELISTERMODEOP_WCP LEFTBRACE_WCP $@410 togglelistermodeopbody RIGHTBRACE_WCP  */
#line 1435 "configparser.yy"
                                                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8974 "configparser.cc"
    break;

  case 934: /* $@411: %empty  */
#line 1437 "configparser.yy"
                                                { std::dynamic_pointer_cast<ToggleListermodeOp>(lconfig_fp1)->setMode( Worker::getID4Name( (yyvsp[-1].strptr) ) ); }
#line 8980 "configparser.cc"
    break;

  case 937: /* $@412: %empty  */
#line 1440 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<SetSortmodeOp>();
                                        std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setMode( 0 );
                                      }
#line 8988 "configparser.cc"
    break;

  case 938: /* setsortmodeop: SETSORTMODEOP_WCP LEFTBRACE_WCP $@412 setsortmodeopbody RIGHTBRACE_WCP  */
#line 1443 "configparser.yy"
                                               { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 8994 "configparser.cc"
    break;

  case 939: /* $@413: %empty  */
#line 1445 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_NAME ); }
#line 9000 "configparser.cc"
    break;

  case 941: /* $@414: %empty  */
#line 1446 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_SIZE ); }
#line 9006 "configparser.cc"
    break;

  case 943: /* $@415: %empty  */
#line 1447 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_ACCTIME ); }
#line 9012 "configparser.cc"
    break;

  case 945: /* $@416: %empty  */
#line 1448 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_MODTIME ); }
#line 9018 "configparser.cc"
    break;

  case 947: /* $@417: %empty  */
#line 1449 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_CHGTIME ); }
#line 9024 "configparser.cc"
    break;

  case 949: /* $@418: %empty  */
#line 1450 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_TYPE ); }
#line 9030 "configparser.cc"
    break;

  case 951: /* $@419: %empty  */
#line 1451 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_OWNER ); }
#line 9036 "configparser.cc"
    break;

  case 953: /* $@420: %empty  */
#line 1452 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_INODE ); }
#line 9042 "configparser.cc"
    break;

  case 955: /* $@421: %empty  */
#line 1453 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_NLINK ); }
#line 9048 "configparser.cc"
    break;

  case 957: /* $@422: %empty  */
#line 1454 "configparser.yy"
                                                 { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_PERMISSION ); }
#line 9054 "configparser.cc"
    break;

  case 959: /* $@423: %empty  */
#line 1455 "configparser.yy"
                                                { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortFlag( SORT_REVERSE ); }
#line 9060 "configparser.cc"
    break;

  case 961: /* $@424: %empty  */
#line 1456 "configparser.yy"
                                                { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortFlag( SORT_DIRLAST ); }
#line 9066 "configparser.cc"
    break;

  case 963: /* $@425: %empty  */
#line 1457 "configparser.yy"
                                                 { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortFlag( SORT_DIRMIXED ); }
#line 9072 "configparser.cc"
    break;

  case 965: /* $@426: %empty  */
#line 1458 "configparser.yy"
                                                { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_EXTENSION ); }
#line 9078 "configparser.cc"
    break;

  case 967: /* $@427: %empty  */
#line 1459 "configparser.yy"
                                                      { std::dynamic_pointer_cast<SetSortmodeOp>(lconfig_fp1)->setSortby( SORT_CUSTOM_ATTRIBUTE ); }
#line 9084 "configparser.cc"
    break;

  case 970: /* $@428: %empty  */
#line 1462 "configparser.yy"
                                          { lconfig_fp1 = std::make_shared<SetFilterOp>(); }
#line 9090 "configparser.cc"
    break;

  case 971: /* setfilterop: SETFILTEROP_WCP LEFTBRACE_WCP $@428 setfilteropbody RIGHTBRACE_WCP  */
#line 1463 "configparser.yy"
                                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9096 "configparser.cc"
    break;

  case 972: /* $@429: %empty  */
#line 1465 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9102 "configparser.cc"
    break;

  case 974: /* $@430: %empty  */
#line 1466 "configparser.yy"
                                          { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setFiltermode( SetFilterOp::EXCLUDE_FILTER ); }
#line 9108 "configparser.cc"
    break;

  case 976: /* $@431: %empty  */
#line 1467 "configparser.yy"
                                          { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setFiltermode( SetFilterOp::INCLUDE_FILTER ); }
#line 9114 "configparser.cc"
    break;

  case 978: /* $@432: %empty  */
#line 1468 "configparser.yy"
                                        { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setFiltermode( SetFilterOp::UNSET_FILTER ); }
#line 9120 "configparser.cc"
    break;

  case 980: /* $@433: %empty  */
#line 1469 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setFiltermode( SetFilterOp::UNSET_ALL ); }
#line 9126 "configparser.cc"
    break;

  case 982: /* $@434: %empty  */
#line 1470 "configparser.yy"
                                           { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setFilter( (yyvsp[-1].strptr) ); }
#line 9132 "configparser.cc"
    break;

  case 984: /* $@435: %empty  */
#line 1471 "configparser.yy"
                                                  { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setBookmarkLabel( (yyvsp[-1].strptr) ); }
#line 9138 "configparser.cc"
    break;

  case 986: /* $@436: %empty  */
#line 1472 "configparser.yy"
                                                              { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ONLY_BOOKMARKS ); }
#line 9144 "configparser.cc"
    break;

  case 988: /* $@437: %empty  */
#line 1473 "configparser.yy"
                                                          { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ONLY_LABEL ); }
#line 9150 "configparser.cc"
    break;

  case 990: /* $@438: %empty  */
#line 1474 "configparser.yy"
                                                    { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setBookmarkFilter( DirFilterSettings::SHOW_ALL ); }
#line 9156 "configparser.cc"
    break;

  case 992: /* $@439: %empty  */
#line 1475 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setOptionMode( SetFilterOp::SET_OPTION ); }
#line 9162 "configparser.cc"
    break;

  case 994: /* $@440: %empty  */
#line 1476 "configparser.yy"
                                               { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setOptionMode( SetFilterOp::INVERT_OPTION ); }
#line 9168 "configparser.cc"
    break;

  case 996: /* $@441: %empty  */
#line 1477 "configparser.yy"
                                            { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setChangeFilters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9174 "configparser.cc"
    break;

  case 998: /* $@442: %empty  */
#line 1478 "configparser.yy"
                                              { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setChangeBookmarks( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9180 "configparser.cc"
    break;

  case 1000: /* $@443: %empty  */
#line 1479 "configparser.yy"
                                         { std::dynamic_pointer_cast<SetFilterOp>(lconfig_fp1)->setQueryLabel( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9186 "configparser.cc"
    break;

  case 1003: /* $@444: %empty  */
#line 1482 "configparser.yy"
                                                        { lconfig_fp1 = std::make_shared<ShortkeyFromListOp>(); }
#line 9192 "configparser.cc"
    break;

  case 1004: /* shortkeyfromlistop: SHORTKEYFROMLISTOP_WCP LEFTBRACE_WCP $@444 RIGHTBRACE_WCP  */
#line 1483 "configparser.yy"
                                  { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9198 "configparser.cc"
    break;

  case 1005: /* $@445: %empty  */
#line 1485 "configparser.yy"
                                  { lconfig_fp1 = std::make_shared<ChOwnOp>(); }
#line 9204 "configparser.cc"
    break;

  case 1006: /* chownop: CHOWNOP_WCP LEFTBRACE_WCP $@445 chownopbody RIGHTBRACE_WCP  */
#line 1486 "configparser.yy"
                                   { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9210 "configparser.cc"
    break;

  case 1007: /* $@446: %empty  */
#line 1488 "configparser.yy"
                                  { std::dynamic_pointer_cast<ChOwnOp>(lconfig_fp1)->setOnFiles( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9216 "configparser.cc"
    break;

  case 1009: /* $@447: %empty  */
#line 1489 "configparser.yy"
                                 { std::dynamic_pointer_cast<ChOwnOp>(lconfig_fp1)->setOnDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9222 "configparser.cc"
    break;

  case 1011: /* $@448: %empty  */
#line 1490 "configparser.yy"
                                    { std::dynamic_pointer_cast<ChOwnOp>(lconfig_fp1)->setRecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9228 "configparser.cc"
    break;

  case 1013: /* $@449: %empty  */
#line 1491 "configparser.yy"
                                       { std::dynamic_pointer_cast<ChOwnOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9234 "configparser.cc"
    break;

  case 1016: /* $@450: %empty  */
#line 1494 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<ScriptOp>(); }
#line 9240 "configparser.cc"
    break;

  case 1017: /* scriptop: SCRIPTOP_WCP LEFTBRACE_WCP $@450 scriptopbody RIGHTBRACE_WCP  */
#line 1495 "configparser.yy"
                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9246 "configparser.cc"
    break;

  case 1018: /* $@451: %empty  */
#line 1497 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_NOP ); }
#line 9252 "configparser.cc"
    break;

  case 1020: /* $@452: %empty  */
#line 1498 "configparser.yy"
                                    { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_PUSH ); }
#line 9258 "configparser.cc"
    break;

  case 1022: /* $@453: %empty  */
#line 1499 "configparser.yy"
                                     { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_LABEL ); }
#line 9264 "configparser.cc"
    break;

  case 1024: /* $@454: %empty  */
#line 1500 "configparser.yy"
                                  { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_IF ); }
#line 9270 "configparser.cc"
    break;

  case 1026: /* $@455: %empty  */
#line 1501 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_END ); }
#line 9276 "configparser.cc"
    break;

  case 1028: /* $@456: %empty  */
#line 1502 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_POP ); }
#line 9282 "configparser.cc"
    break;

  case 1030: /* $@457: %empty  */
#line 1503 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_SETTINGS ); }
#line 9288 "configparser.cc"
    break;

  case 1032: /* $@458: %empty  */
#line 1504 "configparser.yy"
                                      { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_WINDOW ); }
#line 9294 "configparser.cc"
    break;

  case 1034: /* $@459: %empty  */
#line 1505 "configparser.yy"
                                    { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_GOTO ); }
#line 9300 "configparser.cc"
    break;

  case 1036: /* $@460: %empty  */
#line 1506 "configparser.yy"
                                           { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setType( ScriptOp::SCRIPT_EVALCOMMAND ); }
#line 9306 "configparser.cc"
    break;

  case 1038: /* $@461: %empty  */
#line 1507 "configparser.yy"
                                         { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setPushUseOutput( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9312 "configparser.cc"
    break;

  case 1040: /* $@462: %empty  */
#line 1508 "configparser.yy"
                                                { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setPushOutputReturnCode( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9318 "configparser.cc"
    break;

  case 1042: /* $@463: %empty  */
#line 1509 "configparser.yy"
                                   { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setDoDebug( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9324 "configparser.cc"
    break;

  case 1044: /* $@464: %empty  */
#line 1510 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWPURecursive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9330 "configparser.cc"
    break;

  case 1046: /* $@465: %empty  */
#line 1511 "configparser.yy"
                                       { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWPUTakeDirs( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9336 "configparser.cc"
    break;

  case 1048: /* $@466: %empty  */
#line 1512 "configparser.yy"
                                      { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setStackNr( (yyvsp[-1].num) ); }
#line 9342 "configparser.cc"
    break;

  case 1050: /* $@467: %empty  */
#line 1513 "configparser.yy"
                                       { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setLabel( (yyvsp[-1].strptr) ); }
#line 9348 "configparser.cc"
    break;

  case 1052: /* $@468: %empty  */
#line 1514 "configparser.yy"
                                            { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setPushString( (yyvsp[-1].strptr) ); }
#line 9354 "configparser.cc"
    break;

  case 1054: /* $@469: %empty  */
#line 1515 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setIfTest( (yyvsp[-1].strptr) ); }
#line 9360 "configparser.cc"
    break;

  case 1056: /* $@470: %empty  */
#line 1516 "configparser.yy"
                                         { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setIfLabel( (yyvsp[-1].strptr) ); }
#line 9366 "configparser.cc"
    break;

  case 1058: /* $@471: %empty  */
#line 1517 "configparser.yy"
                                       { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_OPEN ); }
#line 9372 "configparser.cc"
    break;

  case 1060: /* $@472: %empty  */
#line 1518 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_CLOSE ); }
#line 9378 "configparser.cc"
    break;

  case 1062: /* $@473: %empty  */
#line 1519 "configparser.yy"
                                        { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWinType( ScriptOp::SCRIPT_WINDOW_LEAVE ); }
#line 9384 "configparser.cc"
    break;

  case 1064: /* $@474: %empty  */
#line 1520 "configparser.yy"
                                          { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setChangeProgress( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9390 "configparser.cc"
    break;

  case 1066: /* $@475: %empty  */
#line 1521 "configparser.yy"
                                      { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setChangeText( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9396 "configparser.cc"
    break;

  case 1068: /* $@476: %empty  */
#line 1522 "configparser.yy"
                                             { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setProgressUseOutput( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9402 "configparser.cc"
    break;

  case 1070: /* $@477: %empty  */
#line 1523 "configparser.yy"
                                            { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWinTextUseOutput( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9408 "configparser.cc"
    break;

  case 1072: /* $@478: %empty  */
#line 1524 "configparser.yy"
                                          { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setProgress( (yyvsp[-1].strptr) ); }
#line 9414 "configparser.cc"
    break;

  case 1074: /* $@479: %empty  */
#line 1525 "configparser.yy"
                                         { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setWinText( (yyvsp[-1].strptr) ); }
#line 9420 "configparser.cc"
    break;

  case 1076: /* $@480: %empty  */
#line 1526 "configparser.yy"
                                               { std::dynamic_pointer_cast<ScriptOp>(lconfig_fp1)->setCommandStr( (yyvsp[-1].strptr) ); }
#line 9426 "configparser.cc"
    break;

  case 1079: /* $@481: %empty  */
#line 1529 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<ShowDirCacheOp>(); }
#line 9432 "configparser.cc"
    break;

  case 1080: /* showdircacheop: SHOWDIRCACHEOP_WCP LEFTBRACE_WCP $@481 RIGHTBRACE_WCP  */
#line 1530 "configparser.yy"
                              { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9438 "configparser.cc"
    break;

  case 1081: /* $@482: %empty  */
#line 1532 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<ParentActionOp>(); }
#line 9444 "configparser.cc"
    break;

  case 1082: /* parentactionop: PARENTACTIONOP_WCP LEFTBRACE_WCP $@482 RIGHTBRACE_WCP  */
#line 1533 "configparser.yy"
                              { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9450 "configparser.cc"
    break;

  case 1083: /* $@483: %empty  */
#line 1535 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<NoOperationOp>(); }
#line 9456 "configparser.cc"
    break;

  case 1084: /* nooperationop: NOOPERATIONOP_WCP LEFTBRACE_WCP $@483 RIGHTBRACE_WCP  */
#line 1536 "configparser.yy"
                             { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9462 "configparser.cc"
    break;

  case 1085: /* $@484: %empty  */
#line 1538 "configparser.yy"
                                  { lconfig_fp1 = std::make_shared<GoFTPOp>(); }
#line 9468 "configparser.cc"
    break;

  case 1086: /* goftpop: GOFTPOP_WCP LEFTBRACE_WCP $@484 goftpopbody RIGHTBRACE_WCP  */
#line 1539 "configparser.yy"
                                   { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9474 "configparser.cc"
    break;

  case 1087: /* $@485: %empty  */
#line 1541 "configparser.yy"
                                       { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9480 "configparser.cc"
    break;

  case 1089: /* $@486: %empty  */
#line 1542 "configparser.yy"
                                         { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setHost( (yyvsp[-1].strptr) ); }
#line 9486 "configparser.cc"
    break;

  case 1091: /* $@487: %empty  */
#line 1543 "configparser.yy"
                                         { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setUser( (yyvsp[-1].strptr) ); }
#line 9492 "configparser.cc"
    break;

  case 1093: /* $@488: %empty  */
#line 1544 "configparser.yy"
                                         { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setPass( (yyvsp[-1].strptr) ); }
#line 9498 "configparser.cc"
    break;

  case 1095: /* $@489: %empty  */
#line 1545 "configparser.yy"
                                       { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setDontEnterFTP( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9504 "configparser.cc"
    break;

  case 1097: /* $@490: %empty  */
#line 1546 "configparser.yy"
                                        { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setAlwaysStorePW( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9510 "configparser.cc"
    break;

  case 1099: /* $@491: %empty  */
#line 1547 "configparser.yy"
                                           { std::dynamic_pointer_cast<GoFTPOp>(lconfig_fp1)->setAVFSModule( (yyvsp[-1].strptr) ); }
#line 9516 "configparser.cc"
    break;

  case 1102: /* $@492: %empty  */
#line 1550 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<InternalViewOp>(); }
#line 9522 "configparser.cc"
    break;

  case 1103: /* internalviewop: INTERNALVIEWOP_WCP LEFTBRACE_WCP $@492 internalviewopbody RIGHTBRACE_WCP  */
#line 1551 "configparser.yy"
                                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9528 "configparser.cc"
    break;

  case 1104: /* $@493: %empty  */
#line 1553 "configparser.yy"
                                              { std::dynamic_pointer_cast<InternalViewOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9534 "configparser.cc"
    break;

  case 1106: /* $@494: %empty  */
#line 1554 "configparser.yy"
                                            { std::dynamic_pointer_cast<InternalViewOp>(lconfig_fp1)->setCustomFiles( (yyvsp[-1].strptr) ); }
#line 9540 "configparser.cc"
    break;

  case 1108: /* $@495: %empty  */
#line 1555 "configparser.yy"
                                         { std::dynamic_pointer_cast<InternalViewOp>(lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_ACTIVE_FILE ); }
#line 9546 "configparser.cc"
    break;

  case 1110: /* $@496: %empty  */
#line 1556 "configparser.yy"
                                         { std::dynamic_pointer_cast<InternalViewOp>(lconfig_fp1)->setShowFileMode( InternalViewOp::SHOW_CUSTOM_FILES ); }
#line 9552 "configparser.cc"
    break;

  case 1113: /* $@497: %empty  */
#line 1560 "configparser.yy"
                                          { lconfig_fp1 = std::make_shared<ClipboardOp>(); }
#line 9558 "configparser.cc"
    break;

  case 1114: /* clipboardop: CLIPBOARDOP_WCP LEFTBRACE_WCP $@497 clipboardopbody RIGHTBRACE_WCP  */
#line 1561 "configparser.yy"
                                           { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9564 "configparser.cc"
    break;

  case 1115: /* $@498: %empty  */
#line 1563 "configparser.yy"
                                                    { std::dynamic_pointer_cast<ClipboardOp>(lconfig_fp1)->setClipboardString( (yyvsp[-1].strptr) ); }
#line 9570 "configparser.cc"
    break;

  case 1118: /* $@499: %empty  */
#line 1566 "configparser.yy"
                                    { lconfig_fp1 = std::make_shared<SearchOp>(); }
#line 9576 "configparser.cc"
    break;

  case 1119: /* searchop: SEARCHOP_WCP LEFTBRACE_WCP $@499 searchopbody RIGHTBRACE_WCP  */
#line 1567 "configparser.yy"
                                     { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9582 "configparser.cc"
    break;

  case 1120: /* $@500: %empty  */
#line 1569 "configparser.yy"
                                             { std::dynamic_pointer_cast<SearchOp>(lconfig_fp1)->setEditCommand( (yyvsp[-1].strptr) ); }
#line 9588 "configparser.cc"
    break;

  case 1122: /* $@501: %empty  */
#line 1570 "configparser.yy"
                                           { std::dynamic_pointer_cast<SearchOp>(lconfig_fp1)->setShowPrevResults( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9594 "configparser.cc"
    break;

  case 1125: /* $@502: %empty  */
#line 1573 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<DirBookmarkOp>(); }
#line 9600 "configparser.cc"
    break;

  case 1126: /* dirbookmarkop: DIRBOOKMARKOP_WCP LEFTBRACE_WCP $@502 RIGHTBRACE_WCP  */
#line 1574 "configparser.yy"
                             { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9606 "configparser.cc"
    break;

  case 1127: /* $@503: %empty  */
#line 1576 "configparser.yy"
                                                      { lconfig_fp1 = std::make_shared<OpenContextMenuOp>(); }
#line 9612 "configparser.cc"
    break;

  case 1128: /* opencontextmenuop: OPENCONTEXTMENUOP_WCP LEFTBRACE_WCP $@503 opencontextmenuopbody RIGHTBRACE_WCP  */
#line 1577 "configparser.yy"
                                                       { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9618 "configparser.cc"
    break;

  case 1129: /* $@504: %empty  */
#line 1579 "configparser.yy"
                                                          { std::dynamic_pointer_cast<OpenContextMenuOp>(lconfig_fp1)->setHighlightUserAction( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9624 "configparser.cc"
    break;

  case 1132: /* $@505: %empty  */
#line 1582 "configparser.yy"
                                                  { lconfig_fp1 = std::make_shared<RunCustomAction>(); }
#line 9630 "configparser.cc"
    break;

  case 1133: /* runcustomaction: RUNCUSTOMACTION_WCP LEFTBRACE_WCP $@505 runcustomactionbody RIGHTBRACE_WCP  */
#line 1583 "configparser.yy"
                                                   { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9636 "configparser.cc"
    break;

  case 1134: /* $@506: %empty  */
#line 1585 "configparser.yy"
                                                   { std::dynamic_pointer_cast<RunCustomAction>(lconfig_fp1)->setCustomName( (yyvsp[-1].strptr) ); }
#line 9642 "configparser.cc"
    break;

  case 1137: /* $@507: %empty  */
#line 1588 "configparser.yy"
                                                    { lconfig_fp1 = std::make_shared<OpenWorkerMenuOp>(); }
#line 9648 "configparser.cc"
    break;

  case 1138: /* openworkermenuop: OPENWORKERMENUOP_WCP LEFTBRACE_WCP $@507 RIGHTBRACE_WCP  */
#line 1589 "configparser.yy"
                                { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9654 "configparser.cc"
    break;

  case 1139: /* $@508: %empty  */
#line 1591 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<ChangeLabelOp>(); }
#line 9660 "configparser.cc"
    break;

  case 1140: /* changelabelop: CHANGELABELOP_WCP LEFTBRACE_WCP $@508 changelabelopbody RIGHTBRACE_WCP  */
#line 1592 "configparser.yy"
                                               { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9666 "configparser.cc"
    break;

  case 1141: /* $@509: %empty  */
#line 1594 "configparser.yy"
                                            { std::dynamic_pointer_cast<ChangeLabelOp>(lconfig_fp1)->setAskForLabel( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9672 "configparser.cc"
    break;

  case 1143: /* $@510: %empty  */
#line 1595 "configparser.yy"
                                            { std::dynamic_pointer_cast<ChangeLabelOp>(lconfig_fp1)->setLabel( (yyvsp[-1].strptr) ); }
#line 9678 "configparser.cc"
    break;

  case 1146: /* $@511: %empty  */
#line 1598 "configparser.yy"
                                            { lconfig_fp1 = std::make_shared<ModifyTabsOp>(); }
#line 9684 "configparser.cc"
    break;

  case 1147: /* modifytabsop: MODIFYTABSOP_WCP LEFTBRACE_WCP $@511 modifytabsopbody RIGHTBRACE_WCP  */
#line 1599 "configparser.yy"
                                             { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9690 "configparser.cc"
    break;

  case 1148: /* $@512: %empty  */
#line 1601 "configparser.yy"
                                               { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::NEW_TAB ); }
#line 9696 "configparser.cc"
    break;

  case 1150: /* $@513: %empty  */
#line 1602 "configparser.yy"
                                                        { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::CLOSE_CURRENT_TAB ); }
#line 9702 "configparser.cc"
    break;

  case 1152: /* $@514: %empty  */
#line 1603 "configparser.yy"
                                                { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::NEXT_TAB ); }
#line 9708 "configparser.cc"
    break;

  case 1154: /* $@515: %empty  */
#line 1604 "configparser.yy"
                                                { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::PREV_TAB ); }
#line 9714 "configparser.cc"
    break;

  case 1156: /* $@516: %empty  */
#line 1605 "configparser.yy"
                                                        { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::TOGGLE_LOCK_STATE ); }
#line 9720 "configparser.cc"
    break;

  case 1158: /* $@517: %empty  */
#line 1606 "configparser.yy"
                                                    { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::MOVE_TAB_LEFT ); }
#line 9726 "configparser.cc"
    break;

  case 1160: /* $@518: %empty  */
#line 1607 "configparser.yy"
                                                     { std::dynamic_pointer_cast<ModifyTabsOp>(lconfig_fp1)->setTabAction( ModifyTabsOp::MOVE_TAB_RIGHT ); }
#line 9732 "configparser.cc"
    break;

  case 1163: /* $@519: %empty  */
#line 1610 "configparser.yy"
                                                {
                 lconfig_fp1 = std::make_shared<ChangeLayoutOp>();
                 layout_sets = LayoutSettings();
             }
#line 9741 "configparser.cc"
    break;

  case 1164: /* changelayoutop: CHANGELAYOUTOP_WCP LEFTBRACE_WCP $@519 changelayoutopbody RIGHTBRACE_WCP  */
#line 1613 "configparser.yy"
                                                 {
                 std::dynamic_pointer_cast<ChangeLayoutOp>(lconfig_fp1)->setLayout( layout_sets );
                 lconfig_listcom.push_back( lconfig_fp1 );
             }
#line 9750 "configparser.cc"
    break;

  case 1165: /* $@520: %empty  */
#line 1618 "configparser.yy"
                                   { layout_sets.pushBackOrder( LayoutSettings::LO_STATEBAR ); }
#line 9756 "configparser.cc"
    break;

  case 1167: /* $@521: %empty  */
#line 1619 "configparser.yy"
                                   { layout_sets.pushBackOrder( LayoutSettings::LO_CLOCKBAR ); }
#line 9762 "configparser.cc"
    break;

  case 1169: /* $@522: %empty  */
#line 1620 "configparser.yy"
                                  { layout_sets.pushBackOrder( LayoutSettings::LO_BUTTONS ); }
#line 9768 "configparser.cc"
    break;

  case 1171: /* $@523: %empty  */
#line 1621 "configparser.yy"
                                    { layout_sets.pushBackOrder( LayoutSettings::LO_LISTVIEWS ); }
#line 9774 "configparser.cc"
    break;

  case 1173: /* $@524: %empty  */
#line 1622 "configparser.yy"
                              { layout_sets.pushBackOrder( LayoutSettings::LO_BLL ); }
#line 9780 "configparser.cc"
    break;

  case 1175: /* $@525: %empty  */
#line 1623 "configparser.yy"
                              { layout_sets.pushBackOrder( LayoutSettings::LO_LBL ); }
#line 9786 "configparser.cc"
    break;

  case 1177: /* $@526: %empty  */
#line 1624 "configparser.yy"
                              { layout_sets.pushBackOrder( LayoutSettings::LO_LLB ); }
#line 9792 "configparser.cc"
    break;

  case 1179: /* $@527: %empty  */
#line 1625 "configparser.yy"
                             { layout_sets.pushBackOrder( LayoutSettings::LO_BL ); }
#line 9798 "configparser.cc"
    break;

  case 1181: /* $@528: %empty  */
#line 1626 "configparser.yy"
                             { layout_sets.pushBackOrder( LayoutSettings::LO_LB ); }
#line 9804 "configparser.cc"
    break;

  case 1183: /* $@529: %empty  */
#line 1627 "configparser.yy"
                                            { layout_sets.setButtonVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9810 "configparser.cc"
    break;

  case 1185: /* $@530: %empty  */
#line 1628 "configparser.yy"
                                              { layout_sets.setListViewVert( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9816 "configparser.cc"
    break;

  case 1187: /* $@531: %empty  */
#line 1629 "configparser.yy"
                                                  { layout_sets.setListViewWeight( (yyvsp[-1].num) ); }
#line 9822 "configparser.cc"
    break;

  case 1189: /* $@532: %empty  */
#line 1630 "configparser.yy"
                                               { layout_sets.setWeightRelToActive( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 9828 "configparser.cc"
    break;

  case 1192: /* $@533: %empty  */
#line 1633 "configparser.yy"
                                                  {
                  lconfig_fp1 = std::make_shared<ChangeColumnsOp>();
                  columns.clear();
                }
#line 9837 "configparser.cc"
    break;

  case 1193: /* changecolumnsop: CHANGECOLUMNSOP_WCP LEFTBRACE_WCP $@533 changecolumnsopbody RIGHTBRACE_WCP  */
#line 1636 "configparser.yy"
                                                     {
                  std::dynamic_pointer_cast<ChangeColumnsOp>(lconfig_fp1)->setColumns( columns );
                  lconfig_listcom.push_back( lconfig_fp1 );
                }
#line 9846 "configparser.cc"
    break;

  case 1194: /* $@534: %empty  */
#line 1641 "configparser.yy"
                               { columns.push_back( WorkerTypes::LISTCOL_NAME ); }
#line 9852 "configparser.cc"
    break;

  case 1196: /* $@535: %empty  */
#line 1642 "configparser.yy"
                               { columns.push_back( WorkerTypes::LISTCOL_SIZE ); }
#line 9858 "configparser.cc"
    break;

  case 1198: /* $@536: %empty  */
#line 1643 "configparser.yy"
                               { columns.push_back( WorkerTypes::LISTCOL_TYPE ); }
#line 9864 "configparser.cc"
    break;

  case 1200: /* $@537: %empty  */
#line 1644 "configparser.yy"
                                     { columns.push_back( WorkerTypes::LISTCOL_PERM ); }
#line 9870 "configparser.cc"
    break;

  case 1202: /* $@538: %empty  */
#line 1645 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_OWNER ); }
#line 9876 "configparser.cc"
    break;

  case 1204: /* $@539: %empty  */
#line 1646 "configparser.yy"
                                      { columns.push_back( WorkerTypes::LISTCOL_DEST ); }
#line 9882 "configparser.cc"
    break;

  case 1206: /* $@540: %empty  */
#line 1647 "configparser.yy"
                                  { columns.push_back( WorkerTypes::LISTCOL_MOD ); }
#line 9888 "configparser.cc"
    break;

  case 1208: /* $@541: %empty  */
#line 1648 "configparser.yy"
                                  { columns.push_back( WorkerTypes::LISTCOL_ACC ); }
#line 9894 "configparser.cc"
    break;

  case 1210: /* $@542: %empty  */
#line 1649 "configparser.yy"
                                  { columns.push_back( WorkerTypes::LISTCOL_CHANGE ); }
#line 9900 "configparser.cc"
    break;

  case 1212: /* $@543: %empty  */
#line 1650 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_INODE ); }
#line 9906 "configparser.cc"
    break;

  case 1214: /* $@544: %empty  */
#line 1651 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_NLINK ); }
#line 9912 "configparser.cc"
    break;

  case 1216: /* $@545: %empty  */
#line 1652 "configparser.yy"
                                 { columns.push_back( WorkerTypes::LISTCOL_BLOCKS ); }
#line 9918 "configparser.cc"
    break;

  case 1218: /* $@546: %empty  */
#line 1653 "configparser.yy"
                                { columns.push_back( WorkerTypes::LISTCOL_SIZEH ); }
#line 9924 "configparser.cc"
    break;

  case 1220: /* $@547: %empty  */
#line 1654 "configparser.yy"
                                    { columns.push_back( WorkerTypes::LISTCOL_EXTENSION ); }
#line 9930 "configparser.cc"
    break;

  case 1222: /* $@548: %empty  */
#line 1655 "configparser.yy"
                                          { columns.push_back( WorkerTypes::LISTCOL_CUSTOM_ATTR ); }
#line 9936 "configparser.cc"
    break;

  case 1225: /* $@549: %empty  */
#line 1658 "configparser.yy"
                                                  { lconfig_fp1 = std::make_shared<VolumeManagerOp>(); }
#line 9942 "configparser.cc"
    break;

  case 1226: /* volumemanagerop: VOLUMEMANAGEROP_WCP LEFTBRACE_WCP $@549 RIGHTBRACE_WCP  */
#line 1659 "configparser.yy"
                               { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9948 "configparser.cc"
    break;

  case 1227: /* $@550: %empty  */
#line 1661 "configparser.yy"
                                                        { lconfig_fp1 = std::make_shared<SwitchButtonBankOp>(); }
#line 9954 "configparser.cc"
    break;

  case 1228: /* switchbuttonbankop: SWITCHBUTTONBANKOP_WCP LEFTBRACE_WCP $@550 switchbuttonbankopbody RIGHTBRACE_WCP  */
#line 1662 "configparser.yy"
                                                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9960 "configparser.cc"
    break;

  case 1229: /* $@551: %empty  */
#line 1664 "configparser.yy"
                                                          { std::dynamic_pointer_cast<SwitchButtonBankOp>(lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_NEXT_BANK ); }
#line 9966 "configparser.cc"
    break;

  case 1231: /* $@552: %empty  */
#line 1665 "configparser.yy"
                                                          { std::dynamic_pointer_cast<SwitchButtonBankOp>(lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_PREV_BANK ); }
#line 9972 "configparser.cc"
    break;

  case 1233: /* $@553: %empty  */
#line 1666 "configparser.yy"
                                                        { std::dynamic_pointer_cast<SwitchButtonBankOp>(lconfig_fp1)->setSwitchMode( SwitchButtonBankOp::SWITCH_TO_BANK_NR ); }
#line 9978 "configparser.cc"
    break;

  case 1235: /* $@554: %empty  */
#line 1667 "configparser.yy"
                                              { std::dynamic_pointer_cast<SwitchButtonBankOp>(lconfig_fp1)->setBankNr( (yyvsp[-1].num) ); }
#line 9984 "configparser.cc"
    break;

  case 1238: /* $@555: %empty  */
#line 1670 "configparser.yy"
                                        { lconfig_fp1 = std::make_shared<PathJumpOp>(); }
#line 9990 "configparser.cc"
    break;

  case 1239: /* pathjumpop: PATHJUMPOP_WCP LEFTBRACE_WCP $@555 pathjumpopbody RIGHTBRACE_WCP  */
#line 1671 "configparser.yy"
                                         { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 9996 "configparser.cc"
    break;

  case 1240: /* $@556: %empty  */
#line 1673 "configparser.yy"
                                                  { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_TIME ); }
#line 10002 "configparser.cc"
    break;

  case 1242: /* $@557: %empty  */
#line 1674 "configparser.yy"
                                                    { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_FILTER ); }
#line 10008 "configparser.cc"
    break;

  case 1244: /* $@558: %empty  */
#line 1675 "configparser.yy"
                                                     { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setInitialTab( PathJumpOp::SHOW_BY_PROGRAM ); }
#line 10014 "configparser.cc"
    break;

  case 1246: /* $@559: %empty  */
#line 1676 "configparser.yy"
                                                { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_ALL ); }
#line 10020 "configparser.cc"
    break;

  case 1248: /* $@560: %empty  */
#line 1677 "configparser.yy"
                                                    { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_SUBDIRS ); }
#line 10026 "configparser.cc"
    break;

  case 1250: /* $@561: %empty  */
#line 1678 "configparser.yy"
                                                          { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setDisplayMode( PathJumpOp::SHOW_DIRECT_SUBDIRS ); }
#line 10032 "configparser.cc"
    break;

  case 1252: /* $@562: %empty  */
#line 1679 "configparser.yy"
                                        { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setIncludeAllData( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10038 "configparser.cc"
    break;

  case 1254: /* $@563: %empty  */
#line 1680 "configparser.yy"
                                             { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setHideNonExisting( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10044 "configparser.cc"
    break;

  case 1256: /* $@564: %empty  */
#line 1681 "configparser.yy"
                                              { std::dynamic_pointer_cast<PathJumpOp>(lconfig_fp1)->setLockOnCurrentDir( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10050 "configparser.cc"
    break;

  case 1259: /* $@565: %empty  */
#line 1684 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<CommandMenuOp>(); }
#line 10056 "configparser.cc"
    break;

  case 1260: /* commandmenuop: COMMANDMENUOP_WCP LEFTBRACE_WCP $@565 commandmenuopbody RIGHTBRACE_WCP  */
#line 1685 "configparser.yy"
                                               { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10062 "configparser.cc"
    break;

  case 1261: /* $@566: %empty  */
#line 1687 "configparser.yy"
                                           { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setShowRecentlyUsed( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10068 "configparser.cc"
    break;

  case 1263: /* $@567: %empty  */
#line 1688 "configparser.yy"
                                               { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::MENUS ); }
#line 10074 "configparser.cc"
    break;

  case 1265: /* $@568: %empty  */
#line 1689 "configparser.yy"
                                                 { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::BUTTONS ); }
#line 10080 "configparser.cc"
    break;

  case 1267: /* $@569: %empty  */
#line 1690 "configparser.yy"
                                                 { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::HOTKEYS ); }
#line 10086 "configparser.cc"
    break;

  case 1269: /* $@570: %empty  */
#line 1691 "configparser.yy"
                                               { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::PATHS ); }
#line 10092 "configparser.cc"
    break;

  case 1271: /* $@571: %empty  */
#line 1692 "configparser.yy"
                                                 { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::LV_MODES ); }
#line 10098 "configparser.cc"
    break;

  case 1273: /* $@572: %empty  */
#line 1693 "configparser.yy"
                                                    { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::LV_COMMANDS ); }
#line 10104 "configparser.cc"
    break;

  case 1275: /* $@573: %empty  */
#line 1694 "configparser.yy"
                                                  { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::COMMANDS ); }
#line 10110 "configparser.cc"
    break;

  case 1277: /* $@574: %empty  */
#line 1695 "configparser.yy"
                                                  { std::dynamic_pointer_cast<CommandMenuOp>(lconfig_fp1)->setStartNode( WorkerTypes::TOP_LEVEL ); }
#line 10116 "configparser.cc"
    break;

  case 1280: /* $@575: %empty  */
#line 1698 "configparser.yy"
                                                      { lconfig_fp1 = std::make_shared<ViewNewestFilesOp>(); }
#line 10122 "configparser.cc"
    break;

  case 1281: /* viewnewestfilesop: VIEWNEWESTFILESOP_WCP LEFTBRACE_WCP $@575 RIGHTBRACE_WCP  */
#line 1699 "configparser.yy"
                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10128 "configparser.cc"
    break;

  case 1282: /* $@576: %empty  */
#line 1701 "configparser.yy"
                                            { lconfig_fp1 = std::make_shared<DirCompareOp>(); }
#line 10134 "configparser.cc"
    break;

  case 1283: /* dircompareop: DIRCOMPAREOP_WCP LEFTBRACE_WCP $@576 RIGHTBRACE_WCP  */
#line 1702 "configparser.yy"
                            { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10140 "configparser.cc"
    break;

  case 1284: /* $@577: %empty  */
#line 1704 "configparser.yy"
                                              { lconfig_fp1 = std::make_shared<TabProfilesOp>(); }
#line 10146 "configparser.cc"
    break;

  case 1285: /* tabprofilesop: TABPROFILESOP_WCP LEFTBRACE_WCP $@577 RIGHTBRACE_WCP  */
#line 1705 "configparser.yy"
                             { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10152 "configparser.cc"
    break;

  case 1286: /* $@578: %empty  */
#line 1707 "configparser.yy"
                                                { lconfig_fp1 = std::make_shared<ExternalVDirOp>(); }
#line 10158 "configparser.cc"
    break;

  case 1287: /* externalvdirop: EXTERNALVDIROP_WCP LEFTBRACE_WCP $@578 externalvdiropbody RIGHTBRACE_WCP  */
#line 1708 "configparser.yy"
                                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10164 "configparser.cc"
    break;

  case 1288: /* $@579: %empty  */
#line 1710 "configparser.yy"
                                                     { std::dynamic_pointer_cast<ExternalVDirOp>(lconfig_fp1)->setCommandString( (yyvsp[-1].strptr) ); }
#line 10170 "configparser.cc"
    break;

  case 1291: /* $@580: %empty  */
#line 1713 "configparser.yy"
                                              { auto tfp = std::make_shared< ScriptOp >();
                                                tfp->setType( ScriptOp::SCRIPT_EVALCOMMAND );
                                                tfp->setCommandStr( "open_current_tab_menu" );
                                                lconfig_fp1 = tfp;
                                              }
#line 10180 "configparser.cc"
    break;

  case 1292: /* opentabmenuop: OPENTABMENUOP_WCP LEFTBRACE_WCP $@580 RIGHTBRACE_WCP  */
#line 1718 "configparser.yy"
                             { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10186 "configparser.cc"
    break;

  case 1293: /* $@581: %empty  */
#line 1720 "configparser.yy"
                                                    { dir_preset = directory_presets(); }
#line 10192 "configparser.cc"
    break;

  case 1294: /* $@582: %empty  */
#line 1721 "configparser.yy"
                                                 { dir_presets[path] = dir_preset;
                                                 }
#line 10199 "configparser.cc"
    break;

  case 1297: /* $@583: %empty  */
#line 1725 "configparser.yy"
                                          { path  = (yyvsp[-1].strptr); }
#line 10205 "configparser.cc"
    break;

  case 1299: /* $@584: %empty  */
#line 1726 "configparser.yy"
                                          { dir_preset.sortmode = SORT_NAME; }
#line 10211 "configparser.cc"
    break;

  case 1301: /* $@585: %empty  */
#line 1727 "configparser.yy"
                                          { dir_preset.sortmode = SORT_SIZE; }
#line 10217 "configparser.cc"
    break;

  case 1303: /* $@586: %empty  */
#line 1728 "configparser.yy"
                                             { dir_preset.sortmode = SORT_ACCTIME; }
#line 10223 "configparser.cc"
    break;

  case 1305: /* $@587: %empty  */
#line 1729 "configparser.yy"
                                             { dir_preset.sortmode = SORT_MODTIME; }
#line 10229 "configparser.cc"
    break;

  case 1307: /* $@588: %empty  */
#line 1730 "configparser.yy"
                                             { dir_preset.sortmode = SORT_CHGTIME; }
#line 10235 "configparser.cc"
    break;

  case 1309: /* $@589: %empty  */
#line 1731 "configparser.yy"
                                          { dir_preset.sortmode = SORT_TYPE; }
#line 10241 "configparser.cc"
    break;

  case 1311: /* $@590: %empty  */
#line 1732 "configparser.yy"
                                           { dir_preset.sortmode = SORT_OWNER; }
#line 10247 "configparser.cc"
    break;

  case 1313: /* $@591: %empty  */
#line 1733 "configparser.yy"
                                           { dir_preset.sortmode = SORT_INODE; }
#line 10253 "configparser.cc"
    break;

  case 1315: /* $@592: %empty  */
#line 1734 "configparser.yy"
                                           { dir_preset.sortmode = SORT_NLINK; }
#line 10259 "configparser.cc"
    break;

  case 1317: /* $@593: %empty  */
#line 1735 "configparser.yy"
                                                { dir_preset.sortmode = SORT_PERMISSION; }
#line 10265 "configparser.cc"
    break;

  case 1319: /* $@594: %empty  */
#line 1736 "configparser.yy"
                                               { dir_preset.sortmode |= SORT_REVERSE; }
#line 10271 "configparser.cc"
    break;

  case 1321: /* $@595: %empty  */
#line 1737 "configparser.yy"
                                               { dir_preset.sortmode |= SORT_DIRLAST; }
#line 10277 "configparser.cc"
    break;

  case 1323: /* $@596: %empty  */
#line 1738 "configparser.yy"
                                                { dir_preset.sortmode |= SORT_DIRMIXED; }
#line 10283 "configparser.cc"
    break;

  case 1325: /* $@597: %empty  */
#line 1739 "configparser.yy"
                                               { dir_preset.sortmode = SORT_EXTENSION; }
#line 10289 "configparser.cc"
    break;

  case 1327: /* $@598: %empty  */
#line 1740 "configparser.yy"
                                                     { dir_preset.sortmode = SORT_CUSTOM_ATTRIBUTE; }
#line 10295 "configparser.cc"
    break;

  case 1329: /* $@599: %empty  */
#line 1741 "configparser.yy"
                                               { dir_preset.show_hidden = true; }
#line 10301 "configparser.cc"
    break;

  case 1331: /* $@600: %empty  */
#line 1742 "configparser.yy"
                                               { dir_preset.show_hidden = false; }
#line 10307 "configparser.cc"
    break;

  case 1333: /* $@601: %empty  */
#line 1743 "configparser.yy"
                                          { dir_preset_filter = NM_Filter(); }
#line 10313 "configparser.cc"
    break;

  case 1334: /* $@602: %empty  */
#line 1744 "configparser.yy"
                                                       { dir_preset.filters.push_back( dir_preset_filter ); }
#line 10319 "configparser.cc"
    break;

  case 1337: /* $@603: %empty  */
#line 1747 "configparser.yy"
                                                    { dir_preset_filter.setPattern( (yyvsp[-1].strptr) ); }
#line 10325 "configparser.cc"
    break;

  case 1339: /* $@604: %empty  */
#line 1748 "configparser.yy"
                                                  { dir_preset_filter.setCheck( NM_Filter::EXCLUDE ); }
#line 10331 "configparser.cc"
    break;

  case 1341: /* $@605: %empty  */
#line 1749 "configparser.yy"
                                                  { dir_preset_filter.setCheck( NM_Filter::INCLUDE ); }
#line 10337 "configparser.cc"
    break;

  case 1344: /* $@606: %empty  */
#line 1752 "configparser.yy"
                                { auto tfp = std::make_shared< HelpOp >();
                                  lconfig_fp1 = tfp;
                                }
#line 10345 "configparser.cc"
    break;

  case 1345: /* helpop: HELPOP_WCP LEFTBRACE_WCP $@606 RIGHTBRACE_WCP  */
#line 1755 "configparser.yy"
                      { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10351 "configparser.cc"
    break;

  case 1346: /* $@607: %empty  */
#line 1757 "configparser.yy"
                                                    { auto tfp = std::make_shared< ViewCommandLogOp >();
                                                      lconfig_fp1 = tfp;
                                                    }
#line 10359 "configparser.cc"
    break;

  case 1347: /* viewcommandlogop: VIEWCOMMANDLOGOP_WCP LEFTBRACE_WCP $@607 RIGHTBRACE_WCP  */
#line 1760 "configparser.yy"
                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10365 "configparser.cc"
    break;

  case 1348: /* $@608: %empty  */
#line 1762 "configparser.yy"
                                                                { lconfig_fp1 = std::make_shared<ActivateTextViewModeOp>(); }
#line 10371 "configparser.cc"
    break;

  case 1349: /* activatetextviewmodeop: ACTIVATETEXTVIEWMODEOP_WCP LEFTBRACE_WCP $@608 activatetextviewmodeopbody RIGHTBRACE_WCP  */
#line 1763 "configparser.yy"
                                                                 { lconfig_listcom.push_back( lconfig_fp1 ); }
#line 10377 "configparser.cc"
    break;

  case 1350: /* $@609: %empty  */
#line 1765 "configparser.yy"
                                                    { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setActiveSide( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10383 "configparser.cc"
    break;

  case 1352: /* $@610: %empty  */
#line 1766 "configparser.yy"
                                                              { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setSourceMode( TextViewMode::ACTIVE_FILE_OTHER_SIDE ); }
#line 10389 "configparser.cc"
    break;

  case 1354: /* $@611: %empty  */
#line 1767 "configparser.yy"
                                                                 { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setSourceMode( TextViewMode::COMMAND_STR ); }
#line 10395 "configparser.cc"
    break;

  case 1356: /* $@612: %empty  */
#line 1768 "configparser.yy"
                                                                  { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setSourceMode( TextViewMode::FILE_TYPE_ACTION ); }
#line 10401 "configparser.cc"
    break;

  case 1358: /* $@613: %empty  */
#line 1769 "configparser.yy"
                                                             { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setCommandStr( (yyvsp[-1].strptr) ); }
#line 10407 "configparser.cc"
    break;

  case 1360: /* $@614: %empty  */
#line 1770 "configparser.yy"
                                                              { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setFileTypeAction( (yyvsp[-1].strptr) ); }
#line 10413 "configparser.cc"
    break;

  case 1362: /* $@615: %empty  */
#line 1771 "configparser.yy"
                                                      { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->addOption( ( ( (yyvsp[-1].num) ) == 1 ) ? TextViewMode::FOLLOW_ACTIVE : 0 ); }
#line 10419 "configparser.cc"
    break;

  case 1364: /* $@616: %empty  */
#line 1772 "configparser.yy"
                                                   { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->addOption( ( ( (yyvsp[-1].num) ) == 1 ) ? TextViewMode::WATCH_FILE : 0 ); }
#line 10425 "configparser.cc"
    break;

  case 1366: /* $@617: %empty  */
#line 1773 "configparser.yy"
                                                      { std::dynamic_pointer_cast<ActivateTextViewModeOp>(lconfig_fp1)->setRequestParameters( ( ( (yyvsp[-1].num) ) == 1 ) ? true : false ); }
#line 10431 "configparser.cc"
    break;


#line 10435 "configparser.cc"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 1776 "configparser.yy"

/**/
int yymyparse(void)
{
  int erg;

  lconfig_error[0] = '\0';
  lconfig_linenr = 0;
  erg = yyparse();
  /* free all strings found by the lexer
     we don't need them anymore */
  lexer_cleanup();
  return erg;
}

int yyerror( const char *s )
{
  lconfig_cleanup();
  /* the following isn't really needed because yyerror is called inside the parser
     which itselve was started with yymyparse which will call it again
     but it doesn't hurt...*/
  lexer_cleanup();
  snprintf( lconfig_error, sizeof( lconfig_error ), "%s", s );
  return 0;
}
