/* pers_string_list.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009,2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PERS_STRING_LIST_HH
#define PERS_STRING_LIST_HH

#include "wdefines.h"
#include <list>
#include <string>

class PersistentStringList
{
public:
    PersistentStringList( const std::string &filename );

    std::list< std::string > getList();
    void pushEntry( const std::string &v );
    void pushFrontEntry( const std::string &v );
    void removeEntry( const std::string &v );
    void removeFirst();
    void removeLast();
    bool contains( const std::string &v );
    int findPosition( const std::string &v );
    std::list< std::string >::size_type size();
private:
    std::string m_filename;
    time_t m_lastmod;
    loff_t m_lastsize;

    std::list< std::string > m_list;

    void read();
    void write();
};

#endif
