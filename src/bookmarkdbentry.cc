/* bookmarkdbentry.cc
 * This file belongs to Worker, a filemanager for UNIX/X11.
 * Copyright (C) 2007-2008 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "bookmarkdbentry.hh"

BookmarkDBEntry::BookmarkDBEntry( const std::string &category,
                                  const std::string &name,
                                  const std::string &alias,
                                  bool use_parent,
                                  int last_access,
                                  bool sticky ) : _category( category ),
                                                  _name( name ),
                                                  _alias( alias ),
                                                  _use_parent( use_parent ),
                                                  _last_access( last_access ),
                                                  _sticky( sticky )
{
    static int nr = 0;

    /* the nr is used to distinguish copied entries from new entries
       as I currently only return copied entries. Deleting an entry existing
       several times would delete the wrong entry
       TODO: alternatively use a refcount class or something */
    _nr = nr++;
}

void BookmarkDBEntry::setCategory( const std::string &category )
{
    _category = category;
}

void BookmarkDBEntry::setName( const std::string &name )
{
    _name = name;
}

void BookmarkDBEntry::setAlias( const std::string &alias )
{
    _alias = alias;
}

void BookmarkDBEntry::setLastAccess( int last_access )
{
    _last_access = last_access;
}

void BookmarkDBEntry::setUseParent( bool use_parent )
{
    _use_parent = use_parent;
}

void BookmarkDBEntry::setSticky( bool sticky )
{
    _sticky = sticky;
}

std::string BookmarkDBEntry::getCategory() const
{
    return _category;
}

std::string BookmarkDBEntry::getName() const
{
    return _name;
}

std::string BookmarkDBEntry::getAlias() const
{
    return _alias;
}

int BookmarkDBEntry::getLastAccess() const
{
    return _last_access;
}

bool BookmarkDBEntry::getUseParent() const
{
    return _use_parent;
}

bool BookmarkDBEntry::getSticky() const
{
    return _sticky;
}

bool BookmarkDBEntry::equals( const BookmarkDBEntry &e,
                              bool ignore_nr ) const
{
    if ( _name != e.getName() )
        return false;
    if ( _alias != e.getAlias() )
        return false;
    if ( _category != e.getCategory() )
        return false;
    if ( _use_parent != e.getUseParent() )
        return false;
    if ( _last_access != e.getLastAccess() )
        return false;
    if ( _sticky != e.getSticky() )
        return false;
    if ( ignore_nr == false && _nr != e._nr )
        return false;
    return true;
}
