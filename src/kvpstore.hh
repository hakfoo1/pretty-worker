/* kvpstore.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef KVPSTORE_HH
#define KVPSTORE_HH

#include "wdefines.h"
#include <map>
#include <string>

class KVPStore
{
public:
    int getIntValue( const std::string &key );
    int getIntValue( const std::string &key,
                     int not_found_value );
    void setIntValue( const std::string &key,
                      int value );

    const std::string &getStringValue( const std::string &key );
    const std::string &getStringValue( const std::string &key,
                                       const std::string &not_found_value );
    void setStringValue( const std::string &key,
                         const std::string &value );

    void eraseStringValue( const std::string &key );
    void eraseIntValue( const std::string &key );
private:
    std::map< std::string, int > m_int_elements;
    std::map< std::string, std::string > m_string_elements;
};

#endif
