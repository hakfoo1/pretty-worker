/* wconfig_color.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2014 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_color.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/button.h"
#include "aguix/solidbutton.h"
#include "aguix/bevelbox.h"
#include "aguix/fieldlistview.h"

ColorPanel::ColorPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  tempconfig = _baseconfig.duplicate();

  sbsb = NULL;
  sbvb = NULL;
  sbhb = NULL;
  slvbsb = NULL;
  slvbvb = NULL;
  slvbhb = NULL;
  ulvbsb = NULL;
  ulvbvb = NULL;
  ulvbhb = NULL;
  lv = NULL;

  for ( int i = 0; i < 10; i++ ) {
      b[i][0] = NULL;
      b[i][1] = NULL;
  }

  cbsb = NULL;
  cbvb = NULL;
  cbhb = NULL;
  rsb = NULL;
  rvb = NULL;
  rhb = NULL;

  m_faces_lv = nullptr;

  m_face_modify = nullptr;
  m_face_unset = nullptr;
  m_face_default = nullptr;

  m_facedb_copy = nullptr;
}

ColorPanel::~ColorPanel()
{
  delete tempconfig;
  delete m_facedb_copy;
}

int ColorPanel::create()
{
  int i;
  int trow;
  int fg,bg;
  
  Panel::create();

  AContainer *ac1 = setContainer( new AContainer( this, 1, 14 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 672 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 3, 1 ), 0, 1 );
  ac1_1->setBorderWidth( 0 );
  ac1_1->setMinSpace( 5 );
  ac1_1->setMaxSpace( 5 );

  sbsb = (SolidButton*)ac1_1->add( new SolidButton( _aguix, 0, 0, catalog.getLocale( 31 ),
                                                    _aguix->getFaces().getColor( "statusbar-fg" ),
                                                    _aguix->getFaces().getColor( "statusbar-bg" ),
                                                    false ), 0, 0, AContainer::CO_INCW );
  sbvb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 32 ), 0 ),
                              1, 0, AContainer::CO_FIX );
  sbvb->connect( this );
  sbhb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                              2, 0, AContainer::CO_FIX );
  sbhb->connect( this );

  ac1->add( new BevelBox( _aguix, 0, 0, 10, 2, 0 ), 0, 2, AContainer::CO_INCW );

  AContainer *ac1_2 = ac1->add( new AContainer( this, 3, 1 ), 0, 3 );
  ac1_2->setBorderWidth( 0 );
  ac1_2->setMinSpace( 5 );
  ac1_2->setMaxSpace( 5 );

  slvbsb = (SolidButton*)ac1_2->add( new SolidButton( _aguix, 0, 0, catalog.getLocale( 34 ),
                                                      _baseconfig.getFaceDB().getColor( "lvb-active-fg" ),
                                                      _baseconfig.getFaceDB().getColor( "lvb-active-bg" ), false ),
                                     0, 0, AContainer::CO_INCW );
  slvbvb = (Button*)ac1_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 32 ), 0 ),
                                1, 0, AContainer::CO_FIX );
  slvbvb->connect( this );
  slvbhb = (Button*)ac1_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                                2, 0, AContainer::CO_FIX );
  slvbhb->connect( this );

  AContainer *ac1_3 = ac1->add( new AContainer( this, 3, 1 ), 0, 4 );
  ac1_3->setBorderWidth( 0 );
  ac1_3->setMinSpace( 5 );
  ac1_3->setMaxSpace( 5 );

  ulvbsb = (SolidButton*)ac1_3->add( new SolidButton( _aguix, 0, 0, catalog.getLocale( 35 ),
                                                      _baseconfig.getFaceDB().getColor( "lvb-inactive-fg" ),
                                                      _baseconfig.getFaceDB().getColor( "lvb-inactive-bg" ), false ),
                                     0, 0, AContainer::CO_INCW );
  ulvbvb = (Button*)ac1_3->add( new Button( _aguix, 0, 0, catalog.getLocale( 32 ), 0 ),
                                1, 0, AContainer::CO_FIX );
  ulvbvb->connect( this );
  ulvbhb = (Button*)ac1_3->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                                2, 0, AContainer::CO_FIX );
  ulvbhb->connect( this );

  ac1->add( new BevelBox( _aguix, 0, 0, 10, 2, 0 ),
                                       0, 5, AContainer::CO_INCW );
  
  AContainer *ac1_4 = ac1->add( new AContainer( this, 2, 1 ), 0, 6 );
  ac1_4->setBorderWidth( 0 );
  ac1_4->setMinSpace( 5 );
  ac1_4->setMaxSpace( 5 );

  lv = (FieldListView*)ac1_4->add( new FieldListView( _aguix, 0, 0, 50, 50, 0 ),
                                   0, 0, AContainer::CO_INCW );
  lv->setHBarState(0);
  lv->setVBarState(0);
  lv->setShowHeader( true );
  lv->setFieldText( 0, catalog.getLocale( 555 ) );
  lv->setHeaderFG( _baseconfig.getFaceDB().getColor( "dirview-header-fg" ) );
  lv->setHeaderBG( _baseconfig.getFaceDB().getColor( "dirview-header-bg" ) );

  fg = _baseconfig.getFaceDB().getColor( "dirview-dir-normal-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-dir-normal-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 36 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-dir-select-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-dir-select-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 37 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-file-normal-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-file-normal-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 38 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-file-select-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-file-select-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 39 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-dir-active-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-dir-active-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 375 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-dir-selact-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-dir-selact-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 376 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-file-active-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-file-active-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 377 ) );
  setLVColors( trow, fg, bg );

  fg = _baseconfig.getFaceDB().getColor( "dirview-file-selact-fg" );
  bg = _baseconfig.getFaceDB().getColor( "dirview-file-selact-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 378 ) );
  setLVColors( trow, fg, bg );

  fg=1;
  bg = _baseconfig.getFaceDB().getColor( "dirview-bg" );
  trow = lv->addRow();
  lv->setText( trow, 0, catalog.getLocale( 379 ) );
  setLVColors( trow, fg, bg );
  
  lv->maximizeY();
  lv->maximizeX();
  lv->redraw();
  ac1_4->readLimits();

  AContainer *ac1_4_1 = ac1_4->add( new AContainer( this, 2, 10 ), 1, 0 );
  ac1_4_1->setBorderWidth( 0 );
  ac1_4_1->setMinSpace( 0 );
  ac1_4_1->setMaxSpace( 0 );

  for ( i = 0; i < 8; i++ ) {
    b[i][0] = (Button*)ac1_4_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 32 ), 0 ),
                                     0, i + 1, AContainer::CO_FIX );
    b[i][0]->resize( b[i][0]->getWidth(), lv->getRowHeight() );
    
    b[i][1] = (Button*)ac1_4_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                                     1, i + 1, AContainer::CO_FIX );
    b[i][1]->resize( b[i][1]->getWidth(), lv->getRowHeight() );
  }

  b[8][0]=NULL;
  b[8][1] = (Button*)ac1_4_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                                   1, 9, AContainer::CO_FIX );
  b[8][1]->resize( b[8][1]->getWidth(), lv->getRowHeight() );

  b[9][0] = (Button*)ac1_4_1->add( new Button( _aguix,
                                               0,
                                               0,
                                               catalog.getLocale( 32 ), 0 ),
                                   0, 0, AContainer::CO_FIX );
  b[9][0]->resize( b[9][0]->getWidth(), lv->getHeaderHeight() );
  b[9][1] = (Button*)ac1_4_1->add( new Button( _aguix,
                                               0,
                                               0,
                                               catalog.getLocale( 33 ), 0 ),
                                   1, 0, AContainer::CO_FIX );
  b[9][1]->resize( b[9][1]->getWidth(), lv->getHeaderHeight() );
  
  ac1_4_1->readLimits();

  for ( int brow = 0; brow < 10; brow++ ) {
    for ( int bcol = 0; bcol < 2; bcol++ ) {
      if ( b[brow][bcol] != NULL )
        b[brow][bcol]->connect( this );
    }
  }

  ac1->add( new BevelBox( _aguix, 0, 0, 10, 2, 0 ),
                                       0, 7, AContainer::CO_INCW );

  AContainer *ac1_5 = ac1->add( new AContainer( this, 3, 1 ), 0, 8 );
  ac1_5->setBorderWidth( 0 );
  ac1_5->setMinSpace( 5 );
  ac1_5->setMaxSpace( 5 );

  cbsb = (SolidButton*)ac1_5->add( new SolidButton( _aguix, 0, 0, catalog.getLocale( 43 ),
                                                    _baseconfig.getFaceDB().getColor( "clockbar-fg" ),
                                                    _baseconfig.getFaceDB().getColor( "clockbar-bg" ), false ),
                                   0, 0, AContainer::CO_INCW );
  cbvb = (Button*)ac1_5->add( new Button( _aguix, 0, 0, catalog.getLocale( 32 ), 0 ),
                              1, 0, AContainer::CO_FIX );
  cbvb->connect( this );
  cbhb = (Button*)ac1_5->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                              2, 0, AContainer::CO_FIX );
  cbhb->connect( this );

  ac1->add( new BevelBox( _aguix, 0, 0, 10, 2, 0 ), 0, 9, AContainer::CO_INCW );
  
  AContainer *ac1_6 = ac1->add( new AContainer( this, 3, 1 ), 0, 10 );
  ac1_6->setBorderWidth( 0 );
  ac1_6->setMinSpace( 5 );
  ac1_6->setMaxSpace( 5 );

  rsb = (SolidButton*)ac1_6->add( new SolidButton( _aguix, 0, 0, catalog.getLocale( 44 ),
                                                   _baseconfig.getFaceDB().getColor( "request-fg" ),
                                                   _baseconfig.getFaceDB().getColor( "request-bg" ), false ),
                                  0, 0, AContainer::CO_INCW );
  rvb = (Button*)ac1_6->add( new Button( _aguix, 0, 0, catalog.getLocale( 32 ), 0 ),
                             1, 0, AContainer::CO_FIX );
  rvb->connect( this );
  rhb = (Button*)ac1_6->add( new Button( _aguix, 0, 0, catalog.getLocale( 33 ), 0 ),
                             2, 0, AContainer::CO_FIX );
  rhb->connect( this );

  /*
   * Code for faces
   */

  ac1->add( new BevelBox( _aguix, 0, 0, 10, 2, 0 ), 0, 11, AContainer::CO_INCW );
  
  AContainer *ac1_8 = ac1->add( new AContainer( this, 2, 2 ), 0, 12 );
  ac1_8->setBorderWidth( 0 );
  ac1_8->setMinSpace( 5 );
  ac1_8->setMaxSpace( 5 );

  ac1_8->add( new Text( _aguix, 0, 0, catalog.getLocale( 1048 ) ),
              0, 0, AContainer::CO_INCW );

  m_faces_lv = ac1_8->addWidget( new FieldListView( _aguix, 0, 0, 50, 100, 0 ),
                                 0, 1, AContainer::CO_MIN );
  m_faces_lv->connect( this );

  m_faces_lv->setDefaultColorMode( FieldListView::PRECOLOR_ONLYACTIVE );
  m_faces_lv->setHBarState( 2 );
  m_faces_lv->setVBarState( 2 );
  m_faces_lv->setNrOfFields( 3 );
  m_faces_lv->setGlobalFieldSpace( 5 );
  m_faces_lv->setAcceptFocus( true );
  m_faces_lv->setDisplayFocus( true );
  m_faces_lv->setShowHeader( true );
  m_faces_lv->setConsiderHeaderWidthForDynamicWidth( true );
  m_faces_lv->setFieldText( 0, catalog.getLocale( 1050 ) );
  m_faces_lv->setFieldText( 1, catalog.getLocale( 1051 ) );
  m_faces_lv->setFieldText( 2, catalog.getLocale( 1052 ) );

  AContainer *ac1_8_1 = ac1_8->add( new AContainer( this, 1, 4 ),
                                    1, 1 );
  ac1_8_1->setBorderWidth( 0 );
  ac1_8_1->setMinSpace( 5 );
  ac1_8_1->setMaxSpace( 5 );

  m_face_modify = ac1_8_1->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1053 ), 0 ),
                                      0, 0, AContainer::CO_INCW );
  m_face_unset = ac1_8_1->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1054 ), 0 ),
                                     0, 1, AContainer::CO_INCW );
  m_face_default = ac1_8_1->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 196 ), 0 ),
                                       0, 2, AContainer::CO_INCW );

  m_face_modify->connect( this );
  m_face_unset->connect( this );
  m_face_default->connect( this );

  ac1->add( new Text( _aguix, 0, 0, catalog.getLocale( 1049 ) ),
            0, 13, AContainer::CO_INCW );

  m_facedb_copy = new FaceDB( _baseconfig.getFaceDB() );
  prepareFaceLV();

  contMaximize( true );
  return 0;
}

int ColorPanel::saveValues()
{
  _baseconfig.setFaceDB( *m_facedb_copy );
  return 0;
}

WConfigPanel::panel_action_t ColorPanel::setColors( List *colors )
{
  if ( colors != tempconfig->getColors() ) {
    tempconfig->setColors( colors );
  }
  return PANEL_NOACTION;
}

WConfigPanel::panel_action_t ColorPanel::setFaceDB( const FaceDB &faces )
{
    delete m_facedb_copy;
    m_facedb_copy = new FaceDB( faces );
    m_faces_lv->setSize( 0 );
    prepareFaceLV();
    m_faces_lv->redraw();

    for ( auto &name : m_facedb_copy->getListOfFaces() ) {
        applyFaceChange( name );
    }

    return PANEL_NOACTION;
}

void ColorPanel::run( Widget *elem, const AGMessage &msg )
{
  if(msg.type==AG_BUTTONCLICKED) {
    if(msg.button.button==sbvb) {
        selectColor( "statusbar-fg" );
    } else if(msg.button.button==sbhb) {
        selectColor( "statusbar-bg" );
    } else if(msg.button.button==slvbvb) {
        selectColor( "lvb-active-fg" );
    } else if(msg.button.button==slvbhb) {
        selectColor( "lvb-active-bg" );
    } else if(msg.button.button==ulvbvb) {
        selectColor( "lvb-inactive-fg" );
    } else if(msg.button.button==ulvbhb) {
        selectColor( "lvb-inactive-bg" );
    } else if(msg.button.button==cbvb) {
        selectColor( "clockbar-fg" );
    } else if(msg.button.button==cbhb) {
        selectColor( "clockbar-bg" );
    } else if(msg.button.button==rvb) {
        selectColor( "request-fg" );
    } else if(msg.button.button==rhb) {
        selectColor( "request-bg" );
    } else if ( msg.button.button == m_face_modify ) {
        modifyFace();
    } else if ( msg.button.button == m_face_unset ) {
        unsetFace();
    } else if ( msg.button.button == m_face_default ) {
        changeToDefaultFace();
    } else {
      for ( int i = 0; i < 10; i++ ) {
        if(msg.button.button==b[i][0]) {
          switch(i) {
            case 1:
                selectColor( "dirview-dir-select-fg" );
                break;
            case 2:
                selectColor( "dirview-file-normal-fg" );
                break;
            case 3:
                selectColor( "dirview-file-select-fg" );
                break;
            case 4:
                selectColor( "dirview-dir-active-fg" );
                break;
            case 5:
                selectColor( "dirview-dir-selact-fg" );
                break;
            case 6:
                selectColor( "dirview-file-active-fg" );
                break;
            case 7:
                selectColor( "dirview-file-selact-fg" );
                break;
            case 8:
              break;
            case 9:
                selectColor( "dirview-header-fg" );
                break;
            default:
                selectColor( "dirview-dir-normal-fg" );
                break;
          }
        } else if(msg.button.button==b[i][1]) {
          switch(i) {
            case 1:
                selectColor( "dirview-dir-select-bg" );
                break;
            case 2:
                selectColor( "dirview-file-normal-bg" );
                break;
            case 3:
                selectColor( "dirview-file-select-bg" );
                break;
            case 4:
                selectColor( "dirview-dir-active-bg" );
                break;
            case 5:
                selectColor( "dirview-dir-selact-bg" );
                break;
            case 6:
                selectColor( "dirview-file-active-bg" );
                break;
            case 7:
                selectColor( "dirview-file-selact-bg" );
                break;
            case 8:
                selectColor( "dirview-bg" );
                break;
            case 9:
                selectColor( "dirview-header-bg" );
                break;
            default:
                selectColor( "dirview-dir-normal-bg" );
                break;
          }
        }
      }
      lv->redraw();
    }
  }
}

void ColorPanel::setLVColors( int row, int fg, int bg )
{
  lv->setFG( row, FieldListView::CC_NORMAL, fg );
  lv->setFG( row, FieldListView::CC_SELECT, fg );
  lv->setFG( row, FieldListView::CC_ACTIVE, fg );
  lv->setFG( row, FieldListView::CC_SELACT, fg );
  lv->setBG( row, FieldListView::CC_NORMAL, bg );
  lv->setBG( row, FieldListView::CC_SELECT, bg );
  lv->setBG( row, FieldListView::CC_ACTIVE, bg );
  lv->setBG( row, FieldListView::CC_SELACT, bg );
}

void ColorPanel::prepareFaceLV()
{
    auto fl = m_facedb_copy->getListOfFaces();

    fl.sort();

    for ( auto &face_name : fl ) {
        int row = m_faces_lv->addRow();
        Face f = m_facedb_copy->getFace( face_name );

        m_faces_lv->setText( row, 0, f.getName() );

        int col = f.getColor();

        if ( col < 0 ) {
            m_faces_lv->setText( row, 1, "unset" );
        } else {
            m_faces_lv->setText( row, 1, AGUIXUtils::convertToString( f.getColor() ) );
        }

        m_faces_lv->setText( row, 2, f.getParent() );
    }
}

void ColorPanel::modifyFace()
{
    int row = m_faces_lv->getActiveRow();

    if ( ! m_faces_lv->isValidRow( row ) ) {
        return;
    }

    Face f = m_facedb_copy->getFace( m_faces_lv->getText( row, 0 ) );
    int col = tempconfig->palette( f.getColor() );

    if ( col >= 0 ) {
        f.setColor( col );
        m_facedb_copy->setFace( f );

        m_faces_lv->setText( row, 1, AGUIXUtils::convertToString( f.getColor() ) );

        m_faces_lv->redraw();
 
        applyFaceChange( m_faces_lv->getText( row, 0 ) );
   }
}

void ColorPanel::unsetFace()
{
    int row = m_faces_lv->getActiveRow();

    if ( ! m_faces_lv->isValidRow( row ) ) {
        return;
    }

    Face f = m_facedb_copy->getFace( m_faces_lv->getText( row, 0 ) );

    f.setColor( -1 );
    m_facedb_copy->setFace( f );

    m_faces_lv->setText( row, 1, "unset" );

    m_faces_lv->redraw();

    applyFaceChange( m_faces_lv->getText( row, 0 ) );
}

void ColorPanel::changeToDefaultFace()
{
    int row = m_faces_lv->getActiveRow();

    if ( ! m_faces_lv->isValidRow( row ) ) {
        return;
    }

    Face f = m_facedb_copy->getFace( m_faces_lv->getText( row, 0 ) );
    Face default_face = m_facedb_default.getFace( f.getName() );

    f.setColor( default_face.getColor() );
    m_facedb_copy->setFace( f );

    if ( f.getColor() < 0 ) {
        m_faces_lv->setText( row, 1, "unset" );
    } else {
        m_faces_lv->setText( row, 1, AGUIXUtils::convertToString( f.getColor() ) );
    }

    m_faces_lv->redraw();

    applyFaceChange( m_faces_lv->getText( row, 0 ) );
}

void ColorPanel::adjustFaceLV( const std::string &name,
                               int col )
{
    for ( int row = 0; row < m_faces_lv->getElements(); row++ ) {
        if ( m_faces_lv->getText( row, 0 ) == name ) {
            m_faces_lv->setText( row, 1, AGUIXUtils::convertToString( col ) );
            break;
        }
    }
    m_faces_lv->redraw();
}

void ColorPanel::applyFaceChange( const std::string &name )
{
    int col = m_facedb_copy->getColor( name );

    if ( name == "statusbar-fg" ) {
        sbsb->setFG( col );
    } else if ( name == "statusbar-bg" ) {
        sbsb->setBG( col );
    } else if ( name == "lvb-active-fg" ) {
        slvbsb->setFG( col );
    } else if ( name == "lvb-active-bg" ) {
        slvbsb->setBG( col );
    } else if ( name == "lvb-inactive-fg" ) {
        ulvbsb->setFG( col );
    } else if ( name == "lvb-inactive-bg" ) {
        ulvbsb->setBG( col );
    } else if ( name == "clockbar-fg" ) {
        cbsb->setFG( col );
    } else if ( name == "clockbar-bg" ) {
        cbsb->setBG( col );
    } else if ( name == "request-fg" ) {
        rsb->setFG( col );
    } else if ( name == "request-bg" ) {
        rsb->setBG( col );
    } else if ( name == "dirview-dir-select-fg" ) {
        lv->setFG( 1, FieldListView::CC_NORMAL, col );
        lv->setFG( 1, FieldListView::CC_SELECT, col );
        lv->setFG( 1, FieldListView::CC_ACTIVE, col );
        lv->setFG( 1, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-select-bg" ) {
        lv->setBG( 1, FieldListView::CC_NORMAL, col );
        lv->setBG( 1, FieldListView::CC_SELECT, col );
        lv->setBG( 1, FieldListView::CC_ACTIVE, col );
        lv->setBG( 1, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-normal-fg" ) {
        lv->setFG( 0, FieldListView::CC_NORMAL, col );
        lv->setFG( 0, FieldListView::CC_SELECT, col );
        lv->setFG( 0, FieldListView::CC_ACTIVE, col );
        lv->setFG( 0, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-normal-bg" ) {
        lv->setBG( 0, FieldListView::CC_NORMAL, col );
        lv->setBG( 0, FieldListView::CC_SELECT, col );
        lv->setBG( 0, FieldListView::CC_ACTIVE, col );
        lv->setBG( 0, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-select-fg" ) {
        lv->setFG( 3, FieldListView::CC_NORMAL, col );
        lv->setFG( 3, FieldListView::CC_SELECT, col );
        lv->setFG( 3, FieldListView::CC_ACTIVE, col );
        lv->setFG( 3, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-select-bg" ) {
        lv->setBG( 3, FieldListView::CC_NORMAL, col );
        lv->setBG( 3, FieldListView::CC_SELECT, col );
        lv->setBG( 3, FieldListView::CC_ACTIVE, col );
        lv->setBG( 3, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-normal-fg" ) {
        lv->setFG( 2, FieldListView::CC_NORMAL, col );
        lv->setFG( 2, FieldListView::CC_SELECT, col );
        lv->setFG( 2, FieldListView::CC_ACTIVE, col );
        lv->setFG( 2, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-normal-bg" ) {
        lv->setBG( 2, FieldListView::CC_NORMAL, col );
        lv->setBG( 2, FieldListView::CC_SELECT, col );
        lv->setBG( 2, FieldListView::CC_ACTIVE, col );
        lv->setBG( 2, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-selact-fg" ) {
        lv->setFG( 5, FieldListView::CC_NORMAL, col );
        lv->setFG( 5, FieldListView::CC_SELECT, col );
        lv->setFG( 5, FieldListView::CC_ACTIVE, col );
        lv->setFG( 5, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-selact-bg" ) {
        lv->setBG( 5, FieldListView::CC_NORMAL, col );
        lv->setBG( 5, FieldListView::CC_SELECT, col );
        lv->setBG( 5, FieldListView::CC_ACTIVE, col );
        lv->setBG( 5, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-active-fg" ) {
        lv->setFG( 4, FieldListView::CC_NORMAL, col );
        lv->setFG( 4, FieldListView::CC_SELECT, col );
        lv->setFG( 4, FieldListView::CC_ACTIVE, col );
        lv->setFG( 4, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-dir-active-bg" ) {
        lv->setBG( 4, FieldListView::CC_NORMAL, col );
        lv->setBG( 4, FieldListView::CC_SELECT, col );
        lv->setBG( 4, FieldListView::CC_ACTIVE, col );
        lv->setBG( 4, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-selact-fg" ) {
        lv->setFG( 7, FieldListView::CC_NORMAL, col );
        lv->setFG( 7, FieldListView::CC_SELECT, col );
        lv->setFG( 7, FieldListView::CC_ACTIVE, col );
        lv->setFG( 7, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-selact-bg" ) {
        lv->setBG( 7, FieldListView::CC_NORMAL, col );
        lv->setBG( 7, FieldListView::CC_SELECT, col );
        lv->setBG( 7, FieldListView::CC_ACTIVE, col );
        lv->setBG( 7, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-active-fg" ) {
        lv->setFG( 6, FieldListView::CC_NORMAL, col );
        lv->setFG( 6, FieldListView::CC_SELECT, col );
        lv->setFG( 6, FieldListView::CC_ACTIVE, col );
        lv->setFG( 6, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-file-active-bg" ) {
        lv->setBG( 6, FieldListView::CC_NORMAL, col );
        lv->setBG( 6, FieldListView::CC_SELECT, col );
        lv->setBG( 6, FieldListView::CC_ACTIVE, col );
        lv->setBG( 6, FieldListView::CC_SELACT, col );
    } else if ( name == "dirview-header-fg" ) {
        lv->setHeaderFG( col );
    } else if ( name == "dirview-header-bg" ) {
        lv->setHeaderBG( col );
    } else if ( name == "dirview-bg" ) {
        lv->setBG( 8, FieldListView::CC_NORMAL, col );
        lv->setBG( 8, FieldListView::CC_SELECT, col );
        lv->setBG( 8, FieldListView::CC_ACTIVE, col );
        lv->setBG( 8, FieldListView::CC_SELACT, col );
    }

    lv->redraw();
}

void ColorPanel::selectColor( const std::string &name )
{
    int col = tempconfig->palette( m_facedb_copy->getColor( name ) );
    if ( col >= 0 ) {
        m_facedb_copy->setColor( name, col );
        adjustFaceLV( name, col );
        applyFaceChange( name );
    }
}
