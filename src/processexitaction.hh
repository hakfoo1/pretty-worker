/* processexitaction.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2010-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef PROCESSEXITACTION_HH
#define PROCESSEXITACTION_HH

#include "wdefines.h"
#include "generic_callback.hh"
#include <string>
#include <memory>

class Worker;
class WPUContext;

class ProcessExitAction : public GenericCallbackArg< void, int >
{
public:
    ProcessExitAction( bool show_file,
                       const std::string &tmp_name,
                       Worker *worker,
                       std::shared_ptr< WPUContext > wpu );
    ProcessExitAction( const ProcessExitAction &other );
    ~ProcessExitAction();
    ProcessExitAction &operator=( const ProcessExitAction &other );

    void callback( int status );
private:
    bool m_show_file;
    std::string m_tmp_name;
    Worker *m_worker;
    std::shared_ptr< WPUContext > m_wpu;
};

class ProcessExitOutputForCustomAttribute : public GenericCallbackArg< void, int >
{
public:
    ProcessExitOutputForCustomAttribute( const std::string &original_fullname,
                                         int original_id,
                                         const std::string &tmp_name,
                                         Worker *worker,
                                         std::shared_ptr< WPUContext > wpu );
    ProcessExitOutputForCustomAttribute( const ProcessExitOutputForCustomAttribute &other ) = delete;
    ProcessExitOutputForCustomAttribute &operator=( const ProcessExitOutputForCustomAttribute &other ) = delete;

    void callback( int status );
private:
    std::string m_original_fullname;
    int m_original_id = -1;
    std::string m_tmp_name;
    Worker *m_worker;
    std::shared_ptr< WPUContext > m_wpu;
};

#endif
