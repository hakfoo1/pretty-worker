/* fstabmounttable.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FSTABMOUNTTABLE_HH
#define FSTABMOUNTTABLE_HH

#include "wdefines.h"
#include "mounttable.hh"
#include "aguix/refcount.hh"

class FStabMountTable : public MountTable
{
public:
    FStabMountTable( const std::string &file );

    std::list<MountTableEntry> getEntries();
    bool containsDevice( const std::string &dev );
    bool findDevice( const std::string &dev,
                     MountTableEntry &return_entry );
    void update();
private:
    RefCount<MountTable> m_table;
};

#endif
