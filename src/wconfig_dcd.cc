/* wconfig_dcd.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2023 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_dcd.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "aguix/fieldlistview.h"
#include "aguix/button.h"
#include "aguix/choosebutton.h"
#include "simplelist.hh"
#include "datei.h"
#include "nwc_path.hh"

DCDPanel::DCDPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  filv = NULL;
  cb = NULL;
}

DCDPanel::~DCDPanel()
{
}

int DCDPanel::create()
{
  Panel::create();

  int trow;

  AContainer *ac1 = setContainer( new AContainer( this, 1, 4 ), true );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );

  addMultiLineText( catalog.getLocale( 684 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  filv =(FieldListView*)ac1->add( new FieldListView( _aguix, 0, 0, 100, 100, 0 ), 0, 1, AContainer::CO_MIN );
  filv->setHBarState(2);
  filv->setVBarState(2);
  filv->setAcceptFocus( true );
  filv->setDisplayFocus( true );
  filv->setNrOfFields( 2 );
  filv->setGlobalFieldSpace( 5 );
  filv->setShowHeader( true );
  filv->setFieldText( 0, catalog.getLocale( 1530 ) );
  filv->setFieldText( 1, catalog.getLocale( 1531 ) );

  AContainer *ac1_1 = ac1->add( new AContainer( this, 3, 1 ), 0, 2 );
  ac1_1->setMinSpace( 0 );
  ac1_1->setMaxSpace( 0 );
  ac1_1->setBorderWidth( 0 );

  newb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 181 ), 0 ),
			      0, 0, AContainer::CO_INCW );
  newb->connect( this );
  delb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 183 ), 0 ),
			      1, 0, AContainer::CO_INCW );
  delb->connect( this );
  editb = (Button*)ac1_1->add( new Button( _aguix, 0, 0, catalog.getLocale( 182 ), 0 ),
			       2, 0, AContainer::CO_INCW );
  editb->connect( this );

  cb = (ChooseButton*)ac1->add( new ChooseButton( _aguix, 0, 0, _baseconfig.getDontCheckVirtual(),
						  catalog.getLocale( 619 ), LABEL_RIGHT, 0 ),
				0, 3, AContainer::CO_INCWNR );

  contMaximize( true );

  m_entries = _baseconfig.getDontCheckDirs();
  for ( const auto &d : m_entries ) {
      trow = filv->addRow();
      filv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
      filv->setText( trow, 0, d.dir.c_str() );
      filv->setText( trow, 1, d.is_pattern ? catalog.getLocale( 575 ) : catalog.getLocale( 1532 ) );
  }
  filv->redraw();
  return 0;
}

int DCDPanel::saveValues()
{
    if ( filv == NULL || cb == NULL ) return 1;

    _baseconfig.setDontCheckDirs( m_entries );
    _baseconfig.setDontCheckVirtual( cb->getState() );

    return 0;
}

void DCDPanel::run( Widget *elem, const AGMessage &msg )
{
    if ( msg.type == AG_BUTTONCLICKED ) {
        if ( msg.button.button == newb ) {
            WConfigTypes::dont_check_dir ne;

            if ( request_entry( ne ) == 0 ) {
                if ( ! ne.dir.empty() ) {
                    int trow = filv->addRow();
                    filv->setPreColors( trow, FieldListView::PRECOLOR_ONLYACTIVE );
                    filv->setText( trow, 0, ne.dir.c_str() );
                    filv->setText( trow, 1, ne.is_pattern ? catalog.getLocale( 575 ) : catalog.getLocale( 1532 ) );
                    filv->setActiveRow( trow );
                    filv->showActive();
                    filv->redraw();

                    m_entries.push_back( ne );
                }
            }

        } else if ( msg.button.button == delb ) {
            int trow = filv->getActiveRow();
            if ( filv->isValidRow( trow ) == true ) {
                filv->deleteRow( trow );
                filv->redraw();
                auto it = m_entries.begin();
                std::advance( it, trow );
                m_entries.erase( it );
            }
        } else if ( msg.button.button == editb ) {
            int trow = filv->getActiveRow();
            if ( filv->isValidRow( trow ) == true ) {
                auto it = m_entries.begin();
                std::advance( it, trow );
                WConfigTypes::dont_check_dir ne = *it;

                if ( request_entry( ne ) == 0 ) {
                    if ( ! ne.dir.empty() ) {
                        filv->setText( trow, 0, ne.dir.c_str() );
                        filv->setText( trow, 1, ne.is_pattern ? catalog.getLocale( 575 ) : catalog.getLocale( 1532 ) );
                        filv->redraw();

                        *it = ne;
                    }
                }
            }
        }
    }
}

int DCDPanel::request_entry( WConfigTypes::dont_check_dir &e )
{
    auto req = std::make_unique< Requester >( Worker::getAGUIX(), this );
    int erg;
  
    auto buttonstr = AGUIXUtils::formatStringToString( "%s|%s",
                                                       catalog.getLocale( 11 ),
                                                       catalog.getLocale( 8 ) );

    bool v = e.is_pattern;
    char *dest = NULL;

    erg = req->string_request_choose( catalog.getLocale( 123 ),
                                      catalog.getLocale( 199 ),
                                      e.dir.c_str(),
                                      catalog.getLocale( 1533 ),
                                      v,
                                      buttonstr.c_str(),
                                      &dest );

    if ( erg == 0 ) {
        e.dir = dest;
        e.is_pattern = v;
    }

    return erg;
}
