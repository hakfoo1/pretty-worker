/* wconfig_font.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2006-2022 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_font.hh"
#include "wconfig.h"
#include "worker.h"
#include "worker_locale.h"
#include "fontreq.h"
#include "aguix/button.h"

FontPanel::FontPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
  freq = new FontRequester( _aguix );
}

FontPanel::~FontPanel()
{
  delete freq;
}

int FontPanel::create()
{
  Panel::create();

  ac1 = setContainer( new AContainer( this, 1, 15 ), true );
  ac1->setBorderWidth( 5 );
  ac1->setMinSpace( 5 );
  ac1->setMaxSpace( 5 );
  
  addMultiLineText( catalog.getLocale( 673 ),
                    *ac1,
                    0, 0,
                    NULL, NULL );

  bs[0] = (Button*)ac1->add( new Button( _aguix, 0, 0, catalog.getLocale( 241 ), 0 ),
                             0, 1, AContainer::CO_INCW );
  bs[0]->connect( this );
  
  ts[0] = (Text*)ac1->add( new Text( _aguix, 0, 0, _baseconfig.getFont( 0 ).c_str() ),
                           0, 2, AContainer::CO_INCW );
  ts[0]->setFont( _baseconfig.getFont( 0 ).c_str() );

  bs[1] = (Button*)ac1->add( new Button( _aguix, 0, 0, catalog.getLocale( 242 ), 0 ),
                             0, 3, AContainer::CO_INCW );
  bs[1]->connect( this );

  ts[1] = (Text*)ac1->add( new Text( _aguix, 0, 0, _baseconfig.getFont( 1 ).c_str() ),
                           0, 4, AContainer::CO_INCW );
  ts[1]->setFont( _baseconfig.getFont( 1 ).c_str() );
  
  bs[2] = (Button*)ac1->add( new Button( _aguix, 0, 0, catalog.getLocale( 243 ), 0 ),
                             0, 5, AContainer::CO_INCW );
  bs[2]->connect( this );
  
  ts[2] = (Text*)ac1->add( new Text( _aguix, 0, 0, _baseconfig.getFont( 2 ).c_str() ),
                           0, 6, AContainer::CO_INCW );
  ts[2]->setFont( _baseconfig.getFont( 2 ).c_str() );

  bs[3] = (Button*)ac1->add( new Button( _aguix, 0, 0, catalog.getLocale( 244 ), 0 ),
                             0, 7, AContainer::CO_INCW );
  bs[3]->connect( this );

  ts[3] = (Text*)ac1->add( new Text( _aguix, 0, 0, _baseconfig.getFont( 3 ).c_str() ),
                           0, 8, AContainer::CO_INCW );
  ts[3]->setFont( _baseconfig.getFont( 3 ).c_str() );

  bs[4] = (Button*)ac1->add( new Button( _aguix, 0, 0, catalog.getLocale( 915 ), 0 ),
                             0, 9, AContainer::CO_INCW );
  bs[4]->connect( this );

  ts[4] = (Text*)ac1->add( new Text( _aguix, 0, 0, _baseconfig.getFont( 4 ).c_str() ),
                           0, 10, AContainer::CO_INCW );
  ts[4]->setFont( _baseconfig.getFont( 4 ).c_str() );

  bs[5] = ac1->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1208 ), 0 ),
                          0, 13, AContainer::CO_INCW );
  bs[5]->connect( this );

  ts[5] = ac1->addWidget( new Text( _aguix, 0, 0, _baseconfig.getFont( 5 ).c_str() ),
                          0, 14, AContainer::CO_INCW );
  ts[5]->setFont( _baseconfig.getFont( 5 ).c_str() );

  bs[6] = ac1->addWidget( new Button( _aguix, 0, 0, catalog.getLocale( 1453 ), 0 ),
                          0, 11, AContainer::CO_INCW );
  bs[6]->connect( this );

  ts[6] = ac1->addWidget( new Text( _aguix, 0, 0, _baseconfig.getFont( 6 ).c_str() ),
                          0, 12, AContainer::CO_INCW );
  ts[6]->setFont( _baseconfig.getFont( 6 ).c_str() );

  ac1->readLimits();
  contMaximize( true );
  return 0;
}

int FontPanel::saveValues()
{
  _baseconfig.setFont( 0, ts[0]->getText() );
  _baseconfig.setFont( 1, ts[1]->getText() );
  _baseconfig.setFont( 2, ts[2]->getText() );
  _baseconfig.setFont( 3, ts[3]->getText() );
  _baseconfig.setFont( 4, ts[4]->getText() );
  _baseconfig.setFont( 5, ts[5]->getText() );
  _baseconfig.setFont( 6, ts[6]->getText() );
  return 0;
}

void FontPanel::run( Widget *elem, const AGMessage &msg )
{
  int erg, i;
  char *tstr;

  if ( msg.type == AG_BUTTONCLICKED ) {
    for ( i = 0; i < 7; i++ ) {
      if ( msg.button.button == bs[i] ) {
        tstr = NULL;
        erg = freq->request( ts[i]->getText(), &tstr );
        if ( erg == 0 ) {
          if ( _aguix->getFont( tstr ) != NULL ) {
            ts[i]->setText( tstr );
            ts[i]->setFont( tstr );

            ac1->readLimits();
            contMaximize();
          } else {
            request( catalog.getLocale( 124 ), catalog.getLocale( 365 ), catalog.getLocale( 11 ) );
          }
          _freesafe( tstr );
        }
      }
    }
  }
}
