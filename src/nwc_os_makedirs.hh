/* nwc_os_makedirs.hh
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2017 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef NWC_OS_MAKEDIRS_HH
#define NWC_OS_MAKEDIRS_HH

#include "wdefines.h"

#include <string>
#include <vector>

namespace NWC
{
    namespace OS {

        class deep_dir_info {
        public:
            deep_dir_info( const std::string &segment_name,
                           mode_t mode,
                           uid_t user,
                           gid_t group ) : m_segment_name( segment_name ),
                                           m_mode( mode ),
                                           m_user( user ),
                                           m_group( group ),
                                           m_was_created( false )
            {}

            const std::string &get_segment_name() const
            {
                return m_segment_name;
            }

            mode_t get_mode() const
            {
                return m_mode;
            }

            uid_t get_user() const
            {
                return m_user;
            }

            gid_t get_group() const
            {
                return m_group;
            }

            bool was_created() const
            {
                return m_was_created;
            }

            void set_was_created( bool val )
            {
                m_was_created = val;
            }
        private:
            std::string m_segment_name;
            mode_t m_mode;
            uid_t m_user;
            gid_t m_group;
            bool m_was_created;
        };

        int prepare_deep_dir_info( const std::string &base_dir,
                                   const std::string &sub_segments,
                                   std::vector< deep_dir_info > &segments );

        int make_dirs( const std::string &base_dir,
                       std::vector< deep_dir_info > &segments,
                       bool post_run );

        int make_dirs( const std::string &dirname );
    }
}

#endif
