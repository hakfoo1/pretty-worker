/* wconfig_volman.cc
 * This file belongs to Worker, a file manager for UN*X/X11.
 * Copyright (C) 2009-2015 Ralf Hoffmann.
 * You can contact me at: ralf@boomerangsworld.de
 *   or http://www.boomerangsworld.de/worker
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "wconfig_volman.hh"
#include "wconfig.h"
#include "aguix/button.h"
#include "aguix/stringgadget.h"
#include "aguix/choosebutton.h"
#include "aguix/cyclebutton.h"
#include "flagreplacer.hh"
#include "worker_locale.h"

VolManPanel::VolManPanel( AWindow &basewin, WConfig &baseconfig ) : WConfigPanel( basewin, baseconfig )
{
}

VolManPanel::~VolManPanel()
{
}

int VolManPanel::create()
{
    std::string str1;
    
    Panel::create();
    
    //TODO more info text?
    //TODO add file select buttons
    AContainer *ac1 = setContainer( new AContainer( this, 1, 4 ), true );
    ac1->setBorderWidth( 5 );
    ac1->setMinSpace( 5 );
    ac1->setMaxSpace( 5 );
    
    addMultiLineText( catalog.getLocale( 865 ),
                      *ac1,
                      0, 0,
                      NULL, NULL );
    
    AContainer *ac1_0 = ac1->add( new AContainer( this, 2, 7), 0, 1 );
    ac1_0->setBorderWidth( 0 );
    ac1_0->setMinSpace( 5 );
    ac1_0->setMaxSpace( 5 );

    /* mount */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 866 ) ),
                0, 0, AContainer::CO_FIX );
    
    AContainer *ac1_1 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 0 );
    ac1_1->setBorderWidth( 0 );
    ac1_1->setMinSpace( 5 );
    ac1_1->setMaxSpace( 5 );
    
    AContainer *ac1_1_2 = ac1_1->add( new AContainer( this, 2, 1 ), 0, 0 );
    ac1_1_2->setBorderWidth( 0 );
    ac1_1_2->setMinSpace( 0 );
    ac1_1_2->setMaxSpace( 0 );
    
    m_mount_sg = (StringGadget*)ac1_1_2->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMMountCommand().c_str(), 0 ),
					      0, 0, AContainer::CO_INCW );
    m_mount_flag_b = (Button*)ac1_1_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 897 ), 0 ),
					    1, 0, AContainer::CO_FIX );
    m_mount_flag_b->connect( this );

    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 898 ),
                                             HW_VM_MOUNT_CMD );
    ac1_1->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );
    
    /* unmount */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 867 ) ),
                0, 1, AContainer::CO_FIX );
    
    AContainer *ac1_2 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 1 );
    ac1_2->setBorderWidth( 0 );
    ac1_2->setMinSpace( 5 );
    ac1_2->setMaxSpace( 5 );
    
    AContainer *ac1_2_2 = ac1_2->add( new AContainer( this, 2, 1 ), 0, 0 );
    ac1_2_2->setBorderWidth( 0 );
    ac1_2_2->setMinSpace( 0 );
    ac1_2_2->setMaxSpace( 0 );
    
    m_unmount_sg = (StringGadget*)ac1_2_2->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMUnmountCommand().c_str(), 0 ),
						0, 0, AContainer::CO_INCW );
    m_unmount_flag_b = (Button*)ac1_2_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 897 ), 0 ),
					      1, 0, AContainer::CO_FIX );
    m_unmount_flag_b->connect( this );
    
    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 898 ),
                                             HW_VM_UNMOUNT_CMD );
    ac1_2->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );
    
    /* eject */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 927 ) ),
                0, 2, AContainer::CO_FIX );
    
    AContainer *ac1_6 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 2 );
    ac1_6->setBorderWidth( 0 );
    ac1_6->setMinSpace( 5 );
    ac1_6->setMaxSpace( 5 );
    
    AContainer *ac1_6_2 = ac1_6->add( new AContainer( this, 2, 1 ), 0, 0 );
    ac1_6_2->setBorderWidth( 0 );
    ac1_6_2->setMinSpace( 0 );
    ac1_6_2->setMaxSpace( 0 );
    
    m_eject_sg = (StringGadget*)ac1_6_2->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMEjectCommand().c_str(), 0 ),
					      0, 0, AContainer::CO_INCW );
    m_eject_flag_b = (Button*)ac1_6_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 897 ), 0 ),
					    1, 0, AContainer::CO_FIX );
    m_eject_flag_b->connect( this );

    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 898 ),
                                             HW_VM_EJECT_CMD );
    ac1_6->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );


    /* close tray */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 928 ) ),
                0, 3, AContainer::CO_FIX );
    
    AContainer *ac1_7 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 3 );
    ac1_7->setBorderWidth( 0 );
    ac1_7->setMinSpace( 5 );
    ac1_7->setMaxSpace( 5 );
    
    AContainer *ac1_7_2 = ac1_7->add( new AContainer( this, 2, 1 ), 0, 0 );
    ac1_7_2->setBorderWidth( 0 );
    ac1_7_2->setMinSpace( 0 );
    ac1_7_2->setMaxSpace( 0 );
    
    m_closetray_sg = (StringGadget*)ac1_7_2->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMCloseTrayCommand().c_str(), 0 ),
						  0, 0, AContainer::CO_INCW );
    m_closetray_flag_b = (Button*)ac1_7_2->add( new Button( _aguix, 0, 0, catalog.getLocale( 897 ), 0 ),
					      1, 0, AContainer::CO_FIX );
    m_closetray_flag_b->connect( this );

    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 898 ),
                                             HW_VM_CLOSETRAY_CMD );
    ac1_7->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );

    /* fstab */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 868 ) ),
                0, 4, AContainer::CO_FIX );
    
    AContainer *ac1_3 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 4 );
    ac1_3->setBorderWidth( 0 );
    ac1_3->setMinSpace( 5 );
    ac1_3->setMaxSpace( 5 );
    
    m_fstab_sg = (StringGadget*)ac1_3->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMFStabFile().c_str(), 0 ),
                                            0, 0, AContainer::CO_INCW );
    
    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 899 ),
                                             HW_VM_FSTAB );
    ac1_3->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );
    
    /* mtab */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 869 ) ),
                0, 5, AContainer::CO_FIX );
    
    AContainer *ac1_4 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 5 );
    ac1_4->setBorderWidth( 0 );
    ac1_4->setMinSpace( 5 );
    ac1_4->setMaxSpace( 5 );
    
    m_mtab_sg = (StringGadget*)ac1_4->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMMtabFile().c_str(), 0 ),
                                           0, 0, AContainer::CO_INCW );
    
    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 899 ),
                                             HW_VM_MTAB );
    ac1_4->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );
    
    /* partition file */
    ac1_0->add( new Text( _aguix, 0, 0, catalog.getLocale( 870 ) ),
                0, 6, AContainer::CO_FIX );
    
    AContainer *ac1_5 = ac1_0->add( new AContainer( this, 1, 2 ), 1, 6 );
    ac1_5->setBorderWidth( 0 );
    ac1_5->setMinSpace( 5 );
    ac1_5->setMaxSpace( 5 );
    
    m_part_sg = (StringGadget*)ac1_5->add( new StringGadget( _aguix, 0, 0, 30, _baseconfig.getVMPartitionFile().c_str(), 0 ),
                                           0, 0, AContainer::CO_INCW );
    
    str1 = AGUIXUtils::formatStringToString( catalog.getLocale( 899 ),
                                             HW_VM_PARTS );
    ac1_5->add( new Text( _aguix, 0, 0, str1.c_str() ),
                0, 1, AContainer::CO_INCW );

    /* request action */
    m_request_action_cb = (ChooseButton*)ac1->add( new ChooseButton( _aguix, 0, 0,
                                                                     _baseconfig.getVMRequestAction(),
                                                                     catalog.getLocale( 906 ),
                                                                     LABEL_RIGHT, 0 ),
                                                   0, 2, AContainer::CO_INCWNR );

    AContainer *ac1_uv = ac1->add( new AContainer( this, 2, 1 ), 0, 3 );
    ac1_uv->setMinSpace( 5 );
    ac1_uv->setMaxSpace( 5 );
    ac1_uv->setBorderWidth( 0 );

    ac1_uv->add( new Text( _aguix, 0, 0, catalog.getLocale( 1116 ) ), 0, 0, AContainer::CO_FIX );

    m_udisks_version_cyb = ac1_uv->addWidget( new CycleButton( _aguix, 0, 0, 10, 0 ), 1, 0, AContainer::CO_INCWNR );
    m_udisks_version_cyb->connect( this );

    m_udisks_version_cyb->addOption( catalog.getLocale( 1117 ) );
    m_udisks_version_cyb->addOption( catalog.getLocale( 1118 ) );
    m_udisks_version_cyb->resize( m_udisks_version_cyb->getMaxSize(),
                                  m_udisks_version_cyb->getHeight() );
    ac1_uv->readLimits();

    if ( _baseconfig.getVMPreferredUdisksVersion() == 2 ) {
        m_udisks_version_cyb->setOption( 1 );
    } else {
        m_udisks_version_cyb->setOption( 0 );
    }
    
    contMaximize( true );
    return 0;
}

int VolManPanel::saveValues()
{
    _baseconfig.setVMMountCommand( m_mount_sg->getText() );
    _baseconfig.setVMUnmountCommand( m_unmount_sg->getText() );
    _baseconfig.setVMFStabFile( m_fstab_sg->getText() );
    _baseconfig.setVMMtabFile( m_mtab_sg->getText() );
    _baseconfig.setVMPartitionFile( m_part_sg->getText() );
    _baseconfig.setVMRequestAction( m_request_action_cb->getState() );
    _baseconfig.setVMEjectCommand( m_eject_sg->getText() );
    _baseconfig.setVMCloseTrayCommand( m_closetray_sg->getText() );
    _baseconfig.setVMPreferredUdisksVersion( m_udisks_version_cyb->getSelectedOption() == 1 ? 2 : 1 );
    return 0;
}

void VolManPanel::run( Widget *elem, const AGMessage &msg )
{
    if ( msg.type == AG_BUTTONCLICKED ) {
        FlagReplacer::FlagHelp flags;
        
	if ( msg.button.button == m_mount_flag_b ||
	     msg.button.button == m_unmount_flag_b ) {
	    flags.registerFlag( "{f}", catalog.getLocale( 873 ) );
	}
        flags.registerFlag( "{d}", catalog.getLocale( 874 ) );

        if ( msg.button.button == m_mount_flag_b ) {
            std::string str1 = FlagReplacer::requestFlag( flags );
            if ( str1.length() > 0 ) {
                m_mount_sg->insertAtCursor( str1.c_str() );
            }
        } else if ( msg.button.button == m_unmount_flag_b ) {
            std::string str1 = FlagReplacer::requestFlag( flags );
            if ( str1.length() > 0 ) {
                m_unmount_sg->insertAtCursor( str1.c_str() );
            }
        } else if ( msg.button.button == m_eject_flag_b ) {
            std::string str1 = FlagReplacer::requestFlag( flags );
            if ( str1.length() > 0 ) {
                m_eject_sg->insertAtCursor( str1.c_str() );
            }
        } else if ( msg.button.button == m_closetray_flag_b ) {
            std::string str1 = FlagReplacer::requestFlag( flags );
            if ( str1.length() > 0 ) {
                m_closetray_sg->insertAtCursor( str1.c_str() );
            }
        }
    }
}
