dnl xinerama check macro
dnl Copyright (C) 2012 Ralf Hoffmann <ralf@boomerangsworld.de>
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

AC_DEFUN([CHECK_XINERAMA],
[
  AC_CHECK_LIB([Xinerama], [XineramaQueryScreens],
               [LIBS="$LIBS -lXinerama"
                xinerama_lib=yes
               ],
               [xinerama_lib=no])
   
  AS_IF([test "x$xinerama_lib" = "xyes"],
        [
          dnl check for Xinerama
          AC_MSG_CHECKING(for working Xinerama)
         
          save_CPPFLAGS=$CPPFLAGS
          CPPFLAGS="$CPPFLAGS -Werror"
          AC_TRY_COMPILE([
        #include <X11/extensions/Xinerama.h>
                         ],
                         [XineramaQueryScreens( NULL, NULL );],
                         xinerama=yes,
                         xinerama=no );
          CPPFLAGS=$save_CPPFLAGS
          AC_MSG_RESULT($xinerama)
        ],
        [
          xinerama=no
        ])
   
  AS_IF([test "x$xinerama" = "xyes"],
        [
          ifelse([$1], , :, [$1])
        ],
        [
          ifelse([$2], , :, [$2])
        ])
])
